package kz.bgpro.www.dalagram.activitys.profile;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import kz.bgpro.www.dalagram.MainActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.ActivityChatMessage;
import kz.bgpro.www.dalagram.activitys.ActivityZoomImage;
import kz.bgpro.www.dalagram.activitys.SharedMedia;

import static kz.bgpro.www.dalagram.MainActivity.MY_USER_ID;
import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.activitys.ActivityChatMessage.Is_mute;
import static kz.bgpro.www.dalagram.utils.MyConstants.media;
import static kz.bgpro.www.dalagram.utils.MyConstants.mute;
import static kz.bgpro.www.dalagram.utils.MyConstants.profile;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

public class ActivityChatProfile extends Activity implements ObservableScrollViewCallbacks {




    @BindView(R.id.scroll)
    ObservableScrollView scroll;

    @BindView(R.id.toolBar)
    RelativeLayout toolBar;

    @BindView(R.id.avatar)
    ImageView avatar;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.status)
    TextView status;

    @BindView(R.id.status_hint)
    TextView status_hint;

    @BindView(R.id.media_count)
    TextView media_count;

    @BindView(R.id.posts_count)
    TextView posts_count;

    @BindView(R.id.phone)
    TextView phone;

    @BindView(R.id.email_hint)
    TextView email_hint;

    @BindView(R.id.email)
    TextView email;

    @BindView(R.id.last_visible)
    TextView last_visible;

    @BindView(R.id.notification)
    Switch notification;

    int user_id;
    String AVATAR="";
    JSONObject media_response=null;

    String Name;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatprofile);
        ButterKnife.bind(this);
        scroll.setScrollViewCallbacks(this);

        if (getIntent().hasExtra("user_id")){
            user_id = getIntent().getIntExtra("user_id",0);
            GetProfileFromServer();
        }


        if (Is_mute==0){
            notification.setChecked(true);
        }else {
            notification.setChecked(false);
        }


        notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d("icheck",isChecked+" ");

                Is_mute = isChecked ? 0 : 1;

                AndroidNetworking.post(mute+TOKEN)
                        .addBodyParameter("is_mute",Is_mute+"")
                        .addBodyParameter("partner_id",user_id+"")
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("mutemute",response+" ");
                        Log.d("Is_mute",Is_mute+" ");

                    }

                    @Override
                    public void onError(ANError anError) {
                        Toasty.error(ActivityChatProfile.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });

    }

    @OnClick(R.id.to_chat)
    void to_chat(){


        String avatar;
        boolean is_ava ;
        if (TextUtils.isEmpty(AVATAR)){
            if(Name.length()>1)
                avatar = Name.charAt(0)+""+Name.charAt(1);
            else
                avatar = Name.charAt(0)+"";
            is_ava = false;
        }else {
            is_ava = true;
            avatar = AVATAR;
        }

        startActivity(new Intent(ActivityChatProfile.this, ActivityChatMessage.class)
                .putExtra("who","chat")
                .putExtra("user_id",user_id)
                .putExtra("name",Name)
                .putExtra("avatar",avatar)
                .putExtra("is_ava",is_ava)
                .putExtra("dialog_id",user_id+"U")
        );

        finish();
    }

    @OnClick(R.id.phone)
    void phone(){
        Uri call = Uri.parse("tel:" + phone.getText().toString());
        Intent surf = new Intent(Intent.ACTION_DIAL, call);
        startActivity(surf);
    }
    @OnClick(R.id.email)
    void email(){

        Intent mailer = new Intent(Intent.ACTION_SEND);
        mailer.setType("text/plain");
        mailer.putExtra(Intent.EXTRA_EMAIL, new String[]{email.getText().toString()});
        mailer.putExtra(Intent.EXTRA_SUBJECT, "");
        mailer.putExtra(Intent.EXTRA_TEXT, "");
        startActivity(Intent.createChooser(mailer, ""));

    }


    public void GetProfileFromServer(){
        Log.d("url", profile+TOKEN+"&user_id="+user_id);

        AndroidNetworking.get(profile+TOKEN+"&user_id="+user_id)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("PPP",response+" *");
                try {
                    if(response.getBoolean("status")){
                        Log.d("profile",response+"++");
                        JSONObject data = response.getJSONObject("data");
                        name.setText(NurString(data,"nickname"));
                        Name = NurString(data,"nickname");
                        phone.setText(NurString(data,"phone"));

                        if (!TextUtils.isEmpty(NurString(data,"email"))){
                            email.setText(NurString(data,"email"));
                        }else {
                            email.setVisibility(View.GONE);
                            email_hint.setVisibility(View.GONE);
                        }
                        if (!TextUtils.isEmpty(NurString(data,"user_status"))){
                             status.setText(NurString(data,"user_status"));
                        }else {
                             status.setVisibility(View.GONE);
                            status_hint.setVisibility(View.GONE);
                        }
                        if (!TextUtils.isEmpty(NurString(data,"last_visit"))){
                            last_visible.setText(NurString(data,"last_visit"));
                        }else {
                            last_visible.setVisibility(View.GONE);
                        }

                        if(!TextUtils.isEmpty(NurString(data,"avatar"))){
                            AVATAR = data.getString("avatar");
                            Glide.with(ActivityChatProfile.this).load(AVATAR).into(avatar);

                        }
                        int publication_count = NurInt(data,"publication_count");
                        if (publication_count==0){
                            posts_count.setText(" ");
                        }else {
                            posts_count.setText(publication_count+" ");
                        }


                        GetMediaFromServer();

                    }else {

                        Toasty.error(ActivityChatProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityChatProfile.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });


    }


    @OnClick(R.id.back)
    void back(){
        finish();
    }


    @OnClick(R.id.all_posts)
    void all_posts(){
        boolean is_my = user_id == MY_USER_ID;

        startActivity(new Intent(ActivityChatProfile.this, ActivityMyProfile.class).putExtra("user_id",user_id).putExtra("is_my",is_my));

    }

    @OnClick(R.id.shared_media)
    void shared_media(){
        if(media_response!=null){
            Intent intent = new Intent(ActivityChatProfile.this,SharedMedia.class);
            intent.putExtra("media_response",media_response+"");
            startActivity(intent);
        }

    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        int baseColor = getResources().getColor(R.color.colorPrimary);
        float alpha = Math.min(1, (float) scrollY / 300);
        toolBar.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @OnClick(R.id.avatar)
    void  avatarclick (){
        if(!TextUtils.isEmpty(AVATAR)){
            Intent intent = new Intent(ActivityChatProfile.this, ActivityZoomImage.class);
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("url", new ArrayList<>(Arrays.asList(AVATAR)));
            bundle.putInt("position", 0);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }



    private void GetMediaFromServer() {
        Log.d("MEdia",media+TOKEN+"&partner_id="+user_id+"&page=1&per_page=10000");
        AndroidNetworking.get(media+TOKEN+"&partner_id="+user_id+"&page=1&per_page=10000").build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if(response.getBoolean("status")){
                        media_response = response;
                        Log.d("media",response+"++");
                        JSONArray data = response.getJSONArray("data");
                        int l = data.length();
                        media_count.setText(l+" ");

                    }else {

                        Toasty.error(ActivityChatProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityChatProfile.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });

    }


}
