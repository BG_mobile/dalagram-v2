package kz.bgpro.www.dalagram.models;

import java.io.File;
import java.io.Serializable;

/**
 * Created by nurbaqyt on 22.09.17.
 */

public class FeedItem implements Serializable {
    private int id;
    private String name,phone;

    private boolean is_typing,has_recipient,is_selected,is_upload;

    private int chat_id,group_id,channel_id,is_mute,user_id,recipient_user_id,answer_chat_id,is_read,is_admin,i_block_partner,partner_block_me,is_has_file,new_message_count,file_time,view_count,answer_user_id,answer_is_has_file,answer_file_time,is_bookmark,is_resend,is_contact,message_type,is_exist,contact_user_id,is_has_link;
    private String chat_kind,action_name,chat_name,recipient_phone,recipient_chat_name,recipient_avatar,avatar,contact_user_name,chat_text,chat_date,chat_time,user_status,last_visit,dialog_id,answer_name,answer_text,answer_file_url,answer_file_format,answer_file_name,contact_name,contact_phone,link_description,link_title,link_image;

    private String [] file_url;
    private String [] file_format;
    private String [] file_name;

    private boolean is_own_last_message,save_storage,is_playaudio;

    private String sfile_url, sfile_format;

    private int is_sticker,sticker_id;
    private String sticker_image;

    private int answer_is_contact;
    private String answer_contact_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIs_bookmark() {
        return is_bookmark;
    }

    public void setIs_bookmark(int is_bookmark) {
        this.is_bookmark = is_bookmark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public int getChat_id() {
        return chat_id;
    }

    public void setChat_id(int chat_id) {
        this.chat_id = chat_id;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public int getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(int channel_id) {
        this.channel_id = channel_id;
    }

    public int getIs_mute() {
        return is_mute;
    }

    public void setIs_mute(int is_mute) {
        this.is_mute = is_mute;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getChat_kind() {
        return chat_kind;
    }

    public void setChat_kind(String chat_kind) {
        this.chat_kind = chat_kind;
    }

    public String getAction_name() {
        return action_name;
    }

    public void setAction_name(String action_name) {
        this.action_name = action_name;
    }

    public String getChat_name() {
        return chat_name;
    }

    public void setChat_name(String chat_name) {
        this.chat_name = chat_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getContact_user_name() {
        return contact_user_name;
    }

    public void setContact_user_name(String contact_user_name) {
        this.contact_user_name = contact_user_name;
    }

    public String getChat_text() {
        return chat_text;
    }

    public void setChat_text(String chat_text) {
        this.chat_text = chat_text;
    }

    public String getChat_date() {
        return chat_date;
    }

    public void setChat_date(String chat_date) {
        this.chat_date = chat_date;
    }

    public String getChat_time() {
        return chat_time;
    }

    public void setChat_time(String chat_time) {
        this.chat_time = chat_time;
    }

    public int getAnswer_chat_id() {
        return answer_chat_id;
    }

    public void setAnswer_chat_id(int answer_chat_id) {
        this.answer_chat_id = answer_chat_id;
    }


    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public String getLast_visit() {
        return last_visit;
    }

    public void setLast_visit(String last_visit) {
        this.last_visit = last_visit;
    }

    public boolean isIs_typing() {
        return is_typing;
    }

    public void setIs_typing(boolean is_typing) {
        this.is_typing = is_typing;
    }


    public int getIs_read() {
        return is_read;
    }

    public void setIs_read(int is_read) {
        this.is_read = is_read;
    }

    public int getIs_admin() {
        return is_admin;
    }

    public void setIs_admin(int is_admin) {
        this.is_admin = is_admin;
    }

    public int getI_block_partner() {
        return i_block_partner;
    }

    public void setI_block_partner(int i_block_partner) {
        this.i_block_partner = i_block_partner;
    }

    public int getPartner_block_me() {
        return partner_block_me;
    }

    public void setPartner_block_me(int partner_block_me) {
        this.partner_block_me = partner_block_me;
    }

    public int getIs_has_file() {
        return is_has_file;
    }

    public void setIs_has_file(int is_has_file) {
        this.is_has_file = is_has_file;
    }

    public int getNew_message_count() {
        return new_message_count;
    }

    public void setNew_message_count(int new_message_count) {
        this.new_message_count = new_message_count;
    }

    public String getDialog_id() {
        return dialog_id;
    }

    public void setDialog_id(String dialog_id) {
        this.dialog_id = dialog_id;
    }

    public String[] getFile_url() {
        return file_url;
    }

    public void setFile_url(String[] file_url) {
        this.file_url = file_url;
    }

    public String[] getFile_format() {
        return file_format;
    }

    public void setFile_format(String[] file_format) {
        this.file_format = file_format;
    }

    public String[] getFile_name() {
        return file_name;
    }

    public void setFile_name(String[] file_name) {
        this.file_name = file_name;
    }

    public boolean isHas_recipient() {
        return has_recipient;
    }

    public void setHas_recipient(boolean has_recipient) {
        this.has_recipient = has_recipient;
    }


    public int getRecipient_user_id() {
        return recipient_user_id;
    }

    public void setRecipient_user_id(int recipient_user_id) {
        this.recipient_user_id = recipient_user_id;
    }

    public String getRecipient_phone() {
        return recipient_phone;
    }

    public void setRecipient_phone(String recipient_phone) {
        this.recipient_phone = recipient_phone;
    }

    public String getRecipient_chat_name() {
        return recipient_chat_name;
    }

    public void setRecipient_chat_name(String recipient_chat_name) {
        this.recipient_chat_name = recipient_chat_name;
    }

    public String getRecipient_avatar() {
        return recipient_avatar;
    }

    public void setRecipient_avatar(String recipient_avatar) {
        this.recipient_avatar = recipient_avatar;
    }


    public boolean isIs_selected() {
        return is_selected;
    }

    public void setIs_selected(boolean is_selected) {
        this.is_selected = is_selected;
    }


    public String getAnswer_name() {
        return answer_name;
    }

    public void setAnswer_name(String answer_name) {
        this.answer_name = answer_name;
    }

    public String getAnswer_text() {
        return answer_text;
    }

    public void setAnswer_text(String answer_text) {
        this.answer_text = answer_text;
    }

    public int getFile_time() {
        return file_time;
    }

    public void setFile_time(int file_time) {
        this.file_time = file_time;
    }

    public String getSfile_url() {
        return sfile_url;
    }

    public void setSfile_url(String sfile_url) {
        this.sfile_url = sfile_url;
    }

    public String getSfile_format() {
        return sfile_format;
    }

    public void setSfile_format(String sfile_format) {
        this.sfile_format = sfile_format;
    }

    public boolean isIs_upload() {
        return is_upload;
    }

    public void setIs_upload(boolean is_upload) {
        this.is_upload = is_upload;
    }

    public int getView_count() {
        return view_count;
    }

    public void setView_count(int view_count) {
        this.view_count = view_count;
    }


    public boolean isIs_own_last_message() {
        return is_own_last_message;
    }

    public void setIs_own_last_message(boolean is_own_last_message) {
        this.is_own_last_message = is_own_last_message;
    }

    public int getAnswer_user_id() {
        return answer_user_id;
    }

    public void setAnswer_user_id(int answer_user_id) {
        this.answer_user_id = answer_user_id;
    }

    public boolean isSave_storage() {
        return save_storage;
    }

    public void setSave_storage(boolean save_storage) {
        this.save_storage = save_storage;
    }

    public int getAnswer_is_has_file() {
        return answer_is_has_file;
    }

    public void setAnswer_is_has_file(int answer_is_has_file) {
        this.answer_is_has_file = answer_is_has_file;
    }

    public int getAnswer_file_time() {
        return answer_file_time;
    }

    public void setAnswer_file_time(int answer_file_time) {
        this.answer_file_time = answer_file_time;
    }

    public String getAnswer_file_url() {
        return answer_file_url;
    }

    public void setAnswer_file_url(String answer_file_url) {
        this.answer_file_url = answer_file_url;
    }

    public String getAnswer_file_format() {
        return answer_file_format;
    }

    public void setAnswer_file_format(String answer_file_format) {
        this.answer_file_format = answer_file_format;
    }

    public String getAnswer_file_name() {
        return answer_file_name;
    }

    public void setAnswer_file_name(String answer_file_name) {
        this.answer_file_name = answer_file_name;
    }

    public int getIs_resend() {
        return is_resend;
    }

    public void setIs_resend(int is_resend) {
        this.is_resend = is_resend;
    }


    public int getIs_contact() {
        return is_contact;
    }

    public void setIs_contact(int is_contact) {
        this.is_contact = is_contact;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getContact_phone() {
        return contact_phone;
    }

    public void setContact_phone(String contact_phone) {
        this.contact_phone = contact_phone;
    }


    public boolean isIs_playaudio() {
        return is_playaudio;
    }

    public void setIs_playaudio(boolean is_playaudio) {
        this.is_playaudio = is_playaudio;
    }


    public int getMessage_type() {
        return message_type;
    }

    public void setMessage_type(int message_type) {
        this.message_type = message_type;
    }

    public int getIs_exist() {
        return is_exist;
    }

    public void setIs_exist(int is_exist) {
        this.is_exist = is_exist;
    }

    public int getContact_user_id() {
        return contact_user_id;
    }

    public void setContact_user_id(int contact_user_id) {
        this.contact_user_id = contact_user_id;
    }

    public int getIs_has_link() {
        return is_has_link;
    }

    public void setIs_has_link(int is_has_link) {
        this.is_has_link = is_has_link;
    }

    public String getLink_description() {
        return link_description;
    }

    public void setLink_description(String link_description) {
        this.link_description = link_description;
    }

    public String getLink_title() {
        return link_title;
    }

    public void setLink_title(String link_title) {
        this.link_title = link_title;
    }

    public String getLink_image() {
        return link_image;
    }

    public void setLink_image(String link_image) {
        this.link_image = link_image;
    }

    public int getAnswer_is_contact() {
        return answer_is_contact;
    }

    public void setAnswer_is_contact(int answer_is_contact) {
        this.answer_is_contact = answer_is_contact;
    }

    public String getAnswer_contact_name() {
        return answer_contact_name;
    }

    public void setAnswer_contact_name(String answer_contact_name) {
        this.answer_contact_name = answer_contact_name;
    }

    public int getIs_sticker() {
        return is_sticker;
    }

    public void setIs_sticker(int is_sticker) {
        this.is_sticker = is_sticker;
    }

    public String getSticker_image() {
        return sticker_image;
    }

    public void setSticker_image(String sticker_image) {
        this.sticker_image = sticker_image;
    }

    public int getSticker_id() {
        return sticker_id;
    }

    public void setSticker_id(int sticker_id) {
        this.sticker_id = sticker_id;
    }
}
