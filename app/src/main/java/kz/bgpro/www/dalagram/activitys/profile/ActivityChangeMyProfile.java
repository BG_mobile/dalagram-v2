package kz.bgpro.www.dalagram.activitys.profile;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.*;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.fxn.pix.Pix;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;

import kz.bgpro.www.dalagram.utils.MyConstants;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.utils.NurJS;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.*;

public class ActivityChangeMyProfile extends AppCompatActivity implements ObservableScrollViewCallbacks {

    SharedPreferences settings;


    @BindView(R.id.scroll)
    ObservableScrollView scroll;

    @BindView(R.id.toolBar)
    RelativeLayout toolBar;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.phone)
    EditText phone;

    @BindView(R.id.status)
    EditText status;

    @BindView(R.id.avatar)
    CircleImageView avatar;

    @BindView(R.id.inner_image)
    ImageView inner_image;

    String NAME,EMAIL,PHONE,STATUS,BIRTHDAY,CITY,UNIVER,WORK,LANGUAGE,FAMILY;

    @BindView(R.id.birthday) EditText birthday;
    @BindView(R.id.city) EditText city;
    @BindView(R.id.university) EditText university;
    @BindView(R.id.work) EditText work;
    @BindView(R.id.language) EditText language;
    @BindView(R.id.family) EditText family;

    @BindView(R.id.password) EditText password;
    @BindView(R.id.re_password) EditText re_password;

    int DIALOG_DATE = 1;
    int myYear = 1980;
    int myMonth = 00;
    int myDay = 01;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changemyprofile);
        ButterKnife.bind(this);
        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        scroll.setScrollViewCallbacks(this);

        Intent intent = getIntent();
        if (intent.hasExtra("profile_response")) {
            String profile_response = intent.getStringExtra("profile_response");
            try {
                JSONObject response = new JSONObject(profile_response);
                if(response.getBoolean("status")){
                    Log.d("profile",response+"++");
                    JSONObject data = response.getJSONObject("data");
                    NAME = NurJS.NurString(data,"user_name");
                    name.setText(NAME);
                    EMAIL=NurJS.NurString(data,"email");
                    email.setText(EMAIL);
                    PHONE = NurJS.NurString(data,"phone");
                    phone.setText(PHONE);
                    STATUS = NurJS.NurString(data,"user_status");
                    status.setText(STATUS);
                    BIRTHDAY = NurJS.NurString(data,"birth_date");
                    birthday.setText(BIRTHDAY);
                    CITY = NurJS.NurString(data,"city");
                    city.setText(CITY);
                    UNIVER = NurJS.NurString(data,"university");
                    university.setText(UNIVER);
                    WORK = NurJS.NurString(data,"work");
                    work.setText(WORK);
                    LANGUAGE = NurJS.NurString(data,"language");
                    language.setText(LANGUAGE);
                    FAMILY = NurJS.NurString(data,"family");
                    family.setText(FAMILY);

                    if(!TextUtils.isEmpty(NurJS.NurString(data,"avatar"))){
                        Glide.with(this).load(data.getString("avatar")).into(avatar);
                    }
                    if(!TextUtils.isEmpty(NurJS.NurString(data,"background"))){
                        Glide.with(this).load(data.getString("background")).into(inner_image);
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    @OnClick(R.id.birthday)
    void birthdays(){
        showDialog(DIALOG_DATE);
    }

    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_DATE) {
            DatePickerDialog tpd = new DatePickerDialog(this, myCallBack, myYear, myMonth, myDay);
            return tpd;
        }
        return super.onCreateDialog(id);
    }

    DatePickerDialog.OnDateSetListener myCallBack = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myYear = year;
            myMonth = monthOfYear+1;
            myDay = dayOfMonth;
            birthday.setText(  myDay + "." + myMonth + "." + myYear);
        }
    };



    @OnClick(R.id.back)
    void back(){
        finish();
    }
    @OnClick(R.id.done)
    void done(){
        String Name = name.getText().toString();
        String Email = email.getText().toString();
        String Status = status.getText().toString();

        String Birthday = birthday.getText().toString();
        String City = city.getText().toString();
        String University = university.getText().toString();
        String Work = work.getText().toString();
        String Language = language.getText().toString();
        String Family = family.getText().toString();
        if (TextUtils.isEmpty(Name)){
            Toasty.info(ActivityChangeMyProfile.this,"заполните имя", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(Email)){
            Toasty.info(ActivityChangeMyProfile.this,"заполните почта", Toast.LENGTH_SHORT).show();
        }else {
            try {
                boolean is_change=false;
                JSONObject js = new JSONObject();
                if (!NAME.equals(Name)) {
                    js.put("user_name", Name);
                    js.put("nickname", Name);
                    is_change=true;
                }

                if (!EMAIL.equals(Email)) {
                    js.put("email", Email);
                    is_change=true;
                }

                if (!STATUS.equals(Status)) {
                    js.put("user_status", Status);
                    is_change=true;
                }

                if (!BIRTHDAY.equals(Birthday)) {
                    js.put("birth_date", Birthday);
                    is_change=true;
                }
                if (!CITY.equals(City)) {
                    js.put("city", City);
                    is_change=true;
                }
                if (!UNIVER.equals(University)) {
                    js.put("university", University);
                    is_change=true;
                }
                if (!WORK.equals(Work)) {
                    js.put("work", Work);
                    is_change=true;
                }
                if (!LANGUAGE.equals(Language)) {
                    js.put("language", Language);
                    is_change=true;
                }
                if (!FAMILY.equals(Family)) {
                    js.put("family", Family);
                    is_change=true;
                }

                if (is_change){
                    SendJsonToServer(js,Name,Status);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.save_password)
    void save_password(){
        String Password = password.getText().toString();
        String Re_Password = re_password.getText().toString();
        Log.d("ppppppp",Password+"__"+Re_Password);
        if (TextUtils.isEmpty(Password) || TextUtils.isEmpty(Re_Password) ){
            Toasty.info(ActivityChangeMyProfile.this,"Заполните пароль!", Toast.LENGTH_SHORT).show();
        }else if (!Password.equals(Re_Password)){
            Toasty.info(ActivityChangeMyProfile.this,"Введите одинаковый пароль!", Toast.LENGTH_SHORT).show();
        }else {
            try {

                JSONObject js = new JSONObject();
                js.put("password",Password);
                js.put("confirm_password",Re_Password);
                SendJsonToServer(js,NAME,STATUS);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void SendJsonToServer(JSONObject js,String Name, String Status){
        Log.d("json object ", js + " ");
        Log.d("URL ", profile + TOKEN + " ");
        AndroidNetworking.post(profile + TOKEN).addJSONObjectBody(js).build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response", response + " ");
                try {
                    if (response.getBoolean("status")) {
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("user_name", Name);
                        editor.putString("user_status", Status);
                        editor.commit();

                        finish();
                    } else {
                        Toasty.error(ActivityChangeMyProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityChangeMyProfile.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.avatar_btn)
    void avatar_btn(){
        Pix.start(ActivityChangeMyProfile.this,                    //Activity or Fragment Instance
                704,                //Request code for activity results
                1);
    }
    @OnClick(R.id.add_innerphoto)
    void add_innerphoto(){
        Pix.start(ActivityChangeMyProfile.this,                    //Activity or Fragment Instance
                707,                //Request code for activity results
                1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 704) {
                ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
               String new_filename =  MyConstants.compressImage(returnValue.get(0),ActivityChangeMyProfile.this);
                File f = new File(new_filename);
                Bitmap rotatedBMP = new BitmapDrawable(getResources(), f.getAbsolutePath()).getBitmap();
                avatar.setImageBitmap(rotatedBMP);
                UploadImage(f,1);
            } else if (requestCode == 707) {
                ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                String new_filename =  MyConstants.compressImage(returnValue.get(0),ActivityChangeMyProfile.this);
                File f = new File(new_filename);
                Bitmap rotatedBMP = new BitmapDrawable(getResources(), f.getAbsolutePath()).getBitmap();
                inner_image.setImageBitmap(rotatedBMP);
                UploadImage(f,2);
            }
        }
    }

    public void UploadImage(File  f, int i) {
        String url;
        if (i==1){
            url = my_avatar+TOKEN;
        }else {
            url = background_ava+TOKEN;
        }
        AndroidNetworking.upload(url)
                .addMultipartFile("image", f)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                deleteFileName(f.getName());
                Log.d("rrrrr  name"," "+f.getName());
                Log.d("rrrrr  photo"," "+response);
            }

            @Override
             public void onError(ANError anError) {
               Toasty.error(ActivityChangeMyProfile.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }
    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        int baseColor = getResources().getColor(R.color.colorPrimary);
        float alpha = Math.min(1, (float) scrollY / 300);
        toolBar.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @Override
    protected void onStart() {
        super.onStart();
//        GetProfileFromServer();
    }
}
