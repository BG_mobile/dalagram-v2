package kz.bgpro.www.dalagram.activitys.profile;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.GridView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.adapters.GallereyListAdapter;
import kz.bgpro.www.dalagram.models.NewsItem;

import java.util.ArrayList;

public class  AllGallery extends Activity {


    ArrayList<NewsItem> feedItems_gallerey;

    @BindView(R.id.grid)
    GridView grid;
    boolean is_my;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_gallery);
        ButterKnife.bind(this);
        is_my = getIntent().getBooleanExtra("is_my",false);
        feedItems_gallerey = (ArrayList<NewsItem>) getIntent().getSerializableExtra("feedItems_gallerey");
        GallereyListAdapter gallerey_adapter = new GallereyListAdapter(feedItems_gallerey,AllGallery.this,is_my,2);
        grid.setAdapter(gallerey_adapter);

    }

    @OnClick(R.id.back)
    void back(){
        finish();
    }
}
