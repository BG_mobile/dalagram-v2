package kz.bgpro.www.dalagram.Realm;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nurbaqyt on 24.07.2018.
 */
//public class DialogDetail {}

public class DialogDetail extends RealmObject {

    @PrimaryKey
    private int chat_id;


    private int position,is_read,is_has_file,user_id,file_time,answer_chat_id,answer_user_id,answer_file_time, answer_is_has_file,is_resend,recipient_user_id,view_count,is_contact, is_exist,contact_user_id,is_has_link;
    private String dialog_id,chat_kind,action_name,chat_text,chat_time,chat_date,user_name,avatar,phone,answer_name,answer_text,file_url,file_format,file_name,answer_file_url,answer_file_format,answer_file_name,recipient_phone,recipient_chat_name,recipient_avatar,contact_name,contact_phone,link_description,link_title,link_image;
    private boolean save_storage, is_upload,has_recipient,is_playaudio;
    private long time;

    int answer_is_contact;
    String answer_contact_name;
    private int is_sticker;
    String sticker_image;

    public DialogDetail(){

    }

    public DialogDetail(String dialog_id, int chat_id, int is_read, int is_has_file, int user_id, int file_time, int answer_chat_id, int answer_user_id, String chat_kind, String action_name, String chat_text, String chat_time, String chat_date, String user_name, String avatar, String phone, String answer_name, String answer_text, String file_url, String file_format, String file_name,  boolean save_storage,long time,String answer_file_url,String answer_file_format, String answer_file_name,
                        int answer_file_time,int answer_is_has_file,boolean is_upload,int is_resend,boolean has_recipient, int recipient_user_id, String recipient_phone,String recipient_chat_name,String recipient_avatar,int view_count, int is_contact, String contact_name, String contact_phone, boolean is_playaudio, int is_exist,int contact_user_id,int is_has_link,String link_description,String link_title,String link_image,int answer_is_contact,String answer_contact_name, int is_sticker,String sticker_image) {

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        Number maxId = realm.where(DialogDetail.class).max("position");
        int nextPosition = (maxId == null) ? 1 : maxId.intValue() + 1;

        DialogDetail dialogDetail = new DialogDetail();
        dialogDetail.setPosition(nextPosition);
        dialogDetail.setDialog_id(dialog_id);
        dialogDetail.setChat_id(chat_id);
        dialogDetail.setIs_read(is_read);
        dialogDetail.setIs_has_file(is_has_file);
        dialogDetail.setUser_id(user_id);
        dialogDetail.setFile_time(file_time);
        dialogDetail.setAnswer_chat_id(answer_chat_id);
        dialogDetail.setAnswer_user_id(answer_user_id);
        dialogDetail.setChat_kind(chat_kind);
        dialogDetail.setAction_name(action_name);
        dialogDetail.setChat_text(chat_text);
        dialogDetail.setChat_time(chat_time);
        dialogDetail.setChat_date(chat_date);
        dialogDetail.setUser_name(user_name);
        dialogDetail.setAvatar(avatar);
        dialogDetail.setPhone(phone);
        dialogDetail.setAnswer_name(answer_name);
        dialogDetail.setAnswer_text(answer_text);
        dialogDetail.setFile_url(file_url);
        dialogDetail.setFile_format(file_format);
        dialogDetail.setFile_name(file_name);
        dialogDetail.setSave_storage(save_storage);
        dialogDetail.setTime(time);

        dialogDetail.setAnswer_file_format(answer_file_format);
        dialogDetail.setAnswer_file_name(answer_file_name);
        dialogDetail.setAnswer_file_url(answer_file_url);
        dialogDetail.setAnswer_file_time(answer_file_time);
        dialogDetail.setAnswer_is_has_file(answer_is_has_file);
        dialogDetail.setIs_upload(is_upload);
        dialogDetail.setIs_resend(is_resend);

        dialogDetail.setHas_recipient(has_recipient);
        dialogDetail.setRecipient_user_id(recipient_user_id);
        dialogDetail.setRecipient_phone(recipient_phone);
        dialogDetail.setRecipient_chat_name(recipient_chat_name);
        dialogDetail.setRecipient_avatar(recipient_avatar);
        dialogDetail.setView_count(view_count);
        dialogDetail.setIs_contact(is_contact);
        dialogDetail.setContact_name(contact_name);
        dialogDetail.setContact_phone(contact_phone);
        dialogDetail.setIs_playaudio(is_playaudio);
        dialogDetail.setIs_exist(is_exist);
        dialogDetail.setContact_user_id(contact_user_id);
        dialogDetail.setContact_user_id(contact_user_id);
        dialogDetail.setIs_has_link(is_has_link);

        dialogDetail.setLink_description(link_description);
        dialogDetail.setLink_title(link_title);
        dialogDetail.setLink_image(link_image);
        dialogDetail.setAnswer_is_contact(answer_is_contact);
        dialogDetail.setAnswer_contact_name(answer_contact_name);
        dialogDetail.setIs_sticker(is_sticker);
        dialogDetail.setSticker_image(sticker_image);

        realm.copyToRealmOrUpdate(dialogDetail);
        realm.commitTransaction();

    }


    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getDialog_id() {
        return dialog_id;
    }

    public void setDialog_id(String dialog_id) {
        this.dialog_id = dialog_id;
    }

    public int getChat_id() {
        return chat_id;
    }

    public void setChat_id(int chat_id) {
        this.chat_id = chat_id;
    }

    public int getIs_read() {
        return is_read;
    }

    public void setIs_read(int is_read) {
        this.is_read = is_read;
    }

    public int getIs_has_file() {
        return is_has_file;
    }

    public void setIs_has_file(int is_has_file) {
        this.is_has_file = is_has_file;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getFile_time() {
        return file_time;
    }

    public void setFile_time(int file_time) {
        this.file_time = file_time;
    }

    public int getAnswer_chat_id() {
        return answer_chat_id;
    }

    public void setAnswer_chat_id(int answer_chat_id) {
        this.answer_chat_id = answer_chat_id;
    }

    public int getAnswer_user_id() {
        return answer_user_id;
    }

    public void setAnswer_user_id(int answer_user_id) {
        this.answer_user_id = answer_user_id;
    }

    public String getChat_kind() {
        return chat_kind;
    }

    public void setChat_kind(String chat_kind) {
        this.chat_kind = chat_kind;
    }

    public String getAction_name() {
        return action_name;
    }

    public void setAction_name(String action_name) {
        this.action_name = action_name;
    }

    public String getChat_text() {
        return chat_text;
    }

    public void setChat_text(String chat_text) {
        this.chat_text = chat_text;
    }

    public String getChat_time() {
        return chat_time;
    }

    public void setChat_time(String chat_time) {
        this.chat_time = chat_time;
    }

    public String getChat_date() {
        return chat_date;
    }

    public void setChat_date(String chat_date) {
        this.chat_date = chat_date;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAnswer_name() {
        return answer_name;
    }

    public void setAnswer_name(String answer_name) {
        this.answer_name = answer_name;
    }

    public String getAnswer_text() {
        return answer_text;
    }

    public void setAnswer_text(String answer_text) {
        this.answer_text = answer_text;
    }

    public String getFile_url() {
        return file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }

    public String getFile_format() {
        return file_format;
    }

    public void setFile_format(String file_format) {
        this.file_format = file_format;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public boolean isSave_storage() {
        return save_storage;
    }

    public void setSave_storage(boolean save_storage) {
        this.save_storage = save_storage;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getAnswer_file_time() {
        return answer_file_time;
    }

    public void setAnswer_file_time(int answer_file_time) {
        this.answer_file_time = answer_file_time;
    }

    public String getAnswer_file_url() {
        return answer_file_url;
    }

    public void setAnswer_file_url(String answer_file_url) {
        this.answer_file_url = answer_file_url;
    }

    public String getAnswer_file_format() {
        return answer_file_format;
    }

    public void setAnswer_file_format(String answer_file_format) {
        this.answer_file_format = answer_file_format;
    }

    public String getAnswer_file_name() {
        return answer_file_name;
    }

    public void setAnswer_file_name(String answer_file_name) {
        this.answer_file_name = answer_file_name;
    }

    public int getAnswer_is_has_file() {
        return answer_is_has_file;
    }

    public void setAnswer_is_has_file(int answer_is_has_file) {
        this.answer_is_has_file = answer_is_has_file;
    }

    public boolean isIs_upload() {
        return is_upload;
    }

    public void setIs_upload(boolean is_upload) {
        this.is_upload = is_upload;
    }

    public int getIs_resend() {
        return is_resend;
    }

    public void setIs_resend(int is_resend) {
        this.is_resend = is_resend;
    }

    public int getRecipient_user_id() {
        return recipient_user_id;
    }

    public void setRecipient_user_id(int recipient_user_id) {
        this.recipient_user_id = recipient_user_id;
    }

    public String getRecipient_phone() {
        return recipient_phone;
    }

    public void setRecipient_phone(String recipient_phone) {
        this.recipient_phone = recipient_phone;
    }

    public String getRecipient_chat_name() {
        return recipient_chat_name;
    }

    public void setRecipient_chat_name(String recipient_chat_name) {
        this.recipient_chat_name = recipient_chat_name;
    }

    public String getRecipient_avatar() {
        return recipient_avatar;
    }

    public void setRecipient_avatar(String recipient_avatar) {
        this.recipient_avatar = recipient_avatar;
    }

    public boolean isHas_recipient() {
        return has_recipient;
    }

    public void setHas_recipient(boolean has_recipient) {
        this.has_recipient = has_recipient;
    }


    public int getView_count() {
        return view_count;
    }

    public void setView_count(int view_count) {
        this.view_count = view_count;
    }


    public int getIs_contact() {
        return is_contact;
    }

    public void setIs_contact(int is_contact) {
        this.is_contact = is_contact;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getContact_phone() {
        return contact_phone;
    }

    public void setContact_phone(String contact_phone) {
        this.contact_phone = contact_phone;
    }


    public boolean isIs_playaudio() {
        return is_playaudio;
    }

    public void setIs_playaudio(boolean is_playaudio) {
        this.is_playaudio = is_playaudio;
    }


    public int getIs_exist() {
        return is_exist;
    }

    public void setIs_exist(int is_exist) {
        this.is_exist = is_exist;
    }


    public int getContact_user_id() {
        return contact_user_id;
    }

    public void setContact_user_id(int contact_user_id) {
        this.contact_user_id = contact_user_id;
    }

    public int getIs_has_link() {
        return is_has_link;
    }

    public void setIs_has_link(int is_has_link) {
        this.is_has_link = is_has_link;
    }

    public String getLink_description() {
        return link_description;
    }

    public void setLink_description(String link_description) {
        this.link_description = link_description;
    }

    public String getLink_title() {
        return link_title;
    }

    public void setLink_title(String link_title) {
        this.link_title = link_title;
    }

    public String getLink_image() {
        return link_image;
    }

    public void setLink_image(String link_image) {
        this.link_image = link_image;
    }


    public int getAnswer_is_contact() {
        return answer_is_contact;
    }

    public void setAnswer_is_contact(int answer_is_contact) {
        this.answer_is_contact = answer_is_contact;
    }

    public String getAnswer_contact_name() {
        return answer_contact_name;
    }

    public void setAnswer_contact_name(String answer_contact_name) {
        this.answer_contact_name = answer_contact_name;
    }


    public int getIs_sticker() {
        return is_sticker;
    }

    public void setIs_sticker(int is_sticker) {
        this.is_sticker = is_sticker;
    }

    public String getSticker_image() {
        return sticker_image;
    }

    public void setSticker_image(String sticker_image) {
        this.sticker_image = sticker_image;
    }
}
