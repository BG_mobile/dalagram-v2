package kz.bgpro.www.dalagram.activitys;

import android.app.Activity;
import android.app.Dialog;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.channel.ActivityChannelProfile;
import kz.bgpro.www.dalagram.activitys.groups.ActivityGroupProfile;
import kz.bgpro.www.dalagram.models.FeedItem;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.channel;
import static kz.bgpro.www.dalagram.utils.MyConstants.group;

/**
 * Created by nurbaqyt on 02.08.2018.
 */

public class AdminMenuDialog {

    public void showDialog(Activity activity, int who,ActivityGroupProfile activityGroupProfile, ActivityChannelProfile activityChannelProfile, FeedItem item, int X, int Y, int group_id){

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.admin_menu_dialog);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.LEFT;
        wlp.x = X;
        wlp.y= Y-48;

        Log.d("PX ASASAS",wlp.x+"_"+ wlp.y);
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        final TextView admin = (TextView) dialog.findViewById(R.id.admin);
        final TextView delete = (TextView) dialog.findViewById(R.id.delete);

        if (item.getIs_admin()==0){
            admin.setText("Сделать админом");
        }else {
            admin.setText("Убырать из админа");
        }

        dialog.show();


        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url;
                if (who==1){
                    url = group+"/admin/"+group_id+"?token="+TOKEN+"&user_id="+item.getUser_id();
                }else {
                    url = channel+"/admin/"+group_id+"?token="+TOKEN+"&user_id="+item.getUser_id();
                }


                if (item.getIs_admin()==0){
                    AndroidNetworking.post(url).build().getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("AAAAA11",response+" ");
                            try {
                                if (response.getBoolean("status")){
                                    if (who==1){
                                        activityGroupProfile.GetGroupUsers();
                                    }else {
                                        activityChannelProfile.GetGroupUsers();
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toasty.error(activity,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                        }
                    });
                }else {
                    AndroidNetworking.delete(url).build().getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getBoolean("status")){
                                    if (who==1){
                                        activityGroupProfile.GetGroupUsers();
                                    }else {
                                        activityChannelProfile.GetGroupUsers();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.d("AAAAA22",response+" ");
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toasty.error(activity,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
                dialog.dismiss();

            }
        });


        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url;
                if (who==1){
                    url = group+"/user/"+group_id+"?token="+TOKEN+"&user_id="+item.getUser_id();
                }else {
                    url = channel+"/user/"+group_id+"?token="+TOKEN+"&user_id="+item.getUser_id();
                }



                AndroidNetworking.delete(url).build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")){
                                if (who==1){
                                    activityGroupProfile.GetGroupUsers();
                                }else {
                                    activityChannelProfile.GetGroupUsers();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("AAAAA33",response+" ");
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toasty.error(activity,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                    }
                });


                dialog.dismiss();

            }
        });

    }
}