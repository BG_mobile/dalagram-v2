package kz.bgpro.www.dalagram.activitys.adapters;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;

import com.github.pavlospt.roundedletterview.RoundedLetterView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.Picasso;
import com.vanniktech.emoji.EmojiTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.Realm.DialogDetail;
import kz.bgpro.www.dalagram.activitys.ActivityChatMessage;
import kz.bgpro.www.dalagram.activitys.ActivityContactMessage;
import kz.bgpro.www.dalagram.activitys.ActivityResend;
import kz.bgpro.www.dalagram.activitys.ActivityVideoPlayer;
import kz.bgpro.www.dalagram.activitys.ActivityZoomImage;
import kz.bgpro.www.dalagram.activitys.profile.ActivityChatProfile;
import kz.bgpro.www.dalagram.models.FeedItem;
import kz.bgpro.www.dalagram.utils.RoundRectCornerImageView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

import static kz.bgpro.www.dalagram.MainActivity.MY_USER_ID;
import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.activitys.ActivityChatMessage.PAGE_m;
import static kz.bgpro.www.dalagram.activitys.ActivityChatMessage.PAGE_p;
import static kz.bgpro.www.dalagram.activitys.ActivityChatMessage.URL;
import static kz.bgpro.www.dalagram.activitys.ActivityChatMessage.mediaPlayer;
import static kz.bgpro.www.dalagram.utils.MessageType.*;
import static kz.bgpro.www.dalagram.utils.MyConstants.page;

public class ChatAdapter extends BaseAdapter  implements StickyListHeadersAdapter {





    private ArrayList<FeedItem> feed_item;
    Activity activity;
    private FeedItem item;
    private int USER_ID;

    private Handler mHandler;
    private Runnable mRunnable ;

    public ArrayList<Integer> selectedIds = new ArrayList<Integer>();

    int is_play = 10000;


    static File myDir;
    Realm realm;
    String dialog_id;
    String invite_text;
    String WHO;
    float scale;
    FeedItem item_1;

    public ChatAdapter(Activity activityChat, ArrayList<FeedItem> feedItem , int USER_ID, String dialog_id,String WHO) {
        this.feed_item = feedItem;
        this.activity = activityChat;
        this.USER_ID = USER_ID;
        this.dialog_id = dialog_id;
        this.WHO = WHO;
        realm = Realm.getDefaultInstance();
        mHandler = new Handler();
        myDir  = new File( Environment.getExternalStorageDirectory().toString() + "/Dalagram");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        invite_text = activity.getResources().getString(R.string.invite_text);
        scale = activity.getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (12 *scale + 0.5f);
    }


    @Override
    public int getCount() {
        return feed_item.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void toggleSelected(Integer position)
    {
        if(selectedIds.contains(position))
        {
            selectedIds.remove(position);
        }
        else
        {
            selectedIds.add(position);
        }
    }

    public void toggleSelectedClear() {
        selectedIds.clear();
        notifyDataSetChanged();
    }
    public void deleteItem(int position) {

       feed_item.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holders;
        if (convertView == null) {
            holders = new HeaderViewHolder();
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.header_data, parent, false);
            holders.header_actiontext = (TextView) convertView.findViewById(R.id.header_actiontext);
            convertView.setTag(holders);
        } else {
            holders = (HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name
        item = feed_item.get(position);
        if (item.getChat_date().contains(":")){
            holders.header_actiontext.setText("Сегодня");
        }else {
            holders.header_actiontext.setText(item.getChat_date());
        }

        return convertView;
    }
    class HeaderViewHolder{
        TextView header_actiontext;
    }

    @Override
    public long getHeaderId(int position) {

        item = feed_item.get(position);

        int k;

        if (item.getChat_date().toUpperCase().equals("ВЧЕРА")){
            k = 777;
        }else if (item.getChat_date().contains(":")){
            k = 666;
        }else {
            String s = String.valueOf(item.getChat_date().subSequence(0, 2));
            k = Integer.valueOf(s);
        }
       return k;
    }


    class ViewHolder {
        LinearLayout container_layout;
        ImageView status;
        TextView data;
        TextView resend_message;
        ImageView resend_btn_m;
        ImageView resend_btn_y;
        LinearLayout layout;

        LinearLayout answerlayout;
        TextView answer_name;
        TextView answer_txt;
        RoundRectCornerImageView answer_image;

        FrameLayout group_avatar_layout;
        RoundedLetterView group_avatar_tv;
        CircleImageView group_avatar;
        TextView group_name;

        ImageView view_icon;
        TextView view_count;



        // Text layout

        LinearLayout textlayout;
        EmojiTextView text;
        TextView more_btn;
        LinearLayout link_layout;
        TextView link_title;
        TextView link_desc;
        ImageView link_image;


       // Image layout
        ImageView single_image;

        LinearLayout imagelayout;
        TextView more_image;
        List<ImageView> images;
        FrameLayout moreimage_layout;
        FrameLayout image_click;


        // Audio layout
        CircleImageView audio_avatar_m;
        CircleImageView audio_avatar_y;
        TextView audioduration;
        ProgressBar audioProgressbar;
        SeekBar audioseek;
        ImageView audioplay;

        // Video layout
        ImageView videoview;
        ImageView playvideo;
        ProgressBar videoProgressbar;

        //Contact layout
        RoundedLetterView contact_avatar;
        TextView contact_name;
        TextView contactsave;
        TextView contactchat;

        //File layout
       LinearLayout file_layout;
       TextView file_name;


        //ACTION layout
       TextView actiontext;


    }

    @SuppressLint("RtlHardcoded")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int type = getItemViewType(position);
        LayoutInflater minflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        item = feed_item.get(position);
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();

            if (type==ACTION_MESSAGE){
                convertView = minflater.inflate(R.layout.message_action, parent, false);
                viewHolder.actiontext = (TextView) convertView.findViewById(R.id.actiontext);
            }else {
                switch (type) {
                    case TEXT_MESSAGE:
                        convertView = minflater.inflate(R.layout.message_text, parent, false);
                        viewHolder.textlayout = (LinearLayout) convertView.findViewById(R.id.textlayout);
                        viewHolder.text = (EmojiTextView) convertView.findViewById(R.id.text);
                        viewHolder.more_btn = (TextView) convertView.findViewById(R.id.more_btn);
                        viewHolder.link_layout = (LinearLayout) convertView.findViewById(R.id.link_layout);
                        viewHolder.link_title = (TextView) convertView.findViewById(R.id.link_title);
                        viewHolder.link_desc = (TextView) convertView.findViewById(R.id.link_desc);
                        viewHolder.link_image = (ImageView) convertView.findViewById(R.id.link_image);

                        break;
                    case IMAGE_MESSAGE:
                        convertView = minflater.inflate(R.layout.message_image, parent, false);
                        viewHolder.single_image = (ImageView) convertView.findViewById(R.id.single_image);
                        viewHolder.imagelayout = (LinearLayout) convertView.findViewById(R.id.imagelayout);
                        viewHolder.more_image = (TextView) convertView.findViewById(R.id.more_image);
                        viewHolder.image_click = (FrameLayout) convertView.findViewById(R.id.image_click);
                        viewHolder.moreimage_layout = (FrameLayout) convertView.findViewById(R.id.moreimage_layout);
                        viewHolder.resend_btn_m = (ImageView) convertView.findViewById(R.id.resend_btn_m);
                        viewHolder.resend_btn_y = (ImageView) convertView.findViewById(R.id.resend_btn_y);


                        viewHolder.images = new ArrayList<ImageView>();
                        viewHolder.images.add((ImageView) convertView.findViewById(R.id.image1));
                        viewHolder.images.add((ImageView) convertView.findViewById(R.id.image2));
                        viewHolder.images.add((ImageView) convertView.findViewById(R.id.image3));
                        viewHolder.images.add((ImageView) convertView.findViewById(R.id.image4));
                        break;
                    case AUDIO_MESSAGE:
                        convertView = minflater.inflate(R.layout.message_audio, parent, false);
                        viewHolder.resend_btn_m = (ImageView) convertView.findViewById(R.id.resend_btn_m);
                        viewHolder.resend_btn_y = (ImageView) convertView.findViewById(R.id.resend_btn_y);
                        viewHolder.audio_avatar_m = (CircleImageView) convertView.findViewById(R.id.audio_avatar_m);
                        viewHolder.audio_avatar_y = (CircleImageView) convertView.findViewById(R.id.audio_avatar_y);
                        viewHolder.audioduration = (TextView) convertView.findViewById(R.id.audioduration);
                        viewHolder.audioProgressbar = (ProgressBar) convertView.findViewById(R.id.audioProgressbar);
                        viewHolder.audioseek = (SeekBar) convertView.findViewById(R.id.audioseek);
                        viewHolder.audioplay = (ImageView) convertView.findViewById(R.id.audioplay);

                        break;
                    case VIDEO_MESSAGE:
                        convertView = minflater.inflate(R.layout.message_video, parent, false);
                        viewHolder.resend_btn_m = (ImageView) convertView.findViewById(R.id.resend_btn_m);
                        viewHolder.resend_btn_y = (ImageView) convertView.findViewById(R.id.resend_btn_y);
                        viewHolder.videoview = (ImageView) convertView.findViewById(R.id.videoview);
                        viewHolder.playvideo = (ImageView) convertView.findViewById(R.id.playvideo);
                        viewHolder.videoProgressbar = (ProgressBar) convertView.findViewById(R.id.videoProgressbar);

                        break;
                    case CONTACT_MESSAGE:
                        convertView = minflater.inflate(R.layout.message_contact, parent, false);
                        viewHolder.contact_avatar = (RoundedLetterView) convertView.findViewById(R.id.contact_avatar);
                        viewHolder.contact_name = (TextView) convertView.findViewById(R.id.contact_name);
                        viewHolder.contactsave = (TextView) convertView.findViewById(R.id.contactsave);
                        viewHolder.contactchat = (TextView) convertView.findViewById(R.id.contactchat);
                        break;
                    case FILE_MESSAGE:
                        convertView = minflater.inflate(R.layout.message_file, parent, false);
                        viewHolder.file_name = (TextView) convertView.findViewById(R.id.file_name);
                        viewHolder.file_layout = (LinearLayout) convertView.findViewById(R.id.file_layout);
                        break;
                    case STICKER_MESSAGE:
                        convertView = minflater.inflate(R.layout.message_sticker, parent, false);
                        viewHolder.single_image = (ImageView) convertView.findViewById(R.id.single_image);
                        viewHolder.resend_btn_m = (ImageView) convertView.findViewById(R.id.resend_btn_m);
                        viewHolder.resend_btn_y = (ImageView) convertView.findViewById(R.id.resend_btn_y);
                        break;

                }

                viewHolder.container_layout = (LinearLayout) convertView.findViewById(R.id.container_layout);
                viewHolder.status = (ImageView) convertView.findViewById(R.id.status);
                viewHolder.data = (TextView) convertView.findViewById(R.id.data);
                viewHolder.resend_message = (TextView) convertView.findViewById(R.id.resend_message);
                viewHolder.answerlayout = (LinearLayout) convertView.findViewById(R.id.answerlayout);
                viewHolder.answer_name = (TextView) convertView.findViewById(R.id.answer_name);
                viewHolder.answer_txt = (TextView) convertView.findViewById(R.id.answer_txt);
                viewHolder.answer_image = (RoundRectCornerImageView) convertView.findViewById(R.id.answer_image);
                viewHolder.layout = (LinearLayout) convertView.findViewById(R.id.layout);

                viewHolder.group_avatar_layout = (FrameLayout) convertView.findViewById(R.id.group_avatar_layout);
                viewHolder.group_avatar_tv = (RoundedLetterView) convertView.findViewById(R.id.group_avatar_tv);
                viewHolder.group_avatar = (CircleImageView) convertView.findViewById(R.id.group_avatar);
                viewHolder.group_name = (TextView) convertView.findViewById(R.id.group_name);

                viewHolder.view_icon = (ImageView) convertView.findViewById(R.id.view_icon);
                viewHolder.view_count = (TextView) convertView.findViewById(R.id.view_count);

            }




            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }



        if (selectedIds.contains(position)) {
            convertView.setSelected(true);
            convertView.setPressed(true);
            convertView.setBackgroundColor(Color.parseColor("#4F5281B9"));
        } else {
            convertView.setSelected(false);
            convertView.setPressed(false);
            convertView.setBackgroundColor(Color.parseColor("#00000000"));
        }

        if (type==ACTION_MESSAGE){
            String text = item.getChat_time()+" "+item.getName()+" "+item.getAction_name();
            if(item.isHas_recipient()){
                text = text+" "+item.getRecipient_chat_name();
            }
            viewHolder.actiontext.setText(text);
        } else {
            if (!WHO.equals("channel")) {
                viewHolder.view_count.setVisibility(View.GONE);
                viewHolder.view_icon.setVisibility(View.GONE);


                if(WHO.equals("group") && item.getUser_id() != MY_USER_ID){
                    if (position!=0){
                        FeedItem item_1 = feed_item.get(position-1);
                        if (item_1.getUser_id()==item.getUser_id()){
                            viewHolder.group_avatar_layout.setVisibility(View.INVISIBLE);
                            viewHolder.group_name.setVisibility(View.GONE);
                        }else {
                            viewHolder.group_avatar_layout.setVisibility(View.VISIBLE);
                            viewHolder.group_name.setVisibility(View.VISIBLE);
                            if(TextUtils.isEmpty(item.getAvatar())){
                                viewHolder.group_avatar.setVisibility(View.GONE);
                                viewHolder.group_avatar_tv.setVisibility(View.VISIBLE);
                                viewHolder.group_avatar_tv.setTitleText("G");

                            }else {
                                viewHolder.group_avatar.setVisibility(View.VISIBLE);
                                viewHolder.group_avatar_tv.setVisibility(View.GONE);
                                Glide.with(activity).load(item.getAvatar()).into(viewHolder.group_avatar);
                            }
                            viewHolder.group_name.setText(item.getName());
                        }
                    }

                    viewHolder.group_avatar_layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            activity.startActivity(new Intent(activity,ActivityChatProfile.class)
                                    .putExtra("user_id",item.getUser_id()).putExtra("Is_mute",0));
                        }
                    });
                    viewHolder.group_name.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            activity.startActivity(new Intent(activity,ActivityChatProfile.class)
                                    .putExtra("user_id",item.getUser_id()).putExtra("Is_mute",0));
                        }
                    });

                }else {
                    viewHolder.group_avatar_layout.setVisibility(View.GONE);
                    viewHolder.group_name.setVisibility(View.GONE);
                }

                if (position < feed_item.size() - 1) {
                     item_1 = feed_item.get(position + 1);

                    if (item.getUser_id() == MY_USER_ID && item_1.getUser_id() == MY_USER_ID) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        layoutParams.gravity = Gravity.RIGHT;

                       if (type != TEXT_MESSAGE && type != CONTACT_MESSAGE && type != FILE_MESSAGE) {
                           layoutParams.setMargins(0, 0, 0, 5);
                           viewHolder.layout.setLayoutParams(layoutParams);
                        } else {
                           layoutParams.setMargins(90, 0, 0, 5);
                            viewHolder.container_layout.setLayoutParams(layoutParams);
                        }


                        if (position!=0){
                           item_1 = feed_item.get(position-1);
                           if (item_1.getUser_id()==item.getUser_id()){
                               viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.my_message_r));
                           }else {
                               viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.my_message));
                           }
                       }else {
                           viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.my_message));
                       }



                    } else if (item.getUser_id() != MY_USER_ID && item_1.getUser_id() != MY_USER_ID) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        layoutParams.gravity = Gravity.LEFT;
                        if (type != TEXT_MESSAGE && type != CONTACT_MESSAGE && type != FILE_MESSAGE) {
                            layoutParams.setMargins(0, 0, 0, 5);
                            viewHolder.layout.setLayoutParams(layoutParams);
                        } else {
                            layoutParams.setMargins(0, 0, 60, 5);
                            viewHolder.container_layout.setLayoutParams(layoutParams);
                        }

                        if (position!=0){
                            item_1 = feed_item.get(position-1);
                            if (item_1.getUser_id()==item.getUser_id()){
                                viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.your_message_r));
                            }else {
                                viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.your_message));
                            }
                        }else {
                            viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.your_message));
                        }

                    } else if (item.getUser_id() == MY_USER_ID && item_1.getUser_id() != MY_USER_ID) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        layoutParams.gravity = Gravity.RIGHT;
                        if (type != TEXT_MESSAGE && type != CONTACT_MESSAGE && type != FILE_MESSAGE) {
                            layoutParams.setMargins(0, 0, 0, 30);
                            viewHolder.layout.setLayoutParams(layoutParams);
                        } else {
                            layoutParams.setMargins(90, 0, 0, 30);
                            viewHolder.container_layout.setLayoutParams(layoutParams);
                        }
                        if (position!=0){
                            item_1 = feed_item.get(position-1);
                            if (item_1.getUser_id()==item.getUser_id()){
                                viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.my_message_r));
                            }else {
                                viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.my_message));
                            }
                        }else {
                            viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.my_message));
                        }

                    } else if (item.getUser_id() != MY_USER_ID && item_1.getUser_id() == MY_USER_ID) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        layoutParams.gravity = Gravity.LEFT;
                        if (type != TEXT_MESSAGE && type != CONTACT_MESSAGE && type != FILE_MESSAGE) {
                            layoutParams.setMargins(0, 0, 0, 30);
                            viewHolder.layout.setLayoutParams(layoutParams);
                        } else {
                            layoutParams.setMargins(0, 0, 60, 30);
                            viewHolder.container_layout.setLayoutParams(layoutParams);
                        }
                        if (position!=0){
                            item_1 = feed_item.get(position-1);
                            if (item_1.getUser_id()==item.getUser_id()){
                                viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.your_message_r));
                            }else {
                                viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.your_message));
                            }
                        }else {
                            viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.your_message));
                        }
                    }
                } else if (position == feed_item.size() - 1 || position == 0) {
                    if (item.getUser_id() == MY_USER_ID) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        layoutParams.gravity = Gravity.RIGHT;
                        if (type != TEXT_MESSAGE && type != CONTACT_MESSAGE && type != FILE_MESSAGE) {
                            layoutParams.setMargins(0, 0, 0, 5);
                            viewHolder.layout.setLayoutParams(layoutParams);
                        } else {
                            layoutParams.setMargins(90, 0, 0, 5);
                            viewHolder.container_layout.setLayoutParams(layoutParams);
                        }
                        if (type == IMAGE_MESSAGE || type == STICKER_MESSAGE){
                            viewHolder.layout.setLayoutParams(layoutParams);
                        }else {
                            viewHolder.container_layout.setLayoutParams(layoutParams);
                        }
                        if (position!=0){
                            item_1 = feed_item.get(position-1);
                            if (item_1.getUser_id()==item.getUser_id()){
                                viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.my_message_r));
                            }else {
                                viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.my_message));
                            }
                        }else {
                            viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.my_message));
                        }

                    } else if (item.getUser_id() != MY_USER_ID) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        layoutParams.gravity = Gravity.LEFT;
                        if (type != TEXT_MESSAGE && type != CONTACT_MESSAGE && type != FILE_MESSAGE) {
                            viewHolder.layout.setLayoutParams(layoutParams);
                            layoutParams.setMargins(0, 0, 0, 5);
                        } else {
                            viewHolder.container_layout.setLayoutParams(layoutParams);
                            layoutParams.setMargins(0, 0, 60, 5);
                        }
                        if (position!=0){
                            item_1 = feed_item.get(position-1);
                            if (item_1.getUser_id()==item.getUser_id()){
                                viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.your_message_r));
                            }else {
                                viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.your_message));
                            }
                        }else {
                            viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.your_message));
                        }
                    }
                }
            } else {
                viewHolder.view_count.setVisibility(View.VISIBLE);
                viewHolder.view_icon.setVisibility(View.VISIBLE);

                viewHolder.view_count.setText(" "+item.getView_count());

                viewHolder.group_avatar_layout.setVisibility(View.GONE);
                viewHolder.group_name.setVisibility(View.GONE);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.LEFT;
                if (type != TEXT_MESSAGE && type != CONTACT_MESSAGE && type != FILE_MESSAGE) {
                    viewHolder.layout.setLayoutParams(layoutParams);
                    layoutParams.setMargins(0, 0, 0, 5);
                } else {
                    viewHolder.container_layout.setLayoutParams(layoutParams);
                    layoutParams.setMargins(0, 0, 60, 5);
                }
                if (position!=0){
                    item_1 = feed_item.get(position-1);
                    if (item_1.getUser_id()==item.getUser_id()){
                        viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.your_message_r));
                    }else {
                        viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.your_message));
                    }
                }else {
                    viewHolder.container_layout.setBackground(activity.getResources().getDrawable(R.drawable.your_message));
                }
            }


            if (item.getIs_resend() == 0) {
                viewHolder.resend_message.setVisibility(View.GONE);
            } else {
                viewHolder.resend_message.setVisibility(View.VISIBLE);
            }

            if (item.getAnswer_chat_id() == 0) {

                viewHolder.answerlayout.setVisibility(View.GONE);
            } else {

                viewHolder.answerlayout.setVisibility(View.VISIBLE);
                String user_name = item.getAnswer_name();
                if (item.getAnswer_user_id() == MY_USER_ID) {
                    user_name = "Вы";
                }
                viewHolder.answer_name.setText(user_name);
                viewHolder.answer_txt.setText(Html.fromHtml(item.getAnswer_text()));

                if (item.getAnswer_is_contact() != 0) {
                    viewHolder.answer_image.setVisibility(View.GONE);
                    viewHolder.answer_txt.setText(Html.fromHtml(item.getAnswer_contact_name()));
                    viewHolder.answer_txt.setCompoundDrawablesWithIntrinsicBounds( R.drawable.ic_person, 0, 0, 0);
                    Drawable[] drawable = viewHolder.answer_txt.getCompoundDrawables();
                    drawable[0].setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(viewHolder.answer_txt.getContext(), R.color.grey_500), PorterDuff.Mode.SRC_IN));
                }else if (item.getAnswer_is_has_file() == 0) {
                    viewHolder.answer_image.setVisibility(View.GONE);
                    viewHolder.answer_txt.setCompoundDrawablesWithIntrinsicBounds( 0, 0, 0, 0);
                } else {
                    viewHolder.answer_txt.setCompoundDrawablesWithIntrinsicBounds( 0, 0, 0, 0);

                    int dpAsPixels = (int) (12 *scale + 0.5f);
                    switch (item.getAnswer_file_format()) {
                        case "image":
                            viewHolder.answer_txt.setText("фото");
                            viewHolder.answer_image.setVisibility(View.VISIBLE);
                            Glide.with(activity).load(item.getAnswer_file_url()).into(viewHolder.answer_image);
                            break;
                        case "audio":
                            int second = item.getAnswer_file_time() % 60;
                            int minute = item.getAnswer_file_time() / 60;
                            String timeString = String.format("%02d:%02d", minute, second);

                            if (item.getAnswer_file_url().contains("AudioRecordingvoice") || item.getAnswer_file_url().contains(".ogg")) {
                                viewHolder.answer_txt.setText("голосовое сообщение: (" + timeString + ")");
                                viewHolder.answer_image.setVisibility(View.GONE);
                            } else {
                                viewHolder.answer_image.setVisibility(View.VISIBLE);
                                viewHolder.answer_image.setImageResource(R.drawable.music);
                                viewHolder.answer_txt.setText("аудио: (" + timeString + ")");
                                viewHolder.answer_image.setPadding(dpAsPixels,dpAsPixels,dpAsPixels,dpAsPixels);
                            }


                            break;
                        case "video":
                            viewHolder.answer_image.setVisibility(View.VISIBLE);
                            viewHolder.answer_txt.setText("видео");
                            Glide.with(activity).load(item.getAnswer_file_url()).into(viewHolder.answer_image);
                            break;
                        case "file":
                            viewHolder.answer_image.setVisibility(View.VISIBLE);
                            viewHolder.answer_image.setImageResource(R.drawable.file);
                            viewHolder.answer_image.setPadding(dpAsPixels,dpAsPixels,dpAsPixels,dpAsPixels);
                            viewHolder.answer_txt.setText("файл");
                            break;
                    }

                }

                viewHolder.answerlayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        item = feed_item.get(position);
                        int chat_id = item.getAnswer_chat_id();
                        Log.d("AAAAA",page+TOKEN+"&chat_id="+chat_id+"&per_page="+ ActivityChatMessage.PER_PAGE+"***");
                        AndroidNetworking.get(page+TOKEN+"&chat_id="+chat_id+"&per_page="+ ActivityChatMessage.PER_PAGE).build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.d("PPPP",response+"");
                                        try {
                                            if(response.has("page")){
                                                PAGE_p = response.getInt("page2");
                                                PAGE_m = response.getInt("page");
                                                ((ActivityChatMessage)activity).GetDataFromJSONByPAGE(true,PAGE_m,URL,chat_id,0);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onError(ANError anError) {
                                        Toasty.error(activity,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                                    }
                                });
                    }
                });



            }


            if (item.getIs_read() == 0) {
                viewHolder.status.setImageResource(R.drawable.check);
            } else {
                viewHolder.status.setImageResource(R.drawable.check2);
            }
            String space_text;
            if (WHO.equals("channel")){
                viewHolder.status.setVisibility(View.GONE);
                space_text = "&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;";
            }else if (item.getUser_id() == MY_USER_ID  ) {
                viewHolder.status.setVisibility(View.VISIBLE);
                space_text = "&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;";
            } else {
                viewHolder.status.setVisibility(View.GONE);
                space_text = "&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;";

            }

            viewHolder.data.setText(item.getChat_time());

            String[] file_url = item.getFile_url();
            String[] file_name = item.getFile_name();
            int length = file_url.length;


            switch (type) {
                //////////////////////////////////////  T E X T   ///////////////////////////////////////////
                case TEXT_MESSAGE:
                    item = feed_item.get(position);

                    String chat_text = item.getChat_text().replaceAll("\n","<br>");
                    viewHolder.text.setText(Html.fromHtml(chat_text + space_text));
                    if (item.getChat_text().length() > 500) {
                        viewHolder.more_btn.setVisibility(View.VISIBLE);
                        viewHolder.more_btn.setText("далее...");
                        viewHolder.text.setMaxLines(15);
                    } else {
                        viewHolder.more_btn.setVisibility(View.GONE);
                        viewHolder.text.setMaxLines(Integer.MAX_VALUE);
                    }

                    if (item.getIs_has_link()==1){
                        viewHolder.link_layout.setVisibility(View.VISIBLE);
                        viewHolder.link_title.setText(Html.fromHtml(item.getLink_title()));
                        viewHolder.link_desc.setText(Html.fromHtml(item.getLink_description()));
                        if (item.getLink_title().isEmpty() && item.getLink_description().isEmpty() && item.getLink_image().isEmpty()){
                            viewHolder.link_layout.setVisibility(View.GONE);
                        }else {
                            viewHolder.link_layout.setVisibility(View.VISIBLE);
                        }
                        if (!item.getLink_image().isEmpty() && !item.getLink_image().equals(" ")) {
                            viewHolder.link_image.setVisibility(View.VISIBLE);
                            Picasso.get().load(item.getLink_image())
                                    .placeholder(R.drawable.no_image)
                                    .into(viewHolder.link_image);
                        }else {
                            viewHolder.link_image.setVisibility(View.GONE);
                        }

                    }else {
                        viewHolder.link_layout.setVisibility(View.GONE);

                    }

                    viewHolder.more_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            if (viewHolder.more_btn.getText().toString().equals("далее...")) {
                                viewHolder.text.setMaxLines(Integer.MAX_VALUE);//your TextView
                                viewHolder.more_btn.setText("меньше");
                            } else {
                                viewHolder.text.setMaxLines(15);//your TextView
                                viewHolder.more_btn.setText("далее...");
                            }
                        }
                    });


                    break;
                //////////////////////////////////////   I M A G E   ///////////////////////////////////////////
                case IMAGE_MESSAGE:
                    item = feed_item.get(position);
                    if (item.getUser_id() == MY_USER_ID && !WHO.equals("channel")) {
                        viewHolder.resend_btn_m.setVisibility(View.VISIBLE);
                        viewHolder.resend_btn_y.setVisibility(View.GONE);

                    } else {
                        viewHolder.resend_btn_m.setVisibility(View.GONE);
                        viewHolder.resend_btn_y.setVisibility(View.VISIBLE);
                    }



                    if (length == 1) {
                        Picasso.get().load(file_url[0])
                                .placeholder(R.drawable.no_image)
                                .into(viewHolder.single_image);
                        viewHolder.single_image.setVisibility(View.VISIBLE);
                        viewHolder.imagelayout.setVisibility(View.GONE);
                    } else {
                        if (length > 4) {
                            int k = length - 3;
                            length = 4;
                            viewHolder.more_image.setText("+ " + k);
                            viewHolder.more_image.setVisibility(View.VISIBLE);
                        } else {
                            viewHolder.more_image.setVisibility(View.GONE);
                        }
                        viewHolder.single_image.setVisibility(View.GONE);
                        viewHolder.imagelayout.setVisibility(View.VISIBLE);

                        for (int f = 0; f < length; f++) {
                            Glide.with(activity).load(file_url[f]).into(viewHolder.images.get(f));
                            if (f == 0 || f == 1) {
                                viewHolder.images.get(0).setVisibility(View.VISIBLE);
                                viewHolder.images.get(1).setVisibility(View.VISIBLE);
                                viewHolder.images.get(2).setVisibility(View.GONE);
                                viewHolder.images.get(3).setVisibility(View.GONE);
                                viewHolder.moreimage_layout.setVisibility(View.GONE);
                            } else if (f == 2) {
                                viewHolder.images.get(0).setVisibility(View.VISIBLE);
                                viewHolder.images.get(1).setVisibility(View.VISIBLE);
                                viewHolder.images.get(2).setVisibility(View.VISIBLE);
                                viewHolder.images.get(3).setVisibility(View.GONE);
                                viewHolder.moreimage_layout.setVisibility(View.GONE);
                            } else {
                                viewHolder.images.get(0).setVisibility(View.VISIBLE);
                                viewHolder.images.get(1).setVisibility(View.VISIBLE);
                                viewHolder.images.get(2).setVisibility(View.VISIBLE);
                                viewHolder.images.get(3).setVisibility(View.VISIBLE);
                                viewHolder.moreimage_layout.setVisibility(View.VISIBLE);

                            }
                        }
                    }

                    viewHolder.image_click.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (selectedIds.size() > 0) {
                                ( (ActivityChatMessage)activity).SelectItem(position);
                            }else {
                                item = feed_item.get(position);
                                String[] file_url = item.getFile_url();
                                String[] file_format = item.getFile_format();
                                if (file_format[0].equals("image")) {
                                    Intent intent = new Intent(activity, ActivityZoomImage.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putStringArrayList("url", new ArrayList<>(Arrays.asList(file_url)));
                                    bundle.putInt("position", 0);
                                    intent.putExtras(bundle);
                                    activity.startActivity(intent);
                                }
                            }
                        }
                    });

                    viewHolder.image_click.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            ( (ActivityChatMessage)activity).SelectItem(position);
                            return true;
                        }
                    });

                    viewHolder.resend_btn_m.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            try {
                                JSONObject js = new JSONObject();

                                JSONArray chat_ids = new JSONArray();
                                chat_ids.put(item.getChat_id());

                                js.put("chat_ids", chat_ids);
                                Intent intent = new Intent(activity, ActivityResend.class);
                                intent.putExtra("js", js + "");
                                activity.startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    viewHolder.resend_btn_y.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            try {
                                JSONObject js = new JSONObject();

                                JSONArray chat_ids = new JSONArray();
                                chat_ids.put(item.getChat_id());

                                js.put("chat_ids", chat_ids);
                                Intent intent = new Intent(activity, ActivityResend.class);
                                intent.putExtra("js", js + "");
                                activity.startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });



                    break;
                ////////////////////////////////////// A U D I O   ///////////////////////////////////////////
                case AUDIO_MESSAGE:
                    item = feed_item.get(position);
                    int second = item.getFile_time() % 60;
                    int minute = item.getFile_time() / 60;
                    String timeString = String.format("%02d:%02d", minute, second);
                    viewHolder.audioduration.setText(timeString);
                    viewHolder.audioProgressbar.setVisibility(View.GONE);

                    if (item.getUser_id() == MY_USER_ID && !WHO.equals("channel")) {
                        viewHolder.resend_btn_m.setVisibility(View.VISIBLE);
                        viewHolder.resend_btn_y.setVisibility(View.GONE);
                        viewHolder.audio_avatar_m.setVisibility(View.VISIBLE);
                        viewHolder.audio_avatar_y.setVisibility(View.GONE);


                        if (TextUtils.isEmpty(item.getAvatar())) {
                            viewHolder.audio_avatar_m.setImageResource(R.drawable.ic_person);
                            viewHolder.audio_avatar_m.setColorFilter(ContextCompat.getColor(activity, R.color.grey_500), android.graphics.PorterDuff.Mode.SRC_IN);

                        } else {

                            if (file_name[0].contains("AudioRecordingvoice") || file_url[0].contains(".ogg")) {
                                Glide.with(activity).load(item.getAvatar()).into(viewHolder.audio_avatar_m);
                            } else {
                                viewHolder.audio_avatar_m.setImageResource(R.drawable.music);
                            }

                        }

                        viewHolder.audioplay.setColorFilter(ContextCompat.getColor(activity, R.color.black_87), android.graphics.PorterDuff.Mode.SRC_IN);
                        viewHolder.audioseek.getThumb().setColorFilter(activity.getResources().getColor(R.color.black_87), PorterDuff.Mode.SRC_ATOP);


                        if (is_play == position) {
                            viewHolder.audioplay.setImageResource(R.drawable.ic_pause);
                        } else {
                            viewHolder.audioplay.setImageResource(R.drawable.ic_play);
                            viewHolder.audioseek.setMax(0);
                        }


                    } else {
                        viewHolder.resend_btn_m.setVisibility(View.GONE);
                        viewHolder.resend_btn_y.setVisibility(View.VISIBLE);
                        viewHolder.audio_avatar_m.setVisibility(View.GONE);
                        viewHolder.audio_avatar_y.setVisibility(View.VISIBLE);


                        if (TextUtils.isEmpty(item.getAvatar())) {
                            viewHolder.audio_avatar_y.setImageResource(R.drawable.ic_person);
                            viewHolder.audio_avatar_y.setColorFilter(ContextCompat.getColor(activity, R.color.grey_500), android.graphics.PorterDuff.Mode.SRC_IN);

                        } else {
                            if (file_name[0].contains("AudioRecordingvoice") || file_url[0].contains(".ogg")) {
                                Glide.with(activity).load(item.getAvatar()).into(viewHolder.audio_avatar_y);
                            } else {
                                viewHolder.audio_avatar_y.setImageResource(R.drawable.music);
                            }

                        }
                        if (!item.isIs_playaudio()) {
                            viewHolder.audioplay.setColorFilter(ContextCompat.getColor(activity, R.color.done_all), android.graphics.PorterDuff.Mode.SRC_IN);
                            viewHolder.audioseek.getThumb().setColorFilter(activity.getResources().getColor(R.color.done_all), PorterDuff.Mode.SRC_ATOP);
                        } else {
                            viewHolder.audioplay.setColorFilter(ContextCompat.getColor(activity, R.color.bright_blue), android.graphics.PorterDuff.Mode.SRC_IN);
                            viewHolder.audioseek.getThumb().setColorFilter(activity.getResources().getColor(R.color.bright_blue), PorterDuff.Mode.SRC_ATOP);
                        }

                        File audio_file;
                        if (file_url[0].contains("AudioRecordingvoice") || file_url[0].contains(".ogg")) {
                            audio_file = new File(myDir + "/.voiceaudio/" + item.getFile_name()[0]);
                        } else {
                            audio_file = new File(myDir + "/Dalagram audio/" + item.getFile_name()[0]);
                        }
                        if (item.isSave_storage() && audio_file.exists()) {
                            if (is_play == position) {
                                viewHolder.audioplay.setImageResource(R.drawable.ic_pause);
                            } else {
                                viewHolder.audioplay.setImageResource(R.drawable.ic_play);
                                viewHolder.audioseek.setMax(0);
                            }
                        } else {
                            viewHolder.audioplay.setImageResource(R.drawable.ic_download);
                        }

                    }


                    viewHolder.audioplay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            item = feed_item.get(position);
                            File audio_file;
                            if (file_url[0].contains("AudioRecordingvoice") || file_url[0].contains(".ogg")) {
                                audio_file = new File(myDir + "/.voiceaudio/" + item.getFile_name()[0]);
                            } else {
                                audio_file = new File(myDir + "/Dalagram audio/" + item.getFile_name()[0]);
                            }


                            if (item.isSave_storage()) {
                                if (is_play != position) {

                                    if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                                        mediaPlayer.stop();
                                    }

                                    if (audio_file.exists()) {
                                        viewHolder.audioplay.setImageResource(R.drawable.ic_pause);
                                        Uri uri = Uri.fromFile(audio_file);
                                        mediaPlayer = MediaPlayer.create(activity, uri);
                                        mediaPlayer.start();
                                        initializeSeekBar(viewHolder.audioseek);
                                        is_play = position;
                                        realm.beginTransaction();
                                        DialogDetail dialogDetail1 = realm.where(DialogDetail.class).equalTo("dialog_id", dialog_id).and().equalTo("chat_id", item.getChat_id()).findFirst();

                                        if (dialogDetail1 != null) {
                                            dialogDetail1.setIs_playaudio(true);
                                        }

                                        realm.commitTransaction();
                                        item.setIs_playaudio(true);
                                        notifyDataSetChanged();
                                    } else {
                                        viewHolder.audioProgressbar.setVisibility(View.VISIBLE);
                                        downloadFile(position, "audio");
                                    }

                                } else {
                                    if (mediaPlayer.isPlaying()) {
                                        mediaPlayer.pause();
                                        viewHolder.audioplay.setImageResource(R.drawable.ic_play);
                                    } else {
                                        mediaPlayer.start();
                                        viewHolder.audioplay.setImageResource(R.drawable.ic_pause);
                                    }

                                }
                                if (mediaPlayer != null) {
                                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        public void onCompletion(MediaPlayer mp) {
                                            if (mediaPlayer != null) {
                                                mediaPlayer = null;
                                                is_play = 10000;
                                                notifyDataSetChanged();
                                            }
                                        }
                                    });
                                }
                            } else {
                                viewHolder.audioProgressbar.setVisibility(View.VISIBLE);
                                downloadFile(position, "audio");
                            }

                        }

                    });

                    viewHolder.audioseek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            if (mediaPlayer != null && fromUser) {
                                mediaPlayer.seekTo(progress * 100);
                            }
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }
                    });

                    viewHolder.resend_btn_m.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            try {
                                JSONObject js = new JSONObject();

                                JSONArray chat_ids = new JSONArray();
                                chat_ids.put(item.getChat_id());

                                js.put("chat_ids", chat_ids);
                                Intent intent = new Intent(activity, ActivityResend.class);
                                intent.putExtra("js", js + "");
                                activity.startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    viewHolder.resend_btn_y.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            try {
                                JSONObject js = new JSONObject();

                                JSONArray chat_ids = new JSONArray();
                                chat_ids.put(item.getChat_id());

                                js.put("chat_ids", chat_ids);
                                Intent intent = new Intent(activity, ActivityResend.class);
                                intent.putExtra("js", js + "");
                                activity.startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    break;
                ////////////////////////////////////// V I D E O   ///////////////////////////////////////////


                case VIDEO_MESSAGE:
                    item = feed_item.get(position);
                    viewHolder.videoProgressbar.setVisibility(View.GONE);

                    if (item.getUser_id() == MY_USER_ID && !WHO.equals("channel")) {
                        viewHolder.resend_btn_m.setVisibility(View.VISIBLE);
                        viewHolder.resend_btn_y.setVisibility(View.GONE);

                    } else {
                        viewHolder.resend_btn_m.setVisibility(View.GONE);
                        viewHolder.resend_btn_y.setVisibility(View.VISIBLE);

                        File audio_file = new File(myDir + "/Dalagram video/" + item.getFile_name()[0]);
                        if (item.isSave_storage() && audio_file.exists()) {
                            viewHolder.playvideo.setImageResource(R.drawable.ic_play);
                        } else {
                            viewHolder.playvideo.setImageResource(R.drawable.ic_download);
                        }

                    }

                    Glide.with(activity)
                            .asBitmap()
                            .load(file_url[0])
                            .into(viewHolder.videoview);


                    viewHolder.playvideo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);

                            File file_path = new File(myDir + "/Dalagram video/" + item.getFile_name()[0]);


                            if (item.isSave_storage() && file_path.exists()) {
                                Intent intent = new Intent(activity, ActivityVideoPlayer.class);
                                intent.putExtra("file_url", file_path.getPath());
                                activity.startActivity(intent);
                            } else {
                                viewHolder.videoProgressbar.setVisibility(View.VISIBLE);
                                downloadFile(position, "video");
                            }


                        }
                    });


                    viewHolder.resend_btn_m.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            try {
                                JSONObject js = new JSONObject();

                                JSONArray chat_ids = new JSONArray();
                                chat_ids.put(item.getChat_id());

                                js.put("chat_ids", chat_ids);
                                Intent intent = new Intent(activity, ActivityResend.class);
                                intent.putExtra("js", js + "");
                                activity.startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    viewHolder.resend_btn_y.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            try {
                                JSONObject js = new JSONObject();

                                JSONArray chat_ids = new JSONArray();
                                chat_ids.put(item.getChat_id());

                                js.put("chat_ids", chat_ids);
                                Intent intent = new Intent(activity, ActivityResend.class);
                                intent.putExtra("js", js + "");
                                activity.startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });


                    break;

                ////////////////////////////////////// C O N T A C T   ///////////////////////////////////////////

                case CONTACT_MESSAGE:
                    item = feed_item.get(position);

                    viewHolder.contact_name.setText(item.getContact_name());
                    viewHolder.contact_avatar.setTitleText(item.getContact_name().charAt(0) + "" + item.getContact_name().charAt(1));
                    if (item.getIs_exist()==0){
                        viewHolder.contactchat.setText("Пригласить");
                    }else {
                        viewHolder.contactchat.setText("Написать");
                    }

                    viewHolder.contact_name.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            Intent intent = new Intent(activity, ActivityContactMessage.class);
                            intent.putExtra("contact_name", item.getContact_name());
                            intent.putExtra("contact_phone", item.getContact_phone());
                            activity.startActivity(intent);
                        }
                    });
                    viewHolder.contact_avatar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            Intent intent = new Intent(activity, ActivityContactMessage.class);
                            intent.putExtra("contact_name", item.getContact_name());
                            intent.putExtra("contact_phone", item.getContact_phone());
                            activity.startActivity(intent);
                        }
                    });

                    viewHolder.contactsave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            Dexter.withActivity(activity)
                                    .withPermission(Manifest.permission.WRITE_CONTACTS)
                                    .withListener(new PermissionListener() {
                                        @Override
                                        public void onPermissionGranted(PermissionGrantedResponse response) {

                                            Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                            contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                                            contactIntent
                                                    .putExtra(ContactsContract.Intents.Insert.NAME, item.getContact_name())
                                                    .putExtra(ContactsContract.Intents.Insert.PHONE, item.getContact_phone());

                                            activity.startActivity(contactIntent);

                                        }

                                        @Override
                                        public void onPermissionDenied(PermissionDeniedResponse response) {
                                            Log.d("SSSSSSS", "2" + response.getPermissionName());
                                        }

                                        @Override
                                        public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                            Log.d("SSSSSSS", "3" + permission.getName());

                                        }
                                    }).check();

                        }
                    });
                    viewHolder.contactchat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            if (item.getIs_exist()==0){
//                                Dexter.withActivity(activity)
//                                        .withPermission(Manifest.permission.SEND_SMS)
//                                        .withListener(new PermissionListener() {
//                                            @Override
//                                            public void onPermissionGranted(PermissionGrantedResponse response) {
//
//                                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + item.getContact_phone()));
//                                                intent.putExtra("sms_body", invite_text);
//                                                activity.startActivity(intent);
//                                            }
//
//                                            @Override
//                                            public void onPermissionDenied(PermissionDeniedResponse response) {
//                                                Log.d("SSSSSSS", "2" + response.getPermissionName());
//                                            }
//
//                                            @Override
//                                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
//                                                Log.d("SSSSSSS", "3" + permission.getName());
//
//                                            }
//                                        }).check();
                                String shareBody = "пригласить";
                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareBody);
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, invite_text);
                                activity.startActivity(Intent.createChooser(sharingIntent, shareBody));
                            }else {
                                activity.startActivity(new Intent(activity, ActivityChatMessage.class)
                                        .putExtra("who","chat")
                                        .putExtra("user_id",item.getContact_user_id())
                                        .putExtra("is_mute",0)
                                        .putExtra("name",item.getContact_name())
                                        .putExtra("chat_text","")
                                        .putExtra("avatar","")
                                        .putExtra("is_ava",false)
                                        .putExtra("dialog_id",item.getContact_user_id()+"U")
                                );
                            }

                        }
                    });

                    break;

                ////////////////////////////////////// F I L E   ///////////////////////////////////////////

                case FILE_MESSAGE:
                    item = feed_item.get(position);

                    viewHolder.file_name.setText(file_name[0]);

                    viewHolder.file_layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            downloadFile(position, "file");
                        }
                    });

                    break;

                //////////////////////////////////////   S T I C K E R   ///////////////////////////////////////////
                case STICKER_MESSAGE:
                    item = feed_item.get(position);

                    viewHolder.container_layout.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

                    viewHolder.resend_btn_m.setVisibility(View.GONE);
                    viewHolder.resend_btn_y.setVisibility(View.GONE);


                    Picasso.get().load(item.getSticker_image())
                                .into(viewHolder.single_image);
                        viewHolder.single_image.setVisibility(View.VISIBLE);


//                    viewHolder.image_click.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            if (selectedIds.size() > 0) {
//                                ( (ActivityChatMessage)activity).SelectItem(position);
//                            }
//                        }
//                    });
//
//                    viewHolder.image_click.setOnLongClickListener(new View.OnLongClickListener() {
//                        @Override
//                        public boolean onLongClick(View v) {
//                            ( (ActivityChatMessage)activity).SelectItem(position);
//                            return true;
//                        }
//                    });


                    break;

            }

        }


        return convertView;
    }
    @Override
    public int getViewTypeCount() {
        return MAX_LAYOUT_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        item = feed_item.get(position);
        return item.getMessage_type();
    }

    private void initializeSeekBar(final SeekBar mSeekBar) {

        int  duration = mediaPlayer.getDuration()/100;
        mSeekBar.setMax(duration);
        mRunnable = new Runnable() {
            @Override
            public void run() {
                if(mediaPlayer!=null){
                    int mCurrentPosition = mediaPlayer.getCurrentPosition()/100;
//                            duration_m.setText(mCurrentPosition+"_");
                    mSeekBar.setProgress(mCurrentPosition);

                }
                mHandler.postDelayed(mRunnable,100);
            }
        };
        mHandler.postDelayed(mRunnable,100);
    }


    private void downloadFile (int position,String format){
        item = feed_item.get(position);
        String File_url = item.getFile_url()[0];
        String File_name = item.getFile_name()[0];
        File savefile;
        Log.d("File_name",File_url);
        if (format.equals("audio") ){
            if  (File_url.contains("AudioRecordingvoice") || File_url.contains(".ogg")){
                savefile = new File(myDir +"/.voice"+format);
                Log.d("File_name1",savefile+" ");
            }else {
                savefile = new File(myDir +"/Dalagram "+format);
                Log.d("File_name2",savefile+" ");
            }

        }else {
            savefile = new File(myDir +"/Dalagram "+format);
        }

        File file_url = new File(savefile+"/"+File_name);

        if (!file_url.exists()) {
            AndroidNetworking.download(File_url,savefile.getPath(),File_name)
                    .setTag("downloadTest")
                    .setPriority(Priority.HIGH)
                    .build()
                    .startDownload(new DownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            if (format.equals("file")){

                                Uri pdfUri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", file_url);
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(pdfUri, "application/*");
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                Intent chooser = Intent.createChooser(intent, "");

                                activity.startActivity(chooser);

                            }else {
                                realm.beginTransaction();
                                DialogDetail dialogDetail1 = realm.where(DialogDetail.class).equalTo("dialog_id", dialog_id).and().equalTo("chat_id", item.getChat_id()).findFirst();

                                if (dialogDetail1 != null) {
                                    dialogDetail1.setSave_storage(true);
                                }

                                realm.commitTransaction();
                                item.setSave_storage(true);
                                notifyDataSetChanged();
                            }

                        }
                        @Override
                        public void onError(ANError anError) {
                            Toasty.error(activity, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                        }
                    });
        }else {
            if (format.equals("file")){
//                Uri path = Uri.fromFile(file_url);

                // создаём новое намерение
                Uri pdfUri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", file_url);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(pdfUri, "application/*");
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Intent chooser = Intent.createChooser(intent, "");

                activity.startActivity(chooser);



            }else {
                realm.beginTransaction();
                DialogDetail dialogDetail1 = realm.where(DialogDetail.class).equalTo("dialog_id", dialog_id).and().equalTo("chat_id", item.getChat_id()).findFirst();

                if (dialogDetail1 != null) {
                    dialogDetail1.setSave_storage(true);
                }

                realm.commitTransaction();
                item.setSave_storage(true);
                notifyDataSetChanged();
            }
        }


    }

}



