package kz.bgpro.www.dalagram.activitys.profile;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.*;
import android.widget.*;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.fxn.pix.Pix;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import de.hdodenhof.circleimageview.CircleImageView;
import it.sephiroth.android.library.widget.HListView;
import kz.bgpro.www.dalagram.activitys.ActivityChatMessage;
import kz.bgpro.www.dalagram.activitys.ActivityZoomImage;
import kz.bgpro.www.dalagram.activitys.adapters.GallereyListAdapter;
import kz.bgpro.www.dalagram.fragment.adapters.NewsListAdapter;
import kz.bgpro.www.dalagram.models.NewsItem;
import kz.bgpro.www.dalagram.utils.MyConstants;
import kz.bgpro.www.dalagram.utils.NonScrollListView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.SplashActivity;
import kz.bgpro.www.dalagram.News.ActivityPublishNews;
import kz.bgpro.www.dalagram.utils.NurJS;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import static kz.bgpro.www.dalagram.MainActivity.MY_USER_ID;
import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.ListViewUtil.setListViewHeightBasedOnChildren;
import static kz.bgpro.www.dalagram.utils.MyConstants.*;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

public class ActivityMyProfile extends AppCompatActivity implements ObservableScrollViewCallbacks {

    SharedPreferences settings;


    @BindView(R.id.scroll)
    ObservableScrollView scroll;

    @BindView(R.id.toolBar)
    RelativeLayout toolBar;


    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.avatar)
    CircleImageView avatar;

    @BindView(R.id.inner_image)
    ImageView inner_image;

    @BindView(R.id.mini_name)
    TextView mini_name;

    @BindView(R.id.mini_avatar)
    ImageView mini_avatar;

    @BindView(R.id.status)
    TextView status;

    @BindView(R.id.buttons_layout)
    LinearLayout buttons_layout;

    @BindView(R.id.publish_layout)
    RelativeLayout publish_layout;

    @BindView(R.id.change_info)
    TextView change_info;

    @BindView(R.id.menu)
    ImageButton menu;

    Dialog dialog;

    TextView change;
    TextView setting;
    TextView exit;

    JSONObject profile_response = null;


    ArrayList<NewsItem> feedItems;

    ArrayList<NewsItem> feedItems_gallerey;

    NewsListAdapter adapter;

    GallereyListAdapter gallerey_adapter;

    @BindView(R.id.list)
    NonScrollListView listView;

    @BindView(R.id.gallerey_list)
    HListView hlistView;


    @BindView(R.id.posts_count)
    TextView posts_count;

    @BindView(R.id.friends_count)
    TextView friends_count;

    @BindView(R.id.photos_count)
    TextView photos_count;

    @BindView(R.id.gallerey_count)
    TextView gallerey_count;

    @BindView(R.id.followers_count)
    TextView followers_count;

    @BindView(R.id.add_friend)
    Button add_friend;

    @BindView(R.id.add_subscribe)
    Button add_subscribe;


    int user_id;
    boolean is_my;
    String AVATAR = "";
    String BACKGROUND = "";
    String Name;

    int RequestCodeForGallerey = 123;
    ArrayList<String> returnValue;

    @BindView(R.id.city)
    TextView city;
    @BindView(R.id.job)
    TextView job;
    @BindView(R.id.univer)
    TextView univer;
    @BindView(R.id.birthday)
    TextView birthday;
    @BindView(R.id.work)
    TextView work;
    @BindView(R.id.language)
    TextView language;
    @BindView(R.id.family)
    TextView family;

    @BindView(R.id.full_information)
    LinearLayout full_information;
    @BindView(R.id.view_text)
    TextView view_text;
    boolean visible_info = false;


    public static boolean from_news = false;

    JSONArray response_news;

    View footer;
    boolean loadingMore = true;
    public int page = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myprofile);
        ButterKnife.bind(this);
        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        scroll.setScrollViewCallbacks(this);

        user_id = getIntent().getIntExtra("user_id", MY_USER_ID);
        is_my = getIntent().getBooleanExtra("is_my", false);
        if (user_id == MY_USER_ID) {
            is_my = true;
        }


        if (is_my) {
            buttons_layout.setVisibility(View.GONE);
            change_info.setVisibility(View.VISIBLE);
            publish_layout.setVisibility(View.VISIBLE);
            menu.setVisibility(View.VISIBLE);
        } else {
            buttons_layout.setVisibility(View.VISIBLE);
            change_info.setVisibility(View.GONE);
            publish_layout.setVisibility(View.GONE);
            menu.setVisibility(View.GONE);
        }

        initMoreDialogView();

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(ActivityMyProfile.this, ActivityChangeMyProfile.class)
                        .putExtra("profile_response", profile_response + ""));

            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();

                realm.deleteAll();
                realm.commitTransaction();

                SharedPreferences.Editor editor = settings.edit();
                editor.clear();
                editor.commit();
                startActivity(new Intent(ActivityMyProfile.this, SplashActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }
        });

//        listView.setFocusable(false);
//        scroll.setFocusable(true);


        feedItems_gallerey = new ArrayList<NewsItem>();
        gallerey_adapter = new GallereyListAdapter(feedItems_gallerey, ActivityMyProfile.this, is_my, 1);
        hlistView.setAdapter(gallerey_adapter);

        feedItems = new ArrayList<NewsItem>();

        adapter = new NewsListAdapter(feedItems, ActivityMyProfile.this, is_my, response_news, listView);
        listView.setAdapter(adapter);


        footer = View.inflate(getApplicationContext(), R.layout.footer_progress, null);
        listView.addFooterView(footer);
        footer.setVisibility(View.GONE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            scroll.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (scroll.getChildAt(0).getBottom()
                            <= (scroll.getHeight() + scroll.getScrollY())) {
                        if (!loadingMore) {
                            Log.d("oooo", "loadingMore");
                            GetNewsFromServer(page);
                            loadingMore = true;
                        }
                    }
                }
            });
        }

//        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
//
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//            }
//
//            public void onScroll(AbsListView view, int firstVisibleItem,
//                                 int visibleItemCount, int totalItemCount) {
//
//                Log.d("oooo", totalItemCount+"ыыыы"+firstVisibleItem+"ыыыы"+visibleItemCount);
//
//                if (totalItemCount != 0 && firstVisibleItem + visibleItemCount+1 == totalItemCount) {
//                    if (!loadingMore) {
//                        Log.d("oooo", "loadingMore");
//                        GetNewsFromServer(page);
//                        loadingMore = true;
//                    }
//                }
//            }
//        });


        GetGallereyFromServer();

        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(AVATAR)) {
                    Intent intent = new Intent(ActivityMyProfile.this, ActivityZoomImage.class);
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("url", new ArrayList<>(Arrays.asList(AVATAR)));
                    bundle.putInt("position", 0);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }

            }
        });

        inner_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(BACKGROUND)) {
                    Intent intent = new Intent(ActivityMyProfile.this, ActivityZoomImage.class);
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("url", new ArrayList<>(Arrays.asList(BACKGROUND)));
                    bundle.putInt("position", 0);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }

            }
        });

    }

    @OnClick(R.id.change_info)
    void Change_info() {
        startActivity(new Intent(ActivityMyProfile.this, ActivityChangeMyProfile.class)
                .putExtra("profile_response", profile_response + ""));

    }

    @OnClick(R.id.all_gallery)
    void All_gallery() {
        startActivity(new Intent(ActivityMyProfile.this, AllGallery.class)
                .putExtra("feedItems_gallerey", feedItems_gallerey)
                .putExtra("is_my", is_my)
        );

    }

    @OnClick(R.id.view_information)
    void view_information() {
        if (visible_info) {
            full_information.setVisibility(View.GONE);
            visible_info = false;
            view_text.setText("Посмотреть все");
            view_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_down_up1, 0);
        } else {
            full_information.setVisibility(View.VISIBLE);
            visible_info = true;
            view_text.setText("Скрыть");
            view_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_down_up2, 0);
        }

    }


    @OnClick(R.id.to_chat)
    void to_chat() {


        String avatar;
        boolean is_ava;
        if (TextUtils.isEmpty(AVATAR)) {
            if (Name.length() > 1)
                avatar = Name.charAt(0) + "" + Name.charAt(1);
            else
                avatar = Name.charAt(0) + "";
            is_ava = false;
        } else {
            is_ava = true;
            avatar = AVATAR;
        }

        startActivity(new Intent(ActivityMyProfile.this, ActivityChatMessage.class)
                .putExtra("who", "chat")
                .putExtra("user_id", user_id)
                .putExtra("name", Name)
                .putExtra("avatar", avatar)
                .putExtra("is_ava", is_ava)
                .putExtra("dialog_id", user_id + "U")
        );

        finish();
    }

    @OnClick(R.id.add_friend)
    void add_friend() {
        AndroidNetworking.post(follower_send + TOKEN).addBodyParameter("partner_id", user_id + "")
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response", response + "   ");
                try {
                    if (response.getBoolean("status")) {
                        Toasty.info(ActivityMyProfile.this, response.getString("message"), Toast.LENGTH_SHORT).show();

                    } else {
                        Toasty.error(ActivityMyProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityMyProfile.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.add_subscribe)
    void add_subscribe() {

    }


    public void AddGalarey() {
        Pix.start(ActivityMyProfile.this,                    //Activity or Fragment Instance
                RequestCodeForGallerey,                //Request code for activity results
                1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == RequestCodeForGallerey) {


                returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                SendImageToServer(returnValue.get(0));


            }
        }
    }

    public void SendImageToServer(String name) {

        String new_filename = MyConstants.compressImage(name, ActivityMyProfile.this);
        File f = new File(new_filename);

        AndroidNetworking.upload(file + TOKEN)
                .setTag("upload")
                .addMultipartFile("file", f)
                .setPriority(Priority.HIGH)
                .build()
//                    .setUploadProgressListener(new UploadProgressListener() {
//                        @Override
//                        public void onProgress(long bytesUploaded, long totalBytes) {
//                            // do anything with progress
//                        }
//                    })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")) {
                                String file_name = response.getString("file_name");
                                String file_url = response.getString("file_url");
                                String file_format = response.getString("file_format");
                                SendGalerey(file_name, file_url, file_format);
                            } else {
                                Toasty.error(ActivityMyProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                            }

                            deleteFileName(f.getName());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("file response _ ", response + "***");
                    }

                    @Override
                    public void onError(ANError anError) {
//                        Toasty.error(ActivityChatMessage.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                        Log.d("file anError _ ", "Проверить подключение интернета" + "***");
                        Log.d("file anError _ ", anError.getErrorCode() + "***");
                    }
                });
    }

    public void SendGalerey(String file_name, String file_url, String file_format) {
        JSONObject js = new JSONObject();
        try {
            js.put("publication_desc", " ");

            JSONArray file_list = new JSONArray();
            JSONObject file = new JSONObject();
            file.put("file_url", file_url);
            file.put("file_name", file_name);
            file.put("file_format", file_format);
            file_list.put(file);

            js.put("is_gallery", 1);

            js.put("file_list", file_list);


            Log.d("BBBBBBBBB", js + "mmm");
            AndroidNetworking.post(publication + TOKEN)
                    .addJSONObjectBody(js)
                    .build().getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("AAAAAAMMMMM", response + "mmm");
                    try {
                        if (response.getBoolean("status")) {
                            feedItems_gallerey.clear();
                            GetGallereyFromServer();

                        } else {
                            Toasty.error(ActivityMyProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(ANError anError) {
                    Toasty.error(ActivityMyProfile.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void GetGallereyFromServer() {
        if (is_my) {
            NewsItem item = new NewsItem();
            feedItems_gallerey.add(item);
        }

        Log.d("NEWS", gallery + TOKEN + "&page=1&per_page=100&author_id=" + user_id);
        AndroidNetworking.get(gallery + TOKEN + "&page=1&per_page=100&author_id=" + user_id).build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response_ gallery", response + "^^^^^");
                try {
                    if (response.getBoolean("status")) {
                        JSONArray data = response.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jsonObject = (JSONObject) data.get(i);
                            NewsItem item = new NewsItem();

                            item.setPublication_id(NurInt(jsonObject, "publication_id"));
                            item.setPublication_desc(NurString(jsonObject, "publication_desc"));
                            item.setPublication_date(NurString(jsonObject, "publication_date"));
                            item.setView_count(NurInt(jsonObject, "view_count"));
                            item.setComment_count(NurInt(jsonObject, "comment_count"));
                            item.setShare_count(NurInt(jsonObject, "share_count"));
                            item.setLike_count(NurInt(jsonObject, "like_count"));
                            item.setIs_has_file(NurInt(jsonObject, "is_has_file"));
                            item.setIs_i_liked(NurInt(jsonObject, "is_i_liked"));


                            JSONObject author = jsonObject.getJSONObject("author");
                            item.setUser_id(NurInt(author, "user_id"));
                            item.setUser_name(NurString(author, "nickname"));
                            item.setPhone(NurString(author, "phone"));
                            item.setAvatar(NurString(author, "avatar"));

                            JSONArray file_list = jsonObject.getJSONArray("file_list");
                            int l = file_list.length();
                            String[] file_url = new String[l];
                            String[] file_format = new String[l];
                            String[] file_name = new String[l];
                            int file_time = 0;
                            int image_count = 0;
                            for (int f = 0; f < l; f++) {
                                JSONObject file = (JSONObject) file_list.get(f);
                                file_url[f] = file.getString("file_url");
                                file_format[f] = file.getString("file_format");
                                file_name[f] = file.getString("file_name");
                                file_time = file.getInt("file_time");
                                if (file.getString("file_format").equals("image") || file.getString("file_format").equals("video")) {
                                    image_count++;
                                }
                            }
                            item.setImage_count(image_count);
                            item.setFile_url(file_url);
                            item.setFile_format(file_format);
                            item.setFile_name(file_name);
                            item.setFile_time(file_time);
                            int is_has_link = NurInt(jsonObject, "is_has_link");
                            String link_description = " ", link_title = " ", link_image = " ";
                            if (is_has_link == 1) {
                                JSONObject meta_tags = jsonObject.getJSONObject("meta_tags");
                                link_description = meta_tags.getString("description");
                                link_title = meta_tags.getString("title");
                                link_image = meta_tags.getString("image");
                            }
                            item.setIs_has_link(is_has_link);
                            item.setLink_description(link_description);
                            item.setLink_title(link_title);
                            item.setLink_image(link_image);

                            feedItems_gallerey.add(item);
                        }


                        gallerey_adapter.notifyDataSetChanged();


                    } else {
                        Toasty.error(ActivityMyProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                GetNewsFromServer(page);
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityMyProfile.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void GetNewsFromServer(int Page) {
        if (page==1) feedItems.clear();
        Log.d("NEWS", publication + TOKEN + "&page=" + Page + "&per_page=10&author_id=" + user_id);
        AndroidNetworking.get(publication + TOKEN + "&page=" + Page + "&per_page=10&author_id=" + user_id).build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("response_news", response + "^^^^^");
                try {
                    if (response.getBoolean("status")) {
                        JSONArray data = response.getJSONArray("data");

                        int total_page = response.getInt("total_page");
                        if (total_page > page) {
                            loadingMore = false;
                            footer.setVisibility(View.VISIBLE);
                        } else {
                            loadingMore = true;
                            footer.setVisibility(View.GONE);
                        }

                        response_news = data;
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jsonObject = (JSONObject) data.get(i);
                            NewsItem item = new NewsItem();

                            item.setPublication_id(NurInt(jsonObject, "publication_id"));
                            item.setPublication_desc(NurString(jsonObject, "publication_desc"));
                            item.setPublication_date(NurString(jsonObject, "publication_date"));
                            item.setView_count(NurInt(jsonObject, "view_count"));
                            item.setComment_count(NurInt(jsonObject, "comment_count"));

                            if (NurInt(jsonObject, "comment_count") != 0) {
                                JSONObject comment = jsonObject.getJSONObject("comment");
                                item.setComment_id(NurInt(comment, "comment_id"));
                                item.setComment_text(NurString(comment, "comment_text"));
                                item.setComment_date(NurString(comment, "comment_date"));
                                JSONObject comment_author = comment.getJSONObject("author");
                                item.setComment_avatar(NurString(comment_author, "avatar"));
                                item.setComment_user_name(NurString(comment_author, "nickname"));
                                item.setIs_i_liked_comment(NurInt(comment, "is_liked"));
                            } else {
                                item.setComment_id(0);
                                item.setComment_text(" ");
                                item.setComment_date(" ");
                                item.setComment_avatar(" ");
                                item.setComment_user_name(" ");
                                item.setIs_i_liked_comment(0);
                            }

                            item.setShare_count(NurInt(jsonObject, "share_count"));
                            item.setLike_count(NurInt(jsonObject, "like_count"));
                            item.setIs_has_file(NurInt(jsonObject, "is_has_file"));
                            item.setIs_i_liked(NurInt(jsonObject, "is_i_liked"));


                            JSONObject author = jsonObject.getJSONObject("author");
                            item.setUser_id(NurInt(author, "user_id"));
                            item.setUser_name(NurString(author, "nickname"));
                            item.setPhone(NurString(author, "phone"));
                            item.setAvatar(NurString(author, "avatar"));

                            JSONArray file_list = jsonObject.getJSONArray("file_list");
                            int l = file_list.length();
                            String[] file_url = new String[l];
                            String[] file_format = new String[l];
                            String[] file_name = new String[l];
                            int file_time = 0;
                            int image_count = 0;
                            for (int f = 0; f < l; f++) {
                                JSONObject file = (JSONObject) file_list.get(f);
                                file_url[f] = file.getString("file_url");
                                file_format[f] = file.getString("file_format");
                                file_name[f] = file.getString("file_name");
                                file_time = file.getInt("file_time");
                                if (file.getString("file_format").equals("image") || file.getString("file_format").equals("video")) {
                                    image_count++;
                                }
                            }
                            item.setImage_count(image_count);
                            item.setFile_url(file_url);
                            item.setFile_format(file_format);
                            item.setFile_name(file_name);
                            item.setFile_time(file_time);
                            int is_has_link = NurInt(jsonObject, "is_has_link");
                            String link_description = " ", link_title = " ", link_image = " ";
                            if (is_has_link == 1) {
                                JSONObject meta_tags = jsonObject.getJSONObject("meta_tags");
                                link_description = meta_tags.getString("description");
                                link_title = meta_tags.getString("title");
                                link_image = meta_tags.getString("image");
                            }
                            item.setIs_has_link(is_has_link);
                            item.setLink_description(link_description);
                            item.setLink_title(link_title);
                            item.setLink_image(link_image);

                            feedItems.add(item);
                        }


                        adapter.notifyDataSetChanged();
                        if (Page != 1) {
//                            int currentPosition = listView.getFirstVisiblePosition()==0?0:listView.getFirstVisiblePosition()+1;
//                            listView.setSelectionFromTop(currentPosition,0);
                        }

                        page++;

                    } else {
                        Toasty.error(ActivityMyProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityMyProfile.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @OnClick(R.id.insert_layout)
    void insert_layout() {
        startActivity(new Intent(ActivityMyProfile.this, ActivityPublishNews.class));
    }

    private void initMoreDialogView() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.myprofile_more_menu_dialog);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        int pixels = (int) (200 * getResources().getDisplayMetrics().density);
        wlp.width = pixels;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        change = (TextView) dialog.findViewById(R.id.change);
        setting = (TextView) dialog.findViewById(R.id.setting);
        exit = (TextView) dialog.findViewById(R.id.exit);

    }


    public void GetProfileFromServer() {
        String url = profile + TOKEN;
        if (!is_my) {
            url = profile + TOKEN + "&user_id=" + user_id;
        }
        Log.d("profile", url);
        AndroidNetworking.get(url)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")) {
                        profile_response = response;
                        Log.d("profile", response + "++");
                        JSONObject data = response.getJSONObject("data");

                        Name = NurJS.NurString(data, "user_name");
                        name.setText(Name);
                        mini_name.setText(Name);
                        status.setText(NurJS.NurString(data, "user_status"));
                        if (!NurJS.NurString(data, "city").equals("")) {
                            city.setText(NurJS.NurString(data, "city"));
                        } else {
                            city.setText("нет информация");
                        }
                        if (!NurJS.NurString(data, "work").equals("")) {
                            job.setText(NurJS.NurString(data, "work"));
                        } else {
                            job.setText("нет информация");
                        }
                        if (!NurJS.NurString(data, "university").equals("")) {
                            univer.setText(NurJS.NurString(data, "university"));
                        } else {
                            univer.setText("нет информация");
                        }

                        if (!NurJS.NurString(data, "birth_date").equals("")) {
                            birthday.setText(NurJS.NurString(data, "birth_date"));
                        } else {
                            birthday.setText("нет информация");
                        }
                        if (!NurJS.NurString(data, "work").equals("")) {
                            work.setText(NurJS.NurString(data, "work"));
                        } else {
                            work.setText("нет информация");
                        }
                        if (!NurJS.NurString(data, "language").equals("")) {
                            language.setText(NurJS.NurString(data, "language"));
                        } else {
                            language.setText("нет информация");
                        }
                        if (!NurJS.NurString(data, "family").equals("")) {
                            family.setText(NurJS.NurString(data, "family"));
                        } else {
                            family.setText("нет информация");
                        }


                        posts_count.setText(NurJS.NurInt(data, "publication_count") + "");
                        photos_count.setText(NurJS.NurInt(data, "photo_count") + "");
                        if (NurJS.NurInt(data, "photo_count") != 0) {
                            gallerey_count.setText(NurJS.NurInt(data, "photo_count") + "");
                        }
                        friends_count.setText(NurJS.NurInt(data, "friend_count") + "");
                        followers_count.setText(NurJS.NurInt(data, "follower_count") + "");

                        AVATAR = NurJS.NurString(data, "avatar");

                        BACKGROUND = NurJS.NurString(data, "background");
                        if (!TextUtils.isEmpty(AVATAR)) {
                            Glide.with(ActivityMyProfile.this).load(data.getString("avatar")).into(avatar);
                            Glide.with(ActivityMyProfile.this).load(data.getString("avatar")).into(mini_avatar);
                        }
                        if (!TextUtils.isEmpty(BACKGROUND)) {
                            Glide.with(ActivityMyProfile.this).load(BACKGROUND).into(inner_image);
                        }

                        if (is_my) {
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("avatar", NurJS.NurString(data, "avatar"));
                            editor.putString("user_name", NurJS.NurString(data, "user_name"));
                            editor.putString("phone", NurJS.NurString(data, "phone"));
                            editor.commit();
                        } else {
                            if (NurInt(data, "is_friend") == 1) {
                                add_friend.setText("У вас в друзьях");
                            } else {
                                add_friend.setText("Добавить в друзья");
                            }
                            if (NurInt(data, "is_i_subscribe") == 1) {
                                add_subscribe.setText("Подписан");
                            } else {
                                add_subscribe.setText("Подписаться");
                            }
                        }


                    } else {

                        Toasty.error(ActivityMyProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityMyProfile.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });


    }


    @OnClick(R.id.back)
    void back() {
        finish();
    }


    @OnClick(R.id.menu)
    void menu() {
        dialog.show();
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        int baseColor = getResources().getColor(R.color.colorPrimary);
        float alpha = Math.min(1, (float) scrollY / 300);
        toolBar.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        page = 1;
        GetProfileFromServer();
        if (from_news) {
            GetNewsFromServer(page);
        }
    }

}