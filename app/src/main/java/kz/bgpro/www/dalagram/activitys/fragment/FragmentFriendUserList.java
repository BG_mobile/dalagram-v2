package kz.bgpro.www.dalagram.activitys.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.ActivityAddUser;
import kz.bgpro.www.dalagram.activitys.ActivityChatMessage;
import kz.bgpro.www.dalagram.activitys.ActivitySelectedUser;
import kz.bgpro.www.dalagram.activitys.adapters.SelectedContactsAdapter;
import kz.bgpro.www.dalagram.models.FeedItem;
import kz.bgpro.www.dalagram.utils.NurJS;

import static android.widget.Toast.LENGTH_SHORT;
import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.channel_adduser;
import static kz.bgpro.www.dalagram.utils.MyConstants.friend;
import static kz.bgpro.www.dalagram.utils.MyConstants.group_adduser;

@SuppressLint("ValidFragment")
public class FragmentFriendUserList extends Fragment {

    @BindView(R.id.grid)
    GridView grid;
    int pos;

    public static SelectedContactsAdapter  selected_adapter;

    ActivitySelectedUser activitySelectedUser;
    ActivityAddUser activityAddUser;

    public FragmentFriendUserList(ActivitySelectedUser a, ActivityAddUser b, int k) {
        this.activitySelectedUser = a;
        this.activityAddUser = b;
        this.pos = k;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_media, container, false);
        ButterKnife.bind(this, view);

        GetJSONFromServer();


        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FeedItem item;
                if (pos == 1) {
                    item = activitySelectedUser.feedItem_contacts.get(position);
                    if (activitySelectedUser.who == 3) {

                        String Name = item.getName();
                        String avatar;
                        boolean is_ava;
                        if (TextUtils.isEmpty(item.getAvatar())) {
                            if (Name.length() > 1)
                                avatar = Name.charAt(0) + "" + Name.charAt(1);
                            else
                                avatar = Name.charAt(0) + "";
                            is_ava = false;
                        } else {
                            is_ava = true;
                            avatar = item.getAvatar();
                        }

                        startActivity(new Intent(getActivity(), ActivityChatMessage.class)
                                        .putExtra("who","chat")
                                        .putExtra("user_id",item.getUser_id())
                                        .putExtra("name",Name)
                                        .putExtra("chat_text",item.getLast_visit())
                                        .putExtra("avatar",avatar)
                                        .putExtra("is_ava",is_ava)
                                        .putExtra("dialog_id",item.getUser_id()+"U")
                        );


                    } else {
                        if (item.isIs_selected()) {
                            item.setIs_selected(false);
                            activitySelectedUser.h_feedItem.remove(item);
                            activitySelectedUser.hadapter.notifyDataSetChanged();
                            selected_adapter.notifyDataSetChanged();
                            activitySelectedUser.selected_count--;
                        } else {
                            item.setIs_selected(true);
                            activitySelectedUser.h_feedItem.add(item);
                            activitySelectedUser.hadapter.notifyDataSetChanged();
                            selected_adapter.notifyDataSetChanged();
                            activitySelectedUser.selected_count++;

                        }

                        activitySelectedUser.count.setText("выбран " + activitySelectedUser.selected_count);
                    }
                }else {
                    item =  activityAddUser.feedItem_contacts.get(position);
                    AlertCreate(item,pos);
                }
            }
        });


        return view;
    }

    private void AlertCreate(FeedItem item, int pos) {
        if (pos==2){
            AlertDialog.Builder ad = new AlertDialog.Builder(getActivity());
            ad.setMessage("Добавить "+item.getName()+" в группу?"); // сообщение
            ad.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {

                    AndroidNetworking.post(group_adduser+activityAddUser.group_id+"?token="+TOKEN)
                            .addBodyParameter("user_id",item.getUser_id()+"")
                            .build().getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Toasty.success(getActivity(),response.getString("message"),LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toasty.error(getActivity(),"Проверить подключение интернета",LENGTH_SHORT).show();
                        }
                    });
                }
            });
            ad.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {

                }
            });
            ad.setCancelable(true);
            ad.show();
        }else {
            AlertDialog.Builder ad = new AlertDialog.Builder(getActivity());
            ad.setMessage("Добавить "+item.getName()+" в канал?"); // сообщение
            ad.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {

                    AndroidNetworking.post(channel_adduser+activityAddUser.channel_id+"?token="+TOKEN)
                            .addBodyParameter("user_id",item.getUser_id()+"")
                            .build().getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Toasty.success(getActivity(),response.getString("message"),LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.d("AAAAAsssss",response+" *");
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toasty.error(getActivity(),"Проверить подключение интернета",LENGTH_SHORT).show();
                        }
                    });
                }
            });
            ad.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {

                }
            });
            ad.setCancelable(true);
            ad.show();
        }

    }

    private void GetJSONFromServer() {
        if (pos==1){
            activitySelectedUser.feedItem_contacts = new ArrayList<FeedItem>();
        }else {
            activityAddUser.feedItem_contacts = new ArrayList<FeedItem>();
        }
        AndroidNetworking.get(friend+TOKEN+"&page=1&per_page=50").build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("ASAS",response+"*");
                try {
                    if(response.getBoolean("status")){
                        JSONArray jsonArray = response.getJSONArray("data");
                      int  total_count = jsonArray.length();
                        for (int i=0;i<total_count;i++){
                            JSONObject object = (JSONObject) jsonArray.get(i);
                            FeedItem item = new FeedItem();
                            item.setUser_id(object.getInt("user_id"));
                            item.setName(object.getString("user_name"));
                            item.setName(object.getString("nickname"));
                            item.setAvatar(NurJS.NurString(object,"avatar"));
                            item.setUser_status(object.getString("user_status"));
                            item.setLast_visit(object.getString("last_visit"));
                            item.setIs_selected(false);
                            if (pos==1){
                                activitySelectedUser.feedItem_contacts.add(item);
                            }else {
                                activityAddUser.feedItem_contacts.add(item);
                            }
                        }

                        if (pos==1){
                            activitySelectedUser.count.setText("выбран "+activitySelectedUser.selected_count);
                            selected_adapter= new SelectedContactsAdapter(activitySelectedUser.feedItem_contacts, getActivity());
                        }else {
                            activityAddUser.count.setText("выбран "+activityAddUser.selected_count);
                            selected_adapter= new SelectedContactsAdapter(activityAddUser.feedItem_contacts, getActivity());
                        }

                        grid.setAdapter(selected_adapter);



                    }else {
                        Toasty.error(getActivity(),response.getString("error"),LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(getActivity(),"Проверить подключение интернета",LENGTH_SHORT).show();

            }
        });
    }

}
