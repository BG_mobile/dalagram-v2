package kz.bgpro.www.dalagram.activitys;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.News.ActivitySingleNews;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.fragment.FragmentPagerImageZoom;
import kz.bgpro.www.dalagram.models.NewsItem;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.like;

public class ActivityGallereyZoomImage extends FragmentActivity {

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.like_layout)
    LinearLayout like_layout;
    @BindView(R.id.like_count)
    TextView like_count;

    @BindView(R.id.like_img)
    ImageView like_img;
    @BindView(R.id.comment_layout)
    LinearLayout comment_layout;
    @BindView(R.id.comment_count)
    TextView comment_count;
    @BindView(R.id.share_layout)
    LinearLayout share_layout;
    @BindView(R.id.share_count)
    TextView share_count;
    @BindView(R.id.view_count)
    TextView view_count;


    ArrayList<NewsItem> feed_item;
    int position;
    NewsItem item;


    int current_position;
    private List<String> images;

    MyFragmentPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallereyzoom_image);
        ButterKnife.bind(this);

        feed_item = (ArrayList<NewsItem>) getIntent().getSerializableExtra("feed");
        position = getIntent().getIntExtra("position", 0);
        item = feed_item.get(position);
        images = Collections.singletonList(item.getFile_url()[0]);

         adapter = new MyFragmentPagerAdapter(images, getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(current_position);


        if (item.getLike_count()==0){
            like_count.setText(" ");
        }else {
            like_count.setText(item.getLike_count()+"");
        }

        if (item.getComment_count() == 0) {
            comment_count.setText(" ");
        }else {
            comment_count.setText(item.getComment_count()+"");
        }

        if (item.getShare_count() == 0) {
            share_count.setText(" ");
        }else {
            share_count.setText(item.getShare_count()+"");
        }

        if (item.getView_count() == 0) {
            view_count.setText(" ");
        }else {
            view_count.setText(item.getView_count()+"");
        }

        if (item.getIs_i_liked()==1){
            like_img.setImageResource(R.drawable.ic_like);
            like_img.setColorFilter(ContextCompat.getColor(this, R.color.like_color), android.graphics.PorterDuff.Mode.SRC_IN);
        }else {
            like_img.setImageResource(R.drawable.ic_dislike);
            like_img.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
        }


        like_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                AndroidNetworking.post(like+TOKEN).addBodyParameter("publication_id",item.getPublication_id()+"")
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")){
                                item.setLike_count(response.getInt("like_count"));
                                like_count.setText(response.getInt("like_count")+"");
                                if (item.getIs_i_liked()==1){
                                    item.setIs_i_liked(0);
                                    like_img.setImageResource(R.drawable.ic_dislike);
                                    like_img.setColorFilter(ContextCompat.getColor(ActivityGallereyZoomImage.this, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
                                }else {
                                    item.setIs_i_liked(1);
                                    like_img.setImageResource(R.drawable.ic_like);
                                    like_img.setColorFilter(ContextCompat.getColor(ActivityGallereyZoomImage.this, R.color.like_color), android.graphics.PorterDuff.Mode.SRC_IN);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("LIKE", response+"  ");
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("LIKEerr", anError+"  ");
                    }
                });
            }
        });

        comment_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                Intent intent = new Intent(ActivityGallereyZoomImage.this, ActivitySingleNews.class);
                intent.putExtra("publication_id",item.getPublication_id());
                intent.putExtra("is_my",true);
                startActivity(intent);
            }
        });

    }

    @OnClick(R.id.back)
    void back(){
        finish();
    }

    @OnClick(R.id.download)
    void download(){
        downloadImage(images.get(current_position));
    }

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        List<String> images;

        public MyFragmentPagerAdapter(List<String> images, FragmentManager fm) {
            super(fm);
            this.images = images;
        }

        @Override
        public Fragment getItem(int position) {
            return FragmentPagerImageZoom.newInstance(images.get(position));
        }

        @Override
        public int getCount() {
            return images.size();
        }

    }


    private void downloadImage (String url){



        String fileName = url.substring(url.lastIndexOf('/') + 1);
        String DIR_NAME = "/Dalagram/Dalagram image";
        File direct =
                new File(Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                        .getAbsolutePath() + "/" + DIR_NAME + "/");


        if (!direct.exists()) {
            direct.mkdir();
            Log.d("aaa", "dir created for first time");
        }

        DownloadManager dm = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        Uri downloadUri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle(fileName)
                .setMimeType("image/jpeg")
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES,
                        File.separator + DIR_NAME + File.separator + fileName);

        dm.enqueue(request);
    }



}