package kz.bgpro.www.dalagram.Auth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;

import static kz.bgpro.www.dalagram.utils.MyConstants.login;


public class ActivityConfirmPhone extends Activity {


    @BindView(R.id.number)
    EditText number;

    @BindView(R.id.progress)
    ProgressBar progress;

    @BindView(R.id.next)
    Button next;

    @BindString(R.string.input_phone_number)
    String input_phone_number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_phone);
        ButterKnife.bind(this);

        progress.setVisibility(View.GONE);
        next.setClickable(true);
        number.setEnabled(true);



        number.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s)
            {
                String text = number.getText().toString();
                int  textLength = number.getText().length();
                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return;
                if (textLength == 4)
                {
                    if (!text.contains(" "))
                    {
                        number.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                        number.setSelection(number.getText().length());
                    }
                }
                else if (textLength == 8)
                {
                    number.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                    number.setSelection(number.getText().length());
                }
            }
        });


    }

    @OnClick(R.id.next)
    void next(){


        String phone = number.getText().toString();
        phone = phone.replace(" ","");
        if(!TextUtils.isEmpty(phone)){

            progress.setVisibility(View.VISIBLE);
            next.setClickable(false);
            number.setEnabled(false);
            final String finalPhone = phone;
            AndroidNetworking.post(login)
                    .addBodyParameter("phone", phone)
                    .build().getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if(response.getBoolean("status")){
                            Toasty.success(ActivityConfirmPhone.this,response.getString("message"), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(ActivityConfirmPhone.this,ActivityConfirmCode.class).putExtra("phone", finalPhone));
                        }else {
                            progress.setVisibility(View.GONE);
                            next.setClickable(true);
                            number.setEnabled(true);
                            Toasty.error(ActivityConfirmPhone.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(ANError anError) {
                    progress.setVisibility(View.GONE);
                    next.setClickable(true);
                    number.setEnabled(true);
                    Log.d("responseString","Проверить подключение интернета"+"--"+anError.getErrorCode());
                    Toasty.error(ActivityConfirmPhone.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                }
            });

        }else {
            Toasty.info(this, input_phone_number, Toast.LENGTH_SHORT).show();
        }

    }

}

