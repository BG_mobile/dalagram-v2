package kz.bgpro.www.dalagram.Realm;


import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nurbaqyt on 24.07.2018.
 */

public class Contact extends RealmObject {
    @PrimaryKey
    private int user_id;

    protected String name="";
    protected String contact_name="";
    protected String avatar="";
    protected String user_status="";
    protected String last_visit="";



    public Contact(){
    }

    public Contact(int user_id, String name, String contact_name,String avatar,String user_status) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();


            Contact contact1 = new Contact();
            contact1.setUser_id(user_id);
            contact1.setName(name);
            contact1.setContact_name(contact_name);

            contact1.setAvatar(avatar);
            contact1.setUser_status(user_status);


        realm.copyToRealmOrUpdate(contact1);
        realm.commitTransaction();
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public void setLast_visit(String last_visit) {
        this.last_visit = last_visit;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getName() {
        return name;
    }

    public String getContact_name() {
        return contact_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getUser_status() {
        return user_status;
    }

    public String getLast_visit() {
        return last_visit;
    }
}
