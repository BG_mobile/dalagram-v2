package kz.bgpro.www.dalagram.fragment.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.util.Log;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.squareup.picasso.Picasso;
import com.vanniktech.emoji.EmojiTextView;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.News.ActivityChangeNews;
import kz.bgpro.www.dalagram.News.ActivityPublishNews;
import kz.bgpro.www.dalagram.activitys.ActivityVideoPlayer;
import kz.bgpro.www.dalagram.activitys.ActivityZoomImage;
import kz.bgpro.www.dalagram.activitys.profile.ActivityMyProfile;
import kz.bgpro.www.dalagram.utils.MyBounceInterpolator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import kz.bgpro.www.dalagram.News.ActivitySingleNews;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.models.NewsItem;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.fragment.FragmentNews.click_position;
import static kz.bgpro.www.dalagram.fragment.FragmentNews.newsmediaPlayer;
import static kz.bgpro.www.dalagram.utils.ListViewUtil.setListViewHeightBasedOnChildren;
import static kz.bgpro.www.dalagram.utils.MyConstants.*;

/**
 * Created by nurbaqyt on 22.05.2018.
 */

public class NewsListAdapter extends BaseAdapter {

    ArrayList<NewsItem> feed_item;
    NewsItem item;
    Activity activity;
    boolean is_my;
    int play_position=1000000;
     Animation myAnim;
     Animation myAnim_comment;
    JSONArray response_news = null;
    ListView listView = null;
    public NewsListAdapter(ArrayList<NewsItem> feedItem, Activity activity,boolean my) {
        this.activity = activity;
        this.feed_item = feedItem;
        this.is_my = my;
        newsmediaPlayer = new MediaPlayer();
          myAnim = AnimationUtils.loadAnimation(activity, R.anim.scale);
        myAnim_comment = AnimationUtils.loadAnimation(activity, R.anim.scale);
          MyBounceInterpolator interpolator = new MyBounceInterpolator(0.1, 60);
        myAnim.setInterpolator(interpolator);
        myAnim_comment.setInterpolator(interpolator);
    }
    public NewsListAdapter(ArrayList<NewsItem> feedItem, Activity activity,boolean my, JSONArray response_news) {
        this.activity = activity;
        this.feed_item = feedItem;
        this.is_my = my;
        this.response_news = response_news;
        newsmediaPlayer = new MediaPlayer();
          myAnim = AnimationUtils.loadAnimation(activity, R.anim.scale);
        myAnim_comment = AnimationUtils.loadAnimation(activity, R.anim.scale);
          MyBounceInterpolator interpolator = new MyBounceInterpolator(0.1, 60);
        myAnim.setInterpolator(interpolator);
        myAnim_comment.setInterpolator(interpolator);
    }
    public NewsListAdapter(ArrayList<NewsItem> feedItem, Activity activity,boolean my, JSONArray response_news,ListView listView) {
        this.activity = activity;
        this.feed_item = feedItem;
        this.is_my = my;
        this.response_news = response_news;
        this.listView = listView;
        newsmediaPlayer = new MediaPlayer();
          myAnim = AnimationUtils.loadAnimation(activity, R.anim.scale);
        myAnim_comment = AnimationUtils.loadAnimation(activity, R.anim.scale);
          MyBounceInterpolator interpolator = new MyBounceInterpolator(0.1, 60);
        myAnim.setInterpolator(interpolator);
        myAnim_comment.setInterpolator(interpolator);
    }


    @Override
    public int getCount() {
        return feed_item.size();
    }

    @Override
    public Object getItem(int location) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {

        @BindView(R.id.avatar)
        CircleImageView avatar;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.date)
        TextView date;

        @BindView(R.id.desc)
        EmojiTextView desc;
        @BindView(R.id.more_btn)
        TextView more_btn;

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.two_image)
        LinearLayout two_image;
        @BindView(R.id.image1)
        ImageView image1;
        @BindView(R.id.image2)
        ImageView image2;
        @BindView(R.id.more_image)
        TextView more_image;
        @BindView(R.id.playvideo2)
        ImageView playvideo2;

        @BindView(R.id.like_layout)
        LinearLayout like_layout;
        @BindView(R.id.like_count)
        TextView like_count;

        @BindView(R.id.like_img)
        ImageView like_img;

        @BindView(R.id.comment_layout)
        MaterialRippleLayout comment_layout;
        @BindView(R.id.comment_count)
        TextView comment_count;
        @BindView(R.id.share_layout)
        MaterialRippleLayout share_layout;
        @BindView(R.id.share_count)
        TextView share_count;
        @BindView(R.id.view_count)
        TextView view_count;

        @BindView(R.id.audio_layout)
        RelativeLayout audio_layout;

        @BindView(R.id.audio_play)
        ImageButton audio_play;
        @BindView(R.id.audio_name)
        TextView audio_name;
        @BindView(R.id.audio_time)
        TextView audio_time;

        @BindView(R.id.link_layout)
        LinearLayout link_layout;

        @BindView(R.id.link_title)
        TextView link_title;

        @BindView(R.id.link_desc)
        TextView link_desc;

        @BindView(R.id.link_image)
        ImageView link_image;

        @BindView(R.id.videolayout)
        FrameLayout videolayout;
        @BindView(R.id.videoview)
        ImageView videoview;
        @BindView(R.id.playvideo)
        ImageView playvideo;
        @BindView(R.id.videoProgressbar)
        ProgressBar videoProgressbar;


        @BindView(R.id.file_layout)
        View file_layout;
        @BindView(R.id.file_image)
        ImageView send_file_image;
        @BindView(R.id.file_name)
        TextView send_file_name;
        @BindView(R.id.file_format)
        TextView send_file_format;
        @BindView(R.id.file_upload_progress)
        ProgressBar file_upload_progress;

        @BindView(R.id.more_menu)
        ImageButton more_menu;

        //Comment
        @BindView(R.id.all_comment)
        TextView all_comment;
        @BindView(R.id.comment_inlayout)
        View  comment_inlayout;

        @BindView(R.id.comment_avatar)
        CircleImageView comment_avatar;

        @BindView(R.id.comment_name)
        TextView comment_name;
        @BindView(R.id.comment_date)
        TextView comment_date;
        @BindView(R.id.comment_desc)
        EmojiTextView comment_desc;

        @BindView(R.id.comment_like_img)
        ImageView comment_like_img;

        @BindView(R.id.comment_like_layout)
        LinearLayout comment_like_layout;

        @BindView(R.id.comment_like_count)
        TextView comment_like_count;

        @BindView(R.id.complain_btn)
        TextView complain_btn;

        @BindView(R.id.answer_btn)
        LinearLayout answer_btn;
        @BindView(R.id.comment_image)
        ImageView comment_image;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_news_list, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        item = feed_item.get(position);

        if (is_my){
            viewHolder.more_menu.setVisibility(View.VISIBLE);
        }else {
            viewHolder.more_menu.setVisibility(View.GONE);
        }

        viewHolder.name.setText(item.getUser_name());

        String desc_text = item.getPublication_desc().replaceAll("\n","<br>");
        String more = "  <font  color=\"grey\"> ...далее<font>";




        if ( item.getPublication_desc().length() > 170) {
            String newtext = desc_text.substring(0,170);
            viewHolder.desc.setText(Html.fromHtml(newtext+more));
            viewHolder.desc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    String desc_ = item.getPublication_desc().replaceAll("\n","<br>");
                    viewHolder.desc.setText(Html.fromHtml(desc_));
                }
            });
        } else {
            viewHolder.desc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    String desc_ = item.getPublication_desc().replaceAll("\n","<br>");
                    viewHolder.desc.setText(Html.fromHtml(desc_));
                }
            });
            viewHolder.desc.setText(Html.fromHtml(desc_text));
        }


        viewHolder.date.setText(item.getPublication_date());

        if (item.getLike_count()==0){
            viewHolder.like_count.setText(" ");
        }else {
            viewHolder.like_count.setText(item.getLike_count()+"");
        }

        if (item.getComment_count() == 0) {
            viewHolder.all_comment.setVisibility(View.GONE);
            viewHolder.comment_inlayout.setVisibility(View.GONE);
            viewHolder.comment_count.setVisibility(View.GONE);
            viewHolder.complain_btn.setVisibility(View.GONE);
            viewHolder.answer_btn.setVisibility(View.GONE);
        }else {
            viewHolder.comment_inlayout.setVisibility(View.VISIBLE);
            viewHolder.all_comment.setVisibility(View.VISIBLE);
            viewHolder.all_comment.setVisibility(View.VISIBLE);
            viewHolder.complain_btn.setVisibility(View.VISIBLE);
            viewHolder.answer_btn.setVisibility(View.VISIBLE);
            viewHolder.comment_count.setVisibility(View.VISIBLE);
            viewHolder.comment_count.setText(item.getComment_count()+"");
            viewHolder.all_comment.setText("Посмотреть все комментарии "+item.getComment_count());
            if (item.getComment_avatar().isEmpty()){
                Glide.with(activity).load(R.drawable.ic_person).into(viewHolder.comment_avatar);
            }else {
                Glide.with(activity).load(item.getComment_avatar()).into(viewHolder.comment_avatar);
            }

            if (item.getIs_i_liked_comment()!=1){
                viewHolder.comment_like_img.setImageResource(R.drawable.ic_dislike);
                viewHolder.comment_like_count.setTextColor(activity.getResources().getColor(R.color.chattext));
            }else {
                viewHolder.comment_like_img.setImageResource(R.drawable.ic_like);
                viewHolder.comment_like_count.setTextColor(activity.getResources().getColor(R.color.like_color));
            }

            viewHolder.comment_name.setText(item.getComment_user_name());
            viewHolder.comment_date.setText(item.getComment_date());
            String comment_text = item.getComment_text().replaceAll("\n","<br>");

            if ( item.getComment_text().length() > 170) {
                String newtext = comment_text.substring(0, 170);
                viewHolder.comment_desc.setText(Html.fromHtml(newtext + more));

            }else {
                viewHolder.comment_desc.setText(Html.fromHtml(comment_text));
            }

            if (item.getComment_like_count()!=0){
                viewHolder.comment_like_count.setText(item.getComment_like_count()+"");
            }else {
                viewHolder.comment_like_count.setText(" ");
            }


            if (item.getComment_is_has_file()==1 && !item.getComment_file_url().equals("")) {
                viewHolder.comment_desc.setVisibility(View.GONE);
                viewHolder.comment_image.setVisibility(View.VISIBLE);
//                viewHolder.comment_videolayout.setVisibility(View.GONE);
//                viewHolder.comment_audio_layout.setVisibility(View.GONE);
//                viewHolder.comment_file_layout.setVisibility(View.GONE);
                switch (item.getComment_file_format()) {
                    case "image":
                        viewHolder.comment_image.setVisibility(View.VISIBLE);
                        Glide.with(activity).load(item.getComment_file_url()).into(viewHolder.comment_image);
                        viewHolder.comment_image.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                item = feed_item.get(position);
                                Intent intent = new Intent(activity, ActivityZoomImage.class);
                                Bundle bundle = new Bundle();
                                bundle.putStringArrayList("url", new ArrayList<>(Arrays.asList(item.getComment_file_url())));
                                bundle.putInt("position", 0);
                                intent.putExtras(bundle);
                                activity.startActivity(intent);
                            }
                        });
                        break;
                }
            }else{
                    viewHolder.comment_desc.setVisibility(View.VISIBLE);
                    viewHolder.comment_image.setVisibility(View.GONE);
//                viewHolder.comment_videolayout.setVisibility(View.GONE);
//                viewHolder.comment_audio_layout.setVisibility(View.GONE);
//                viewHolder.comment_file_layout.setVisibility(View.GONE);
                }

            viewHolder.comment_like_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    AndroidNetworking.post(like+TOKEN).addBodyParameter("comment_id",item.getComment_id()+"")
                            .build().getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("ressssss",response+"**");

                            try {
                                if (response.getBoolean("status")){
                                    item.setComment_like_count(response.getInt("like_count"));
                                    if (item.getIs_i_liked_comment()==1){
                                        item.setIs_i_liked_comment(0);
                                        viewHolder.comment_like_img.setImageResource(R.drawable.ic_dislike);
                                        viewHolder.comment_like_count.setTextColor(activity.getResources().getColor(R.color.chattext));
                                    }else {
                                        item.setIs_i_liked_comment(1);
                                        viewHolder.comment_like_img.setImageResource(R.drawable.ic_like);
                                        viewHolder.comment_like_count.setTextColor(activity.getResources().getColor(R.color.like_color));
                                        viewHolder.comment_like_img.startAnimation(myAnim_comment);

                                    }

                                    notifyDataSetChanged();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.d("LIKE", response+"  ");
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("LIKEerr", anError+"  ");
                        }
                    });
                }
            });

        }

        if (item.getShare_count() == 0) {
            viewHolder.share_count.setText(" ");
        }else {
            viewHolder.share_count.setText(item.getShare_count()+"");
        }

        if (item.getView_count() == 0) {
            viewHolder.view_count.setText(" ");
        }else {
            viewHolder.view_count.setText(item.getView_count()+"");
        }



        if (item.getAvatar().isEmpty()){
            Glide.with(activity).load(R.drawable.ic_person).into(viewHolder.avatar);
        }else {
            Glide.with(activity).load(item.getAvatar()).into(viewHolder.avatar);
        }

        if (item.getIs_i_liked()==1){
            viewHolder.like_count.setTextColor(activity.getResources().getColor(R.color.like_color));
            viewHolder.like_img.setImageResource(R.drawable.ic_like);
        }else {
            viewHolder.like_count.setTextColor(activity.getResources().getColor(R.color.chattext));
            viewHolder.like_img.setImageResource(R.drawable.ic_dislike);
        }

        if (item.getIs_has_link()==1){
            viewHolder.link_layout.setVisibility(View.VISIBLE);
            viewHolder.link_title.setText(Html.fromHtml(item.getLink_title()));
            viewHolder.link_desc.setText(Html.fromHtml(item.getLink_description()));
            if (item.getLink_title().isEmpty() && item.getLink_description().isEmpty() && item.getLink_image().isEmpty()){
                viewHolder.link_layout.setVisibility(View.GONE);
            }else {
                viewHolder.link_layout.setVisibility(View.VISIBLE);
            }
            if (!item.getLink_image().isEmpty() && !item.getLink_image().equals(" ")) {
                viewHolder.link_image.setVisibility(View.VISIBLE);
                Picasso.get().load(item.getLink_image())
                        .placeholder(R.drawable.no_image)
                        .into(viewHolder.link_image);
            }else {
                viewHolder.link_image.setVisibility(View.GONE);
            }

        }else {
            viewHolder.link_layout.setVisibility(View.GONE);

        }

        if (item.getIs_has_file()==1){
            viewHolder.videolayout.setVisibility(View.GONE);
            viewHolder.file_layout.setVisibility(View.GONE);
            viewHolder.audio_layout.setVisibility(View.GONE);
            viewHolder.image.setVisibility(View.GONE);
            viewHolder.two_image.setVisibility(View.GONE);
            int image_count = item.getImage_count();


            if (image_count>2){
                viewHolder.more_image.setVisibility(View.VISIBLE);
                viewHolder.more_image.setText("+"+(image_count-2));
            }else {
                viewHolder.more_image.setVisibility(View.GONE);
            }
            for (int i=0;i<item.getFile_format().length;i++) {
                switch (item.getFile_format()[i]) {
                    case "image":
                        if (image_count>1){

                            viewHolder.two_image.setVisibility(View.VISIBLE);
                            viewHolder.image.setVisibility(View.GONE);
                            if (i==0){
                                Glide.with(activity).load(item.getFile_url()[i]).into(viewHolder.image1);
                            }else if (i==item.getFile_format().length-1){
                                Glide.with(activity).load(item.getFile_url()[i]).into(viewHolder.image2);
                            }
                        }else {
                            Glide.with(activity)
                                    .load(item.getFile_url()[i])
                                    .into(viewHolder.image);
                            viewHolder.two_image.setVisibility(View.GONE);
                            viewHolder.image.setVisibility(View.VISIBLE);
                        }


                        break;
                    case "audio":
                        viewHolder.audio_layout.setVisibility(View.VISIBLE);
                        viewHolder.audio_name.setText(item.getFile_name()[i]);
                        int second = item.getFile_time() % 60;
                        int minute = item.getFile_time() / 60;
                        String timeString = String.format("%02d:%02d", minute, second);
                        viewHolder.audio_time.setText(timeString);
                        if (position == play_position) {
                            viewHolder.audio_play.setImageResource(R.drawable.ic_pause);
                        } else {
                            viewHolder.audio_play.setImageResource(R.drawable.ic_play);
                        }
                        break;
                    case "video":
                        if (image_count>1){
                            Glide.with(activity)
                                    .asBitmap()
                                    .load(item.getFile_url()[i])
                                    .into(viewHolder.image2);
                            viewHolder.two_image.setVisibility(View.VISIBLE);
                            viewHolder.playvideo2.setVisibility(View.VISIBLE);
                            viewHolder.videoview.setVisibility(View.GONE);
                        }else {
                            viewHolder.videolayout.setVisibility(View.VISIBLE);

                            Glide.with(activity)
                                    .asBitmap()
                                    .load(item.getFile_url()[i])
                                    .into(new SimpleTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(Bitmap bitmap,
                                                                    Transition<? super Bitmap> transition) {
                                            int w = bitmap.getWidth();
                                            int h = bitmap.getHeight();
                                            if (h>w){
                                                viewHolder.videoview.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                            }else {
                                                viewHolder.videoview.setScaleType(viewHolder.videoview.getScaleType());
                                            }
                                            viewHolder.videoview.setImageBitmap(bitmap);
                                        }
                                    });
//                            Glide.with(activity)
//                                    .load(item.getFile_url()[i])
//                                    .into(viewHolder.videoview);
                            viewHolder.two_image.setVisibility(View.GONE);
                            viewHolder.playvideo2.setVisibility(View.GONE);
                            viewHolder.videoview.setVisibility(View.VISIBLE);
                        }


                        viewHolder.videoProgressbar.setVisibility(View.GONE);
                        break;

                    case "file":
                        viewHolder.file_layout.setVisibility(View.VISIBLE);


                        viewHolder.send_file_name.setText(item.getFile_name()[i]);
                        final String extensionRemoved = item.getFile_name()[i].split("\\.")[1];
                        viewHolder.send_file_format.setText(extensionRemoved);

                        switch (extensionRemoved){
                            case "pdf":
                                viewHolder.send_file_image.setColorFilter(activity.getResources().getColor(R.color.color_pdf));
                                break;
                            case "xls":
                            case "xlsx":
                                viewHolder.send_file_image.setColorFilter(activity.getResources().getColor(R.color.color_xls));
                                break;
                            case "doc":
                            case "docx":
                                viewHolder.send_file_image.setColorFilter(activity.getResources().getColor(R.color.color_doc));
                                break;
                            case "ppt":
                            case "pptx":
                                viewHolder.send_file_image.setColorFilter(activity.getResources().getColor(R.color.color_ppt));
                                break;
                            default:
                                viewHolder.send_file_image.setColorFilter(activity.getResources().getColor(R.color.color_txt));
                                break;
                        }

                        break;

                }
            }
        }else {
            viewHolder.videolayout.setVisibility(View.GONE);
            viewHolder.file_layout.setVisibility(View.GONE);
            viewHolder.audio_layout.setVisibility(View.GONE);
            viewHolder.image.setVisibility(View.GONE);
            viewHolder.two_image.setVisibility(View.GONE);
        }


        viewHolder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_position = position+1;
                item = feed_item.get(position);
                for (int i=0;i<item.getFile_format().length;i++) {
                    if (item.getFile_format()[i].equals("image")) {
                        if (item.getFile_url().length > 0) {
                            Intent intent = new Intent(activity, ActivityZoomImage.class);
                            Bundle bundle = new Bundle();
                            bundle.putStringArrayList("url", new ArrayList<>(Arrays.asList(item.getFile_url()[i])));
                            bundle.putInt("position", 0);
                            intent.putExtras(bundle);
                            activity.startActivity(intent);
                        }
                    }
                }
            }
        });




//        viewHolder.image1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                item = feed_item.get(position);
//                Intent intent = new Intent(activity, ActivityZoomImage.class);
//                Bundle bundle = new Bundle();
//                bundle.putStringArrayList("url", new ArrayList<>( viewHolder.img_file_url));
//                bundle.putInt("position", 0);
//                intent.putExtras(bundle);
//                activity.startActivity(intent);
//            }
//        });
//
//        viewHolder.image2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                item = feed_item.get(position);
//                Intent intent = new Intent(activity, ActivityZoomImage.class);
//                Bundle bundle = new Bundle();
//                bundle.putStringArrayList("url", new ArrayList<>( viewHolder.img_file_url));
//                bundle.putInt("position", 1);
//                intent.putExtras(bundle);
//                activity.startActivity(intent);
//            }
//        });

        viewHolder.file_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                for (int i=0;i<item.getFile_format().length;i++) {
                    if (item.getFile_format()[i].equals("file")) {
                        viewHolder.file_upload_progress.setVisibility(View.VISIBLE);
                        downloadFile(item.getFile_url()[i],item.getFile_name()[i],"file",viewHolder.file_upload_progress);
                    }
                }

            }
        });
        viewHolder.more_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_position = position+1;
                item = feed_item.get(position);
                item.setView_count(item.getView_count()+1);
                Intent intent = new Intent(activity, ActivitySingleNews.class);
                intent.putExtra("publication_id",item.getPublication_id());
                intent.putExtra("is_my",is_my);
                activity.startActivity(intent);

            }
        });

        viewHolder.comment_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_position = position+1;
                item = feed_item.get(position);
                item.setView_count(item.getView_count()+1);
                Intent intent = new Intent(activity, ActivitySingleNews.class);
                intent.putExtra("publication_id",item.getPublication_id());
                intent.putExtra("tocomment",true);
                intent.putExtra("is_my",is_my);
                activity.startActivity(intent);
            }
        });

        viewHolder.comment_inlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_position = position+1;
                item = feed_item.get(position);
                item.setView_count(item.getView_count()+1);
                Intent intent = new Intent(activity, ActivitySingleNews.class);
                intent.putExtra("publication_id",item.getPublication_id());
                intent.putExtra("tocomment",true);
                intent.putExtra("is_my",is_my);
                activity.startActivity(intent);
            }
        });

        viewHolder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_position = position+1;
                item = feed_item.get(position);
                activity.startActivity(new Intent(activity, ActivityMyProfile.class).putExtra("user_id",item.getUser_id()));


            }
        });

        viewHolder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_position = position+1;
                item = feed_item.get(position);
                activity.startActivity(new Intent(activity, ActivityMyProfile.class).putExtra("user_id",item.getUser_id()));
            }
        });

        viewHolder.like_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                item = feed_item.get(position);
                AndroidNetworking.post(like+TOKEN).addBodyParameter("publication_id",item.getPublication_id()+"")
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            if (response.getBoolean("status")){

                                item.setLike_count(response.getInt("like_count"));
                                if (item.getIs_i_liked()==1){
                                    item.setIs_i_liked(0);
                                    viewHolder.like_img.setImageResource(R.drawable.ic_dislike);
                                    viewHolder.like_count.setTextColor(activity.getResources().getColor(R.color.chattext));
                                }else {
                                    item.setIs_i_liked(1);
                                    viewHolder.like_img.setImageResource(R.drawable.ic_like);
                                    viewHolder.like_count.setTextColor(activity.getResources().getColor(R.color.like_color));
                                    viewHolder.like_img.startAnimation(myAnim);
                                }
                                notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("LIKE", response+"  ");
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("LIKEerr", anError+"  ");
                    }
                });
            }
        });

        viewHolder.audio_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                for (int i=0;i<item.getFile_format().length;i++) {
                    if (item.getFile_format()[i].equals("audio")){
                        if (play_position != position){


                            if (newsmediaPlayer != null && newsmediaPlayer.isPlaying()) {
                                newsmediaPlayer.stop();
                                newsmediaPlayer.release();
                            }

                            try {
                                newsmediaPlayer = new MediaPlayer();
                                newsmediaPlayer.setDataSource(item.getFile_url()[i]);
                                newsmediaPlayer.prepare();
                                newsmediaPlayer.start();
                                viewHolder.audio_play.setImageResource(R.drawable.ic_pause);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            play_position=position;
                            notifyDataSetChanged();
                        }else {
                            if (newsmediaPlayer != null) {

                                if (newsmediaPlayer.isPlaying()) {
                                    viewHolder.audio_play.setImageResource(R.drawable.ic_play);
                                    newsmediaPlayer.pause();
                                } else {
                                    viewHolder.audio_play.setImageResource(R.drawable.ic_pause);
                                    newsmediaPlayer.start();
                                }

                            } else {
                                play_position=position;

                                if (newsmediaPlayer.isPlaying()){
                                    newsmediaPlayer.stop();
                                    newsmediaPlayer.release();
                                }

                                try {
                                    newsmediaPlayer = new MediaPlayer();
                                    newsmediaPlayer.setDataSource(item.getFile_url()[i]);
                                    newsmediaPlayer.prepare();
                                    newsmediaPlayer.start();
                                    viewHolder.audio_play.setImageResource(R.drawable.ic_pause);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                notifyDataSetChanged();
                            }
                        }
                    }
                }


            }
        });

        viewHolder.playvideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_position = position+1;
                item = feed_item.get(position);
                for (int i=0;i<item.getFile_format().length;i++) {
                    if (item.getFile_format()[i].equals("video")) {
                        Intent intent = new Intent(activity, ActivityVideoPlayer.class);
                        intent.putExtra("file_url", item.getFile_url()[i]);
                        activity.startActivity(intent);
                    }
                }

            }
        });



        viewHolder.more_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] location = new int[2];
                viewHolder.more_menu.getLocationInWindow(location);

                Log.d("Button location","x = "+location[0]+"\ny = "+ location[1]);
                item = feed_item.get(position);
                Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.myprofile_more_menu_dialog);

                Window window = dialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.gravity = Gravity.TOP | Gravity.RIGHT;
                int pixels = (int) (200 * activity.getResources().getDisplayMetrics().density);
                wlp.width = pixels;
                wlp.x = 100;
                wlp.y = location[1];
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);

                TextView  change = (TextView) dialog.findViewById(R.id.change);
                TextView setting = (TextView) dialog.findViewById(R.id.setting);
                setting.setText("Скопировать");
                setting.setVisibility(View.GONE);
                TextView exit = (TextView) dialog.findViewById(R.id.exit);
                exit.setText("Удалить");
                dialog.show();
                exit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        item = feed_item.get(position);
                        AndroidNetworking.delete(publication_byId+"/"+item.getPublication_id()+"?token="+TOKEN)
                                .build().getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getBoolean("status")){
                                        feed_item.remove(position);
                                        notifyDataSetChanged();
                                    }else {
                                        Toasty.error(activity, response.getString("error"), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {

                            }
                        });
                        dialog.dismiss();
                    }
                });

                change.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        item = feed_item.get(position);
                        dialog.dismiss();
//                        try {
//                        ActivityMyProfile.from_news = true;
//                        Intent intent = new Intent(activity, ActivityChangeNews.class);
//                        intent.putExtra("item",item);
//
//                        JSONObject object = response_news.getJSONObject(position);
//
//                        intent.putExtra("response_news",object+"");
//                        activity.startActivity(intent);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    }
                });

            }
        });



        viewHolder.all_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_position=position+1;
                item = feed_item.get(position);
                item.setView_count(item.getView_count()+1);
                Intent intent = new Intent(activity, ActivitySingleNews.class);
                intent.putExtra("publication_id",item.getPublication_id());
                intent.putExtra("tocomment",true);
                intent.putExtra("is_my",is_my);
                activity.startActivity(intent);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                click_position=position+1;
                item = feed_item.get(position);
                item.setView_count(item.getView_count()+1);
                Intent intent = new Intent(activity, ActivitySingleNews.class);
                intent.putExtra("publication_id",item.getPublication_id());
                intent.putExtra("is_my",is_my);
                activity.startActivity(intent);
            }
        });


        return convertView;
    }

    private void downloadFile(String File_url, String File_name, String format, ProgressBar file_upload_progress){
        File myDir  = new File( Environment.getExternalStorageDirectory().toString() + "/.dalagram");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        File savefile = new File(myDir +"/Dalagram "+format);


        File file_url = new File(savefile+"/"+File_name);

        if (!file_url.exists()) {
            AndroidNetworking.download(File_url,savefile.getPath(),File_name)
                    .setTag("downloadTest")
                    .setPriority(Priority.HIGH)
                    .build()
                    .startDownload(new DownloadListener() {
                        @Override
                        public void onDownloadComplete() {

                            file_upload_progress.setVisibility(View.GONE);
                            Uri pdfUri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", file_url);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(pdfUri, "application/*");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            Intent chooser = Intent.createChooser(intent, "");

                            activity.startActivity(chooser);
                        }
                        @Override
                        public void onError(ANError anError) {
                            Toasty.error(activity, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                        }
                    });
        }else {
            file_upload_progress.setVisibility(View.GONE);
                Uri pdfUri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", file_url);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(pdfUri, "application/*");
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Intent chooser = Intent.createChooser(intent, "");

                activity.startActivity(chooser);

        }


    }




}
