package kz.bgpro.www.dalagram.activitys;

import android.app.Activity;
import android.app.SearchableInfo;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.channels.AsynchronousByteChannel;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.Realm.DialogDetail;
import kz.bgpro.www.dalagram.activitys.adapters.SearchChatListAdapter;
import kz.bgpro.www.dalagram.activitys.groups.ActivityNewGroup;
import kz.bgpro.www.dalagram.fragment.adapters.ChatListAdapter;
import kz.bgpro.www.dalagram.models.FeedItem;

import static kz.bgpro.www.dalagram.MainActivity.MY_USER_ID;
import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.activitys.ActivityChatMessage.PAGE_m;
import static kz.bgpro.www.dalagram.activitys.ActivityChatMessage.PAGE_p;
import static kz.bgpro.www.dalagram.activitys.ActivityChatMessage.URL;
import static kz.bgpro.www.dalagram.utils.MessageType.*;
import static kz.bgpro.www.dalagram.utils.MyConstants.chat;
import static kz.bgpro.www.dalagram.utils.MyConstants.chat_detail;
import static kz.bgpro.www.dalagram.utils.MyConstants.file;
import static kz.bgpro.www.dalagram.utils.MyConstants.page;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

public class ActivitySearchbyChatDetail extends Activity  implements SwipeRefreshLayout.OnRefreshListener{


   ArrayList<FeedItem> feedItem;

    @BindView(R.id.list)
    ListView listView;

    @BindView(R.id.ed_search)
    EditText search;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    SearchChatListAdapter chatListAdapter;

    int user_id;
    String dialog_id,WHO,URL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_chat);
        ButterKnife.bind(this);

        swipe.setOnRefreshListener(this);

        user_id = getIntent().getIntExtra("user_id",0);
        dialog_id = getIntent().getStringExtra("dialog_id");
        WHO = getIntent().getStringExtra("who");
        URL = getIntent().getStringExtra("url");


        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0){
                    GetDataFromJSON(search.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        feedItem = new ArrayList<FeedItem>();



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (feedItem.size()>0){

                    FeedItem item = feedItem.get(position);
                    int chat_id = item.getChat_id();

                    Log.d("AAAAA",page+TOKEN+"&chat_id="+chat_id+"&per_page="+ ActivityChatMessage.PER_PAGE+"***");
                    AndroidNetworking.get(page+TOKEN+"&chat_id="+chat_id+"&per_page="+ ActivityChatMessage.PER_PAGE).build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d("PPPP",response+"");
                                    try {
                                        if(response.has("page")){
                                            FeedItem item = feedItem.get(position);
                                            Intent intent = new Intent();
                                            intent.putExtra("page_m",response.getInt("page"))
                                                    .putExtra("page_p",response.getInt("page2"))
                                                    .putExtra("chat_id",item.getChat_id());
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onError(ANError anError) {
                                    Toasty.error(ActivitySearchbyChatDetail.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                                }
                            });
                }
            }
        });

    }

    @OnClick(R.id.back)
    void back(){
        finish();
    }

    @OnClick(R.id.clear)
    void clear(){
        search.setText(null);
        feedItem.clear();
        chatListAdapter.notifyDataSetChanged();
    }
    @Override
    public void onRefresh() {
        swipe.setRefreshing(false);
    }

    public void GetDataFromJSON(String search_txt) {


        swipe.setRefreshing(true);

        Log.d("chat_list",URL+"100&search="+search_txt);
        AndroidNetworking.get(URL+"100&search="+search_txt)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response",response+"**");

                Log.d("messageList",response+"*");
                try {
                    if(response.getBoolean("status")){

                        feedItem.clear();
                        JSONArray list = response.getJSONArray("data");

                        int length = list.length();
                        for (int i = 0; i <length; i++) {
                            JSONObject jsonObject = (JSONObject) list.get(i);



                            JSONObject sender = jsonObject.getJSONObject("sender");
                            JSONArray file_list = jsonObject.getJSONArray("file_list");
                            int f_l = file_list.length();
                            String[] sfile_url = new String[f_l];
                            String[] sfile_format= new String[f_l];
                            String[] sfile_name= new String[f_l];
                            int file_time = 0;
                            for (int f = 0; f < f_l; f++) {
                                JSONObject file = (JSONObject) file_list.get(f);
                                file_time = file.getInt("file_time");
                                sfile_url[f]=file.getString("file_url");
                                sfile_format[f]=file.getString("file_format");
                                sfile_name[f]=file.getString("file_name");
                            }
                            int answer_user_id = 0;
                            String answer_file_url = "";
                            String answer_file_format = "";
                            String answer_file_name = "";
                            int answer_file_time = 0;
                            int answer_is_has_file = 0;
                            String answer_name = " ", answer_text = " ";
                            if (jsonObject.getInt("answer_chat_id") != 0) {
                                JSONObject answer_chat = jsonObject.getJSONObject("answer_chat");
                                answer_name = NurString(answer_chat, "nickname");
                                answer_is_has_file = answer_chat.getInt("is_has_file");
                                answer_user_id = answer_chat.getInt("user_id");
                                answer_text = answer_chat.getString("chat_text");
                                if (answer_is_has_file != 0) {
                                    JSONArray answer_file_list = answer_chat.getJSONArray("file_list");
                                    if (answer_file_list.length() > 0) {
                                        JSONObject answer_file = (JSONObject) answer_file_list.get(0);
                                        answer_file_url = answer_file.getString("file_url");
                                        answer_file_format = answer_file.getString("file_format");
                                        answer_file_name = answer_file.getString("file_name");
                                        answer_file_time = answer_file.getInt("file_time");
                                    }
                                }
                            }

                            int is_contact = NurInt(jsonObject, "is_contact");
                            int is_exist=0,contact_user_id=0;
                            String contact_name = "", contact_phone = "";
                            if (is_contact == 1) {
                                JSONObject contact = jsonObject.getJSONObject("contact");
                                contact_name = NurString(contact, "contact_name");
                                contact_phone = NurString(contact, "contact_phone");
                                is_exist = NurInt(contact, "is_exist");
                                contact_user_id = NurInt(contact, "user_id");
                            }


                            FeedItem item = new FeedItem();
                            item.setChat_id(NurInt(jsonObject, "chat_id"));
                            item.setIs_read(NurInt(jsonObject, "is_read"));
                            item.setIs_has_file(NurInt(jsonObject, "is_has_file"));
                            item.setUser_id(NurInt(sender, "user_id"));
                            item.setFile_time(file_time);
                            item.setAnswer_chat_id(NurInt(jsonObject, "answer_chat_id"));
                            item.setAnswer_user_id(answer_user_id);
                            item.setChat_kind(NurString(jsonObject, "chat_kind"));
                            item.setAction_name(NurString(jsonObject, "action_name"));
                            item.setChat_text(NurString(jsonObject, "chat_text"));
                            item.setChat_time(NurString(jsonObject, "chat_time"));
                            item.setChat_date(NurString(jsonObject, "chat_date"));
                            item.setName(NurString(sender, "nickname"));
                            item.setAvatar(NurString(sender, "avatar"));
                            item.setPhone(NurString(sender, "phone"));
                            item.setAnswer_name(answer_name);
                            item.setAnswer_text(answer_text);

                            item.setAnswer_file_url(answer_file_url);
                            item.setAnswer_file_format(answer_file_format);
                            item.setAnswer_file_name(answer_file_name);
                            item.setAnswer_is_has_file(answer_is_has_file);
                            item.setAnswer_file_time(answer_file_time);


                            item.setFile_url(sfile_url);
                            item.setFile_format(sfile_format);
                            item.setFile_name(sfile_name);

                            item.setSave_storage(true);
                            item.setIs_upload(false);
                            item.setIs_resend(NurInt(jsonObject, "is_resend"));
                            item.setView_count(NurInt(jsonObject, "view_count"));

                            item.setIs_contact(is_contact);
                            item.setContact_name(contact_name);
                            item.setContact_phone(contact_phone);
                            item.setIs_playaudio(true);
                            item.setIs_exist(is_exist);
                            item.setContact_user_id(contact_user_id);
                            int is_sticker = NurInt(jsonObject, "is_sticker");
                            String sticker_image="";
                            if (is_sticker==1){
                                JSONObject sticker = jsonObject.getJSONObject("sticker");
                                sticker_image =  NurString(sticker, "sticker_image");
                            }


                            item.setIs_sticker(is_sticker);
                            item.setSticker_image(sticker_image);



                            if (NurString(jsonObject, "chat_kind").equals("action")){
                                item.setMessage_type(ACTION_MESSAGE);
                            }else if (is_contact==1){
                                item.setMessage_type(CONTACT_MESSAGE);
                            }else if (is_sticker==1){
                                item.setMessage_type(STICKER_MESSAGE);
                            }else if (NurInt(jsonObject, "is_has_file")!=1){
                                item.setMessage_type(TEXT_MESSAGE);
                            }


                            feedItem.add(item);
                        }
                        swipe.setRefreshing(false);
                        chatListAdapter = new SearchChatListAdapter(ActivitySearchbyChatDetail.this,feedItem, user_id,dialog_id,WHO,search_txt);
                        listView.setAdapter(chatListAdapter);
                    }else {
                        Toasty.error(ActivitySearchbyChatDetail.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {

                Toasty.error(ActivitySearchbyChatDetail.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();


            }
        });

    }




}
