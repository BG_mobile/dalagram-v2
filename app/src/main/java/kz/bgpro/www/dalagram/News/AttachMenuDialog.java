package kz.bgpro.www.dalagram.News;


import android.app.Dialog;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import kz.bgpro.www.dalagram.R;

public class AttachMenuDialog {


    ActivitySingleNews activitySingleNews;

    public void attachView( ActivitySingleNews activityChatMessage) {
        this.activitySingleNews = activityChatMessage;

    }

    public void showDialog(){
        final Dialog dialog = new Dialog(activitySingleNews);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.attach_menu_dialog);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        int pixels = (int) (180 * activitySingleNews.getResources().getDisplayMetrics().density);
        wlp.width = pixels;
        wlp.x = 100;
        wlp.y = 120;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        final TextView attach_photo = (TextView) dialog.findViewById(R.id.attach_photo);
        final TextView attach_video = (TextView) dialog.findViewById(R.id.attach_video);
        final TextView attach_music = (TextView) dialog.findViewById(R.id.attach_music);
        final TextView attach_file = (TextView) dialog.findViewById(R.id.attach_file);

        dialog.show();
        attach_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activitySingleNews.SendFile(1);

            }
        });

        attach_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activitySingleNews.SendFile(2);

            }
        });

        attach_music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activitySingleNews.SendFile(3);

            }
        });

        attach_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activitySingleNews.SendFile(4);

            }
        });

    }
}