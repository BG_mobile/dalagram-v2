package kz.bgpro.www.dalagram.utils;


import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.*;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by nurbaqyt on 17.05.2018.
 */

public class MyConstants {

    public static final String BASE_URL = "https://api.dalagram.com/api/";

    public static final String PREFS_NAME = "nurik";

    public static final String CHAT_SERVER_URL = "https://dalagram.com:8008";

    public static final String login_password = BASE_URL+"auth/login/password";
    public static final String login = BASE_URL+"auth/login";
    public static final String reset = BASE_URL+"auth/reset";
    public static final String confirm = BASE_URL+"auth/confirm";
    public static final String profile = BASE_URL+"profile?token=";
    public static final String my_avatar = BASE_URL+"profile/avatar?token=";
    public static final String background_ava = BASE_URL+"profile/background?token=";
    public static final String chat = BASE_URL+"chat?token=";
    public static final String chat_detail = BASE_URL+"chat/detail?token=";
    public static final String contact = BASE_URL+"contact?token=";
    public static final String server_image = BASE_URL+"image?token=";
    public static final String file = BASE_URL+"chat/file?token=";
    public static final String push = BASE_URL+"push?token=";
    public static final String mute = BASE_URL+"chat/mute?token=";
    public static final String channel = BASE_URL+"channel";
    public static final String channel_adduser = BASE_URL+"channel/user/";
    public static final String new_channel = BASE_URL+"channel?token=";
    public static final String channel_avatar = BASE_URL+"channel/avatar?token=";
    public static final String group = BASE_URL+"group";
    public static final String group_adduser = BASE_URL+"group/user/";
    public static final String group_avatar = BASE_URL+"group/avatar?token=";
    public static final String media = BASE_URL+"chat/media?token=";
    public static final String block = BASE_URL+"chat/block?token=";

    public static final String friend = BASE_URL+"follower/friend?token=";
    public static final String bookmark = BASE_URL+"chat/bookmark?token=";
    public static final String resend = BASE_URL+"chat/resend?token=";
    public static final String page = BASE_URL+"chat/page?token=";

    public static final String publication = BASE_URL+"publication?token=";
    public static final String publication_byId = BASE_URL+"publication";
    public static final String gallery = BASE_URL+"publication/gallery?token=";
    public static final String notification = BASE_URL+"notification?token=";
    public static final String follower = BASE_URL+"follower?token=";
    public static final String follower_send = BASE_URL+"follower/send?token=";
    public static final String follower_in = BASE_URL+"follower/in?token=";
    public static final String follower_friend = BASE_URL+"follower/friend?token=";
    public static final String follower_accept = BASE_URL+"follower/accept?token=";
    public static final String follower_reject = BASE_URL+"follower/reject?token=";

    public static final String comment = BASE_URL+"comment?token=";
    public static final String like = BASE_URL+"like?token=";
    public static final String access = BASE_URL+"access?token=";
    public static final String sticker_url = BASE_URL+"sticker?token=";
    public static final String users = BASE_URL+"users?token=";








    public static String compressImage(String imageUri, Context mcontext) {

        String filePath = getRealPathFromURI(imageUri,mcontext);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;
            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public static String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), ".unknown/.unknown");

        if (!file.exists()){
            file.mkdirs();
        }

        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }


    public static void deleteFileName(String filename){
        File file = new File(Environment.getExternalStorageDirectory().getPath(), ".unknown/.unknown/"+filename);

        if (file.exists()){
            file.delete();
        }
    }

    private static String getRealPathFromURI(String contentURI, Context mc) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = mc.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }






}
