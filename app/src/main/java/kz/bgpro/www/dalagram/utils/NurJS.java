package kz.bgpro.www.dalagram.utils;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by nurbaqyt on 21.07.2018.
 */

public class NurJS {



    public static String NurString(JSONObject jsonObject, String name) throws JSONException {
        String result = "";
        if(jsonObject.has(name) && !jsonObject.isNull(name) && !jsonObject.getString(name).equals("null"))
            result = jsonObject.getString(name);
        return result;

    }
    public static int NurInt(JSONObject jsonObject, String name) throws JSONException {
        int result = 0;
        if(jsonObject.has(name) && !jsonObject.isNull(name))
            result = jsonObject.getInt(name);

        return result;

    }
}


