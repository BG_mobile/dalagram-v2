package kz.bgpro.www.dalagram.Auth;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.MainActivity;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.utils.NurJS;

import static kz.bgpro.www.dalagram.utils.MyConstants.PREFS_NAME;
import static kz.bgpro.www.dalagram.utils.MyConstants.login_password;


public class LoginActivity extends Activity {

    @BindView(R.id.number)
    EditText number;

    @BindView(R.id.password)
    EditText password;

    @BindView(R.id.progress)
    ProgressBar progress;

    SharedPreferences settings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        CheckPermission();

        number.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s)
            {
                String text = number.getText().toString();
                int  textLength = number.getText().length();
                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return;
                if (textLength == 4)
                {
                    if (!text.contains(" "))
                    {
                        number.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                        number.setSelection(number.getText().length());
                    }
                }
                else if (textLength == 8)
                {
                    number.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                    number.setSelection(number.getText().length());
                }
            }
        });


    }


    @OnClick(R.id.login_btn)
    void login_btn(){
        String phone = number.getText().toString();
        phone = phone.replace(" ","");

        String Password = password.getText().toString();
        if (!TextUtils.isEmpty(phone) && !TextUtils.isEmpty(Password)){
            if (phone.length()!=10){
                progress.setVisibility(View.GONE);
                Toasty.error(this, "Введите корректный номер телефона", Toast.LENGTH_SHORT).show();
            }else {
                progress.setVisibility(View.VISIBLE);
                AndroidNetworking.post(login_password)
                        .addBodyParameter("phone", phone)
                        .addBodyParameter("password", Password)
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("response login",response+"* ");
                        try {
                            if (response.getBoolean("status")) {

                                JSONObject data = response.getJSONObject("data");
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putInt("user_id", data.getInt("user_id"));
                                editor.putString("token", data.getString("token"));
                                Log.d("111token", data.getString("token"));
                                editor.putString("avatar", data.has("avatar") ? data.getString("avatar") : "null");
                                editor.putString("user_name", data.has("user_name") ? data.getString("user_name") : "");
                                editor.putString("phone", NurJS.NurString(data, "phone"));
                                editor.commit();

                                startActivity(new Intent(LoginActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

                                finish();
                            } else {
                                progress.setVisibility(View.GONE);
                                Toasty.error(LoginActivity.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toasty.error(LoginActivity.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }else {
            progress.setVisibility(View.GONE);
            Toasty.error(this, "заполните все поля", Toast.LENGTH_SHORT).show();
        }

    }
    @OnClick(R.id.reset)
    void reset(){
        startActivity( new Intent(LoginActivity.this,ResetActivity.class));
    }


    @OnClick(R.id.register_btn)
    void register_btn(){
        startActivity( new Intent(LoginActivity.this,ActivityConfirmPhone.class));
    }

    private void CheckPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {/* ... */}
            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
        }).check();
    }
}
