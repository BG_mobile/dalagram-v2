package kz.bgpro.www.dalagram.activitys;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.devlomi.record_view.OnBasketAnimationEnd;
import com.devlomi.record_view.OnRecordClickListener;
import com.devlomi.record_view.OnRecordListener;
import com.devlomi.record_view.RecordButton;
import com.devlomi.record_view.RecordView;
import com.fxn.pix.Pix;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.github.pavlospt.roundedletterview.RoundedLetterView;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;
import com.vanniktech.emoji.EmojiTextView;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.NormalFile;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;

import kz.bgpro.www.dalagram.Realm.Stickers;
import kz.bgpro.www.dalagram.activitys.adapters.StickersAdapter;
import kz.bgpro.www.dalagram.activitys.profile.ActivityChangeMyProfile;
import kz.bgpro.www.dalagram.utils.MyConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmResults;
import kz.bgpro.www.dalagram.ChatApplication;
import kz.bgpro.www.dalagram.MainActivity;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.Realm.DialogDetail;
import kz.bgpro.www.dalagram.activitys.adapters.ChatAdapter;
import kz.bgpro.www.dalagram.activitys.channel.ActivityChannelProfile;
import kz.bgpro.www.dalagram.activitys.profile.ActivityChatProfile;
import kz.bgpro.www.dalagram.activitys.groups.ActivityGroupProfile;
import kz.bgpro.www.dalagram.models.FeedItem;
import kz.bgpro.www.dalagram.utils.MyPicassoEngine;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

import static io.realm.internal.network.NetworkStateReceiver.isOnline;
import static kz.bgpro.www.dalagram.MainActivity.MY_USER_ID;
import static kz.bgpro.www.dalagram.MainActivity.MY_USER_NAME;
import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MessageType.*;
import static kz.bgpro.www.dalagram.utils.MyConstants.*;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

/**
 * Created by nurbaqyt on 10.08.2018.
 */

public class ActivityChatMessage extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private Socket mSocket;
    String TAG = "Nurikkkkk";
    private Boolean isConnected = false;

    int USER_ID,GROUP_ID,CHANNEL_ID;
    public int answer_chat_id=0,answer_position=0,answer_user_id;
    public static int Is_mute,i_block_partner;
    String Dialog_id,Name,Avatar,answer_nickname,answer_chat_txt;
    boolean is_ava ,is_typing=false;

    @BindView(R.id.connect_txt) TextView connect_txt;

    @BindView(R.id.ripple)
    MaterialRippleLayout ripple;

    @BindView(R.id.avatar_tv)
    RoundedLetterView avatar_tv;

    @BindView(R.id.avatar)
    CircleImageView avatar;

    @BindView(R.id.name) TextView name;

    @BindView(R.id.is_mute) ImageView is_mute;

    @BindView(R.id.chat_text) TextView chat_text;

    @BindView(R.id.record_view)
    RecordView recordView ;

    @BindView(R.id.record_button)
    RecordButton recordButton ;

    @BindView(R.id.file_layout) LinearLayout file_layout ;

    @BindView(R.id.insert_layout) FrameLayout insert_layout ;

    @BindView(R.id.rootView) ViewGroup rootView;


    @BindView(R.id.emojiEditText)
    EmojiEditText emojiEditText;

    @BindView(R.id.attach_btn) ImageButton attach_btn ;

    @BindView(R.id.stickers) GridView stickers_grid ;

    @BindView(R.id.sticker_btn) ImageButton sticker_btn ;
    @BindView(R.id.sticker_layout) LinearLayout sticker_layout ;

    @BindView(R.id.input_layout) LinearLayout input_layout ;

    @BindView(R.id.answer_layout) RelativeLayout answer_layout;

    @BindView(R.id.answer_name) TextView answer_name;

    @BindView(R.id.answer_chat_text) EmojiTextView answer_chat_text;

    @BindView(R.id.answer_image) ImageView answer_image;

    @BindView(R.id.listView)
    StickyListHeadersListView listView;

    @BindView(R.id.emotion_btn) ImageButton emotion_btn;

    @BindView(R.id.notification_btn) TextView notification_btn;

    @BindView(R.id.blocked) TextView blocked;


    View footerview;
    FrameLayout video_upload_layout;
    ImageView upload_view;
    TextView upload_byte,upload_byte_audio,upload_byte_file,filename;

    FrameLayout image_upload_layout;
    LinearLayout image_upload_layout2;
    ImageView single_image,image1,image2,image3,image4,image5;

    FrameLayout audiolayout;

    LinearLayout filelayout;

    ImageButton cancel_upload;

    private ChatAdapter adapter;

    ArrayList<FeedItem> feedItem = new ArrayList<FeedItem>();
    ArrayList<FeedItem> feedItem_stickers = new ArrayList<FeedItem>();

    int RequestCodeForGallerey = 700;
    String AudioSavePathInDevice = null;
    MediaRecorder mediaRecorder ;
    Random random ;
    String RandomAudioFileName = "abcdefghigklmnopqrstuvwxyz";

    String []file_name;
    String []file_url;
    String []file_format;

    Handler handler = new Handler();
    Handler handler_online = new Handler();

    int REQUEST_TAKE_GALLERY_VIDEO = 713;
    int REQUEST_TAKE_FILE = 518;
    int REQUEST_AUDIO_FILE = 320;
    int REQUEST_SEARCH = 888;
    EmojiPopup emojiPopup;
    long file_time=0;
    InputMethodManager imm ;


    public ArrayList<Integer> selectedIds = new ArrayList<Integer>();

    private ArrayList<MediaFile> mediaFiles = new ArrayList<>();

   @BindView(R.id.action_bar) RelativeLayout action_bar;
    @BindView(R.id.selected_bar) RelativeLayout selected_bar;
    @BindView(R.id.selected_count) TextView selected_count;
    @BindView(R.id.selected_answer) ImageButton selected_answer;



   public static  MediaPlayer mediaPlayer;
    Realm realm;

    int is_contact=0;
    String contact_name,contact_phone;


    String WHO;
    public static String URL;
    int is_admin;

    String user_response;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    public static int PAGE_p=1;
    public static int PAGE_m=1;
    public static int PER_PAGE=30;

    String last_visible;
    int total_page;
    int minus_visible_page;
    boolean sticker_visble = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_message);
        ButterKnife.bind(this);
        swipe.setOnRefreshListener(this);
        realm = Realm.getDefaultInstance();
        SocketConnect();
        PAGE_p=1;
        PAGE_m=1;

        sticker_visble = false;
        sticker_layout.setVisibility(View.GONE);

        listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        listView.setStackFromBottom(true);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                Log.d("MMMMM",scrollState+"****"+listView.getLastVisiblePosition()+"***"+feedItem.size());


                if(scrollState == SCROLL_STATE_IDLE && listView.getLastVisiblePosition() == feedItem.size()-1 ){


                    Log.d("MMMMM",listView.getLastVisiblePosition()+"***"+feedItem.size());

                   if (PAGE_m>1){
                       minus_visible_page = listView.getFirstVisiblePosition();
                       PAGE_m--;
                       GetDataFromJSONMinus(URL);
                       Log.d("MMMMM","minus"+minus_visible_page);
                   }

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //we don't need this method, so we leave it empty
            }
        });
        action_bar.setVisibility(View.VISIBLE);
        selected_bar.setVisibility(View.GONE);
        random = new Random();
        CheckPermission();
        recordView.setLessThanSecondAllowed(false);
        recordView.setSoundEnabled(false);
        recordButton.setRecordView(recordView);

        emojiPopup = EmojiPopup.Builder.fromRootView(rootView).build(emojiEditText);
        emojiPopup = EmojiPopup.Builder.fromRootView(rootView)
                .setOnEmojiBackspaceClickListener(ignore -> Log.d(TAG, "Clicked on Backspace"))
                .setOnEmojiClickListener((ignore, ignore2) -> Log.d(TAG, "Clicked on emoji"))
                .setOnEmojiPopupShownListener(() -> emotion_btn.setImageResource(R.drawable.ic_keyboard))
                .setOnSoftKeyboardOpenListener(ignore -> Log.d(TAG, "Opened soft keyboard"))
                .setOnEmojiPopupDismissListener(() -> emotion_btn.setImageResource(R.drawable.ic_emoticon))
                .setOnSoftKeyboardCloseListener(() -> Log.d(TAG, "Closed soft keyboard"))
                .build(emojiEditText);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        footerview = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.footer_upload_video, null, false);
        initFooterView();


        Intent i  = getIntent();
        WHO = i.getStringExtra("who");

            is_admin = i.getIntExtra("is_admin", 0);
            USER_ID = i.getIntExtra("user_id", 0);
            GROUP_ID = i.getIntExtra("group_id", 0);
            CHANNEL_ID = i.getIntExtra("channel_id", 0);
            Log.d("channel_id", CHANNEL_ID + "  *");
            Is_mute = i.getIntExtra("is_mute", 0);
            Dialog_id = i.getStringExtra("dialog_id");
            Avatar = i.getStringExtra("avatar");
            is_ava = i.getBooleanExtra("is_ava", false);
            Name = i.getStringExtra("name");
        i_block_partner = i.getIntExtra("i_block_partner",0);


        if (Is_mute==0){
            is_mute.setVisibility(View.GONE);
            notification_btn.setText("Отключить уведомление");
        }else {
            is_mute.setVisibility(View.VISIBLE);
            notification_btn.setText("Включить уведомление");
        }


        switch (WHO){
            case "chat":
                last_visible = i.getStringExtra("chat_text");
                chat_text.setText(last_visible);
                URL = chat_detail+TOKEN+"&recipient_user_id="+USER_ID+"&per_page=";

                if (i.getIntExtra("partner_block_me",0)==0){
                    blocked.setVisibility(View.GONE);
                    insert_layout.setVisibility(View.VISIBLE);
                    notification_btn.setVisibility(View.GONE);
                    file_layout.setVisibility(View.GONE);
                }else {
                    blocked.setVisibility(View.VISIBLE);
                    notification_btn.setVisibility(View.GONE);
                    insert_layout.setVisibility(View.GONE);
                    file_layout.setVisibility(View.GONE);
                }


                break;
            case "channel":
                chat_text.setText("участник");
                URL = chat_detail+TOKEN+"&channel_id="+CHANNEL_ID+"&per_page=";
                if(is_admin==1){
                    insert_layout.setVisibility(View.VISIBLE);
                    notification_btn.setVisibility(View.GONE);
                    file_layout.setVisibility(View.GONE);
                }else {
                    insert_layout.setVisibility(View.GONE);
                    notification_btn.setVisibility(View.VISIBLE);
                    file_layout.setVisibility(View.GONE);
                }
                GetGroupUsers(channel+"/user/"+CHANNEL_ID+"?token="+TOKEN);
                break;
            case "group":
                chat_text.setText("группа");
                URL = chat_detail+TOKEN+"&group_id="+GROUP_ID+"&per_page=";
                insert_layout.setVisibility(View.VISIBLE);
                file_layout.setVisibility(View.GONE);
                GetGroupUsers(group+"/user/"+GROUP_ID+"?token="+TOKEN);
                break;


        }
        if(is_ava){
            avatar_tv.setVisibility(View.GONE);
            avatar.setVisibility(View.VISIBLE);
            Glide.with(this)
                    .load(Avatar)
                    .into(avatar);
        }else {
            avatar_tv.setVisibility(View.VISIBLE);
            avatar.setVisibility(View.GONE);
            avatar_tv.setTitleText(Avatar);
        }

        name.setText(Name);

        Log.d("USER_ID",USER_ID+"*");
        recordButton.setImageResource(R.drawable.ic_mic);



        if(isConnected) {
            ripple.setVisibility(View.GONE);
            connect_txt.setVisibility(View.VISIBLE);
        }else {
            ripple.setVisibility(View.VISIBLE);
            connect_txt.setVisibility(View.GONE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("dialog_id", Dialog_id);
                jsonObject.put("sender_id", MY_USER_ID);
                jsonObject.put("type", "read");
                jsonObject.put("os", "android");
                mSocket.emit("read", jsonObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }



        adapter = new ChatAdapter(ActivityChatMessage.this,feedItem,USER_ID,Dialog_id,WHO);
        listView.setAdapter(adapter);

        emojiEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.d("aaaaa",s.length()+"#");
                if(s.length()==0){
                    recordButton.setImageResource(R.drawable.ic_mic);
                    recordButton.setListenForRecord(true);
                    attach_btn.setVisibility(View.VISIBLE);
                    sticker_btn.setVisibility(View.VISIBLE);

                }else {
                    recordButton.setImageResource(R.drawable.ic_send);
                    recordButton.setListenForRecord(false);
                    attach_btn.setVisibility(View.GONE);
                    sticker_btn.setVisibility(View.GONE);

                }

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("sender_id",MY_USER_ID);
                    jsonObject.put("dialog_id",Dialog_id);
                    jsonObject.put("type", "typing");
                    jsonObject.put("os", "android");
                    mSocket.emit("typing",jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        recordView.setOnRecordListener(new OnRecordListener() {
            @Override
            public void onStart() {
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    v.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    v.vibrate(50);
                }

                file_time = 0;
                //Start Recording..
                Log.d("RecordView", "onStart");
                input_layout.setVisibility(View.GONE);
                attach_btn.setVisibility(View.GONE);
                sticker_btn.setVisibility(View.GONE);


                AudioSavePathInDevice =
                        Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                                CreateRandomAudioFileName(5) + "_AudioRecordingvoice.mp3";

                MediaRecorderReady();

                try {


                    mediaRecorder.prepare();

                    mediaRecorder.start();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onCancel() {
                file_time = 0;
                //On Swipe To Cancel
                Log.d("RecordView", "onCancel");

                if (mediaRecorder != null) {
                    mediaRecorder.stop();
                }
            }

            @Override
            public void onFinish(long recordTime) {
                //Stop Recording..
                file_time = TimeUnit.MILLISECONDS.toSeconds(recordTime);
                if (mediaRecorder != null) {
                    mediaRecorder.stop();
                }
                input_layout.setVisibility(View.VISIBLE);
                attach_btn.setVisibility(View.VISIBLE);
                sticker_btn.setVisibility(View.VISIBLE);


                File f = new File(AudioSavePathInDevice);

                file_name   = new String[1];
                file_url    = new String[1];
                file_format = new String[1];
                SendAudioToServer(f);


            }

            @Override
            public void onLessThanSecond() {
                file_time = 0;
                //When the record time is less than One Second
                Log.d("RecordView", "onLessThanSecond");
                input_layout.setVisibility(View.VISIBLE);
                attach_btn.setVisibility(View.VISIBLE);
                sticker_btn.setVisibility(View.VISIBLE);

            }
        });
        recordButton.setOnRecordClickListener(new OnRecordClickListener() {
            @Override
            public void onClick(View v) {
                SendMessage(0);
            }
        });

        recordView.setOnBasketAnimationEndListener(new OnBasketAnimationEnd() {
            @Override
            public void onAnimationEnd() {
                Log.d("RecordView", "Basket Animation Finished");
                input_layout.setVisibility(View.VISIBLE);
                attach_btn.setVisibility(View.VISIBLE);
                sticker_btn.setVisibility(View.VISIBLE);
            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FeedItem item = feedItem.get(position);

                    Log.d("selectedIds", selectedIds.size() + "*****");
                    if (selectedIds.size() > 0) {
                        Integer pos = new Integer(position);
                        if (selectedIds.contains(pos)) {
                            selectedIds.remove(pos);
                        } else {
                            selectedIds.add(pos);
                        }
                        adapter.toggleSelected(pos);
                        selected_count.setText(selectedIds.size() + " ");
                        adapter.notifyDataSetChanged();
                        if (selectedIds.size()>1){
                            selected_answer.setVisibility(View.GONE);
                        }
                    } else {
                        action_bar.setVisibility(View.VISIBLE);
                        selected_bar.setVisibility(View.GONE);

                        MessageInfoDialog alert = new MessageInfoDialog();
                        alert.attachView(position, ActivityChatMessage.this);
                       String user_name = Name;
                        answer_position = position;
                        if (item.getUser_id()==MY_USER_ID){
                            user_name ="Вы";
                        }
                        alert.showDialog(ActivityChatMessage.this, item, answer_layout, answer_name, user_name, answer_chat_text,answer_image);

                    }
                if (selectedIds.size() == 0) {
                    action_bar.setVisibility(View.VISIBLE);
                    selected_bar.setVisibility(View.GONE);
                }
            }
        });


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("CCCCCC","long"+position);
                FeedItem item = feedItem.get(position);
                if (!item.getChat_kind().equals("action")) {
                    Integer pos = new Integer(position);
                    if (selectedIds.contains(pos)) {
                        selectedIds.remove(pos);
                    } else {
                        selectedIds.add(pos);
                    }

                    action_bar.setVisibility(View.GONE);
                    selected_bar.setVisibility(View.VISIBLE);

                    adapter.toggleSelected(pos);
                    Log.d("selectedIdsssss111", selectedIds.size() + "*****");

                    adapter.notifyDataSetChanged();
                    selected_count.setText(selectedIds.size() + " ");

                    if (selectedIds.size() == 0) {
                        action_bar.setVisibility(View.VISIBLE);
                        selected_bar.setVisibility(View.GONE);
                    }else if (selectedIds.size()==1){
                        selected_answer.setVisibility(View.VISIBLE);
                    }else if (selectedIds.size()>1){
                        selected_answer.setVisibility(View.GONE);
                    }

                }
                return true;
            }
        });



        cancel_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidNetworking.cancel("upload");
            }
        });


        SetDetailDialogToList();
        GetStickersFromRealm();

        stickers_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                JSONObject js = new JSONObject();
                try {
                    js.put("answer_chat_id",answer_chat_id);
                    if (answer_chat_id!=0){
                        JSONObject answer_chat = new JSONObject();
                        answer_chat.put("user_id",answer_user_id);
                        answer_chat.put("nickname",answer_nickname);
                        answer_chat.put("chat_text",answer_chat_txt);
                        js.put("answer_chat",answer_chat);
                    }

                    js.put("sender_user_id",MY_USER_ID);
                    js.put("recipient_user_id",USER_ID);
                    js.put("group_id",GROUP_ID);
                    js.put("channel_id",CHANNEL_ID);
                    js.put("dialog_id",Dialog_id);
                    js.put("is_has_file",0);
                    js.put("chat_text","Стикер");
                    JSONObject sender = new JSONObject();
                    sender.put("user_id",MY_USER_ID);
                    sender.put("user_name",MY_USER_NAME);
                    sender.put("contact_user_name",Name);
                    if(is_ava) {sender.put("avatar",Avatar);}
                    else {sender.put("avatar","");}
                    sender.put("user_status","");
                    sender.put("phone","");
                    sender.put("email","");
                    sender.put("last_visit","");
                    js.put("sender",sender);
                    JSONObject recipient = new JSONObject();
                    recipient.put("user_id",USER_ID);
                    recipient.put("chat_name","");
                    js.put("recipient",recipient);
                    js.put("action_name","");
                    js.put("is_read",0);
                    Calendar rightNow = Calendar.getInstance();
                    int hour = rightNow.get(Calendar.HOUR_OF_DAY); // return the hour in 24 hrs format (ranging from 0-23)
                    int minute = rightNow.get(Calendar.MINUTE);

                    js.put("chat_date",hour+":"+minute);
                    js.put("is_resend","0");
                    js.put("is_contact", is_contact);
                    if (is_contact==1) {
                        js.put("contact_name", contact_name);
                        js.put("contact_phone", contact_phone);
                    }
                    JSONArray file_list = new JSONArray();
                    js.put("file_list",file_list);
                    js.put("sticker_id",feedItem_stickers.get(position).getSticker_id());

                    SendToServer(js);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void GetStickers() {
        AndroidNetworking.get(sticker_url+TOKEN).build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")) {
                        JSONArray data = response.getJSONArray("data");
                        for (int i=0;i<data.length();i++){
                            JSONObject object = (JSONObject) data.get(i);
                            Stickers stickers = new Stickers(object.getInt("sticker_id"),object.getString("sticker_image"));

                        }
                        GetStickersFromRealm();
                    } else {
                        Toasty.error(ActivityChatMessage.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {

            }
        });
    }

    private void GetStickersFromRealm() {
        realm.beginTransaction();

        feedItem_stickers.clear();



        RealmResults<Stickers> Result_stickers = realm.where(Stickers.class).findAll();
        if (Result_stickers.size()==0){
            GetStickers();
        }
        Log.d("phoneContacts",Result_stickers.size()+"**");
        for (int i=0;i<Result_stickers.size();i++){
            Stickers sticker = Result_stickers.get(i);
            FeedItem item = new FeedItem();
            item.setSticker_id(sticker.getSticker_id());
            item.setSticker_image(sticker.getSticker_image());

            feedItem_stickers.add(item);
        }
        StickersAdapter stickerAdapter = new StickersAdapter(ActivityChatMessage.this,feedItem_stickers);
        stickers_grid.setAdapter(stickerAdapter);
        realm.commitTransaction();
    }


    public void SelectItem(int position){

        FeedItem item = feedItem.get(position);
        if (!item.getChat_kind().equals("action")) {
            Integer pos = new Integer(position);
            if (selectedIds.contains(pos)) {
                selectedIds.remove(pos);
            } else {
                selectedIds.add(pos);
            }

            action_bar.setVisibility(View.GONE);
            selected_bar.setVisibility(View.VISIBLE);

            adapter.toggleSelected(pos);
            Log.d("selectedIds", selectedIds.size() + "*****");

            adapter.notifyDataSetChanged();
            selected_count.setText(selectedIds.size() + " ");

            if (selectedIds.size() == 0) {
                action_bar.setVisibility(View.VISIBLE);
                selected_bar.setVisibility(View.GONE);
            }else if (selectedIds.size()==1){
                selected_answer.setVisibility(View.VISIBLE);
            }else if (selectedIds.size()>1){
                selected_answer.setVisibility(View.GONE);
            }

        }
    }


    @OnClick(R.id.notification_btn)
    void notification_btn(){

        AndroidNetworking.post(mute+TOKEN)
                .addBodyParameter("is_mute",Is_mute+"")
                .addBodyParameter("channel_id",CHANNEL_ID+"")
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("AAAAACCCCC",response+" ");
                try {
                    if(response.getBoolean("status")){
                        if (Is_mute==0){
                            Is_mute = 1;
                            is_mute.setVisibility(View.VISIBLE);
                            notification_btn.setText("Включить уведомление");
                        }else {
                            Is_mute = 0;
                            is_mute.setVisibility(View.GONE);
                            notification_btn.setText("Отключить уведомление");
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
//                Toasty.error(ActivityChatMessage.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @OnClick(R.id.selected_answer)
    void selected_answer(){
        if (selectedIds.size()>0) {

            FeedItem item = feedItem.get(selectedIds.get(0));
            answer_chat_id = item.getChat_id();

            answer_layout.setVisibility(View.VISIBLE);
            if (item.getIs_has_file()==1){
                switch (item.getFile_format()[0]){
                    case "image":
                        answer_chat_text.setText("фото");
                        answer_image.setVisibility(View.VISIBLE);
                        Glide.with(ActivityChatMessage.this).load(item.getFile_url()[0]).into(answer_image);
                        break;
                    case "audio":
                        answer_image.setVisibility(View.GONE);
                        answer_chat_text.setText("аудио - "+item.getFile_name()[0]);
                        break;
                    case "video":
                        answer_image.setVisibility(View.GONE);
                        answer_chat_text.setText("видео - "+item.getFile_name()[0]);
                        break;
                    case "file":
                        answer_image.setVisibility(View.GONE);
                        answer_chat_text.setText("файл - "+item.getFile_name()[0]);
                        break;
                }

            }else {
                answer_image.setVisibility(View.GONE);
                answer_chat_text.setText(item.getChat_text());
            }


            String user_name = Name;
            answer_position = selectedIds.get(0);
            if (item.getUser_id()==MY_USER_ID){
                user_name ="Вы";
            }
            answer_user_id = item.getUser_id();
            answer_nickname = user_name;
            answer_chat_txt = item.getChat_text();
            answer_name.setText(user_name);

            action_bar.setVisibility(View.VISIBLE);
            selected_bar.setVisibility(View.GONE);


        }
    }
    @OnClick(R.id.selected_resend)
    void selected_resend(){
        if (selectedIds.size()>0) {
            try {
                JSONObject js = new JSONObject();

                JSONArray chat_ids = new JSONArray();
                for (int i = 0; i < selectedIds.size(); i++) {
                    chat_ids.put(feedItem.get(selectedIds.get(i)).getChat_id());
                }
                js.put("chat_ids",chat_ids);
                Intent intent = new Intent(ActivityChatMessage.this,ActivityResend.class);
                intent.putExtra("js",js+"");
                startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @OnClick(R.id.selected_copy)
    void selected_copy(){
        if (selectedIds.size()>0) {
            ClipboardManager clipboard = (ClipboardManager)
                        getSystemService(Context.CLIPBOARD_SERVICE);
            StringBuilder message_txt= new StringBuilder();
            for (int i = 0; i < selectedIds.size(); i++) {
                FeedItem item = feedItem.get(selectedIds.get(i));
                if (item.getIs_has_file()!=1)
                    message_txt.append(item.getChat_text()+"\n");
            }
            ClipData clip = ClipData.newPlainText("message text", message_txt.toString());
            clipboard.setPrimaryClip(clip);
            adapter.toggleSelectedClear();
            selectedIds.clear();
            action_bar.setVisibility(View.VISIBLE);
            selected_bar.setVisibility(View.GONE);

        }
    }

    @OnClick(R.id.selected_delete)
    void  selected_delete(){
        if (selectedIds.size()>0) {
            try {
                JSONObject js = new JSONObject();

                JSONArray chat_ids = new JSONArray();
                for (int i = 0; i < selectedIds.size(); i++) {
                    chat_ids.put(feedItem.get(selectedIds.get(i)).getChat_id());
                }
                js.put("chat_ids",chat_ids);
                js.put("partner_id",USER_ID);



                Log.d("JSJSJSJSJJS",js+" * ");


                AndroidNetworking.delete(chat+TOKEN)
                        .addJSONObjectBody(js)
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        for (int ind = 0; ind < selectedIds.size(); ind++) {
                            int finalInd = ind;
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    DialogDetail dialogDetail1 = realm.where(DialogDetail.class).equalTo("chat_id", feedItem.get(selectedIds.get(finalInd)).getChat_id()).and().equalTo("dialog_id", Dialog_id).findFirst();

                                    dialogDetail1.deleteFromRealm();
                                }
                            });

                            feedItem.remove(selectedIds.get(finalInd));

                            adapter.toggleSelectedClear();
                            adapter.deleteItem(selectedIds.get(finalInd));
                            selectedIds.clear();
                            action_bar.setVisibility(View.VISIBLE);
                            selected_bar.setVisibility(View.GONE);

                        }


                       // GetDataFromJSON(URL);
                        Log.d("delete",response+"");

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }



    @OnClick(R.id.close)
    void close(){
        adapter.toggleSelectedClear();
       selectedIds.clear();
        action_bar.setVisibility(View.VISIBLE);
        selected_bar.setVisibility(View.GONE);
    }


    private void initFooterView() {
         video_upload_layout = (FrameLayout) footerview.findViewById(R.id.video_upload_layout);
         upload_view = (ImageView) footerview.findViewById(R.id.upload_view);
         upload_byte = (TextView) footerview.findViewById(R.id.upload_byte);
        upload_byte_audio = (TextView) footerview.findViewById(R.id.upload_byte_audio);
        upload_byte_file = (TextView) footerview.findViewById(R.id.upload_byte_file);
        filename = (TextView) footerview.findViewById(R.id.file_name);
        cancel_upload = (ImageButton) footerview.findViewById(R.id.cancel_upload);
        image_upload_layout = (FrameLayout) footerview.findViewById(R.id.image_upload_layout);
        audiolayout = (FrameLayout) footerview.findViewById(R.id.audiolayout);
        image_upload_layout2 = (LinearLayout) footerview.findViewById(R.id.image_upload_layout2);
        filelayout = (LinearLayout) footerview.findViewById(R.id.filelayout);
        single_image = (ImageView) footerview.findViewById(R.id.single_image);
        image1 = (ImageView) footerview.findViewById(R.id.image1);
        image2 = (ImageView) footerview.findViewById(R.id.image2);
        image3 = (ImageView) footerview.findViewById(R.id.image3);
        image4 = (ImageView) footerview.findViewById(R.id.image4);
        image5 = (ImageView) footerview.findViewById(R.id.image5);

    }


    private void CheckPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.CAMERA
                ).withListener(new MultiplePermissionsListener() {
            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {/* ... */}
            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
        }).check();
    }

    public void MediaRecorderReady(){
        if (mediaRecorder != null) {
            mediaRecorder.release();
            mediaRecorder = null;
        }
        mediaRecorder=new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    public String CreateRandomAudioFileName(int string){
        StringBuilder stringBuilder = new StringBuilder( string );
        int i = 0 ;
        while(i < string ) {
            stringBuilder.append(RandomAudioFileName.
                    charAt(random.nextInt(RandomAudioFileName.length())));

            i++ ;
        }
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        return stringBuilder.toString()+ts;
    }



    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.i(TAG, "11111cccccc");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!isConnected) {
                        ripple.setVisibility(View.VISIBLE);
                        connect_txt.setVisibility(View.GONE);
                        Log.i(TAG, "connected");
                        isConnected = true;

                    }
                    Log.i(TAG, "111aaaaa");
                }
            });
        }
    };
    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "diconnected");
                    isConnected = false;
                    ripple.setVisibility(View.GONE);
                    connect_txt.setVisibility(View.VISIBLE);
//                    Toasty.warning(getApplicationContext(), "disconnect", Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "Errorrrrrrrr");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "Error connecting");
                    ripple.setVisibility(View.GONE);
                    connect_txt.setVisibility(View.VISIBLE);
//                    Toasty.error(getApplicationContext(), "error_connect", Toast.LENGTH_SHORT).show();
                }
            });
        }
    };


    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.d("data",data+"**");

                    try {
                        JSONObject sender = data.getJSONObject("sender");

                        if(data.getInt("recipient_user_id")==MY_USER_ID && sender.getInt("user_id")==USER_ID){
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    PAGE_p = 1;
                                    PAGE_m = 1;
                                    GetDataFromJSON(URL);
                                    handler.removeCallbacksAndMessages(null);
                                }
                            }, 1500);

                        }
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                        return;
                    }
                }
            });
        }
    };

    private Emitter.Listener onOnline = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.d("handler_online", data + "**" + MY_USER_ID);
                    try {
                        int  sender_id = data.getInt("sender_id");

                        if(sender_id==USER_ID){
                            chat_text.setText("онлайн");
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    chat_text.setText("был(а) недавно");
                                    Log.d("AAAAA","ddddd");
                                    handler.removeCallbacksAndMessages(null);
                                }
                            }, 20000);
                        }


                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                        return;
                    }
                }
            });
        }
    };
 private Emitter.Listener read = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.d("TYYYYY","UUUU"+data);
                    try {
                        String dialog_id = data.getString("dialog_id");
                        int sender_id = data.getInt("sender_id");

                        if(dialog_id.equals(MY_USER_ID+"U") && sender_id==USER_ID){



                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {

                                    RealmResults<DialogDetail> results = realm.where(DialogDetail.class).equalTo("dialog_id", Dialog_id).sort("time").findAll();
                                   Log.d("results",results.size()+"**");
                                    for (int i = 0; i<results.size();i++) {
                                        DialogDetail dialogDetail = results.get(i);
                                        if (dialogDetail != null) {
                                            dialogDetail.setIs_read(1);
                                        }
                                    }
                                    for (int j=0;j<feedItem.size();j++){
                                        feedItem.get(j).setIs_read(1);
                                    }
                                    adapter.notifyDataSetChanged();
                                }
                            });
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                        return;
                    }
                }
            });
        }
    };



    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            final int[] finalI = {0};
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!is_typing) {
                        try {
                            JSONObject data = (JSONObject) args[0];
                            Log.d("typing",data+"*");
                            String dialog_id = data.getString("dialog_id");
                            int sender_id = data.getInt("sender_id");
                            if(dialog_id.contains("U")){
                                if(sender_id==USER_ID && dialog_id.equals(MY_USER_ID+"U")){
                                    is_typing = true;
                                    chat_text.setText("печатаеть...");
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            chat_text.setText("онлайн");
                                            handler.removeCallbacksAndMessages(null);
                                        }
                                    }, 5000);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Log.d("handler_online","start222");
            if(isConnected) {
                Log.d("handler_online","start3333");
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("sender_id", MY_USER_ID);
                    jsonObject.put("type", "online");
                    jsonObject.put("os", "android");
                    mSocket.emit("online", jsonObject);

//                    handler_online.postDelayed(runnable, 5000);
                    Log.d("handler_online","start4444");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }
    };


    @OnClick(R.id.more_menu)
    void more_menu(){

            MoreMenuDialog alert = new MoreMenuDialog();
            alert.attachView( ActivityChatMessage.this);
            alert.showDialog(WHO,ActivityChatMessage.this, USER_ID, Is_mute, is_mute,i_block_partner,CHANNEL_ID,GROUP_ID);

    }
    @OnClick(R.id.search_chat)
    void search_chat(){


        Intent intent = new Intent(ActivityChatMessage.this,ActivitySearchbyChatDetail.class);
        intent.putExtra("user_id",USER_ID);
        intent.putExtra("dialog_id",Dialog_id);
        intent.putExtra("who",WHO);
        intent.putExtra("url",URL);
        startActivityForResult(intent,REQUEST_SEARCH);

    }



    @OnClick(R.id.answer_close)
    void answer_close(){
        answer_chat_id = 0;
        answer_user_id =0;
        answer_nickname="";
        answer_chat_txt="";
        is_contact=0;
        answer_position = 0;
        answer_layout.setVisibility(View.GONE);
    }



    @OnClick(R.id.ripple)
    void ripple(){
        switch (WHO){
            case "chat":
                startActivity(new Intent(ActivityChatMessage.this,ActivityChatProfile.class)
                    .putExtra("user_id",USER_ID).putExtra("Is_mute",Is_mute));
                break;
            case "group":
                startActivity(new Intent(ActivityChatMessage.this,ActivityGroupProfile.class)
                        .putExtra("is_admin",is_admin)
                        .putExtra("group_id",GROUP_ID)
                );
                break;
            case "channel":
                Intent intent  = new Intent(ActivityChatMessage.this,ActivityChannelProfile.class);
                intent.putExtra("is_admin",is_admin);
                intent.putExtra("channel_id",CHANNEL_ID);
                intent.putExtra("user_response",user_response);
                startActivity(intent);
                break;
        }

    }




    @OnClick(R.id.attach_btn)
    void Attach(){

        if (imm.isAcceptingText()) {
            View view = getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(this);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        insert_layout.setVisibility(View.GONE);
        file_layout.setVisibility(View.VISIBLE);
    }



    @OnClick(R.id.sticker_btn)
    void Sticker_btn(){
        if (sticker_visble) {
            sticker_visble = false;
            sticker_layout.setVisibility(View.GONE);
        }else {
            sticker_visble = true;
            sticker_layout.setVisibility(View.VISIBLE);
        }
    }


    @OnClick(R.id.btn_gallerey)
    void btn_gallerey(){
        insert_layout.setVisibility(View.VISIBLE);
        file_layout.setVisibility(View.GONE);
        Pix.start(ActivityChatMessage.this,                    //Activity or Fragment Instance
                RequestCodeForGallerey,                //Request code for activity results
                9);


    }

    @OnClick(R.id.btn_video)
    void btn_video() {
        Log.d("TTTTTTT","btn_video");

        insert_layout.setVisibility(View.VISIBLE);
        file_layout.setVisibility(View.GONE);
//        Matisse.from(ActivityChatMessage.this)
//                .choose(MimeType.ofAll())
//                .countable(true)
//                .maxSelectable(1)
//                .autoHideToolbarOnSingleTap(true)
//                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
//                .thumbnailScale(0.85f)
//                .imageEngine(new MyPicassoEngine())
//                .forResult(REQUEST_TAKE_GALLERY_VIDEO);

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, REQUEST_TAKE_GALLERY_VIDEO);

    }

    @OnClick(R.id.btn_music)
    void btn_music(){
        mediaFiles.clear();
        insert_layout.setVisibility(View.VISIBLE);
        file_layout.setVisibility(View.GONE);
//        Intent intent = new Intent(ActivityChatMessage.this, FilePickerActivity.class);
//        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
//                .setCheckPermission(true)
//                .setSelectedMediaFiles(mediaFiles)
//                .setShowImages(false)
//                .setShowVideos(false)
//                .setShowAudios(true)
//                .setMaxSelection(1)
//                .build());
//        startActivityForResult(intent, REQUEST_AUDIO_FILE);

        Intent audioIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(audioIntent, REQUEST_AUDIO_FILE);


    }

    @OnClick(R.id.btn_file)
    void btn_file(){
        mediaFiles.clear();
        insert_layout.setVisibility(View.VISIBLE);
        file_layout.setVisibility(View.GONE);

        Intent intent4 = new Intent(this, NormalFilePickActivity.class);
        intent4.putExtra(Constant.MAX_NUMBER, 1);
        intent4.putExtra(NormalFilePickActivity.SUFFIX, new String[] {"xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf","zip","rar", "txt"});
        startActivityForResult(intent4, REQUEST_TAKE_FILE);

    }

    @OnClick(R.id.btn_contacts)
    void btn_contacts(){
        insert_layout.setVisibility(View.VISIBLE);
        file_layout.setVisibility(View.GONE);
        startActivityForResult(new Intent(ActivityChatMessage.this,ActivitySendUser.class),704);

    }

    @OnClick(R.id.btn_close)
    void btn_close(){
        insert_layout.setVisibility(View.VISIBLE);
        file_layout.setVisibility(View.GONE);
    }

    @OnClick(R.id.back)
    void BackClick(){
       onBackPressed();

    }

    @Override
    public void onBackPressed() {
        if(selectedIds.size()>0){
            adapter.toggleSelectedClear();
            selectedIds.clear();
            action_bar.setVisibility(View.VISIBLE);
            selected_bar.setVisibility(View.GONE);
        }else {
            finish();
        }

    }

    @OnClick(R.id.emotion_btn)
    void emotion_btnClick(){

        Log.d("SSSSS",emojiPopup.isShowing()+"****");
        if (emojiPopup.isShowing()){
            emojiPopup.dismiss();
        }else {
            emojiPopup.toggle();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK ){
            answer_layout.setVisibility(View.GONE);
            if(requestCode == RequestCodeForGallerey){
                ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);


                File [] f = new File[returnValue.size()];
                for (int i=0;i<returnValue.size();i++){
                    String new_filename =  MyConstants.compressImage(returnValue.get(i), ActivityChatMessage.this);
                    f[i] = new File(new_filename);
                }
                file_name   = new String[f.length];
                file_url    = new String[f.length];
                file_format = new String[f.length];

                Bitmap thumb = null;
                int length = f.length;
                Log.d("LLLL",length+" ");
                listView.addFooterView(footerview);

                filelayout.setVisibility(View.GONE);
                video_upload_layout.setVisibility(View.GONE);
                audiolayout.setVisibility(View.GONE);
                image_upload_layout.setVisibility(View.VISIBLE);

                if (length>0){
                    thumb = BitmapFactory.decodeFile(f[0].getAbsolutePath());
                    single_image.setImageBitmap(thumb);
                    single_image.setVisibility(View.VISIBLE);
                    image1.setVisibility(View.GONE);
                }
                if (length>1){
                    single_image.setVisibility(View.GONE);
                    image1.setVisibility(View.VISIBLE);
                    thumb = BitmapFactory.decodeFile(f[0].getAbsolutePath());
                    image1.setImageBitmap(thumb);
                    image_upload_layout2.setVisibility(View.VISIBLE);
                    thumb = BitmapFactory.decodeFile(f[1].getAbsolutePath());
                    image2.setImageBitmap(thumb);
                    image2.setVisibility(View.VISIBLE);
                }
                if (length>2){
                    thumb = BitmapFactory.decodeFile(f[2].getAbsolutePath());
                    image3.setImageBitmap(thumb);
                    image3.setVisibility(View.VISIBLE);
                }
                if (length>3){
                    thumb = BitmapFactory.decodeFile(f[3].getAbsolutePath());
                    image4.setImageBitmap(thumb);
                    image4.setVisibility(View.VISIBLE);
                }
                if (length>4){
                    thumb = BitmapFactory.decodeFile(f[4].getAbsolutePath());
                    image5.setImageBitmap(thumb);
                    image5.setVisibility(View.VISIBLE);
                }
                listView.setSelection(adapter.getCount() - 1);
                SendImageToServer(f,length,0);

            }else if (requestCode == REQUEST_TAKE_GALLERY_VIDEO) {

//                List<String> paths = Matisse.obtainPathResult(data);
//                if (!paths.isEmpty()) {
//                    String selectedVideoPath = paths.get(0);
//                    Log.d("SSSSSSs2",selectedVideoPath);
//
//                    if (selectedVideoPath != null) {
//                        File f = new File(selectedVideoPath);
//                        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//
//                        retriever.setDataSource(ActivityChatMessage.this, Uri.fromFile(f));
//                        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//                        long timeInMillisec = Long.parseLong(time );
//                        file_time = (int) TimeUnit.MILLISECONDS.toSeconds(timeInMillisec);
//                        file_name = new String[1];
//                        file_url = new String[1];
//                        file_format = new String[1];
//                        SendVideoToServer(f, 0);
//                        listView.addFooterView(footerview);
//                        filelayout.setVisibility(View.GONE);
//                        audiolayout.setVisibility(View.GONE);
//                        video_upload_layout.setVisibility(View.GONE);
//                        image_upload_layout.setVisibility(View.GONE);
//
//                        if (selectedVideoPath.contains(".mp4")){
//                            video_upload_layout.setVisibility(View.VISIBLE);
//                        }else {
//                            image_upload_layout.setVisibility(View.VISIBLE);
//                        }
//                        listView.setSelection(adapter.getCount() - 1);
//                    }
//                }

                if (data != null) {
                    Uri contentURI = data.getData();

                    String selectedVideoPath = getPath(contentURI);
                    Log.d("path",selectedVideoPath);

                    if (selectedVideoPath != null) {
                        File f = new File(selectedVideoPath);
                        MediaMetadataRetriever retriever = new MediaMetadataRetriever();

                        retriever.setDataSource(ActivityChatMessage.this, Uri.fromFile(f));
                        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                        long timeInMillisec = Long.parseLong(time );
                        file_time = (int) TimeUnit.MILLISECONDS.toSeconds(timeInMillisec);
                        file_name = new String[1];
                        file_url = new String[1];
                        file_format = new String[1];
                        SendVideoToServer(f, 0);
                        listView.addFooterView(footerview);
                        filelayout.setVisibility(View.GONE);
                        audiolayout.setVisibility(View.GONE);
                        video_upload_layout.setVisibility(View.GONE);
                        image_upload_layout.setVisibility(View.GONE);

                        if (selectedVideoPath.contains(".mp4")){
                            video_upload_layout.setVisibility(View.VISIBLE);
                        }else {
                            image_upload_layout.setVisibility(View.VISIBLE);
                        }
                        listView.setSelection(adapter.getCount() - 1);
                    }


                }



            } else  if (requestCode == REQUEST_AUDIO_FILE) {

                if (data != null) {
                    Uri contentURI = data.getData();
                    Log.d("DDDDD",data.getData()+"**");
                    Log.d("DDDDD",data+"**");

                    String selectedVideoPath = getPath(contentURI);
                    Log.d("path", selectedVideoPath);




                    listView.addFooterView(footerview);
                filelayout.setVisibility(View.GONE);
                audiolayout.setVisibility(View.VISIBLE);
                video_upload_layout.setVisibility(View.GONE);
                image_upload_layout.setVisibility(View.GONE);


                    if (selectedVideoPath != null) {
                        Log.d("AAAAff", selectedVideoPath);
                        File f =  new File(selectedVideoPath);
                        file_name = new String[1];
                        file_url = new String[1];
                        file_format = new String[1];

                        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                        mmr.setDataSource(ActivityChatMessage.this,contentURI);
                        String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                        int millSecond = Integer.parseInt(durationStr);
                        int sec = (int) TimeUnit.MILLISECONDS.toSeconds(millSecond);
                        Log.d("AAAAA",millSecond+"______"+sec);
                        file_time  = sec;
                        SendAudioToServer(f);
                    }

                }

                listView.setSelection(adapter.getCount() - 1);
            }else  if (requestCode == REQUEST_TAKE_FILE) {
                listView.addFooterView(footerview);
                filelayout.setVisibility(View.VISIBLE);
                audiolayout.setVisibility(View.GONE);
                video_upload_layout.setVisibility(View.GONE);
                image_upload_layout.setVisibility(View.GONE);

                ArrayList<NormalFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);

                if (!list.isEmpty()) {

                    String selectedVideoPath = list.get(0).getPath();
                    if (selectedVideoPath != null) {
                        Log.d("AAAAff", selectedVideoPath);
                        File f = new File(selectedVideoPath);

                        file_name = new String[1];
                        file_url = new String[1];
                        file_format = new String[1];
                        SendFileToServer(f,list.get(0).getName());
                    }
                }

                listView.setSelection(adapter.getCount() - 1);
            }else if (requestCode == 704){
                Log.d("Contacts",data.getStringExtra("name"));
                Log.d("Contacts",data.getStringExtra("phone"));
                is_contact = 1;
                contact_name = data.getStringExtra("name");
                contact_phone =data.getStringExtra("phone");
                SendMessage(0);
            }else if (requestCode == REQUEST_SEARCH){
                PAGE_m = data.getIntExtra("page_m",PAGE_m);
                PAGE_p = data.getIntExtra("page_p",PAGE_p);
                int chat_id = data.getIntExtra("chat_id",0);
                GetDataFromJSONByPAGE(true,PAGE_m,URL,chat_id,0);
            }

        }
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    public void SendVideoToServer(final File file1,  final int position){
        Log.d("TTTTTTT","SendVideoToServer");

//        AddRealmSend(file1);
        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(file1.getAbsolutePath(), MediaStore.Video.Thumbnails.MINI_KIND);
        upload_view.setImageBitmap(thumb);
        video_upload_layout.setVisibility(View.VISIBLE);
        image_upload_layout.setVisibility(View.GONE);
        audiolayout.setVisibility(View.GONE);
        filelayout.setVisibility(View.GONE);

        Log.d("AAAa",file+TOKEN);
        AndroidNetworking.upload(file+TOKEN)
                .addMultipartFile("file", file1)
                .setPriority(Priority.HIGH)
                .setTag("upload")
                .build()
                    .setUploadProgressListener(new UploadProgressListener() {
                        @Override
                        public void onProgress(long bytesUploaded, long totalBytes) {
                            int progress = (int) (bytesUploaded*100 / totalBytes);
                            upload_byte.setText(progress+" %");
                        }
                    })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")) {

                                file_name[position] = response.getString("file_name");
                                file_url[position] = response.getString("file_url");
                                file_format[position] = response.getString("file_format");
                                downloadFile("video",response.getString("file_full_url"),file_name[position]);
                                SendMessage(1);

                            } else {
                                listView.removeFooterView(footerview);
                                Toasty.error(ActivityChatMessage.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("file response _ ", response + "***");
                    }

                    @Override
                    public void onError(ANError anError) {
//                        Toasty.error(ActivityChatMessage.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                        listView.removeFooterView(footerview);
                        Log.d("file anError _ ", "Проверить подключение интернета" + "***");
                        Log.d("file anError _ ", anError.getErrorCode() + "***");
                    }

                });

    }


    public void SendImageToServer(final File[] f, final int length, final int position){



        Log.d("AAAAAAAA",length+"**"+position);
        AndroidNetworking.upload(file+TOKEN)
                .setTag("upload")
                .addMultipartFile("file", f[position])
                .setPriority(Priority.HIGH)
                .build()
//                    .setUploadProgressListener(new UploadProgressListener() {
//                        @Override
//                        public void onProgress(long bytesUploaded, long totalBytes) {
//                            // do anything with progress
//                        }
//                    })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")) {

                                file_name[position] = response.getString("file_name");
                                file_url[position] = response.getString("file_url");
                                file_format[position] = response.getString("file_format");
                                deleteFileName(f[position].getName());
                                if(position+1<length) {
                                    SendImageToServer(f,length,position+1);
                                }else {
                                    SendMessage(1);

                                }

                            } else {
                                listView.removeFooterView(footerview);
                                Toasty.error(ActivityChatMessage.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("file response _ ", response + "***");
                    }

                    @Override
                    public void onError(ANError anError) {
//                        Toasty.error(ActivityChatMessage.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                        listView.removeFooterView(footerview);
                        Log.d("file anError _ ", "Проверить подключение интернета" + "***");
                        Log.d("file anError _ ", anError.getErrorCode() + "***");
                    }
                });

    }

    public void SendAudioToServer(final File f){

        listView.addFooterView(footerview);
        listView.setSelection(adapter.getCount() - 1);

        video_upload_layout.setVisibility(View.GONE);
        image_upload_layout.setVisibility(View.GONE);
        filelayout.setVisibility(View.GONE);
        audiolayout.setVisibility(View.VISIBLE);


        AndroidNetworking.upload(file+TOKEN)
                .addMultipartFile("file", f)
                .addMultipartParameter("file_time",file_time+"")
                .setPriority(Priority.HIGH)
                .setTag("upload")
                .build()
                    .setUploadProgressListener(new UploadProgressListener() {
                        @Override
                        public void onProgress(long bytesUploaded, long totalBytes) {
                            int progress = (int) (bytesUploaded*100 / totalBytes);
                            upload_byte_audio.setText(progress+" %");
                        }
                    })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")) {

                                Log.d("response",response+" ");
                                file_name[0] = response.getString("file_name");
                                file_url[0] = response.getString("file_url");
                                file_format[0] = response.getString("file_format");
                                Log.d("file_url",file_url[0]);
                                downloadFile("audio",response.getString("file_full_url"),file_name[0]);
                                SendMessage(1);

                            } else {
                                file_time = 0;
                                listView.removeFooterView(footerview);
                                Toasty.error(ActivityChatMessage.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("file response _ ", response + "***");
                    }

                    @Override
                    public void onError(ANError anError) {
//                        Toasty.error(ActivityChatMessage.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                        file_time = 0;
                        listView.removeFooterView(footerview);
                        Log.d("file anError _ ", "Проверить подключение интернета" + "***");
                        Log.d("file anError _ ", anError.getErrorCode() + "***");
                    }
                });

    }

    public void SendFileToServer(final File f,String name){



        listView.addFooterView(footerview);
        listView.setSelection(adapter.getCount() - 1);

        video_upload_layout.setVisibility(View.GONE);
        image_upload_layout.setVisibility(View.GONE);
        audiolayout.setVisibility(View.GONE);
        filelayout.setVisibility(View.VISIBLE);

        filename.setText(name);

        Log.d("AAAa",file+TOKEN);
        AndroidNetworking.upload(file+TOKEN)
                .addMultipartFile("file", f)
                .setPriority(Priority.HIGH)
                .setTag("upload")
                .build()
                    .setUploadProgressListener(new UploadProgressListener() {
                        @Override
                        public void onProgress(long bytesUploaded, long totalBytes) {
                            int progress = (int) (bytesUploaded*100 / totalBytes);
                            upload_byte_file.setText(progress+" %");
                        }
                    })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")) {

                                file_name[0] = response.getString("file_name");
                                file_url[0] = response.getString("file_url");
                                file_format[0] = response.getString("file_format");

                                SendMessage(1);

                            } else {
                                listView.removeFooterView(footerview);
                                Toasty.error(ActivityChatMessage.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("file response _ ", response + "***");
                    }

                    @Override
                    public void onError(ANError anError) {
//                        Toasty.error(ActivityChatMessage.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                        listView.removeFooterView(footerview);
                        Log.d("file anError _ ", "Проверить подключение интернета" + "***");
                        Log.d("file anError _ ", anError.getErrorCode() + "***");
                    }
                });

    }




    public void SendMessage(int is_has_file){

        Log.d("is_has_file",is_has_file+"   11");
        answer_layout.setVisibility(View.GONE);
        String Message= emojiEditText.getText().toString();
        Log.d("Message",Message);
        JSONObject js = new JSONObject();


        try {
            js.put("answer_chat_id",answer_chat_id);
            if (answer_chat_id!=0){
                JSONObject answer_chat = new JSONObject();
                answer_chat.put("user_id",answer_user_id);
                answer_chat.put("nickname",answer_nickname);
                answer_chat.put("chat_text",answer_chat_txt);
                js.put("answer_chat",answer_chat);
            }

            js.put("sender_user_id",MY_USER_ID);
            js.put("recipient_user_id",USER_ID);
            js.put("group_id",GROUP_ID);
            js.put("channel_id",CHANNEL_ID);
            js.put("dialog_id",Dialog_id);
            js.put("is_has_file",is_has_file);
            js.put("chat_text",Message);
            JSONObject sender = new JSONObject();
            sender.put("user_id",MY_USER_ID);
            sender.put("user_name",MY_USER_NAME);
            sender.put("contact_user_name",Name);
            if(is_ava) {sender.put("avatar",Avatar);}
            else {sender.put("avatar","");}
            sender.put("user_status","");
            sender.put("phone","");
            sender.put("email","");
            sender.put("last_visit","");
            js.put("sender",sender);
            JSONObject recipient = new JSONObject();
            recipient.put("user_id",USER_ID);
            recipient.put("chat_name","");
            js.put("recipient",recipient);
            js.put("action_name","");
            js.put("is_read",0);
            Calendar rightNow = Calendar.getInstance();
            int hour = rightNow.get(Calendar.HOUR_OF_DAY); // return the hour in 24 hrs format (ranging from 0-23)
            int minute = rightNow.get(Calendar.MINUTE);

            js.put("chat_date",hour+":"+minute);
            js.put("is_resend","0");
            js.put("is_contact", is_contact);
            if (is_contact==1) {
                js.put("contact_name", contact_name);
                js.put("contact_phone", contact_phone);
            }
            JSONArray file_list = new JSONArray();
            if (is_has_file==1) {

                for (int k = 0; k < file_url.length; k++) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("file_url", file_url[k]);
                    jsonObject.put("file_name", file_name[k]);
                    jsonObject.put("file_format", file_format[k]);
                    jsonObject.put("file_time", file_time + "");

                    file_list.put(jsonObject);
                }

            }
            js.put("file_list",file_list);

                SendToServer(js);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void SendToServer(JSONObject js){


        recordButton.setEnabled(false);

        Log.d("AAAAA",js+"");

        AndroidNetworking.post(chat+TOKEN)
                .addJSONObjectBody(js)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("AAAAAAMMMMM",response+"mmm");
                try {
                    if (response.getBoolean("status")) {
                        mSocket.emit("message", js);
                        recordButton.setEnabled(true);
                        emojiEditText.setText(null);
                        answer_chat_id=0;
                        answer_user_id =0;
                        answer_nickname="";
                        answer_chat_txt="";
                        answer_position=0;
                        is_contact = 0;
//                        AddFeedItem(js);

                        LastMessage(response.getJSONArray("data"));

                    } else {
                        Toasty.error(ActivityChatMessage.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }



            @Override
            public void onError(ANError anError) {
//                Toasty.error(ActivityChatMessage.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });
    }


    public void LastMessage( JSONArray list){

        listView.removeFooterView(footerview);

        try {
        for (int i = 0; i <list.length(); i++) {
            JSONObject jsonObject = (JSONObject) list.get(i);


            DialogDetail dialogDetail1 = realm.where(DialogDetail.class).equalTo("chat_id", NurInt(jsonObject, "chat_id")).findFirst();
            boolean is_playaudio = dialogDetail1 != null && dialogDetail1.isIs_playaudio();
            JSONObject sender = jsonObject.getJSONObject("sender");
            JSONArray file_list = jsonObject.getJSONArray("file_list");
            int f_l = file_list.length();
            String file_url = "";
            String[] sfile_url = new String[f_l];
            String file_format = "";
            String[] sfile_format= new String[f_l];
            String file_name = "";
            String[] sfile_name= new String[f_l];
            int file_time = 0;
            for (int f = 0; f < f_l; f++) {
                JSONObject file = (JSONObject) file_list.get(f);
                if (f > 0) {
                    file_url += ",";
                    file_format += ",";
                    file_name += ",";
                }
                file_url += file.getString("file_url");
                file_format += file.getString("file_format");
                file_name += file.getString("file_name");
                file_time = file.getInt("file_time");
                sfile_url[f]=file.getString("file_url");
                sfile_format[f]=file.getString("file_format");
                sfile_name[f]=file.getString("file_name");
            }
            int answer_user_id = 0;
            String answer_file_url = "";
            String answer_file_format = "";
            String answer_file_name = "";
            int answer_file_time = 0;
            int answer_is_has_file = 0;
            int answer_is_contact = 0;
            String answer_contact_name="";
            String answer_name = " ", answer_text = " ";
            if (jsonObject.getInt("answer_chat_id") != 0) {
                JSONObject answer_chat = jsonObject.getJSONObject("answer_chat");
                answer_name = NurString(answer_chat, "nickname");
                answer_is_has_file = answer_chat.getInt("is_has_file");
                answer_is_contact = answer_chat.getInt("is_contact");
                if(answer_is_contact!=0){
                    answer_contact_name = answer_chat.getJSONObject("contact").getString("contact_name");
                }answer_user_id = answer_chat.getInt("user_id");
                answer_text = answer_chat.getString("chat_text");
                if (answer_is_has_file != 0) {
                    JSONArray answer_file_list = answer_chat.getJSONArray("file_list");
                    if (answer_file_list.length() > 0) {
                        JSONObject answer_file = (JSONObject) answer_file_list.get(0);
                        answer_file_url = answer_file.getString("file_url");
                        answer_file_format = answer_file.getString("file_format");
                        answer_file_name = answer_file.getString("file_name");
                        answer_file_time = answer_file.getInt("file_time");
                    }
                }
            }
            JSONObject recipient = jsonObject.optJSONObject("recipient");
            boolean has_recipient;
            int recipient_user_id = 0;
            String recipient_phone = "";
            String recipient_chat_name = "";
            String recipient_avatar = "";
            if (recipient != null) {
                if (recipient.has("user_id")) {
                    has_recipient = true;
                    recipient_user_id = NurInt(recipient, "user_id");
                    if (!NurString(recipient, "nickname").equals(""))
                        recipient_chat_name = NurString(recipient, "nickname");
                    else if (!NurString(recipient, "chat_name").equals(""))
                        recipient_chat_name = NurString(recipient, "chat_name");
                    else
                        recipient_chat_name = NurString(recipient, "phone");
                    recipient_avatar = NurString(recipient, "avatar");
                    recipient_phone = NurString(recipient, "phone");
                } else {
                    has_recipient = false;
                }
            } else {
                has_recipient = false;
            }
            int is_contact = NurInt(jsonObject, "is_contact");
            int is_exist=0,contact_user_id=0;
            String contact_name = "", contact_phone = "";
            if (is_contact == 1) {
                JSONObject contact = jsonObject.getJSONObject("contact");
                contact_name = NurString(contact, "contact_name");
                contact_phone = NurString(contact, "contact_phone");
                is_exist = NurInt(contact, "is_exist");
                contact_user_id = NurInt(contact, "user_id");
            }
            int is_has_link = NurInt(jsonObject, "is_has_link");
            String link_description=" ", link_title=" ",link_image=" ";
            if (is_has_link==1){
                JSONObject meta_tags = jsonObject.getJSONObject("meta_tags");
                link_description = meta_tags.getString("description");
                link_title = meta_tags.getString("title");
                link_image = meta_tags.getString("image");
            }


            int is_sticker = NurInt(jsonObject, "is_sticker");
            String sticker_image="";
            if (is_sticker==1){
                JSONObject sticker = jsonObject.getJSONObject("sticker");
                sticker_image =  NurString(sticker, "sticker_image");
            }

            boolean save_storage = NurInt(sender, "user_id") == MY_USER_ID || dialogDetail1 != null && dialogDetail1.isSave_storage();
            DialogDetail dialogDetail = new DialogDetail(
                    Dialog_id, NurInt(jsonObject, "chat_id"), NurInt(jsonObject, "is_read"), NurInt(jsonObject, "is_has_file"), NurInt(sender, "user_id"), file_time, NurInt(jsonObject, "answer_chat_id"), answer_user_id,
                    NurString(jsonObject, "chat_kind"), NurString(jsonObject, "action_name"), NurString(jsonObject, "chat_text"), NurString(jsonObject, "chat_time"), NurString(jsonObject, "chat_date"), NurString(sender, "user_name"),
                    NurString(sender, "avatar"), NurString(sender, "phone"), answer_name, answer_text, file_url, file_format, file_name, save_storage, System.currentTimeMillis(), answer_file_url, answer_file_format, answer_file_name, answer_file_time, answer_is_has_file, false, NurInt(jsonObject, "is_resend"),
                    has_recipient, recipient_user_id, recipient_phone, recipient_chat_name, recipient_avatar,  NurInt(jsonObject, "view_count"), is_contact, contact_name, contact_phone, is_playaudio, is_exist,contact_user_id,is_has_link,link_description,link_title,link_image,answer_is_contact,answer_contact_name,is_sticker,sticker_image);




            FeedItem item = new FeedItem();
            item.setChat_id(NurInt(jsonObject, "chat_id"));
            item.setIs_read(NurInt(jsonObject, "is_read"));
            item.setIs_has_file(NurInt(jsonObject, "is_has_file"));
            item.setUser_id(NurInt(sender, "user_id"));
            item.setFile_time(file_time);
            item.setAnswer_chat_id(NurInt(jsonObject, "answer_chat_id"));
            item.setAnswer_user_id(answer_user_id);
            item.setChat_kind(NurString(jsonObject, "chat_kind"));
            item.setAction_name(NurString(jsonObject, "action_name"));
            item.setChat_text(NurString(jsonObject, "chat_text"));
            item.setChat_time(NurString(jsonObject, "chat_time"));
            item.setChat_date(NurString(jsonObject, "chat_date"));
            item.setName(NurString(sender, "nickname"));
            item.setAvatar(NurString(sender, "avatar"));
            item.setPhone(NurString(sender, "phone"));
            item.setAnswer_name(answer_name);
            item.setAnswer_text(answer_text);
            item.setView_count(NurInt(jsonObject, "view_count"));

            item.setAnswer_file_url(answer_file_url);
            item.setAnswer_file_format(answer_file_format);
            item.setAnswer_file_name(answer_file_name);
            item.setAnswer_is_has_file(answer_is_has_file);
            item.setAnswer_is_contact(answer_is_contact);
            item.setAnswer_contact_name(answer_contact_name);
            item.setAnswer_file_time(answer_file_time);


            item.setFile_url(sfile_url);
            item.setFile_format(sfile_format);
            item.setFile_name(sfile_name);

            item.setSave_storage(save_storage);
            item.setIs_upload(false);
            item.setIs_resend(NurInt(jsonObject, "is_resend"));

            item.setIs_contact(is_contact);
            item.setContact_name(contact_name);
            item.setContact_phone(contact_phone);
            item.setIs_playaudio(is_playaudio);
            item.setIs_exist(is_exist);
            item.setContact_user_id(contact_user_id);
            item.setIs_has_link(is_has_link);
            item.setLink_description(link_description);
            item.setLink_title(link_title);
            item.setLink_image(link_image);
            item.setIs_sticker(is_sticker);
            item.setSticker_image(sticker_image);



            if (NurString(jsonObject, "chat_kind").equals("action")){
                item.setMessage_type(ACTION_MESSAGE);
            }else if (is_contact==1){
                item.setMessage_type(CONTACT_MESSAGE);
            }else if (is_sticker==1){
                item.setMessage_type(STICKER_MESSAGE);
            }else if (NurInt(jsonObject, "is_has_file")!=1 || sfile_format.length==0){
                item.setMessage_type(TEXT_MESSAGE);
            }else {
                switch (sfile_format[0]){
                    case "image": item.setMessage_type(IMAGE_MESSAGE);break;
                    case "audio": item.setMessage_type(AUDIO_MESSAGE);break;
                    case "video": item.setMessage_type(VIDEO_MESSAGE);break;
                    case "file": item.setMessage_type(FILE_MESSAGE);break;
                }

            }


            feedItem.add(item);
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("dialog_id", Dialog_id);
            jsonObject.put("sender_id", MY_USER_ID);
            jsonObject.put("type", "read");
            jsonObject.put("os", "android");
            mSocket.emit("read", jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        adapter.notifyDataSetChanged();

        listView.setSelection(adapter.getCount()-1);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void DeleteChat(int chat_id,int position){
        AndroidNetworking.delete(chat+TOKEN+"&chat_id="+chat_id+"&partner_id="+USER_ID)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("delete",response+"");
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        DialogDetail dialogDetail1 = realm.where(DialogDetail.class).equalTo("chat_id", chat_id).and().equalTo("dialog_id", Dialog_id).findFirst();

                        dialogDetail1.deleteFromRealm();


                    }
                });
//                feedItem.remove(position);
                adapter.deleteItem(position);

            }

            @Override
            public void onError(ANError anError) {
//                Toasty.error(ActivityChatMessage.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });

    }






    public void GetDataFromJSON(String url) {

        if (PAGE_p==1){
            feedItem.clear();
        }

        Log.d("PAGE_pppp",PAGE_p+"  **  "+PAGE_m);



        Log.d("URL",url+PER_PAGE+"&page="+PAGE_p);
        AndroidNetworking.get(url+PER_PAGE+"&page="+PAGE_p)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("messageList",response+"*");
                try {
                    if(response.getBoolean("status")){

                        total_page = response.getInt("total_page");
                        JSONArray list = response.getJSONArray("data");
                        for (int i = list.length()-1; i >=0; i--) {
                            JSONObject jsonObject = (JSONObject) list.get(i);

                            DialogDetail dialogDetail1 = realm.where(DialogDetail.class).equalTo("chat_id", NurInt(jsonObject, "chat_id")).findFirst();
                            boolean is_playaudio = dialogDetail1 != null && dialogDetail1.isIs_playaudio();
                            JSONObject sender = jsonObject.getJSONObject("sender");
                            JSONArray file_list = jsonObject.getJSONArray("file_list");
                            int f_l = file_list.length();
                            String file_url = "";
                            String[] sfile_url = new String[f_l];
                            String file_format = "";
                            String[] sfile_format= new String[f_l];
                            String file_name = "";
                            String[] sfile_name= new String[f_l];
                            int file_time = 0;
                            for (int f = 0; f < f_l; f++) {
                                JSONObject file = (JSONObject) file_list.get(f);
                                if (f > 0) {
                                    file_url += ",";
                                    file_format += ",";
                                    file_name += ",";
                                }
                                file_url += file.getString("file_url");
                                file_format += file.getString("file_format");
                                file_name += file.getString("file_name");
                                file_time = file.getInt("file_time");
                                sfile_url[f]=file.getString("file_url");
                                sfile_format[f]=file.getString("file_format");
                                sfile_name[f]=file.getString("file_name");
                            }
                            int answer_user_id = 0;
                            String answer_file_url = "";
                            String answer_file_format = "";
                            String answer_file_name = "";
                            int answer_file_time = 0;
                            int answer_is_has_file = 0;
                            int answer_is_contact = 0;
                            String answer_contact_name="";
                            String answer_name = " ", answer_text = " ";
                            if (jsonObject.getInt("answer_chat_id") != 0) {
                                JSONObject answer_chat = jsonObject.getJSONObject("answer_chat");
                                answer_name = NurString(answer_chat, "nickname");
                                answer_is_has_file = answer_chat.getInt("is_has_file");
                                answer_is_contact = answer_chat.getInt("is_contact");
                                if(answer_is_contact!=0){
                                    answer_contact_name = answer_chat.getJSONObject("contact").getString("contact_name");
                                }
                                answer_user_id = answer_chat.getInt("user_id");
                                answer_text = answer_chat.getString("chat_text");
                                if (answer_is_has_file != 0) {
                                    JSONArray answer_file_list = answer_chat.getJSONArray("file_list");
                                    if (answer_file_list.length() > 0) {
                                        JSONObject answer_file = (JSONObject) answer_file_list.get(0);
                                        answer_file_url = answer_file.getString("file_url");
                                        answer_file_format = answer_file.getString("file_format");
                                        answer_file_name = answer_file.getString("file_name");
                                        answer_file_time = answer_file.getInt("file_time");
                                    }
                                }
                            }
                            JSONObject recipient = jsonObject.optJSONObject("recipient");
                            boolean has_recipient;
                            int recipient_user_id = 0;
                            String recipient_phone = "";
                            String recipient_chat_name = "";
                            String recipient_avatar = "";
                            if (recipient != null) {
                                if (recipient.has("user_id")) {
                                    has_recipient = true;
                                    recipient_user_id = NurInt(recipient, "user_id");
                                    if (!NurString(recipient, "nickname").equals(""))
                                        recipient_chat_name = NurString(recipient, "nickname");
                                    else if (!NurString(recipient, "chat_name").equals(""))
                                        recipient_chat_name = NurString(recipient, "chat_name");
                                    else
                                        recipient_chat_name = NurString(recipient, "phone");
                                    recipient_avatar = NurString(recipient, "avatar");
                                    recipient_phone = NurString(recipient, "phone");
                                } else {
                                    has_recipient = false;
                                }
                            } else {
                                has_recipient = false;
                            }
                            int is_contact = NurInt(jsonObject, "is_contact");
                            int is_exist=0,contact_user_id=0;
                            String contact_name = "", contact_phone = "";
                            if (is_contact == 1) {
                                JSONObject contact = jsonObject.getJSONObject("contact");
                                contact_name = NurString(contact, "contact_name");
                                contact_phone = NurString(contact, "contact_phone");
                                is_exist = NurInt(contact, "is_exist");
                                contact_user_id = NurInt(contact, "user_id");
                            }
                            int is_has_link = NurInt(jsonObject, "is_has_link");
                            String link_description=" ", link_title=" ",link_image=" ";
                            if (is_has_link==1){
                                JSONObject meta_tags = jsonObject.getJSONObject("meta_tags");
                                link_description = meta_tags.getString("description");
                                link_title = meta_tags.getString("title");
                                link_image = meta_tags.getString("image");
                            }

                            int is_sticker = NurInt(jsonObject, "is_sticker");
                            String sticker_image="";
                            if (is_sticker==1){
                                JSONObject sticker = jsonObject.getJSONObject("sticker");
                                sticker_image =  NurString(sticker, "sticker_image");
                            }

                            boolean save_storage = NurInt(sender, "user_id") == MY_USER_ID || dialogDetail1 != null && dialogDetail1.isSave_storage();
                            DialogDetail dialogDetail = new DialogDetail(
                                    Dialog_id, NurInt(jsonObject, "chat_id"), NurInt(jsonObject, "is_read"), NurInt(jsonObject, "is_has_file"), NurInt(sender, "user_id"), file_time, NurInt(jsonObject, "answer_chat_id"), answer_user_id,
                                    NurString(jsonObject, "chat_kind"), NurString(jsonObject, "action_name"), NurString(jsonObject, "chat_text"), NurString(jsonObject, "chat_time"), NurString(jsonObject, "chat_date"), NurString(sender, "user_name"),
                                    NurString(sender, "avatar"), NurString(sender, "phone"), answer_name, answer_text, file_url, file_format, file_name, save_storage, System.currentTimeMillis(), answer_file_url, answer_file_format, answer_file_name, answer_file_time, answer_is_has_file, false, NurInt(jsonObject, "is_resend"),
                                    has_recipient, recipient_user_id, recipient_phone, recipient_chat_name, recipient_avatar,  NurInt(jsonObject, "view_count"), is_contact, contact_name, contact_phone, is_playaudio, is_exist,contact_user_id,is_has_link,link_description,link_title,link_image,answer_is_contact,answer_contact_name,is_sticker,sticker_image);


                            FeedItem item = new FeedItem();
                            item.setChat_id(NurInt(jsonObject, "chat_id"));
                            item.setIs_read(NurInt(jsonObject, "is_read"));
                            item.setIs_has_file(NurInt(jsonObject, "is_has_file"));
                            item.setUser_id(NurInt(sender, "user_id"));
                            item.setFile_time(file_time);
                            item.setAnswer_chat_id(NurInt(jsonObject, "answer_chat_id"));
                            item.setAnswer_user_id(answer_user_id);
                            item.setChat_kind(NurString(jsonObject, "chat_kind"));
                            item.setAction_name(NurString(jsonObject, "action_name"));
                            item.setChat_text(NurString(jsonObject, "chat_text"));
                            item.setChat_time(NurString(jsonObject, "chat_time"));
                            item.setChat_date(NurString(jsonObject, "chat_date"));
                            item.setName(NurString(sender, "nickname"));
                            item.setAvatar(NurString(sender, "avatar"));
                            item.setPhone(NurString(sender, "phone"));
                            item.setAnswer_name(answer_name);
                            item.setAnswer_text(answer_text);
                            item.setView_count(NurInt(jsonObject, "view_count"));

                            item.setAnswer_file_url(answer_file_url);
                            item.setAnswer_file_format(answer_file_format);
                            item.setAnswer_file_name(answer_file_name);
                            item.setAnswer_is_has_file(answer_is_has_file);
                            item.setAnswer_is_contact(answer_is_contact);
                            item.setAnswer_contact_name(answer_contact_name);
                            item.setAnswer_file_time(answer_file_time);


                            item.setFile_url(sfile_url);
                            item.setFile_format(sfile_format);
                            item.setFile_name(sfile_name);

                            item.setSave_storage(save_storage);
                            item.setIs_upload(false);
                            item.setIs_resend(NurInt(jsonObject, "is_resend"));

                            item.setIs_contact(is_contact);
                            item.setContact_name(contact_name);
                            item.setContact_phone(contact_phone);
                            item.setIs_playaudio(is_playaudio);
                            item.setIs_exist(is_exist);
                            item.setContact_user_id(contact_user_id);
                            item.setIs_has_link(is_has_link);
                            item.setLink_description(link_description);
                            item.setLink_title(link_title);
                            item.setLink_image(link_image);
                            item.setIs_sticker(is_sticker);
                            item.setSticker_image(sticker_image);



                            if (NurString(jsonObject, "chat_kind").equals("action")){
                                item.setMessage_type(ACTION_MESSAGE);
                            }else if (is_contact==1){
                                item.setMessage_type(CONTACT_MESSAGE);
                            }else if (is_sticker==1){
                                item.setMessage_type(STICKER_MESSAGE);
                            }else if (NurInt(jsonObject, "is_has_file")!=1 || sfile_format.length==0){
                                item.setMessage_type(TEXT_MESSAGE);
                            }else {
                                switch (sfile_format[0]){
                                    case "image": item.setMessage_type(IMAGE_MESSAGE);break;
                                    case "audio": item.setMessage_type(AUDIO_MESSAGE);break;
                                    case "video": item.setMessage_type(VIDEO_MESSAGE);break;
                                    case "file": item.setMessage_type(FILE_MESSAGE);break;
                                }

                            }


                            feedItem.add(0,item);
                        }



                       adapter.notifyDataSetChanged();


                        if(PAGE_p != 1) {
                            listView.setSelection(PER_PAGE-1);
                        }else {
                            listView.setSelection(adapter.getCount()-1);
                        }


                    }else {
                        Toasty.error(ActivityChatMessage.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
//                Toasty.error(ActivityChatMessage.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });


    }


    public void GetDataFromJSONMinus(String url) {


        Log.d("PAGE_mmmm",PAGE_p+"  **  "+PAGE_m);

        Log.d("URL",url+PER_PAGE+"&page="+PAGE_m);
        AndroidNetworking.get(url+PER_PAGE+"&page="+PAGE_m)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("messageList",response+"*");
                try {
                    if(response.getBoolean("status")){

                        total_page = response.getInt("total_page");
                        JSONArray list = response.getJSONArray("data");
                        for (int i = 0; i <list.length(); i++) {
                            JSONObject jsonObject = (JSONObject) list.get(i);

                            DialogDetail dialogDetail1 = realm.where(DialogDetail.class).equalTo("chat_id", NurInt(jsonObject, "chat_id")).findFirst();
                            boolean is_playaudio = dialogDetail1 != null && dialogDetail1.isIs_playaudio();
                            JSONObject sender = jsonObject.getJSONObject("sender");
                            JSONArray file_list = jsonObject.getJSONArray("file_list");
                            int f_l = file_list.length();
                            String file_url = "";
                            String[] sfile_url = new String[f_l];
                            String file_format = "";
                            String[] sfile_format= new String[f_l];
                            String file_name = "";
                            String[] sfile_name= new String[f_l];
                            int file_time = 0;
                            for (int f = 0; f < f_l; f++) {
                                JSONObject file = (JSONObject) file_list.get(f);
                                if (f > 0) {
                                    file_url += ",";
                                    file_format += ",";
                                    file_name += ",";
                                }
                                file_url += file.getString("file_url");
                                file_format += file.getString("file_format");
                                file_name += file.getString("file_name");
                                file_time = file.getInt("file_time");
                                sfile_url[f]=file.getString("file_url");
                                sfile_format[f]=file.getString("file_format");
                                sfile_name[f]=file.getString("file_name");
                            }
                            int answer_user_id = 0;
                            String answer_file_url = "";
                            String answer_file_format = "";
                            String answer_file_name = "";
                            int answer_file_time = 0;
                            int answer_is_has_file = 0;
                            int answer_is_contact = 0;
                            String answer_contact_name = "";
                            String answer_name = " ", answer_text = " ";
                            if (jsonObject.getInt("answer_chat_id") != 0) {
                                JSONObject answer_chat = jsonObject.getJSONObject("answer_chat");
                                answer_name = NurString(answer_chat, "nickname");
                                answer_is_has_file = answer_chat.getInt("is_has_file");
                                answer_is_contact = answer_chat.getInt("is_contact");
                                if(answer_is_contact!=0){
                                    answer_contact_name = answer_chat.getJSONObject("contact").getString("contact_name");
                                } answer_user_id = answer_chat.getInt("user_id");
                                answer_text = answer_chat.getString("chat_text");
                                if (answer_is_has_file != 0) {
                                    JSONArray answer_file_list = answer_chat.getJSONArray("file_list");
                                    if (answer_file_list.length() > 0) {
                                        JSONObject answer_file = (JSONObject) answer_file_list.get(0);
                                        answer_file_url = answer_file.getString("file_url");
                                        answer_file_format = answer_file.getString("file_format");
                                        answer_file_name = answer_file.getString("file_name");
                                        answer_file_time = answer_file.getInt("file_time");
                                    }
                                }
                            }
                            JSONObject recipient = jsonObject.optJSONObject("recipient");
                            boolean has_recipient;
                            int recipient_user_id = 0;
                            String recipient_phone = "";
                            String recipient_chat_name = "";
                            String recipient_avatar = "";
                            if (recipient != null) {
                                if (recipient.has("user_id")) {
                                    has_recipient = true;
                                    recipient_user_id = NurInt(recipient, "user_id");
                                    if (!NurString(recipient, "nickname").equals(""))
                                        recipient_chat_name = NurString(recipient, "nickname");
                                    else if (!NurString(recipient, "chat_name").equals(""))
                                        recipient_chat_name = NurString(recipient, "chat_name");
                                    else
                                        recipient_chat_name = NurString(recipient, "phone");
                                    recipient_avatar = NurString(recipient, "avatar");
                                    recipient_phone = NurString(recipient, "phone");
                                } else {
                                    has_recipient = false;
                                }
                            } else {
                                has_recipient = false;
                            }
                            int is_contact = NurInt(jsonObject, "is_contact");
                            int is_exist=0,contact_user_id=0;
                            String contact_name = "", contact_phone = "";
                            if (is_contact == 1) {
                                JSONObject contact = jsonObject.getJSONObject("contact");
                                contact_name = NurString(contact, "contact_name");
                                contact_phone = NurString(contact, "contact_phone");
                                is_exist = NurInt(contact, "is_exist");
                                contact_user_id = NurInt(contact, "user_id");
                            }
                            int is_has_link = NurInt(jsonObject, "is_has_link");
                            String link_description=" ", link_title=" ",link_image=" ";
                            if (is_has_link==1){
                                JSONObject meta_tags = jsonObject.getJSONObject("meta_tags");
                                link_description = meta_tags.getString("description");
                                link_title = meta_tags.getString("title");
                                link_image = meta_tags.getString("image");
                            }

                            int is_sticker = NurInt(jsonObject, "is_sticker");
                            String sticker_image="";
                            if (is_sticker==1){
                                JSONObject sticker = jsonObject.getJSONObject("sticker");
                                sticker_image =  NurString(sticker, "sticker_image");
                            }

                            boolean save_storage = NurInt(sender, "user_id") == MY_USER_ID || dialogDetail1 != null && dialogDetail1.isSave_storage();
                            DialogDetail dialogDetail = new DialogDetail(
                                    Dialog_id, NurInt(jsonObject, "chat_id"), NurInt(jsonObject, "is_read"), NurInt(jsonObject, "is_has_file"), NurInt(sender, "user_id"), file_time, NurInt(jsonObject, "answer_chat_id"), answer_user_id,
                                    NurString(jsonObject, "chat_kind"), NurString(jsonObject, "action_name"), NurString(jsonObject, "chat_text"), NurString(jsonObject, "chat_time"), NurString(jsonObject, "chat_date"), NurString(sender, "user_name"),
                                    NurString(sender, "avatar"), NurString(sender, "phone"), answer_name, answer_text, file_url, file_format, file_name, save_storage, System.currentTimeMillis(), answer_file_url, answer_file_format, answer_file_name, answer_file_time, answer_is_has_file, false, NurInt(jsonObject, "is_resend"),
                                    has_recipient, recipient_user_id, recipient_phone, recipient_chat_name, recipient_avatar,  NurInt(jsonObject, "view_count"), is_contact, contact_name, contact_phone, is_playaudio, is_exist,contact_user_id,is_has_link,link_description,link_title,link_image,answer_is_contact,answer_contact_name,is_sticker,sticker_image);



                            FeedItem item = new FeedItem();
                            item.setChat_id(NurInt(jsonObject, "chat_id"));
                            item.setIs_read(NurInt(jsonObject, "is_read"));
                            item.setIs_has_file(NurInt(jsonObject, "is_has_file"));
                            item.setUser_id(NurInt(sender, "user_id"));
                            item.setFile_time(file_time);
                            item.setAnswer_chat_id(NurInt(jsonObject, "answer_chat_id"));
                            item.setAnswer_user_id(answer_user_id);
                            item.setChat_kind(NurString(jsonObject, "chat_kind"));
                            item.setAction_name(NurString(jsonObject, "action_name"));
                            item.setChat_text(NurString(jsonObject, "chat_text"));
                            item.setChat_time(NurString(jsonObject, "chat_time"));
                            item.setChat_date(NurString(jsonObject, "chat_date"));
                            item.setName(NurString(sender, "nickname"));
                            item.setAvatar(NurString(sender, "avatar"));

                            item.setPhone(NurString(sender, "phone"));
                            item.setAnswer_name(answer_name);
                            item.setAnswer_text(answer_text);
                            item.setView_count(NurInt(jsonObject, "view_count"));

                            item.setAnswer_file_url(answer_file_url);
                            item.setAnswer_file_format(answer_file_format);
                            item.setAnswer_file_name(answer_file_name);
                            item.setAnswer_is_has_file(answer_is_has_file);
                            item.setAnswer_is_contact(answer_is_contact);
                            item.setAnswer_contact_name(answer_contact_name);
                            item.setAnswer_file_time(answer_file_time);


                            item.setFile_url(sfile_url);
                            item.setFile_format(sfile_format);
                            item.setFile_name(sfile_name);

                            item.setSave_storage(save_storage);
                            item.setIs_upload(false);
                            item.setIs_resend(NurInt(jsonObject, "is_resend"));

                            item.setIs_contact(is_contact);
                            item.setContact_name(contact_name);
                            item.setContact_phone(contact_phone);
                            item.setIs_playaudio(is_playaudio);
                            item.setIs_exist(is_exist);
                            item.setContact_user_id(contact_user_id);
                            item.setIs_has_link(is_has_link);
                            item.setLink_description(link_description);
                            item.setLink_title(link_title);
                            item.setLink_image(link_image);
                            item.setIs_sticker(is_sticker);
                            item.setSticker_image(sticker_image);


                            if (NurString(jsonObject, "chat_kind").equals("action")){
                                item.setMessage_type(ACTION_MESSAGE);
                            }else if (is_contact==1){
                                item.setMessage_type(CONTACT_MESSAGE);
                            }else if (is_sticker==1){
                                item.setMessage_type(STICKER_MESSAGE);
                            }else if (NurInt(jsonObject, "is_has_file")!=1){
                                item.setMessage_type(TEXT_MESSAGE);
                            }else {
                                switch (sfile_format[0]){
                                    case "image": item.setMessage_type(IMAGE_MESSAGE);break;
                                    case "audio": item.setMessage_type(AUDIO_MESSAGE);break;
                                    case "video": item.setMessage_type(VIDEO_MESSAGE);break;
                                    case "file": item.setMessage_type(FILE_MESSAGE);break;
                                }

                            }


                            feedItem.add(item);
                        }

                       listView.setSelection(minus_visible_page+1);



                    }else {
                        Toasty.error(ActivityChatMessage.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
//                Toasty.error(ActivityChatMessage.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });


    }

    public void GetDataFromJSONByPAGE( boolean first,int page, String url, int chat_id,int K) {

        Log.d("PAGE_by_page",PAGE_p+"  **  "+PAGE_m);


        Log.d("pppppp"+chat_id+"    -",url+PER_PAGE+"&page="+page);
        AndroidNetworking.get(url+PER_PAGE+"&page="+page)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("messageList",response+"*");
                try {
                    if(response.getBoolean("status")){
                       if (first){
                           feedItem.clear();
                       }


                        JSONArray list = response.getJSONArray("data");
                        int k=K;
                        int length = list.length();
                        for (int i = 0; i <length; i++) {
                            JSONObject jsonObject = (JSONObject) list.get(i);
                            if (jsonObject.getInt("chat_id")==chat_id){
                                if(!first){
                                    k = i+PER_PAGE;
                                }else {
                                    k = i;
                                }

                            }
                            DialogDetail dialogDetail1 = realm.where(DialogDetail.class).equalTo("chat_id", NurInt(jsonObject, "chat_id")).findFirst();
                            boolean is_playaudio = dialogDetail1 != null && dialogDetail1.isIs_playaudio();
                            JSONObject sender = jsonObject.getJSONObject("sender");
                            JSONArray file_list = jsonObject.getJSONArray("file_list");
                            int f_l = file_list.length();
                            String[] sfile_url = new String[f_l];
                            String[] sfile_format= new String[f_l];
                            String[] sfile_name= new String[f_l];
                            int file_time = 0;
                            for (int f = 0; f < f_l; f++) {
                                JSONObject file = (JSONObject) file_list.get(f);
                               file_time = file.getInt("file_time");
                                sfile_url[f]=file.getString("file_url");
                                sfile_format[f]=file.getString("file_format");
                                sfile_name[f]=file.getString("file_name");
                            }
                            int answer_user_id = 0;
                            String answer_file_url = "";
                            String answer_file_format = "";
                            String answer_file_name = "";
                            int answer_file_time = 0;
                            int answer_is_has_file = 0;
                            int answer_is_contact = 0;
                            String answer_contact_name = "";
                            String answer_name = " ", answer_text = " ";
                            if (jsonObject.getInt("answer_chat_id") != 0) {
                                JSONObject answer_chat = jsonObject.getJSONObject("answer_chat");
                                answer_name = NurString(answer_chat, "nickname");
                                answer_is_has_file = answer_chat.getInt("is_has_file");
                                answer_is_contact = answer_chat.getInt("is_contact");
                                if(answer_is_contact!=0){
                                    answer_contact_name = answer_chat.getJSONObject("contact").getString("contact_name");
                                } answer_user_id = answer_chat.getInt("user_id");
                                answer_text = answer_chat.getString("chat_text");
                                if (answer_is_has_file != 0) {
                                    JSONArray answer_file_list = answer_chat.getJSONArray("file_list");
                                    if (answer_file_list.length() > 0) {
                                        JSONObject answer_file = (JSONObject) answer_file_list.get(0);
                                        answer_file_url = answer_file.getString("file_url");
                                        answer_file_format = answer_file.getString("file_format");
                                        answer_file_name = answer_file.getString("file_name");
                                        answer_file_time = answer_file.getInt("file_time");
                                    }
                                }
                            }

                            int is_contact = NurInt(jsonObject, "is_contact");
                            int is_exist=0,contact_user_id=0;
                            String contact_name = "", contact_phone = "";
                            if (is_contact == 1) {
                                JSONObject contact = jsonObject.getJSONObject("contact");
                                contact_name = NurString(contact, "contact_name");
                                contact_phone = NurString(contact, "contact_phone");
                                is_exist = NurInt(contact, "is_exist");
                                contact_user_id = NurInt(contact, "user_id");
                            }

                            int is_has_link = NurInt(jsonObject, "is_has_link");
                            String link_description=" ", link_title=" ",link_image=" ";
                            if (is_has_link==1){
                                JSONObject meta_tags = jsonObject.getJSONObject("meta_tags");
                                link_description = meta_tags.getString("description");
                                link_title = meta_tags.getString("title");
                                link_image = meta_tags.getString("image");
                            }

                            boolean save_storage = NurInt(sender, "user_id") == MY_USER_ID || dialogDetail1 != null && dialogDetail1.isSave_storage();


                            FeedItem item = new FeedItem();
                            item.setChat_id(NurInt(jsonObject, "chat_id"));
                            item.setIs_read(NurInt(jsonObject, "is_read"));
                            item.setIs_has_file(NurInt(jsonObject, "is_has_file"));
                            item.setUser_id(NurInt(sender, "user_id"));
                            item.setFile_time(file_time);
                            item.setAnswer_chat_id(NurInt(jsonObject, "answer_chat_id"));
                            item.setAnswer_user_id(answer_user_id);
                            item.setChat_kind(NurString(jsonObject, "chat_kind"));
                            item.setAction_name(NurString(jsonObject, "action_name"));
                            item.setChat_text(NurString(jsonObject, "chat_text"));
                            item.setChat_time(NurString(jsonObject, "chat_time"));
                            item.setChat_date(NurString(jsonObject, "chat_date"));
                            item.setName(NurString(sender, "nickname"));
                            item.setAvatar(NurString(sender, "avatar"));
                            item.setPhone(NurString(sender, "phone"));
                            item.setAnswer_name(answer_name);
                            item.setAnswer_text(answer_text);

                            item.setAnswer_file_url(answer_file_url);
                            item.setAnswer_file_format(answer_file_format);
                            item.setAnswer_file_name(answer_file_name);
                            item.setAnswer_is_has_file(answer_is_has_file);
                            item.setAnswer_is_contact(answer_is_contact);
                            item.setAnswer_contact_name(answer_contact_name);
                            item.setAnswer_file_time(answer_file_time);


                            item.setFile_url(sfile_url);
                            item.setFile_format(sfile_format);
                            item.setFile_name(sfile_name);

                            item.setSave_storage(save_storage);
                            item.setIs_upload(false);
                            item.setIs_resend(NurInt(jsonObject, "is_resend"));
                            item.setView_count(NurInt(jsonObject, "view_count"));

                            item.setIs_contact(is_contact);
                            item.setContact_name(contact_name);
                            item.setContact_phone(contact_phone);
                            item.setIs_playaudio(is_playaudio);
                            item.setIs_exist(is_exist);
                            item.setContact_user_id(contact_user_id);
                            item.setIs_has_link(is_has_link);
                            item.setLink_description(link_description);
                            item.setLink_title(link_title);
                            item.setLink_image(link_image);
                            int is_sticker = NurInt(jsonObject, "is_sticker");
                            String sticker_image="";
                            if (is_sticker==1){
                                JSONObject sticker = jsonObject.getJSONObject("sticker");
                                sticker_image =  NurString(sticker, "sticker_image");
                            }
                            item.setIs_sticker(is_sticker);
                            item.setSticker_image(sticker_image);


                            if (NurString(jsonObject, "chat_kind").equals("action")){
                                item.setMessage_type(ACTION_MESSAGE);
                            }else if (is_contact==1){
                                item.setMessage_type(CONTACT_MESSAGE);
                            }else if (is_sticker==1){
                                item.setMessage_type(STICKER_MESSAGE);
                            }else if (NurInt(jsonObject, "is_has_file")!=1){
                                item.setMessage_type(TEXT_MESSAGE);
                            }else if (f_l>0){
                                switch (sfile_format[0]){
                                    case "image": item.setMessage_type(IMAGE_MESSAGE);break;
                                    case "audio": item.setMessage_type(AUDIO_MESSAGE);break;
                                    case "video": item.setMessage_type(VIDEO_MESSAGE);break;
                                    case "file": item.setMessage_type(FILE_MESSAGE);break;
                                }

                            }


                            feedItem.add(item);
                        }
                        if (first){
                            GetDataFromJSONByPAGE(false,PAGE_p,url,chat_id,k);
                        }else {
                            Log.d("kkkkkkk",k+"*");
                            listView.setSelection(k);
                        }





                    }else {
                        Toasty.error(ActivityChatMessage.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
//                Toasty.error(ActivityChatMessage.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });


    }


    private void SetDetailDialogToList() {

//        Log.d("dialog_iddddddd", Dialog_id);
        feedItem.clear();
        realm.beginTransaction();
        RealmResults<DialogDetail> results = realm.where(DialogDetail.class).equalTo("dialog_id", Dialog_id).sort("chat_id").findAll();
        realm.commitTransaction();
        Log.d("TTTTTTT size",results.size()+"*");
        for (int i = 0; i<results.size();i++){
            DialogDetail dialogDetail = results.get(i);
            FeedItem item = new FeedItem();

            item.setDialog_id(dialogDetail.getDialog_id());
            item.setChat_id(dialogDetail.getChat_id());
            item.setIs_read(dialogDetail.getIs_read());
            item.setIs_has_file(dialogDetail.getIs_has_file());
            item.setUser_id(dialogDetail.getUser_id());
            item.setFile_time(dialogDetail.getFile_time());
            item.setAnswer_chat_id(dialogDetail.getAnswer_chat_id());
            item.setAnswer_user_id(dialogDetail.getAnswer_user_id());
            item.setChat_kind(dialogDetail.getChat_kind());
            item.setAction_name(dialogDetail.getAction_name());
            item.setChat_text(dialogDetail.getChat_text());
            item.setChat_time(dialogDetail.getChat_time());
            item.setChat_date(dialogDetail.getChat_date());
            item.setName(dialogDetail.getUser_name());
            item.setAvatar(dialogDetail.getAvatar());
            item.setPhone(dialogDetail.getPhone());
            item.setAnswer_name(dialogDetail.getAnswer_name());
            item.setAnswer_text(dialogDetail.getAnswer_text());
            item.setAnswer_text(dialogDetail.getAnswer_text());

            item.setAnswer_file_url(dialogDetail.getAnswer_file_url());
            item.setAnswer_file_format(dialogDetail.getAnswer_file_format());
            item.setAnswer_file_name(dialogDetail.getAnswer_file_name());
            item.setAnswer_is_has_file(dialogDetail.getAnswer_is_has_file());
            item.setAnswer_file_time(dialogDetail.getAnswer_file_time());
//            item.setView_count(dialogDetail);




            String [] File_url= dialogDetail.getFile_url().split(",");
            String [] File_format = dialogDetail.getFile_format().split(",");
            String [] File_name = dialogDetail.getFile_name().split(",");

            item.setFile_url(File_url);
            item.setFile_format(File_format);
            item.setFile_name(File_name);

            item.setSave_storage(dialogDetail.isSave_storage());
            item.setIs_upload(dialogDetail.isIs_upload());
            item.setIs_resend(dialogDetail.getIs_resend());

            item.setIs_contact(dialogDetail.getIs_contact());
            item.setContact_name(dialogDetail.getContact_name());
            item.setContact_phone(dialogDetail.getContact_phone());
            item.setContact_phone(dialogDetail.getContact_phone());
            item.setIs_playaudio(dialogDetail.isIs_playaudio());
            item.setIs_exist(dialogDetail.getIs_exist());
            item.setContact_user_id(dialogDetail.getContact_user_id());
            item.setIs_has_link(dialogDetail.getIs_has_link());
            item.setLink_description(dialogDetail.getLink_description());
            item.setLink_title(dialogDetail.getLink_title());
            item.setLink_image(dialogDetail.getLink_image());
            item.setIs_sticker(dialogDetail.getIs_sticker());
            item.setSticker_image(dialogDetail.getSticker_image());


            if (item.getChat_kind().equals("action")){
                item.setMessage_type(ACTION_MESSAGE);
            }else if (item.getIs_contact()==1){
                item.setMessage_type(CONTACT_MESSAGE);
            }else if (item.getIs_sticker()==1){
                item.setMessage_type(STICKER_MESSAGE);
            }else if (dialogDetail.getIs_has_file()!=1){
                item.setMessage_type(TEXT_MESSAGE);
            }else {
                switch (File_format[0]){
                    case "image": item.setMessage_type(IMAGE_MESSAGE);break;
                    case "audio": item.setMessage_type(AUDIO_MESSAGE);break;
                    case "video": item.setMessage_type(VIDEO_MESSAGE);break;
                    case "file": item.setMessage_type(FILE_MESSAGE);break;
                }

            }


            feedItem.add(item);

        }


        adapter = new ChatAdapter(ActivityChatMessage.this,feedItem,USER_ID,Dialog_id,WHO);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        listView.setSelection(adapter.getCount() - 1);
        if(isOnline(ActivityChatMessage.this)){

            GetDataFromJSON(URL);
            Log.d("AAAAAAAaaaaa","YES");
        }else {
            Toasty.error(ActivityChatMessage.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();
        }

    }



    @Override
    protected void onPause() {
        Log.d("onPause","onPause");
        mSocket.off("message", onNewMessage);
        mSocket.off("online", onOnline);
        mSocket.off("read", read);
        mSocket.off("typing", onTyping);

        super.onPause();
    }


    @Override
    protected void onStop() {
        if (mediaPlayer != null) {
//            handler_online.removeCallbacks(runnable);
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;

        }
        super.onStop();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        if(selectedIds.size()>0){
            adapter.toggleSelectedClear();
            selectedIds.clear();
            action_bar.setVisibility(View.VISIBLE);
            selected_bar.setVisibility(View.GONE);
        }
        if (Is_mute==0){
            is_mute.setVisibility(View.GONE);
        }else {
            is_mute.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        SocketConnect();
    }

    private void SocketConnect() {
        ChatApplication app = (ChatApplication)getApplication();
        mSocket = app.getSocket();
        mSocket.on(Socket.EVENT_CONNECT,onConnect);
        mSocket.on("message", onNewMessage);
        mSocket.on("online", onOnline);
        mSocket.on("read", read);
        mSocket.on(Socket.EVENT_DISCONNECT,onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on("typing", onTyping);
        mSocket.connect();

        Log.d("handler_online","sssss");
        if (mSocket.connected()){

            handler_online.postDelayed(runnable, 10000);
            Log.d("handler_online","start");
        }



    }

    private void downloadFile (String format,String File_url, String File_name){


      File  myDir  = new File( Environment.getExternalStorageDirectory().toString() + "/.dalagram");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }

        File savefile = new File(myDir +"/"+format);

        Log.d("File_url",File_url);

        AndroidNetworking.download(File_url,savefile.getPath(),File_name)
                .setTag("downloadTest")
                .setPriority(Priority.HIGH)
                .build()
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                    }
                    @Override
                    public void onError(ANError anError) {
//                        Toasty.error(ActivityChatMessage.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                    }
                });

    }

    public void GetGroupUsers(String url) {

        AndroidNetworking.get(url)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("RRRRRR",response+" ");
                try {
                    if(response.getBoolean("status")){
                        user_response = response+" ";
                        Log.d("profile",response+"++");
                        JSONArray data = response.getJSONArray("data");
                        String usersCount;
                        if(data.length()==1){
                            usersCount = data.length()+" участник";
                        }else if(data.length()==2 || data.length()==3 || data.length()==4){
                            usersCount = data.length()+" участника";
                        }else {
                            usersCount = data.length()+" участников";
                        }

                        chat_text.setText(usersCount);
                    }else {

                        Toasty.error(ActivityChatMessage.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
//                Toasty.error(ActivityChatMessage.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onRefresh() {
        if (PAGE_p<total_page){
            PAGE_p++;
            if(isOnline(ActivityChatMessage.this)){
                GetDataFromJSON(URL);
            }
        }

        swipe.setRefreshing(false);
    }
}
