package kz.bgpro.www.dalagram.utils;

public class MessageType {


    public static final int MAX_LAYOUT_COUNT = 8;

    public static final int TEXT_MESSAGE = 0;
    public static final int IMAGE_MESSAGE = 1;
    public static final int AUDIO_MESSAGE = 2;
    public static final int VIDEO_MESSAGE = 3;
    public static final int CONTACT_MESSAGE = 4;
    public static final int FILE_MESSAGE = 5;
    public static final int ACTION_MESSAGE = 6;
    public static final int STICKER_MESSAGE = 7;

}
