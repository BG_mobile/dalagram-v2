package kz.bgpro.www.dalagram.activitys.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.ActivityGallereyZoomImage;
import kz.bgpro.www.dalagram.activitys.profile.ActivityMyProfile;
import kz.bgpro.www.dalagram.models.NewsItem;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.fragment.FragmentNews.newsmediaPlayer;
import static kz.bgpro.www.dalagram.utils.MyConstants.like;

/**
 * Created by nurbaqyt on 22.05.2018.
 */

public class GallereyListAdapter extends BaseAdapter {

    ArrayList<NewsItem> feed_item;
    NewsItem item;
    Activity activity;
    boolean is_my;
    int type;

    public GallereyListAdapter(ArrayList<NewsItem> feedItem, Activity activity, boolean my,int type) {
        this.activity = activity;
        this.feed_item = feedItem;
        this.is_my = my;
        this.type = type;
        newsmediaPlayer = new MediaPlayer();
    }


    @Override
    public int getCount() {
        if (type==2 && is_my){
            return feed_item.size()-1;
        }else {
            return feed_item.size();
        }

    }

    @Override
    public Object getItem(int location) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {

        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.add_gallerey)
        LinearLayout add_gallerey;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_gallerey_list, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        item = feed_item.get(position);

        if (type==1){
            if (is_my && position==0) {
                viewHolder.add_gallerey.setVisibility(View.VISIBLE);
                viewHolder.image.setVisibility(View.GONE);
            }else {
                viewHolder.add_gallerey.setVisibility(View.GONE);
                viewHolder.image.setVisibility(View.VISIBLE);
                if (item.getIs_has_file()==1){
                    Glide.with(activity).load(item.getFile_url()[0]).into(viewHolder.image);
                }
                else
                    Glide.with(activity).load(R.drawable.ic_like).into(viewHolder.image);

            }
        }else {
            viewHolder.add_gallerey.setVisibility(View.GONE);
            viewHolder.image.setVisibility(View.VISIBLE);
            if ( is_my){
                item = feed_item.get(position+1);
            }else {
                item = feed_item.get(position);
            }

            if (item.getIs_has_file()==1){
                Glide.with(activity).load(item.getFile_url()[0]).into(viewHolder.image);
            }
            else
                Glide.with(activity).load(R.drawable.ic_like).into(viewHolder.image);


        }


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                if (type==1){
                    if (is_my){
                        if (position!=0){
                            Intent intent = new Intent(activity, ActivityGallereyZoomImage.class);
                            intent.putExtra("feed",feed_item);
                            intent.putExtra("position",position);
                            activity.startActivity(intent);
                        }else {
                            ((ActivityMyProfile)activity).AddGalarey();
                        }
                    }else {
                        Intent intent = new Intent(activity, ActivityGallereyZoomImage.class);
                        intent.putExtra("feed",feed_item);
                        intent.putExtra("position",position);
                        activity.startActivity(intent);
                    }
                }else {
                    Intent intent = new Intent(activity, ActivityGallereyZoomImage.class);
                    intent.putExtra("feed",feed_item);
                    if (is_my){
                        intent.putExtra("position",position+1);
                    }else {
                        intent.putExtra("position",position);
                    }
                    activity.startActivity(intent);
                }



            }
        });
        return convertView;
    }
}
