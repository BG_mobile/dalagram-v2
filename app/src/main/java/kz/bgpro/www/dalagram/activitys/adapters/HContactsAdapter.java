package kz.bgpro.www.dalagram.activitys.adapters;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.pavlospt.roundedletterview.RoundedLetterView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.models.FeedItem;


/**
 * Created by nurbaqyt on 22.05.2018.
 */

public class HContactsAdapter extends BaseAdapter {

    ArrayList<FeedItem> feed_item;
    FeedItem item;
    Activity activity;

    public HContactsAdapter(ArrayList<FeedItem> feedItem, Activity activity) {
        this.activity = activity;
        this.feed_item = feedItem;
    }


    @Override
    public int getCount() {
        return feed_item.size();
    }

    @Override
    public Object getItem(int location) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {

        @BindView(R.id.avatar_tv)
        RoundedLetterView avatar_tv;

        @BindView(R.id.avatar)
        CircleImageView avatar;

        @BindView(R.id.name)
        TextView name;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_hcontacts_list, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        item = feed_item.get(position);
        viewHolder.name.setText(item.getName());

        if(TextUtils.isEmpty(item.getAvatar())){
            viewHolder.avatar.setVisibility(View.GONE);
            viewHolder.avatar_tv.setVisibility(View.VISIBLE);
            viewHolder.avatar_tv.setTitleText(item.getName().charAt(0)+"");
        }else {
            viewHolder.avatar.setVisibility(View.VISIBLE);
            viewHolder.avatar_tv.setVisibility(View.GONE);
            Glide.with(activity).load(item.getAvatar()).into(viewHolder.avatar);
        }


        return convertView;
    }
}
