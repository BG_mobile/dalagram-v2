package kz.bgpro.www.dalagram.Auth;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.MainActivity;
import kz.bgpro.www.dalagram.R;

import static kz.bgpro.www.dalagram.utils.MyConstants.PREFS_NAME;
import static kz.bgpro.www.dalagram.utils.MyConstants.profile;

public class ActivityEmail extends Activity {


    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.password)
    EditText password;

    @BindView(R.id.re_password)
    EditText re_password;

    SharedPreferences settings;

    @BindString(R.string.invalid_password)
    String invalid_password;

    @BindString(R.string.invalid_email)
    String invalid_email;

    @BindString(R.string.input_all)
    String input_all;

    String nick_name,first_name,second_name,birthday,status;
    int is_male;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        ButterKnife.bind(this);
        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        Intent intent = getIntent();

        nick_name = intent.getStringExtra("nick_name");
        first_name = intent.getStringExtra("first_name");
        second_name = intent.getStringExtra("second_name");
        birthday = intent.getStringExtra("birthday");
        status = intent.getStringExtra("status");
        is_male = intent.getIntExtra("is_male",1);




    }

    @OnClick(R.id.next)
    void next() {
        String Email = email.getText().toString();
        String Password = password.getText().toString();
        String Re_password = re_password.getText().toString();

        if(isEmailValid(email)) {

            if (!TextUtils.isEmpty(Email) && !TextUtils.isEmpty(Password) && !TextUtils.isEmpty(Re_password)) {

                if (Password.equals(Re_password)) {
                    String token = settings.getString("token","");
                    Log.d("111token",profile+token);
                    AndroidNetworking.post(profile+token)
                            .addBodyParameter("user_status", status)
                            .addBodyParameter("user_name", first_name)
                            .addBodyParameter("last_name", second_name)
                            .addBodyParameter("nickname", nick_name)
                            .addBodyParameter("is_male", is_male+"")
                            .addBodyParameter("birth_date", birthday)
                            .addBodyParameter("email", Email)
                            .addBodyParameter("password", Password)
                            .addBodyParameter("confirm_password", Re_password)
                            .build().getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getBoolean("status")) {
                                    SharedPreferences.Editor editor = settings.edit();
                                    editor.putString("user_name", nick_name);
                                    editor.putString("user_status", status);
                                    editor.putString("email", Email);
                                    editor.commit();



                                    Toasty.success(ActivityEmail.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(ActivityEmail.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                    finish();
                                } else {
                                    Toasty.error(ActivityEmail.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toasty.error(ActivityEmail.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                        }
                    });
                } else {
                    Toasty.info(this, invalid_password, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toasty.info(this,input_all , Toast.LENGTH_SHORT).show();
            }
        }else {
            Toasty.info(this, invalid_email, Toast.LENGTH_SHORT).show();
        }

    }

    public static boolean isEmailValid(EditText email) {
        String mail = email.getText().toString();
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = mail;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }



}
