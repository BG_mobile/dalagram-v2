package kz.bgpro.www.dalagram.News.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.vanniktech.emoji.EmojiTextView;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.News.ActivitySingleNews;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.ActivityVideoPlayer;
import kz.bgpro.www.dalagram.activitys.ActivityZoomImage;
import kz.bgpro.www.dalagram.activitys.profile.ActivityMyProfile;
import kz.bgpro.www.dalagram.models.NewsItem;
import kz.bgpro.www.dalagram.utils.MyBounceInterpolator;
import org.json.JSONException;
import org.json.JSONObject;

import static io.realm.internal.SyncObjectServerFacade.getApplicationContext;
import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.News.ActivitySingleNews.mediaPlayernews;
import static kz.bgpro.www.dalagram.utils.MyConstants.like;

/**
 * Created by nurbaqyt on 22.05.2018.
 */

public class CommentListAdapter extends BaseAdapter {

    ArrayList<NewsItem> feed_item;
    NewsItem item;
    Activity activity;
    int play_position=100000;
    Animation myAnim;
    public CommentListAdapter(ArrayList<NewsItem> feedItem, Activity activity) {
        this.activity = activity;
        this.feed_item = feedItem;
        myAnim = AnimationUtils.loadAnimation(activity, R.anim.scale);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.1, 60);
        myAnim.setInterpolator(interpolator);
    }


    @Override
    public int getCount() {
        return feed_item.size();
    }

    @Override
    public Object getItem(int location) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {

        @BindView(R.id.comment_avatar)
        CircleImageView avatar;

        @BindView(R.id.comment_name)
        TextView name;
        @BindView(R.id.comment_date)
        TextView comment_date;

        @BindView(R.id.complain_btn)
        TextView complain_btn;
        @BindView(R.id.answer_btn)
        LinearLayout answer_btn;

        @BindView(R.id.comment_like_layout)
        LinearLayout comment_like_layout;

        @BindView(R.id.comment_like_count)
        TextView comment_like_count;

        @BindView(R.id.comment_like_img)
        ImageView like_img;

        @BindView(R.id.parent_layout)
        LinearLayout parent_layout;

        @BindView(R.id.comment_desc)
        EmojiTextView desc;
        @BindView(R.id.parent_name)
        EmojiTextView parent_name;

        @BindView(R.id.comment_image)
        ImageView comment_image;

        @BindView(R.id.videolayout)
        FrameLayout videolayout;

        @BindView(R.id.videoview)
        ImageView videoview;

        @BindView(R.id.playvideo)
        ImageView playvideo;

        @BindView(R.id.audio_layout)
        RelativeLayout audio_layout;

        @BindView(R.id.audio_play)
        ImageView audio_play;

        @BindView(R.id.audio_name)
        TextView audio_name;

        @BindView(R.id.audio_time)
        TextView audio_time;

        @BindView(R.id.file_layout)
        View file_layout;
        @BindView(R.id.file_image)
        ImageView send_file_image;
        @BindView(R.id.file_name)
        TextView send_file_name;
        @BindView(R.id.file_format)
        TextView send_file_format;
        @BindView(R.id.file_upload_progress)
        ProgressBar file_upload_progress;

        boolean is_more=false;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_comment_list, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        item = feed_item.get(position);

        viewHolder.name.setText(item.getUser_name());
        viewHolder.comment_date.setText(item.getComment_date());
        String chat_text = item.getComment_text().replaceAll("\n","<br>");
        if (item.getParent_id()!=0){
            viewHolder.parent_name.setVisibility(View.VISIBLE);
            viewHolder.parent_layout.setPadding(140,30,30,0);
            String first = "<b>" +chat_text.substring( 0, chat_text.indexOf(",")+1)+ "</b> ";

            chat_text = first+chat_text.substring(chat_text.indexOf(",")+1, chat_text.length());
            viewHolder.parent_name.setText(Html.fromHtml( first));
            viewHolder.desc.setText(Html.fromHtml(chat_text));
        }else {
            viewHolder.parent_name.setVisibility(View.GONE);
            viewHolder.parent_layout.setPadding(30,30,30,0);
            viewHolder.desc.setText(Html.fromHtml(chat_text));
        }

        String more = "  <font  color=\"grey\"> ...далее<font>";

        if (chat_text.length() > 150) {
            String newtext = chat_text.substring(0,150);
            viewHolder.desc.setText(Html.fromHtml(newtext+more));
            viewHolder.is_more= true;

        } else {
            viewHolder.is_more= false;
            viewHolder.desc.setText(Html.fromHtml(chat_text));
        }

        String finalChat_text = chat_text;
        viewHolder.desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                if (viewHolder.is_more){
                    viewHolder.desc.setText(Html.fromHtml(finalChat_text));
                    viewHolder.is_more=false;
                }
            }
        });

        viewHolder.parent_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                if (item.getParent_id()!=0)
                    activity.startActivity(new Intent(activity, ActivityMyProfile.class).putExtra("user_id",item.getParent_author_id()));

            }
        });


        if (item.getLike_count()!=0){
            viewHolder.comment_like_count.setText(item.getLike_count()+"");
        }else {
            viewHolder.comment_like_count.setText(" ");
        }



        if (item.getAvatar().isEmpty()){
            Glide.with(activity).load(R.drawable.ic_person).into(viewHolder.avatar);
        }else {
            Glide.with(activity).load(item.getAvatar()).into(viewHolder.avatar);
        }

        if (item.getIs_i_liked()!=1){
            viewHolder.like_img.setImageResource(R.drawable.ic_dislike);
            viewHolder.comment_like_count.setTextColor(activity.getResources().getColor(R.color.chattext));

        }else {
            viewHolder.like_img.setImageResource(R.drawable.ic_like);
            viewHolder.comment_like_count.setTextColor(activity.getResources().getColor(R.color.like_color));

        }



        if (item.getIs_has_file()==1){
            viewHolder.desc.setVisibility(View.GONE);
            viewHolder.comment_image.setVisibility(View.GONE);
            viewHolder.videolayout.setVisibility(View.GONE);
            viewHolder.audio_layout.setVisibility(View.GONE);
            viewHolder.file_layout.setVisibility(View.GONE);
            switch (item.getFile_format()[0]){
                case "image":
                    viewHolder.comment_image.setVisibility(View.VISIBLE);
                    Glide.with(activity).load(item.getFile_url()[0]).into(viewHolder.comment_image);
                    viewHolder.comment_image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            Intent intent = new Intent(activity, ActivityZoomImage.class);
                            Bundle bundle = new Bundle();
                            bundle.putStringArrayList("url", new ArrayList<>(Arrays.asList(item.getFile_url()[0])));
                            bundle.putInt("position", 0);
                            intent.putExtras(bundle);
                            activity.startActivity(intent);
                        }
                    });
                    break;
                case "video":
                    viewHolder.videolayout.setVisibility(View.VISIBLE);

                    Glide.with(activity)
                            .asBitmap()
                            .load(item.getFile_url()[0])
                            .into(viewHolder.videoview);

                    viewHolder.playvideo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            if (item.getFile_format()[0].equals("video")) {
                                Intent intent = new Intent(activity, ActivityVideoPlayer.class);
                                intent.putExtra("file_url", item.getFile_url()[0]);
                                activity.startActivity(intent);
                            }
                        }
                    });
                    break;
                case "audio":
                    if (position == play_position) {
                        viewHolder.audio_play.setImageResource(R.drawable.ic_pause);
                    } else {
                        viewHolder.audio_play.setImageResource(R.drawable.ic_play);
                    }
                    viewHolder.audio_layout.setVisibility(View.VISIBLE);
                    viewHolder.audio_name.setText(item.getFile_name()[0]);
                    int second = item.getFile_time() % 60;
                    int minute = item.getFile_time() / 60;
                    String timeString = String.format("%02d:%02d", minute, second);
                    Log.d("TIME",item.getFile_time()+"*****"+timeString);
                    viewHolder.audio_time.setText(timeString);

                    viewHolder.audio_play.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            if (play_position != position){


                                if (mediaPlayernews != null && mediaPlayernews.isPlaying()) {
                                    mediaPlayernews.stop();
                                    mediaPlayernews.release();
                                }

                                try {
                                    mediaPlayernews = new MediaPlayer();
                                    mediaPlayernews.setDataSource(item.getFile_url()[0]);
                                    mediaPlayernews.prepare();
                                    mediaPlayernews.start();
                                    viewHolder.audio_play.setImageResource(R.drawable.ic_pause);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                play_position=position;
                                notifyDataSetChanged();
                            }else {
                                if (mediaPlayernews != null) {

                                    if (mediaPlayernews.isPlaying()) {
                                        viewHolder.audio_play.setImageResource(R.drawable.ic_play);
                                        mediaPlayernews.pause();
                                    } else {
                                        viewHolder.audio_play.setImageResource(R.drawable.ic_pause);
                                        mediaPlayernews.start();
                                    }

                                } else {
                                    play_position=position;

                                    if (mediaPlayernews.isPlaying()){
                                        mediaPlayernews.stop();
                                        mediaPlayernews.release();
                                    }

                                    try {
                                        mediaPlayernews = new MediaPlayer();
                                        mediaPlayernews.setDataSource(item.getFile_url()[0]);
                                        mediaPlayernews.prepare();
                                        mediaPlayernews.start();
                                        viewHolder.audio_play.setImageResource(R.drawable.ic_pause);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    notifyDataSetChanged();
                                }
                            }
                        }
                    });
                    break;
                case "file":
                    viewHolder.file_layout.setVisibility(View.VISIBLE);


                    viewHolder.send_file_name.setText(item.getFile_name()[0]);
                    final String extensionRemoved = item.getFile_name()[0].split("\\.")[1];
                    viewHolder.send_file_format.setText(extensionRemoved);

                    switch (extensionRemoved){
                        case "pdf":
                            viewHolder.send_file_image.setColorFilter(activity.getResources().getColor(R.color.color_pdf));
                            break;
                        case "xls":
                        case "xlsx":
                            viewHolder.send_file_image.setColorFilter(activity.getResources().getColor(R.color.color_xls));
                            break;
                        case "doc":
                        case "docx":
                            viewHolder.send_file_image.setColorFilter(activity.getResources().getColor(R.color.color_doc));
                            break;
                        case "ppt":
                        case "pptx":
                            viewHolder.send_file_image.setColorFilter(activity.getResources().getColor(R.color.color_ppt));
                            break;
                        default:
                            viewHolder.send_file_image.setColorFilter(activity.getResources().getColor(R.color.color_txt));
                            break;
                    }

                    viewHolder.file_layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            for (int i=0;i<item.getFile_format().length;i++) {
                                if (item.getFile_format()[i].equals("file")) {
                                    viewHolder.file_upload_progress.setVisibility(View.VISIBLE);
                                    downloadFile(item.getFile_url()[i],item.getFile_name()[i],"file",viewHolder.file_upload_progress);
                                }
                            }

                        }
                    });


                    break;
            }
        }else {
            viewHolder.desc.setVisibility(View.VISIBLE);
            viewHolder.comment_image.setVisibility(View.GONE);
            viewHolder.videolayout.setVisibility(View.GONE);
            viewHolder.audio_layout.setVisibility(View.GONE);
            viewHolder.file_layout.setVisibility(View.GONE);
        }


        viewHolder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                activity.startActivity(new Intent(activity, ActivityMyProfile.class).putExtra("user_id",item.getUser_id()));
            }
        });
        viewHolder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                activity.startActivity(new Intent(activity, ActivityMyProfile.class).putExtra("user_id",item.getUser_id()));
            }
        });

        viewHolder.complain_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog  = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.complain_dialog);
                dialog.setCancelable(true);
                dialog.show();

                Button send = (Button) dialog.findViewById(R.id.send);
                send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });


        viewHolder.answer_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ( (ActivitySingleNews)activity).AnswerComment(position);
            }
        });

        viewHolder.comment_like_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                AndroidNetworking.post(like+TOKEN).addBodyParameter("comment_id",item.getComment_id()+"")
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")){

                                item.setLike_count(response.getInt("like_count"));
                                if (item.getIs_i_liked()==1){
                                    item.setIs_i_liked(0);
                                    viewHolder.like_img.setImageResource(R.drawable.ic_dislike);
                                    viewHolder.comment_like_count.setTextColor(activity.getResources().getColor(R.color.chattext));
                                }else {
                                    item.setIs_i_liked(1);
                                    viewHolder.like_img.setImageResource(R.drawable.ic_like);
                                    viewHolder.comment_like_count.setTextColor(activity.getResources().getColor(R.color.like_color));
                                    viewHolder.like_img.startAnimation(myAnim);
                                }

                                notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("LIKE", response+"  ");
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("LIKEerr", anError+"  ");
                    }
                });
            }
        });
        return convertView;
    }
    private void downloadFile (String File_url ,String File_name ,String format,ProgressBar file_upload_progress){
        File myDir  = new File( Environment.getExternalStorageDirectory().toString() + "/.dalagram");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        File savefile = new File(myDir +"/Dalagram "+format);


        File file_url = new File(savefile+"/"+File_name);

        if (!file_url.exists()) {
            AndroidNetworking.download(File_url,savefile.getPath(),File_name)
                    .setTag("downloadTest")
                    .setPriority(Priority.HIGH)
                    .build()
                    .startDownload(new DownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            file_upload_progress.setVisibility(View.GONE);
                            Uri pdfUri = FileProvider.getUriForFile(activity, getApplicationContext().getPackageName() + ".provider", file_url);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(pdfUri, "application/*");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            Intent chooser = Intent.createChooser(intent, "");

                            activity.startActivity(chooser);
                        }
                        @Override
                        public void onError(ANError anError) {
                            Toasty.error(activity, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                        }
                    });
        }else {
            file_upload_progress.setVisibility(View.GONE);
            Uri pdfUri = FileProvider.getUriForFile(activity, getApplicationContext().getPackageName() + ".provider", file_url);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(pdfUri, "application/*");
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Intent chooser = Intent.createChooser(intent, "");

            activity.startActivity(chooser);

        }


    }

}
