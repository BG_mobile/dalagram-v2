package kz.bgpro.www.dalagram.activitys;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.bgpro.www.dalagram.R;

/**
 * Created by nurbaqyt on 02.08.2018.
 */

public class ActivityAppShare extends Activity{



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appshare);
        ButterKnife.bind(this);

    }
    @OnClick(R.id.back)
    void back(){
        finish();
    }
}
