package kz.bgpro.www.dalagram.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import io.realm.RealmResults;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.Realm.Dialog;
import kz.bgpro.www.dalagram.fragment.adapters.ChatListAdapter;
import kz.bgpro.www.dalagram.models.FeedItem;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.chat;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

public class ActivitySearchChat extends Activity  implements SwipeRefreshLayout.OnRefreshListener{


   ArrayList<FeedItem> feedItem;

    @BindView(R.id.list)
    ListView list;

    @BindView(R.id.ed_search)
    EditText search;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    ChatListAdapter chatListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_chat);
        ButterKnife.bind(this);

        swipe.setOnRefreshListener(this);




        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0){
                    GetDataFromJSON(search.getText().toString());
                } if (s.length()==0){
                    GetDataFromJSON("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        feedItem = new ArrayList<FeedItem>();

        chatListAdapter = new ChatListAdapter(feedItem, ActivitySearchChat.this);
        list.setAdapter(chatListAdapter);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FeedItem item = feedItem.get(position);
                String Name;
                if(item.getUser_id()!=0) {
                    if(!TextUtils.isEmpty(item.getContact_user_name()))
                        Name = item.getContact_user_name();
                    else if(!TextUtils.isEmpty(item.getChat_name()))
                        Name = item.getChat_name();
                    else
                        Name = item.getPhone();
                }else
                    Name = item.getChat_name();

                Log.d("WHAT","IS2");
                String avatar;
                boolean is_ava ;
                if (TextUtils.isEmpty(item.getAvatar())){
                    if(Name.length()>1)
                        avatar = Name.charAt(0)+""+Name.charAt(1);
                    else
                        avatar = Name.charAt(0)+"";
                    is_ava = false;
                }else {
                    is_ava = true;
                    avatar = item.getAvatar();
                }
                Log.d("WHAT","IS3");
                item.setNew_message_count(0);
                chatListAdapter.notifyDataSetChanged();
                if (item.getDialog_id().contains("U")){
                    startActivity(new Intent(ActivitySearchChat.this, ActivityChatMessage.class)
                            .putExtra("who","chat")
                            .putExtra("user_id",item.getUser_id())
                            .putExtra("is_mute",item.getIs_mute())
                            .putExtra("name",Name)
                            .putExtra("chat_text",item.getLast_visit())
                            .putExtra("avatar",avatar)
                            .putExtra("is_ava",is_ava)
                            .putExtra("dialog_id",item.getDialog_id())
                            .putExtra("i_block_partner",item.getI_block_partner())
                            .putExtra("partner_block_me",item.getPartner_block_me())
                    );

                }else if (item.getDialog_id().contains("G")){
                    startActivity(new Intent(ActivitySearchChat.this, ActivityChatMessage.class)
                            .putExtra("who","group")
                            .putExtra("group_id",item.getGroup_id())
                            .putExtra("name",Name)
                            .putExtra("is_mute",item.getIs_mute())
                            .putExtra("chat_text",item.getLast_visit())
                            .putExtra("avatar",avatar)
                            .putExtra("is_ava",is_ava)
                            .putExtra("is_admin",item.getIs_admin())
                            .putExtra("dialog_id",item.getDialog_id())
                            .putExtra("i_block_partner",item.getI_block_partner())
                            .putExtra("partner_block_me",item.getPartner_block_me())
                    );
                }else if(item.getDialog_id().contains("C")){
                    startActivity(new Intent(ActivitySearchChat.this, ActivityChatMessage.class)
                            .putExtra("who","channel")
                            .putExtra("channel_id",item.getChannel_id())
                            .putExtra("is_mute",item.getIs_mute())
                            .putExtra("name",Name)
                            .putExtra("chat_text",item.getLast_visit())
                            .putExtra("avatar",avatar)
                            .putExtra("is_ava",is_ava)
                            .putExtra("is_admin",item.getIs_admin())
                            .putExtra("dialog_id",item.getDialog_id())
                            .putExtra("i_block_partner",item.getI_block_partner())
                            .putExtra("partner_block_me",item.getPartner_block_me())
                    );
                }


            }
        });

    }

    @OnClick(R.id.back)
    void back(){
        finish();
    }

    @OnClick(R.id.clear)
    void clear(){
        search.setText(null);
        feedItem.clear();
        chatListAdapter.notifyDataSetChanged();
    }
    @Override
    public void onRefresh() {
        swipe.setRefreshing(false);
    }

    public void GetDataFromJSON(String search_txt) {


        swipe.setRefreshing(true);
        Log.d("chat_list",chat + TOKEN + "&per_page=30&page=1&search="+search_txt);
        AndroidNetworking.get(chat + TOKEN + "&per_page=30&page=1&search="+search_txt)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response",response+"**");
                try {
                    if (response.getBoolean("status")) {
                        feedItem.clear();
                        JSONArray data = response.getJSONArray("data");
                        for (int i = 0; i<data.length();i++){
                            JSONObject jsonObject = (JSONObject) data.get(i);
                            FeedItem item = new FeedItem();
                            item.setChat_id(NurInt(jsonObject, "chat_id"));
                            item.setChat_kind(NurString(jsonObject, "chat_kind"));
                            item.setIs_read(NurInt(jsonObject, "is_read"));
                            item.setIs_admin(NurInt(jsonObject, "is_admin"));
                            item.setI_block_partner(NurInt(jsonObject, "i_block_partner"));
                            item.setPartner_block_me(NurInt(jsonObject, "partner_block_me"));
                            item.setIs_has_file(NurInt(jsonObject, "is_has_file"));
                            item.setNew_message_count(NurInt(jsonObject, "new_message_count"));
                            item.setAction_name(NurString(jsonObject, "action_name"));
                            item.setGroup_id(NurInt(jsonObject, "group_id"));
                            item.setChannel_id(NurInt(jsonObject, "channel_id"));
                            item.setIs_mute(NurInt(jsonObject, "is_mute"));
                            item.setLast_visit(NurString(jsonObject, "last_visit"));
                            item.setUser_id(NurInt(jsonObject, "user_id"));
                            item.setDialog_id(NurString(jsonObject, "dialog_id"));
                            item.setChat_name(NurString(jsonObject, "chat_name"));
                            item.setAvatar(NurString(jsonObject, "avatar"));
                            item.setPhone(NurString(jsonObject, "phone"));
                            item.setContact_user_name(NurString(jsonObject, "nickname"));
                            item.setChat_text(NurString(jsonObject, "chat_text"));
                            item.setChat_date(NurString(jsonObject, "chat_date"));
                            item.setIs_typing(false);
                            String file_format="";
                            if(jsonObject.getInt("is_has_file")==1){
                                file_format = ((JSONObject)jsonObject.getJSONArray("file_list").get(0)).getString("file_format");
                            }
                            item.setSfile_format(file_format);
                            feedItem.add(item);

                        }


                        chatListAdapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);

                    } else {
                        Toasty.error(ActivitySearchChat.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {

                Toasty.error(ActivitySearchChat.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();


            }
        });

    }




}
