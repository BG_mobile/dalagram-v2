package kz.bgpro.www.dalagram.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.Objects;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.bgpro.www.dalagram.MainActivity;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.fragment.FragmentFriendUserList;
import kz.bgpro.www.dalagram.activitys.fragment.FragmentUserList;

public class FragmentContacts extends Fragment {

    @BindView(R.id.tablayout)
    TabLayout tabLayout;

    @BindView(R.id.pager)
    ViewPager viewPager;

    @BindString(R.string.contacts)
    String contacts;
    @BindString(R.string.friends)
    String friends;
    @BindString(R.string.subscrips)
    String subscrips;

    @BindView(R.id.ed_search)
    EditText search;

    @BindView(R.id.clear)
    ImageButton clear;

    CPagerAdapter adapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);
        ButterKnife.bind(this, view);

        return view;
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

         adapter = new CPagerAdapter(getChildFragmentManager());

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);


        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                String s = search.getText().toString();
                if (s.length()>0){
                    ToSerch(s);
                }else {
                    ToSerch("");
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ToSerch(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void ToSerch(String s){
        Fragment page = adapter.getCurrentFragment();

        if (viewPager.getCurrentItem()==0){
            if (page != null) {
                if (s.length()>0){
                    ((FragmentContactsList)page).GetJSONFromServer(search.getText().toString());

                }else {
                    ((FragmentContactsList)page).GetJSONFromServer();

                }
            }
        }else if (viewPager.getCurrentItem()==1){
            if (page != null) {
                if (s.length()>0){
                    ((FragmentFriends)page).GetDataFromJSON_follower_friend(search.getText().toString());

                }else {
                    ((FragmentFriends)page).GetDataFromJSON_follower_friend("");

                }
            }
        }
    }


    @OnClick(R.id.clear)
    void clear(){
        search.setText(null);
    }


     class CPagerAdapter extends FragmentPagerAdapter {
         private Fragment mCurrentFragment;

        CPagerAdapter(FragmentManager fm)
        {
            super(fm);
        }

         public Fragment getCurrentFragment() {
             return mCurrentFragment;
         }

         @Override
         public void setPrimaryItem(ViewGroup container, int position, Object object) {
             if (getCurrentFragment() != object) {
                 mCurrentFragment = ((Fragment) object);
             }
             super.setPrimaryItem(container, position, object);
         }
        @Override
        public Fragment getItem(int position) {
            switch(position)
            {
                case 0:
                    return new FragmentContactsList();
                case 1:
                    return new FragmentFriends();
                case 2:
                default:
                    return new FragmentFollowers();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            if (position == 0) {
                title = contacts;
            } else if (position == 1) {
                title = friends;
            }else if (position ==2){
                title = subscrips;
            }
            return title;
        }

    }

}
