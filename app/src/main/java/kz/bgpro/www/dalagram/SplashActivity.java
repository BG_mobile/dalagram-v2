package kz.bgpro.www.dalagram;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.Arrays;
import java.util.Locale;

import kz.bgpro.www.dalagram.Auth.LoginActivity;

import static kz.bgpro.www.dalagram.utils.MyConstants.PREFS_NAME;

public class SplashActivity extends AppCompatActivity {
    Handler handler;
    SharedPreferences settings;
    String TOKEN;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);



        String  lang = "ru";
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());


        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        TOKEN = settings.getString("token","*");
        Log.d("token",TOKEN);


        handler = new Handler();
        handler.postDelayed(runn1,2000);
    }
    Runnable runn1 = new Runnable() {
        @Override
        public void run() {
            if(!TOKEN.equals("*")){
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }else {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }

        }
    };

}
