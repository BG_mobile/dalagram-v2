package kz.bgpro.www.dalagram.Realm;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Stickers extends RealmObject {

    @PrimaryKey
    private int sticker_id;

    protected String sticker_image="";

    public Stickers(){
    }



    public Stickers(int sticker_id, String sticker_image) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();


        Stickers sticker = new Stickers();
        sticker.setSticker_id(sticker_id);
        sticker.setSticker_image(sticker_image);

        realm.copyToRealmOrUpdate(sticker);
        realm.commitTransaction();
    }


    public int getSticker_id() {
        return sticker_id;
    }

    public void setSticker_id(int sticker_id) {
        this.sticker_id = sticker_id;
    }

    public String getSticker_image() {
        return sticker_image;
    }

    public void setSticker_image(String sticker_image) {
        this.sticker_image = sticker_image;
    }
}
