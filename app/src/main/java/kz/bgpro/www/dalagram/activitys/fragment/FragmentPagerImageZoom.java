package kz.bgpro.www.dalagram.activitys.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.utils.ZoomImageView;

/**
 * Created by nurbaqyt on 25.08.2018.
 */

@SuppressLint("ValidFragment")
public class FragmentPagerImageZoom extends Fragment {

    static final String ARGUMENT_PAGE_URL = "arg_page_url";

    @BindView(R.id.image)
    ZoomImageView image;


    String url;

    public static FragmentPagerImageZoom newInstance(String url) {
        FragmentPagerImageZoom pageFragment = new FragmentPagerImageZoom();
        Bundle arguments = new Bundle();
        arguments.putString(ARGUMENT_PAGE_URL, url);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       url = getArguments().getString(ARGUMENT_PAGE_URL);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_zoom, null);
        ButterKnife.bind(this, view);


        Picasso.get().load(url).into(image);

        return view;
    }

}

