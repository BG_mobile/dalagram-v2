package kz.bgpro.www.dalagram;

import android.app.Application;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.ios.IosEmojiProvider;

import java.net.URISyntaxException;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import kz.bgpro.www.dalagram.utils.MyConstants;

public class ChatApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        AndroidNetworking.initialize(getApplicationContext());
        Realm.init(this);
        Realm.getInstance(new RealmConfiguration.Builder().build());
        EmojiManager.install(new IosEmojiProvider());
    }


    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(MyConstants.CHAT_SERVER_URL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }


}
