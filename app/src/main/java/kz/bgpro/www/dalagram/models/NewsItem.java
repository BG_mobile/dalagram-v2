package kz.bgpro.www.dalagram.models;

import java.io.Serializable;

public class NewsItem implements Serializable {
    private int publication_id,view_count,comment_count,share_count,like_count,comment_like_count,comment_is_has_file, is_has_file,is_i_liked,is_has_link,is_i_liked_comment;
    private String publication_desc,publication_date,link_description,link_title,link_image;

    private int user_id;
    private String user_name,phone,avatar,comment_avatar,comment_user_name, comment_file_url, comment_file_format, comment_file_name, comment_file_time;

    private String [] file_url;
    private String [] file_format;
    private String [] file_name;
    int file_time;
    int image_count;



    private int comment_id,parent_id,parent_author_id;
    private String comment_text,comment_date;

    public int getPublication_id() {
        return publication_id;
    }

    public void setPublication_id(int publication_id) {
        this.publication_id = publication_id;
    }

    public int getView_count() {
        return view_count;
    }

    public void setView_count(int view_count) {
        this.view_count = view_count;
    }

    public int getComment_count() {
        return comment_count;
    }

    public void setComment_count(int comment_count) {
        this.comment_count = comment_count;
    }

    public int getShare_count() {
        return share_count;
    }

    public void setShare_count(int share_count) {
        this.share_count = share_count;
    }

    public int getLike_count() {
        return like_count;
    }

    public void setLike_count(int like_count) {
        this.like_count = like_count;
    }

    public int getIs_has_file() {
        return is_has_file;
    }

    public void setIs_has_file(int is_has_file) {
        this.is_has_file = is_has_file;
    }

    public String getPublication_desc() {
        return publication_desc;
    }

    public void setPublication_desc(String publication_desc) {
        this.publication_desc = publication_desc;
    }

    public String getPublication_date() {
        return publication_date;
    }

    public void setPublication_date(String publication_date) {
        this.publication_date = publication_date;
    }


    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String[] getFile_url() {
        return file_url;
    }

    public void setFile_url(String[] file_url) {
        this.file_url = file_url;
    }

    public String[] getFile_format() {
        return file_format;
    }

    public void setFile_format(String[] file_format) {
        this.file_format = file_format;
    }

    public String[] getFile_name() {
        return file_name;
    }

    public void setFile_name(String[] file_name) {
        this.file_name = file_name;
    }

    public int getFile_time() {
        return file_time;
    }

    public void setFile_time(int file_time) {
        this.file_time = file_time;
    }


    public int getIs_i_liked() {
        return is_i_liked;
    }

    public void setIs_i_liked(int is_i_liked) {
        this.is_i_liked = is_i_liked;
    }

    public int getIs_has_link() {
        return is_has_link;
    }

    public void setIs_has_link(int is_has_link) {
        this.is_has_link = is_has_link;
    }

    public String getLink_description() {
        return link_description;
    }

    public void setLink_description(String link_description) {
        this.link_description = link_description;
    }

    public String getLink_title() {
        return link_title;
    }

    public void setLink_title(String link_title) {
        this.link_title = link_title;
    }

    public String getLink_image() {
        return link_image;
    }

    public void setLink_image(String link_image) {
        this.link_image = link_image;
    }

    public int getImage_count() {
        return image_count;
    }

    public void setImage_count(int image_count) {
        this.image_count = image_count;
    }

    public int getComment_id() {
        return comment_id;
    }

    public void setComment_id(int comment_id) {
        this.comment_id = comment_id;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public int getParent_author_id() {
        return parent_author_id;
    }

    public void setParent_author_id(int parent_author_id) {
        this.parent_author_id = parent_author_id;
    }

    public String getComment_text() {
        return comment_text;
    }

    public void setComment_text(String comment_text) {
        this.comment_text = comment_text;
    }

    public String getComment_date() {
        return comment_date;
    }

    public void setComment_date(String comment_date) {
        this.comment_date = comment_date;
    }

    public int getIs_i_liked_comment() {
        return is_i_liked_comment;
    }

    public void setIs_i_liked_comment(int is_i_liked_comment) {
        this.is_i_liked_comment = is_i_liked_comment;
    }

    public String getComment_avatar() {
        return comment_avatar;
    }

    public void setComment_avatar(String comment_avatar) {
        this.comment_avatar = comment_avatar;
    }

    public String getComment_user_name() {
        return comment_user_name;
    }

    public void setComment_user_name(String comment_user_name) {
        this.comment_user_name = comment_user_name;
    }

    public int getComment_like_count() {
        return comment_like_count;
    }

    public void setComment_like_count(int comment_like_count) {
        this.comment_like_count = comment_like_count;
    }

    public int getComment_is_has_file() {
        return comment_is_has_file;
    }

    public void setComment_is_has_file(int comment_is_has_file) {
        this.comment_is_has_file = comment_is_has_file;
    }

    public String getComment_file_url() {
        return comment_file_url;
    }

    public void setComment_file_url(String comment_file_url) {
        this.comment_file_url = comment_file_url;
    }

    public String getComment_file_format() {
        return comment_file_format;
    }

    public void setComment_file_format(String comment_file_format) {
        this.comment_file_format = comment_file_format;
    }

    public String getComment_file_name() {
        return comment_file_name;
    }

    public void setComment_file_name(String comment_file_name) {
        this.comment_file_name = comment_file_name;
    }

    public String getComment_file_time() {
        return comment_file_time;
    }

    public void setComment_file_time(String comment_file_time) {
        this.comment_file_time = comment_file_time;
    }
}
