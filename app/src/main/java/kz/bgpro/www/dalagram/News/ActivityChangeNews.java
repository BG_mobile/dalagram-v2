package kz.bgpro.www.dalagram.News;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.bumptech.glide.Glide;
import com.fxn.pix.Pix;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.NormalFile;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.ActivityVideoPlayer;
import kz.bgpro.www.dalagram.activitys.ActivityZoomImage;
import kz.bgpro.www.dalagram.models.FeedItem;
import kz.bgpro.www.dalagram.models.NewsItem;
import kz.bgpro.www.dalagram.utils.MyConstants;
import kz.bgpro.www.dalagram.utils.MyPicassoEngine;
import kz.bgpro.www.dalagram.utils.NonScrollListView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.*;

public class ActivityChangeNews extends AppCompatActivity {
    SharedPreferences settings;
    @BindView(R.id.mini_name)
    TextView mini_name;

    @BindView(R.id.mini_avatar)
    ImageView mini_avatar;

    @BindView(R.id.desc)
    EmojiEditText emojiEditText;

    @BindView(R.id.spinner)
    Spinner spinner;


    @BindView(R.id.audio_layout)
    RelativeLayout audio_layout;
    @BindView(R.id.audio_time)
    TextView audio_time;
    @BindView(R.id.audio_name)
    TextView audio_name;
    @BindView(R.id.audioprogress)
    ProgressBar audioprogress;
    @BindView(R.id.audioplay)
    ImageButton audio_play;

    boolean has_audio = false,has_video = false,has_file=false;

    ArrayList<FeedItem> feedItems;
    ArrayList<String> returnValue;


    int RequestCodeForGallerey = 123;
    int REQUEST_AUDIO_FILE = 320;
    int REQUEST_TAKE_GALLERY_VIDEO = 713;
    int REQUEST_TAKE_FILE = 518;

    private ArrayList<MediaFile> mediaFiles = new ArrayList<>();

    int is_gallery=0;
    String []file_name;
    String []file_url;
    String []file_format;
    int file_time=0;

    String audiofile_name;
    String audiofile_url;
    String audiofile_format;
    MediaPlayer mediaPlayer;


    @BindView(R.id.videolayout)
    FrameLayout videolayout;
    @BindView(R.id.videoview)
    ImageView videoview;
    @BindView(R.id.videoProgressbar)
    ProgressBar videoProgressbar;
    @BindView(R.id.video_process)
    TextView video_process;

    String selectedVideoPath;
    String videofile_name;
    String videofile_url;
    String videofile_format;
    int videofile_time=0;

    @BindView(R.id.emotion_btn)
    ImageButton emotion_btn;

    EmojiPopup emojiPopup;

    @BindView(R.id.rootView)
    ViewGroup rootView;
    String TAG = "Nurikkkkk Newsss";


    @BindView(R.id.file_layout)
    View file_layout;
    @BindView(R.id.file_image)
    ImageView send_file_image;
    @BindView(R.id.file_name)
    TextView send_file_name;
    @BindView(R.id.file_format)
    TextView send_file_format;
    @BindView(R.id.file_upload_progress)
    ProgressBar file_upload_progress;
    @BindView(R.id.delete_file)
    ImageButton delete_file;

    String filefile_name;
    String filefile_url;
    String filefile_format;

    String [] access_name;
    int [] access_id;
    int access_selected_position;


    NewsItem change_item;
    JSONObject response_news;
    @BindView(R.id.images)
    RelativeLayout images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changenews);
        ButterKnife.bind(this);
         audio_layout.setVisibility(View.GONE);
        videolayout.setVisibility(View.GONE);

        emojiPopup = EmojiPopup.Builder.fromRootView(rootView).build(emojiEditText);
        emojiPopup = EmojiPopup.Builder.fromRootView(rootView)
                .setOnEmojiBackspaceClickListener(ignore -> Log.d(TAG, "Clicked on Backspace"))
                .setOnEmojiClickListener((ignore, ignore2) -> Log.d(TAG, "Clicked on emoji"))
                .setOnEmojiPopupShownListener(() -> emotion_btn.setImageResource(R.drawable.ic_keyboard))
                .setOnSoftKeyboardOpenListener(ignore -> Log.d(TAG, "Opened soft keyboard"))
                .setOnEmojiPopupDismissListener(() -> emotion_btn.setImageResource(R.drawable.ic_emoticon))
                .setOnSoftKeyboardCloseListener(() -> Log.d(TAG, "Closed soft keyboard"))
                .build(emojiEditText);




        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        mini_name.setText(settings.getString("user_name", " "));

        if (!TextUtils.isEmpty(settings.getString("avatar", ""))) {
            Glide.with(ActivityChangeNews.this).load(settings.getString("avatar", "")).into(mini_avatar);
        }
        feedItems = new ArrayList<FeedItem>();

        AndroidNetworking.get(access+TOKEN).build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if (response.getBoolean("status")){
                        JSONArray data = response.getJSONArray("data");
                        int len = data.length();
                        access_id = new int[len];
                        access_name = new String[len];
                        for(int i=0;i<len;i++){
                            JSONObject access = (JSONObject) data.get(i);
                            access_id[i] = access.getInt("access_id");
                            access_name[i] = access.getString("access_name");
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ActivityChangeNews.this, android.R.layout.simple_spinner_item, access_name);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                    } else {
                        Toasty.error(ActivityChangeNews.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(ANError anError) {

            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {

                access_selected_position = selectedItemPosition;
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        Intent intent = getIntent();
        change_item = (NewsItem) intent.getSerializableExtra("item");
        try {
            Log.d("response_news",intent.getStringExtra("response_news"));
            response_news = new JSONObject(intent.getStringExtra("response_news"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        emojiEditText.setText(change_item.getPublication_desc());

        if (change_item.getIs_has_file()==1){

            float marginDp = 8 * getResources().getDisplayMetrics().density;

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
            params.topMargin = (int)marginDp;
            ImageView imageView = new ImageView(this); // initialize ImageView
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setAdjustViewBounds(true);
            Glide.with(ActivityChangeNews.this).load(change_item.getFile_url()[0]).into(imageView);

            ImageButton imageButton = new ImageButton(this);
            imageButton.setBackgroundColor(getResources().getColor(R.color.like_color));
            imageButton.setImageResource(R.drawable.delete_image);
            RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params2.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            params2.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
            params2.topMargin = (int)marginDp;
            imageButton.setPadding((int)marginDp,(int)marginDp,(int)marginDp,(int)marginDp);
            images.addView(imageView, params);
            images.addView(imageButton, params2);

            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AndroidNetworking.delete(server_image+TOKEN).addBodyParameter("image_url",change_item.getFile_url()[0])
                            .build().getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("rrrrr",response+"**");
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("rrrrr",anError+"**");

                        }
                    });
                }
            });
        }




    }


    @OnClick(R.id.emotion_btn)
    void emotion_btnClick(){

        Log.d("SSSSS",emojiPopup.isShowing()+"****");
        if (emojiPopup.isShowing()){
            emojiPopup.dismiss();
        }else {
            emojiPopup.toggle();
        }
    }

    @OnClick(R.id.back)
    void back(){
        finish();
    }



    @OnClick(R.id.image_btn)
    void image_btn(){
        Pix.start(ActivityChangeNews.this,                    //Activity or Fragment Instance
                RequestCodeForGallerey,                //Request code for activity results
                5);
    }

    @OnClick(R.id.music_btn)
    void music_btn(){
        mediaFiles.clear();

        Intent intent = new Intent(ActivityChangeNews.this, FilePickerActivity.class);
        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                .setCheckPermission(true)
                .setSelectedMediaFiles(mediaFiles)
                .setShowImages(false)
                .setShowVideos(false)
                .setShowAudios(true)
                .setMaxSelection(1)
                .build());
        startActivityForResult(intent, REQUEST_AUDIO_FILE);

    }

    @OnClick(R.id.video_btn)
    void video_btn(){

        Matisse.from(ActivityChangeNews.this)
                .choose(MimeType.ofVideo())
                .countable(true)
                .maxSelectable(1)
                .autoHideToolbarOnSingleTap(true)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .thumbnailScale(0.85f)
                .imageEngine(new MyPicassoEngine())
                .forResult(REQUEST_TAKE_GALLERY_VIDEO);
    }

    @OnClick(R.id.file_btn)
    void file_btn(){
        Intent intent4 = new Intent(this, NormalFilePickActivity.class);
        intent4.putExtra(Constant.MAX_NUMBER, 1);
        intent4.putExtra(NormalFilePickActivity.SUFFIX, new String[] {"xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf","zip","rar", "txt"});
        startActivityForResult(intent4, REQUEST_TAKE_FILE);
    }


    @OnClick(R.id.send)
    void Send() {


        String Desc = emojiEditText.getText().toString();
        if (Desc.isEmpty() && feedItems.size()==0 ){
            Toasty.info(this, "Заполните что нибудь !", Toast.LENGTH_SHORT).show();
        }else {
            try {
                response_news.put("publication_desc",Desc);

                response_news.put("access_id",access_id[access_selected_position]);
                Log.d("access_id",access_id[access_selected_position]+"**"+access_name[access_selected_position]);

                JSONArray file_list = new JSONArray();


                Log.d("SSSSS",feedItems.size()+"**");
                for (int k=0;k<feedItems.size();k++){
                    JSONObject file = new JSONObject();
                    file.put("file_url",feedItems.get(k).getFile_url()[k]);
                    file.put("file_name",feedItems.get(k).getFile_name()[k]);
                    file.put("file_time",file_time);
                    file.put("file_format",feedItems.get(k).getFile_format()[k]);


                    file_list.put(file);

                }
                if (has_audio){
                    JSONObject file = new JSONObject();
                    file.put("file_url",audiofile_url);
                    file.put("file_name",audiofile_name);
                    file.put("file_time",file_time);
                    file.put("file_format",audiofile_format);


                    file_list.put(file);
                }
                if (has_video){
                    JSONObject file = new JSONObject();
                    file.put("file_url",videofile_url);
                    file.put("file_name",videofile_name);
                    file.put("file_time",videofile_time);
                    file.put("file_format",videofile_format);


                    file_list.put(file);
                }

                if (has_file){
                    JSONObject file = new JSONObject();
                    file.put("file_url",    filefile_url);
                    file.put("file_name",   filefile_name);
                    file.put("file_format", filefile_format);
                    file_list.put(file);
                }


                 SendNewsToServer(response_news);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void SendNewsToServer(JSONObject js) {
        String url =publication_byId+"/"+change_item.getPublication_id()+"?token="+TOKEN;

        Log.d("BBBBBBBBB",js+"mmm");
        AndroidNetworking.post(url)
                .addJSONObjectBody(js)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("AAAAAAMMMMM",response+"mmm");
                try {
                    if (response.getBoolean("status")) {
                        finish();
                    } else {
                        Toasty.error(ActivityChangeNews.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityChangeNews.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK ) {

            if (requestCode == RequestCodeForGallerey) {




                returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                int length = returnValue.size();
                file_name = new String[length];
                file_url = new String[length];
                file_format = new String[length];
                for (int i=0;i<length;i++){
                    FeedItem item = new FeedItem();
                    item.setName("");
                    item.setSfile_url(returnValue.get(i));
                    item.setIs_upload(false);

                    feedItems.add(item);
                }



                SendImageToServer(length,0);


            }else  if (requestCode == REQUEST_AUDIO_FILE) {
                Log.d("audio_ ","request");
                audio_layout.setVisibility(View.VISIBLE);

                mediaFiles.clear();
                mediaFiles.addAll(data.<MediaFile>getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                if (!mediaFiles.isEmpty()) {

                    String selectedVideoPath = mediaFiles.get(0).getPath();
                    if (selectedVideoPath != null) {
                        Log.d("AAAAff", selectedVideoPath);
                        File f =  new File(selectedVideoPath);
                        int sec = (int) TimeUnit.MILLISECONDS.toSeconds(mediaFiles.get(0).getDuration());
                        file_time  = sec;
                        Log.d("AAAAA",sec+"___"+mediaFiles.get(0).getDuration());
                        int second = file_time % 60;
                        int minute = file_time / 60;
                        String timeString = String.format("%02d:%02d", minute, second);
                        audio_time.setText(timeString);
                        audiofile_name = mediaFiles.get(0).getName();
                        audio_name.setText(audiofile_name);
                        SendAudioToServer(f);
                    }

                }
            }else if (requestCode == REQUEST_TAKE_GALLERY_VIDEO) {

                List<String> paths = Matisse.obtainPathResult(data);
                if (!paths.isEmpty()) {
                     selectedVideoPath = paths.get(0);
                    Log.d("SSSSSSs2",selectedVideoPath);

                    if (selectedVideoPath != null) {
                        File f = new File(selectedVideoPath);
                        MediaMetadataRetriever retriever = new MediaMetadataRetriever();

                        retriever.setDataSource(ActivityChangeNews.this, Uri.fromFile(f));
                        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                        long timeInMillisec = Long.parseLong(time );
                        videofile_time = (int) TimeUnit.MILLISECONDS.toSeconds(timeInMillisec);
                        Glide.with(this).load(f).into(videoview);
                        videolayout.setVisibility(View.VISIBLE);
                        SendVideoToServer(f);
                    }
                }
            }else  if (requestCode == REQUEST_TAKE_FILE) {
                ArrayList<NormalFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);

                if (!list.isEmpty()) {
                    file_layout.setVisibility(View.VISIBLE);
                    delete_file.setVisibility(View.VISIBLE);

                    String selectedVideoPath = list.get(0).getPath();
                    if (selectedVideoPath != null) {
                        Log.d("AAAAff", selectedVideoPath);
                        File f = new File(selectedVideoPath);
                        file_upload_progress.setVisibility(View.VISIBLE);

                        filefile_name = list.get(0).getName();
                        send_file_name.setText(filefile_name);
                        final String extensionRemoved = f.getName().split("\\.")[1];
                        send_file_format.setText(extensionRemoved);
                        switch (extensionRemoved){
                            case "pdf":
                                send_file_image.setColorFilter(getResources().getColor(R.color.color_pdf));
                                break;
                            case "xls":
                            case "xlsx":
                                send_file_image.setColorFilter(getResources().getColor(R.color.color_xls));
                                break;
                            case "doc":
                            case "docx":
                                send_file_image.setColorFilter(getResources().getColor(R.color.color_doc));
                                break;
                            case "ppt":
                            case "pptx":
                                send_file_image.setColorFilter(getResources().getColor(R.color.color_ppt));
                                break;
                            default:
                                send_file_image.setColorFilter(getResources().getColor(R.color.color_txt));
                                break;
                        }


                        SendFileToServer(f);
                    }
                }
            }
        }
    }

    @OnClick(R.id.delete_audio)
    void delete_audio(){
        has_audio = false;
        audiofile_name  ="";
        audiofile_url   ="";
        audiofile_format="";
        AndroidNetworking.cancel("uploadaudio");
        audio_layout.setVisibility(View.GONE);
        if (mediaPlayer != null && mediaPlayer.isPlaying())
                mediaPlayer.stop();

    }

    @OnClick(R.id.delete_video)
    void delete_video(){
        has_video = false;
        videofile_name  ="";
        videofile_url   ="";
        videofile_format="";
        videofile_time = 0;
        AndroidNetworking.cancel("uploadvideo");
        videolayout.setVisibility(View.GONE);


    }
    @OnClick(R.id.delete_file)
    void delete_file(){
        has_file = false;
        filefile_name  ="";
        filefile_url   ="";
        filefile_format="";
        AndroidNetworking.cancel("uploadfile");
        file_layout.setVisibility(View.GONE);
    }


    @Override
    protected void onStop() {
        if (mediaPlayer != null) {

            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;

        }
        super.onStop();
    }
    @OnClick(R.id.audioplay)
    void audioplay(){
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                audio_play.setImageResource(R.drawable.ic_play);
                mediaPlayer.pause();
            } else {
                audio_play.setImageResource(R.drawable.ic_pause);
                mediaPlayer.start();
            }
        } else if (!mediaFiles.isEmpty()) {
                audio_play.setImageResource(R.drawable.ic_pause);
                File f =  new File(mediaFiles.get(0).getPath());
                Uri uri = Uri.fromFile(f);
                mediaPlayer = MediaPlayer.create(ActivityChangeNews.this, uri);
                mediaPlayer.start();
                mediaPlayer.start();

        }
    }

    @OnClick(R.id.playvideo)
    void playvideo(){
        if (!selectedVideoPath.isEmpty()){
            Intent intent = new Intent(ActivityChangeNews.this, ActivityVideoPlayer.class);
            intent.putExtra("file_url", selectedVideoPath);
            startActivity(intent);
        }

    }


    private void SendAudioToServer(File f) {
        Log.d("audio_ ","SendAudioToServer");
        audioprogress.setVisibility(View.VISIBLE);
        AndroidNetworking.upload(file+TOKEN)
                .addMultipartFile("file", f)
                .addMultipartParameter("file_time",file_time+"")
                .setPriority(Priority.HIGH)
                .setTag("uploadaudio")
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        int progress = (int) (bytesUploaded*100 / totalBytes);
                        Log.d("pppp",progress+" *");
                        audioprogress.setProgress(progress);
                        audio_name.setText(progress+"%  - "+ audiofile_name);
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")) {
                                Log.d("audio_ ","status");
                                audio_name.setText(audiofile_name);
                                has_audio = true;

                                Log.d("response",response+" ");
                                audiofile_name  = response.getString("file_name");
                                audiofile_url   = response.getString("file_url");
                                audiofile_format= response.getString("file_format");
                                audioprogress.setVisibility(View.GONE);

                            } else {
                                has_audio = false;
                                audioprogress.setVisibility(View.GONE);
                                audio_layout.setVisibility(View.GONE);
                                file_time = 0;
                                Toasty.error(ActivityChangeNews.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("file response _ ", response + "***");
                    }

                    @Override
                    public void onError(ANError anError) {
//                        Toasty.error(ActivityChatMessage.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                        file_time = 0;
                        has_audio = false;
                        audioprogress.setVisibility(View.GONE);
                        audio_layout.setVisibility(View.GONE);

                    }
                });
    }

    private void SendVideoToServer(File f) {
        Log.d("audio_ ","SendVideoToServer");
        videoProgressbar.setVisibility(View.VISIBLE);
        AndroidNetworking.upload(file+TOKEN)
                .addMultipartFile("file", f)
                .addMultipartParameter("file_time",videofile_time+"")
                .setPriority(Priority.HIGH)
                .setTag("uploadvideo")
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        int progress = (int) (bytesUploaded*100 / totalBytes);
                        Log.d("pppp",progress+" *");
                        videoProgressbar.setProgress(progress);
                        video_process.setVisibility(View.VISIBLE);
                        video_process.setText(progress+" %");
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")) {
                                Log.d("audio_ ","status");
                                has_video = true;
                                Log.d("response",response+" ");
                                videofile_name  = response.getString("file_name");
                                videofile_url   = response.getString("file_url");
                                videofile_format= response.getString("file_format");
                                videoProgressbar.setVisibility(View.GONE);
                                video_process.setVisibility(View.GONE);

                            } else {
                                has_video= false;
                                videoProgressbar.setVisibility(View.GONE);
                                videolayout.setVisibility(View.GONE);
                                video_process.setVisibility(View.GONE);
                                videofile_time = 0;
                                Toasty.error(ActivityChangeNews.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("file response _ ", response + "***");
                    }

                    @Override
                    public void onError(ANError anError) {
//                        Toasty.error(ActivityChatMessage.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                        has_video= false;

                        videoProgressbar.setVisibility(View.GONE);
                        videolayout.setVisibility(View.GONE);
                        video_process.setVisibility(View.GONE);
                        videofile_time = 0;
                        Log.d("file anError _ ", "Проверить подключение интернета" + "***");
                        Log.d("file anError _ ", anError.getErrorCode() + "***");
                    }
                });
    }

    private void SendFileToServer(File f) {
        Log.d("audio_ ","SendAudioToServer");
        file_upload_progress.setVisibility(View.VISIBLE);
        AndroidNetworking.upload(file+TOKEN)
                .addMultipartFile("file", f)
                .addMultipartParameter("file_time",file_time+"")
                .setPriority(Priority.HIGH)
                .setTag("uploadaudio")
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        int progress = (int) (bytesUploaded*100 / totalBytes);
                        Log.d("pppp",progress+" *");
                        file_upload_progress.setProgress(progress);
                        send_file_name.setText(progress+"%  - "+ filefile_name);
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")) {
                                Log.d("file_ ","status");
                                send_file_name.setText(filefile_name);
                                has_file = true;

                                Log.d("response",response+" ");
                                filefile_name  = response.getString("file_name");
                                filefile_url   = response.getString("file_url");
                                filefile_format= response.getString("file_format");
                                file_upload_progress.setVisibility(View.GONE);

                            } else {
                                has_file = false;
                                file_upload_progress.setVisibility(View.GONE);
                                file_layout.setVisibility(View.GONE);
                                Toasty.error(ActivityChangeNews.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("file response _ ", response + "***");
                    }

                    @Override
                    public void onError(ANError anError) {
//                        Toasty.error(ActivityChatMessage.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                        has_file = false;
                        file_upload_progress.setVisibility(View.GONE);
                        file_layout.setVisibility(View.GONE);
                        file_upload_progress.setVisibility(View.GONE);

                    }
                });
    }


    public void SendImageToServer( final int length, final int position){

        String new_filename =  MyConstants.compressImage(feedItems.get(position).getName(), ActivityChangeNews.this);
        File f = new File(new_filename);


        Log.d("AAAAAAAA",length+"**"+position);
        AndroidNetworking.upload(file+TOKEN)
                .setTag("upload")
                .addMultipartFile("file", f)
                .setPriority(Priority.HIGH)
                .build()
//                    .setUploadProgressListener(new UploadProgressListener() {
//                        @Override
//                        public void onProgress(long bytesUploaded, long totalBytes) {
//                            // do anything with progress
//                        }
//                    })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")) {

                                file_name[position] = response.getString("file_name");
                                file_url[position] = response.getString("file_url");
                                file_format[position] = response.getString("file_format");
                                feedItems.get(position).setFile_name(file_name);
                                feedItems.get(position).setFile_url(file_url);
                                feedItems.get(position).setFile_format(file_format);
                                feedItems.get(position).setIs_upload(true);
                                if(position+1<length) {
                                    SendImageToServer(length,position+1);
                                }



//                                adapter.notifyDataSetChanged();
                            } else {
                               Toasty.error(ActivityChangeNews.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                            }

                            deleteFileName(f.getName());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("file response _ ", response + "***");
                    }

                    @Override
                    public void onError(ANError anError) {
//                        Toasty.error(ActivityChatMessage.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                        Log.d("file anError _ ", "Проверить подключение интернета" + "***");
                        Log.d("file anError _ ", anError.getErrorCode() + "***");
                    }
                });

    }


}
