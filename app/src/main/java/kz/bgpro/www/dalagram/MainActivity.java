package kz.bgpro.www.dalagram;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmResults;
import kz.bgpro.www.dalagram.Realm.Dialog;
import kz.bgpro.www.dalagram.Realm.DialogDetail;
import kz.bgpro.www.dalagram.activitys.ActivityAppShare;
import kz.bgpro.www.dalagram.activitys.profile.ActivityMyProfile;
import kz.bgpro.www.dalagram.activitys.ActivitySearchChat;
import kz.bgpro.www.dalagram.fragment.FragmentChatlist;
import kz.bgpro.www.dalagram.fragment.FragmentContacts;
import kz.bgpro.www.dalagram.fragment.FragmentNews;
import kz.bgpro.www.dalagram.fragment.FragmentNotifi;
import kz.bgpro.www.dalagram.fragment.FragmentStars;

import static kz.bgpro.www.dalagram.fragment.FragmentChatlist.SetDataToList;
import static kz.bgpro.www.dalagram.utils.MyConstants.PREFS_NAME;
import static kz.bgpro.www.dalagram.utils.MyConstants.bookmark;
import static kz.bgpro.www.dalagram.utils.MyConstants.chat;
import static kz.bgpro.www.dalagram.utils.MyConstants.mute;
import static kz.bgpro.www.dalagram.fragment.FragmentChatlist.feedItem;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

public class MainActivity extends AppCompatActivity {
    @BindViews({R.id.menu1, R.id.menu2, R.id.menu3, R.id.menu4, R.id.menu5})
    ImageButton [] tab_menu;


    @BindView(R.id.profile_image)
    CircleImageView profile_image;

    @BindView(R.id.connect_txt)
    TextView connect_txt;

    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.chat_menu)
    LinearLayout chat_menu;

    public static String TOKEN;
    public static int MY_USER_ID;
    public static String  MY_USER_NAME;
    SharedPreferences settings;

    private Socket mSocket;
    private Boolean isConnected = false;
    Handler handler = new Handler();
    Handler handler_online = new Handler();
    boolean is_typing = false;
    String [] menu_str = {"menu1", "menu2", "menu3", "menu4", "menu5"};
    Fragment [] fragments = {new FragmentChatlist(),new FragmentContacts(),new FragmentNews(), new FragmentStars(),new FragmentNotifi()};
    Realm realm;

    int other_userId=0,position_chat_id=0;
    String Who,Dialog_id=null;

    @BindView(R.id.menu_mute)
    TextView menu_mute;
    @BindView(R.id.menu_pin)
    TextView menu_pin;
    static int selected_menu=2;
    boolean is_first = true;
    public static boolean is_update_news = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        realm = Realm.getDefaultInstance();
        chat_menu.setVisibility(View.GONE);
        other_userId=0;
        Dialog_id=null;
        position_chat_id=0;

        SocketConnect();


    }


    @OnClick(R.id.btn_search)
    void Search(){
        startActivity(new Intent(MainActivity.this,ActivitySearchChat.class));
    }




    @OnClick(R.id.app_share)
    void app_share(){
        startActivity(new Intent(MainActivity.this,ActivityAppShare.class));
    }


    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if(isConnected) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("sender_id", MY_USER_ID);
                    jsonObject.put("type", "online");
                    mSocket.emit("online", jsonObject);
//                    handler_online.postDelayed(this, 15000);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }
    };

    @OnClick(R.id.menu1)
    void menu1(){
        ClickMenuButton(0);
    }

    @OnClick(R.id.menu2)
    void menu2(){
        ClickMenuButton(1);
    }

    @OnClick(R.id.menu3)
    void menu3(){
        ClickMenuButton(2);
    }

    @OnClick(R.id.menu4)
    void menu4(){
        ClickMenuButton(3);
    }

    @OnClick(R.id.menu5)
    void menu5(){
        ClickMenuButton(4);
    }

    private void ClickMenuButton(int position) {
        is_first = false;
        selected_menu = position;
        for (ImageButton aTab_menu : tab_menu) aTab_menu.setSelected(false);
        tab_menu[position].setSelected(true);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content, fragments[position], menu_str[position]);
//        fragmentTransaction.addToBackStack(title);
        fragmentTransaction.commit();

    }




    private void SocketConnect() {
        ChatApplication app = (ChatApplication)getApplication();
        mSocket = app.getSocket();
        mSocket.on(Socket.EVENT_CONNECT,onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT,onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectTimeout);
        mSocket.on("message", onNewMessage);
        mSocket.on("typing", onTyping);
        mSocket.connect();
        if(mSocket.connected()){
           handler_online.postDelayed(runnable, 10000);

            logo.setVisibility(View.VISIBLE);
            connect_txt.setVisibility(View.GONE);
        }else {
            logo.setVisibility(View.GONE);
            connect_txt.setVisibility(View.VISIBLE);

        }
    }
    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!isConnected) {

                        handler_online.postDelayed(runnable, 10000);


//                        Toasty.success(getApplicationContext(), "connect", Toast.LENGTH_SHORT).show();
                        isConnected = true;

                        logo.setVisibility(View.VISIBLE);
                        connect_txt.setVisibility(View.GONE);
//                        JSONObject jsonObject = new JSONObject();
//                        try {
//                            jsonObject.put("sender_id",MY_USER_ID);
//                            mSocket.emit("online",jsonObject);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    }

                }
            });
        }
    };


    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    isConnected = false;
                    logo.setVisibility(View.GONE);
                    connect_txt.setVisibility(View.VISIBLE);
//                    Toasty.warning(getApplicationContext(), "disconnect", Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    isConnected = false;
                    logo.setVisibility(View.GONE);
                    connect_txt.setVisibility(View.VISIBLE);
//                    Toasty.error(getApplicationContext(), "error_connect", Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    private Emitter.Listener onConnectTimeout = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    isConnected = false;
                    logo.setVisibility(View.GONE);
                    connect_txt.setVisibility(View.VISIBLE);
//                    Toasty.error(getApplicationContext(), "onConnectTimeout", Toast.LENGTH_SHORT).show();
                }
            });
        }
    };


    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            final int[] finalI = {0};
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(!is_typing) {
                        try {
                            int i = 0;

                            JSONObject data = (JSONObject) args[0];
                            Log.d("data", data + "**");
                            boolean has_user = false;
                            String dialog_id = data.getString("dialog_id");
                            int sender_id = data.getInt("sender_id");
                            if (dialog_id.contains("U")) {
                                while (!has_user) {
                                    if(feedItem!=null && feedItem.size()>0) {
                                        if (feedItem.get(i).getUser_id() == sender_id && (MY_USER_ID + "U").equals(dialog_id)) {
                                            is_typing = true;
                                            feedItem.get(i).setIs_typing(true);
                                            FragmentChatlist.chatListAdapter.notifyDataSetChanged();
                                            has_user = true;
                                            Log.d("has_user", i + "__");
                                            finalI[0] = i;
                                        }
                                        i++;
                                        if (i == feedItem.size()) {
                                            break;
                                        }
                                    }

                                }
                            }else if (dialog_id.contains("G")){
                                while (!has_user) {
                                    if(feedItem!=null && feedItem.size()>0) {
                                        if ( dialog_id.equals(feedItem.get(i).getDialog_id())) {
                                            is_typing = true;
                                            feedItem.get(i).setIs_typing(true);
                                            FragmentChatlist.chatListAdapter.notifyDataSetChanged();
                                            has_user = true;
                                            Log.d("has_user", i + "__");
                                            finalI[0] = i;
                                        }
                                        i++;
                                        if (i == feedItem.size()) {
                                            break;
                                        }
                                    }

                                }
                            }
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    feedItem.get(finalI[0]).setIs_typing(false);
                                    FragmentChatlist.chatListAdapter.notifyDataSetChanged();
                                    is_typing=false;
                                }
                            }, 2000);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final JSONObject data = (JSONObject) args[0];
                    Log.d("new mwssage data",data+"**");

                    try {

                        final JSONObject sender = data.getJSONObject("sender");
                        if(data.getInt("recipient_user_id")==MY_USER_ID){
                            GetDataFromJSON();
                            createNotificationChannel(sender.getString("user_name"),data.getString("chat_text"));

                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                    Ringtone ringtone = RingtoneManager.getRingtone(MainActivity.this,uri);
                                    ringtone.play();

                                }
                            }, 1500);

                        }
                    } catch (JSONException e) {
                        return;
                    }
                }
            });
        }
    };

    private void createNotificationChannel(String name, String description) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "1")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(name+" новое сообщение")
                .setContentText(description)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

// notificationId is a unique int for each notification that you must define
        notificationManager.notify( 1, mBuilder.build());

    }



    @Override
    protected void onStart() {
        super.onStart();
        SocketConnect();
        position_chat_id = 0;
        other_userId = 0;
        Dialog_id = null;
        TOKEN = settings.getString("token","*");
        MY_USER_ID = settings.getInt("user_id",0);
        MY_USER_NAME = settings.getString("user_name","");
        if (!TextUtils.isEmpty(settings.getString("avatar",""))){

            Glide.with(this).load(settings.getString("avatar","null"))
                    .into(profile_image);
        }

        if ( is_first) {
            is_first = false;
            ClickMenuButton(selected_menu);
            Log.d("AAAAAis_first",is_first+"**");
        }

        Log.d("TOKEN",TOKEN);
        Log.d("MY_USER_ID",MY_USER_ID+"_");
    }

    @Override
    protected void onPause() {
        Log.d("onPause","onPause");
        mSocket.off("message", onNewMessage);
        mSocket.off("typing", onTyping);
        super.onPause();
    }


    int TIME_DELAY = 2000;
    long back_pressed;
    @Override
    public void onBackPressed() {
        chat_menu.setVisibility(View.GONE);
        other_userId=0;
        Dialog_id=null;
        position_chat_id=0;

        if (selected_menu==2){
            if (!is_update_news){
                FragmentNews fragmentNews = (FragmentNews)getSupportFragmentManager().findFragmentByTag( menu_str[2]);

                if(fragmentNews!=null){
                    fragmentNews.callAllMethod();
                }
                is_update_news = true;
            }else {
                if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
                    Intent a = new Intent(Intent.ACTION_MAIN);
                    a.addCategory(Intent.CATEGORY_HOME);
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(a);
                } else {

                    Toast.makeText(getBaseContext(), "Нажмите еще раз, чтобы выйти!",
                            Toast.LENGTH_SHORT).show();

                }
                back_pressed = System.currentTimeMillis();
            }

        }else {
            ClickMenuButton(2);
        }



    }

    public  void ShowMenu(String who, int position,int user_id, String dialog_id){
        Who = who;
        other_userId = user_id;
        Dialog_id = dialog_id;
        position_chat_id = position;
        Log.d("Dialog_id",Dialog_id+"***");
        chat_menu.setVisibility(View.VISIBLE);

        Dialog mydialog = realm.where(Dialog.class).equalTo("chat_id", position_chat_id).findFirst();
        if (mydialog!=null){
            if (mydialog.getIs_mute()==0){
                menu_mute.setText("Отключить уведомление");
            }else {
                menu_mute.setText("Включить уведомление");
            }
            if (mydialog.getIs_bookmark()==1){
                menu_pin.setText("Открепить чат");
            }else {
                menu_pin.setText("Закрепить чат");
            }
        }
    }

    @OnClick(R.id.profile_image)
    void profile_image(){
        startActivity(new Intent(MainActivity.this, ActivityMyProfile.class).putExtra("user_id",MY_USER_ID).putExtra("is_my",true));
    }

    @OnClick(R.id.chat_menu)
    void chat_menu(){
        chat_menu.setVisibility(View.GONE);
        other_userId=0;
        Dialog_id=null;
        position_chat_id=0;
    }

    @OnClick(R.id.mute)
    void mute(){
        Log.d("URL", mute+TOKEN+"__"+other_userId);

        AndroidNetworking.post(mute+TOKEN)
                .addBodyParameter("is_mute","0")
                .addBodyParameter(Who,other_userId+"")
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")){
                        GetDataFromJSON();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                chat_menu.setVisibility(View.GONE);
                other_userId=0;
                Dialog_id=null;
                position_chat_id=0;

                Log.d("mutemute",response+" ");
            }

            @Override
            public void onError(ANError anError) {
                chat_menu.setVisibility(View.GONE);
                other_userId=0;
                Dialog_id=null;
                position_chat_id=0;

                Toasty.error(MainActivity.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                Log.d("mutemute_anError",anError+" ");
            }
        });

    }

    @OnClick(R.id.pin)
    void pin(){
        if (other_userId!=0){
            Log.d("ASDASD",bookmark+TOKEN);
            Log.d("ASDASDqqq",Who+":"+other_userId+"");

            AndroidNetworking.post(bookmark+TOKEN)
                    .addBodyParameter(Who,other_userId+"")
                    .build().getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("response",response+" %");
                    try {
                        if (response.getBoolean("status")){
                            GetDataFromJSON();
                        }else {
                            Toasty.error(MainActivity.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    chat_menu.setVisibility(View.GONE);
                    other_userId=0;
                    Dialog_id=null;
                    position_chat_id=0;

                }

                @Override
                public void onError(ANError anError) {
                    chat_menu.setVisibility(View.GONE);
                    other_userId=0;
                    Dialog_id=null;
                    position_chat_id=0;
                    Toasty.error(MainActivity.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                    SetDataToList(1);
                }
            });
        }

    }

    @OnClick(R.id.clean)
    void clean(){
        Log.d("URL", chat+TOKEN+"&"+Who+"="+other_userId);
        AndroidNetworking.delete(chat+TOKEN+"&"+Who+"="+other_userId)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")){
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                RealmResults<DialogDetail> result = realm.where(DialogDetail.class).equalTo("dialog_id", Dialog_id).findAll();
                                result.deleteAllFromRealm();
                            }
                        });
                        GetDataFromJSON();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("cleanresponse",response+"***");
                chat_menu.setVisibility(View.GONE);
                other_userId=0;
                Dialog_id=null;
                position_chat_id=0;

            }
            @Override
            public void onError(ANError anError) {
                chat_menu.setVisibility(View.GONE);
                other_userId=0;
                Dialog_id=null;
                position_chat_id=0;
                Toasty.error(MainActivity.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                Log.d("cleanresponse_anError",anError+"***");
            }
        });

    }

    @OnClick(R.id.delete_chat)
    void delete_chat(){
        Log.d("URL", chat+TOKEN+"&is_delete_all=1&"+Who+"="+other_userId);
        AndroidNetworking.delete(chat+TOKEN+"&is_delete_all=1&"+Who+"="+other_userId)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")){
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                               Dialog result = realm.where(Dialog.class).equalTo("dialog_id", Dialog_id).findFirst();
                               Log.d("Dialog_id",Dialog_id+"**");
                               Log.d("Dialog_id2",result.getDialog_id());
                                result.deleteFromRealm();
                            }
                        });
                        GetDataFromJSON();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("cleanresponse",response+"***");

                chat_menu.setVisibility(View.GONE);
                other_userId=0;
                Dialog_id=null;
                position_chat_id=0;

            }
            @Override
            public void onError(ANError anError) {
                chat_menu.setVisibility(View.GONE);
                other_userId=0;
                Dialog_id=null;
                position_chat_id=0;

                Toasty.error(MainActivity.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                Log.d("cleanresponse_anError",anError+"***");

            }
        });


    }

    public void GetDataFromJSON() {
        Log.d("chat_list",chat + TOKEN + "&per_page=30&page=1");
        AndroidNetworking.get(chat + TOKEN + "&per_page=30&page=1")
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")) {

                        JSONArray data = response.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {

                            JSONObject jsonObject = (JSONObject) data.get(i);
                            String file_format="";
                            if(jsonObject.getInt("is_has_file")==1){
                                file_format = ((JSONObject)jsonObject.getJSONArray("file_list").get(0)).getString("file_format");
                            }
                            int is_sticker = NurInt(jsonObject, "is_sticker");
                            String sticker_image="";
                            if (is_sticker==1){
                                JSONObject sticker = jsonObject.getJSONObject("sticker");
                                sticker_image =  NurString(sticker, "sticker_image");
                            }

                            Dialog dialog = new Dialog(NurInt(jsonObject, "chat_id"),NurInt(jsonObject, "is_bookmark"), NurInt(jsonObject, "is_read"), NurInt(jsonObject, "is_mute"), NurInt(jsonObject, "i_block_partner"),
                                    NurInt(jsonObject, "partner_block_me"), NurInt(jsonObject, "is_has_file"),  NurInt(jsonObject, "is_contact"), NurInt(jsonObject, "new_message_count"), NurInt(jsonObject, "group_id"), NurInt(jsonObject, "channel_id"),
                                    NurInt(jsonObject, "user_id"), NurInt(jsonObject, "is_admin"), NurString(jsonObject, "chat_kind"), NurString(jsonObject, "action_name"), NurString(jsonObject, "chat_name"),
                                    NurString(jsonObject, "avatar"), NurString(jsonObject, "phone"), NurString(jsonObject, "nickname"), NurString(jsonObject, "chat_text"), NurString(jsonObject, "chat_date"),
                                    NurString(jsonObject, "last_visit"), NurString(jsonObject, "dialog_id"), System.currentTimeMillis(),file_format,jsonObject.getBoolean("is_own_last_message"),is_sticker,sticker_image);

                        }

                        SetDataToList(1);
                    } else {
                        SetDataToList(1);
                        Toasty.error(MainActivity.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                SetDataToList(1);
                Toasty.error(MainActivity.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();


            }
        });

    }

}
