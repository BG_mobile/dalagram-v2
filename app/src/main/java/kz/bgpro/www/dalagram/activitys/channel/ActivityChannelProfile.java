package kz.bgpro.www.dalagram.activitys.channel;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.ActivityAddUser;
import kz.bgpro.www.dalagram.activitys.ActivityZoomImage;
import kz.bgpro.www.dalagram.activitys.SharedMedia;
import kz.bgpro.www.dalagram.activitys.adapters.ChannelUsersAdapter;
import kz.bgpro.www.dalagram.activitys.adapters.UsersAdapter;
import kz.bgpro.www.dalagram.activitys.groups.ActivityGroupProfile;
import kz.bgpro.www.dalagram.activitys.groups.GProfileChange;
import kz.bgpro.www.dalagram.models.FeedItem;
import kz.bgpro.www.dalagram.utils.NonScrollListView;
import kz.bgpro.www.dalagram.utils.NurJS;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.activitys.ActivityChatMessage.Is_mute;
import static kz.bgpro.www.dalagram.utils.MyConstants.channel;
import static kz.bgpro.www.dalagram.utils.MyConstants.group;
import static kz.bgpro.www.dalagram.utils.MyConstants.media;
import static kz.bgpro.www.dalagram.utils.MyConstants.mute;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

public class ActivityChannelProfile extends Activity implements ObservableScrollViewCallbacks {




    @BindView(R.id.scroll)
    ObservableScrollView scroll;

    @BindView(R.id.toolBar)
    RelativeLayout toolBar;

    @BindView(R.id.avatar)
    ImageView avatar;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.channel_login)
    TextView channel_login;

    @BindView(R.id.media_count)
    TextView media_count;

    @BindView(R.id.users_layout)
    LinearLayout users_layout;

    @BindView(R.id.email)
    TextView email;

    @BindView(R.id.last_visible)
    TextView last_visible;

    @BindView(R.id.notification)
    Switch notification;

    @BindView(R.id.menu)
    ImageButton menu;

    @BindView(R.id.add_user)
    LinearLayout add_user;

    @BindView(R.id.participant_list)
    NonScrollListView participant_list;

    ArrayList<FeedItem> feedItem_users ;

    int channel_id,is_admin;
    String AVATAR="";

    JSONObject media_response=null;

    String user_response;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channelprofile);
        ButterKnife.bind(this);
        scroll.setScrollViewCallbacks(this);

        if (getIntent().hasExtra("channel_id")){
            channel_id = getIntent().getIntExtra("channel_id",0);
            is_admin = getIntent().getIntExtra("is_admin",0);
            user_response = getIntent().getStringExtra("user_response");
        }
        if (is_admin==1){
            GetGroupUsers(user_response);
            menu.setVisibility(View.VISIBLE);
            users_layout.setVisibility(View.VISIBLE);
        }else {
            menu.setVisibility(View.GONE);
            users_layout.setVisibility(View.GONE);
        }


        if (Is_mute==0){
            notification.setChecked(true);
        }else {
            notification.setChecked(false);
        }


        notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d("icheck",isChecked+" ");

                Is_mute = isChecked ? 0 : 1;

                AndroidNetworking.post(mute+TOKEN)
                        .addBodyParameter("is_mute",Is_mute+"")
                        .addBodyParameter("channel_id",channel_id+"")
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("mutemute",response+" ");
                        Log.d("Is_mute",Is_mute+" ");

                    }

                    @Override
                    public void onError(ANError anError) {
                        Toasty.error(ActivityChannelProfile.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });

    }

    @OnClick(R.id.menu)
    void menu(){
        if (is_admin==1) {
            startActivity(new Intent(ActivityChannelProfile.this, ChannelProfileChange.class)
                    .putExtra("channel_id", channel_id)
                    .putExtra("name", name.getText().toString()+"")
                    .putExtra("avatar", AVATAR+"")
                    .putExtra("email", email.getText().toString()+"")
                    .putExtra("channel_login", channel_login.getText().toString()+"")
            );
        }


    }

    public void GetProfileFromServer(){

        Log.d("ggg",channel+"/"+channel_id+"?token="+TOKEN);
        AndroidNetworking.get(channel+"/"+channel_id+"?token="+TOKEN)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("PPP",response+" *");
                try {
                    if(response.getBoolean("status")){
                        Log.d("profile",response+"++");
                        JSONObject data = response.getJSONObject("data");
                        name.setText(NurString(data,"channel_name"));
//                        email.setText(NurString(data,"email"));
                        channel_login.setText(NurString(data,"channel_login"));

                        if(!TextUtils.isEmpty(NurString(data,"channel_avatar"))){
                            AVATAR = data.getString("channel_avatar");
                            Glide.with(ActivityChannelProfile.this).load(AVATAR).into(avatar);

                        }


                    }else {

                        Toasty.error(ActivityChannelProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityChannelProfile.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });


    }


    @OnClick(R.id.add_user)
    void add_user(){
        startActivity(new Intent(ActivityChannelProfile.this,ActivityAddUser.class).putExtra("channel_id",channel_id));
    }



    @OnClick(R.id.back)
    void back(){
        finish();
    }


    @OnClick(R.id.shared_media)
    void shared_media(){
        if(media_response!=null){
            Intent intent = new Intent(ActivityChannelProfile.this,SharedMedia.class);
            intent.putExtra("media_response",media_response+"");
            startActivity(intent);
        }

    }


    @OnClick(R.id.delete_exit)
    void delete_exit(){

        String url = channel+"/user/"+channel_id+"?token="+TOKEN;
        AndroidNetworking.delete(url).build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")){
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("AAAAA33",response+" ");
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityChannelProfile.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                Log.d("AAAAA ERr",anError+" ");
            }
        });
    }


    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        int baseColor = getResources().getColor(R.color.colorPrimary);
        float alpha = Math.min(1, (float) scrollY / 300);
        toolBar.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @OnClick(R.id.avatar)
    void  avatarclick (){
        if(!TextUtils.isEmpty(AVATAR)){
            Intent intent = new Intent(ActivityChannelProfile.this, ActivityZoomImage.class);
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("url", new ArrayList<>(Arrays.asList(AVATAR)));
            bundle.putInt("position", 0);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    public void GetGroupUsers(String user_response) {
        feedItem_users = new ArrayList<FeedItem>();
        try {
            JSONObject response = new JSONObject(user_response);
            if (response.getBoolean("status")) {
                Log.d("profile", response + "++");
                JSONArray data = response.getJSONArray("data");
                String usersCount;
                if (data.length() == 1) {
                    usersCount = data.length() + " участник";
                } else if (data.length() == 2 || data.length() == 3 || data.length() == 4) {
                    usersCount = data.length() + " участника";
                } else {
                    usersCount = data.length() + " участников";
                }
                last_visible.setText(usersCount);

                for (int i = 0; i < data.length(); i++) {
                    JSONObject object = (JSONObject) data.get(i);
                    FeedItem item = new FeedItem();
                    item.setUser_id(object.getInt("user_id"));

                    Log.d("sssss1__" + i, NurString(object, "contact_user_name") + "**");

                    if (!NurString(object, "nickname").equals("")) {
                        item.setName(NurString(object, "nickname"));
                    } else if (!NurString(object, "contact_user_name").equals("")) {
                        item.setName(NurString(object, "contact_user_name"));
                    } else if (!NurString(object, "user_name").equals("")) {
                        item.setName(NurString(object, "user_name"));
                    } else {
                        item.setName(NurString(object, "phone"));
                    }

                    item.setPhone(NurJS.NurString(object, "phone"));
                    item.setAvatar(NurJS.NurString(object, "avatar"));
                    item.setUser_status(NurJS.NurString(object, "user_status"));
                    item.setLast_visit(NurJS.NurString(object, "last_visit"));
                    item.setIs_admin(NurInt(object, "is_admin"));
                    feedItem_users.add(item);
                }
                ChannelUsersAdapter usersAdapter = new ChannelUsersAdapter(ActivityChannelProfile.this, feedItem_users, is_admin, channel_id);
                participant_list.setAdapter(usersAdapter);


            } else {

                Toasty.error(ActivityChannelProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void GetGroupUsers() {
        feedItem_users = new ArrayList<FeedItem>();

        Log.d("url",channel+"/user/"+channel_id+"?token="+TOKEN);

        AndroidNetworking.get(channel+"/user/"+channel_id+"?token="+TOKEN)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("RRRRRR",response+" ");
                try {
                    if(response.getBoolean("status")){
                        user_response = response+" ";
                        Log.d("profile",response+"++");
                        JSONArray data = response.getJSONArray("data");
                        String usersCount;
                        if(data.length()==1){
                            usersCount = data.length()+" участник";
                        }else if(data.length()==2 || data.length()==3 || data.length()==4){
                            usersCount = data.length()+" участника";
                        }else {
                            usersCount = data.length()+" участников";
                        }

                        last_visible.setText(usersCount);

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject object = (JSONObject) data.get(i);
                            FeedItem item = new FeedItem();
                            item.setUser_id(object.getInt("user_id"));

                            Log.d("sssss1__" + i, NurString(object, "contact_user_name") + "**");

                            if (!NurString(object, "nickname").equals("")) {
                                item.setName(NurString(object, "nickname"));
                            } else if (!NurString(object, "contact_user_name").equals("")) {
                                item.setName(NurString(object, "contact_user_name"));
                            } else if (!NurString(object, "user_name").equals("")) {
                                item.setName(NurString(object, "user_name"));
                            } else {
                                item.setName(NurString(object, "phone"));
                            }

                            item.setPhone(NurJS.NurString(object, "phone"));
                            item.setAvatar(NurJS.NurString(object, "avatar"));
                            item.setUser_status(NurJS.NurString(object, "user_status"));
                            item.setLast_visit(NurJS.NurString(object, "last_visit"));
                            item.setIs_admin(NurInt(object, "is_admin"));
                            feedItem_users.add(item);
                        }
                        ChannelUsersAdapter usersAdapter = new ChannelUsersAdapter(ActivityChannelProfile.this, feedItem_users, is_admin, channel_id);
                        participant_list.setAdapter(usersAdapter);

                    }else {

                        Toasty.error(ActivityChannelProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityChannelProfile.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });
    }


    private void GetMediaFromServer() {
        Log.d("MEdia",media+TOKEN+"&channel_id="+channel_id);
        AndroidNetworking.get(media+TOKEN+"&channel_id="+channel_id+"&page=1&per_page=10000").build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if(response.getBoolean("status")){
                        media_response = response;
                        Log.d("media",response+"++");
                        JSONArray data = response.getJSONArray("data");
                        int l = data.length();
                        media_count.setText(l+" ");
                    }else {

                        Toasty.error(ActivityChannelProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityChannelProfile.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        GetMediaFromServer();
        GetProfileFromServer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (is_admin==1)
            GetGroupUsers();
    }
}
