package kz.bgpro.www.dalagram.activitys;

import android.Manifest;
import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.webkit.WebView;
import android.widget.TextView;

import com.github.pavlospt.roundedletterview.RoundedLetterView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.bgpro.www.dalagram.R;

/**
 * Created by nurbaqyt on 02.08.2018.
 */

public class ActivityContactMessage extends Activity{



    @BindView(R.id.contact_name)
    TextView contact_name;

    @BindView(R.id.contact_phone)
    TextView contact_phone;

    @BindView(R.id.contact_avatar)
    RoundedLetterView contact_avatar;
    String sContact_name;
    String sContact_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactmessage);
        ButterKnife.bind(this);



         sContact_name = getIntent().getStringExtra("contact_name");
         sContact_phone = getIntent().getStringExtra("contact_phone");

        contact_name.setText(sContact_name);
        contact_avatar.setTitleText(sContact_name.charAt(0)+""+sContact_name.charAt(1));
        contact_phone.setText(sContact_phone);



    }

    @OnClick(R.id.back)
    void back(){
        finish();
    }

    @OnClick(R.id.save_contac)
    void Save(){
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_CONTACTS)
                .withListener(new PermissionListener() {
                    @Override public void onPermissionGranted(PermissionGrantedResponse response) {

                        Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                        contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                        contactIntent
                                .putExtra(ContactsContract.Intents.Insert.NAME, sContact_name)
                                .putExtra(ContactsContract.Intents.Insert.PHONE, sContact_phone);

                        startActivityForResult(contactIntent, 1);

                    }
                    @Override public void onPermissionDenied(PermissionDeniedResponse response) {
                        Log.d("SSSSSSS","2"+response.getPermissionName());
                    }
                    @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        Log.d("SSSSSSS","3"+permission.getName());

                    }
                }).check();

    }


    @OnClick(R.id.call_contac)
    void call_contac(){
        Uri call = Uri.parse("tel:" + sContact_phone);
        Intent surf = new Intent(Intent.ACTION_DIAL, call);
        startActivity(surf);
    }

//    String number = "7777777777";
//    Uri call = Uri.parse("tel:" + number);
//    Intent surf = new Intent(Intent.ACTION_DIAL, call);
//    startActivity(surf);

}
