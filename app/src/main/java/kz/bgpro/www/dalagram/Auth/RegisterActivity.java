package kz.bgpro.www.dalagram.Auth;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;


public class RegisterActivity extends Activity {

    int DIALOG_DATE = 1;
    int myYear = 1980;
    int myMonth = 00;
    int myDay = 01;

    @BindView(R.id.birthday)
    EditText birthday;

    @BindView(R.id.nick_name)
    EditText nick_name;

    @BindView(R.id.first_name)
    EditText first_name;
    @BindView(R.id.second_name)
    EditText second_name;
    @BindView(R.id.status)
    EditText status;

    @BindView(R.id.gender)
    RadioGroup gender;
    @BindView(R.id.male)
    RadioButton male;
    @BindView(R.id.female)
    RadioButton female;
    @BindColor(R.color.black)
    int black;
    @BindColor(R.color.black_87)
    int black_87;

    int is_male=1;

    @BindString(R.string.input_all)
    String input_all;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);


        gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if(checkedId == R.id.male) {
                    is_male=1;
                    male.setTextColor(black);
                    female.setTextColor(black_87);
                } else if(checkedId == R.id.female) {
                    is_male=0;
                    female.setTextColor(black);
                    male.setTextColor(black_87);
                }
            }

        });


    }



    @OnClick(R.id.birthday)
    void birthday(){
        showDialog(DIALOG_DATE);
    }


    @OnClick(R.id.register_btn)
    void register_btn(){
        String Nick = nick_name.getText().toString();
        String First = first_name.getText().toString();
        String Second = second_name.getText().toString();
        String Birth = birthday.getText().toString();
        String Status = status.getText().toString();

        if (TextUtils.isEmpty(Nick) && TextUtils.isEmpty(First) && TextUtils.isEmpty(Second) && TextUtils.isEmpty(Birth) ){
            Toasty.info(this,input_all , Toast.LENGTH_SHORT).show();
        }else {
            Intent intent = new Intent(RegisterActivity.this,ActivityEmail.class);
            intent.putExtra("nick_name",Nick);
            intent.putExtra("first_name",First);
            intent.putExtra("second_name",Second);
            intent.putExtra("birthday",Birth);
            intent.putExtra("status",Status);
            intent.putExtra("is_male",is_male);
            startActivity(intent);
        }

    }




    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_DATE) {
            DatePickerDialog tpd = new DatePickerDialog(this, myCallBack, myYear, myMonth, myDay);
            return tpd;
        }
        return super.onCreateDialog(id);
    }

    DatePickerDialog.OnDateSetListener myCallBack = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myYear = year;
            myMonth = monthOfYear+1;
            myDay = dayOfMonth;
            birthday.setText(  myDay + "." + myMonth + "." + myYear);
        }
    };


}
