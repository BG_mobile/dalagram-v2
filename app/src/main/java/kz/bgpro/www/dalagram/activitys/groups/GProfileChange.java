package kz.bgpro.www.dalagram.activitys.groups;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.fxn.pix.Pix;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import kz.bgpro.www.dalagram.R;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.group;
import static kz.bgpro.www.dalagram.utils.MyConstants.group_avatar;



public class GProfileChange extends AppCompatActivity {



    @BindView(R.id.avatar)
    ImageView avatar;

   @BindView(R.id.name)
   EditText name;

    @BindView(R.id.status)
    EditText status;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.done)
    ImageButton done;

    @BindView(R.id.progress)
    ProgressBar progress;
    int group_id;
    String Name,Status,Email,AVATAR;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groupprofile_change);
        ButterKnife.bind(this);
        progress.setVisibility(View.GONE);
        done.setVisibility(View.VISIBLE);
        if (getIntent().hasExtra("group_id")){
            group_id = getIntent().getIntExtra("group_id",0);
            Name = getIntent().getStringExtra("name");
            Status = getIntent().getStringExtra("status");
            Email = getIntent().getStringExtra("email");
            AVATAR = getIntent().getStringExtra("avatar");
            Glide.with(this).load(AVATAR).into(avatar);
            name.setText(Name);
            status.setText(Status);
            email.setText(Email);
        }


    }
    @OnClick(R.id.avatar_btn)
    void avatar_btn(){
        Pix.start(GProfileChange.this,                    //Activity or Fragment Instance
                704,                //Request code for activity results
                1);
    }
 @OnClick(R.id.back)
    void back(){
        finish();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 704) {
                ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                File f = new File(returnValue.get(0));
               Bitmap rotatedBMP = new BitmapDrawable(getResources(), f.getAbsolutePath()).getBitmap();
                avatar.setImageBitmap(rotatedBMP);
                UploadImage(f);
            }
        }
    }

    @OnClick(R.id.done)
    void done(){
        final String mName = name.getText().toString();
        if (TextUtils.isEmpty(mName)){
            Toasty.info(this, "Введите имя", Toast.LENGTH_SHORT).show();
        }else if (!mName.equals(Name)){
            progress.setVisibility(View.VISIBLE);
            done.setVisibility(View.GONE);

                AndroidNetworking.post(group+"/"+group_id+"?token="+TOKEN)
                        .addBodyParameter("group_name",mName)
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("RRRRRR",response+" ");
                            if(response.getBoolean("status")){

                                    done.setVisibility(View.VISIBLE);
                                    progress.setVisibility(View.GONE);
                                    finish();

                            }else {
                                Toasty.error(GProfileChange.this, response.getString("error"), Toast.LENGTH_SHORT).show();

                                done.setVisibility(View.VISIBLE);
                                progress.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toasty.error(GProfileChange.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                    }
                });


        }else {
            finish();
        }
    }

    public void UploadImage(File  f) {
         AndroidNetworking.upload(group_avatar+TOKEN)
                    .addMultipartFile("image", f)
                    .addMultipartParameter("group_id",group_id+"")
                    .build().getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                }

                @Override
                public void onError(ANError anError) {
                    Toasty.error(GProfileChange.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                }
            });

    }



}
