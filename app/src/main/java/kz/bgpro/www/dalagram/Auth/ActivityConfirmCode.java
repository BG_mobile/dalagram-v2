package kz.bgpro.www.dalagram.Auth;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.MainActivity;
import kz.bgpro.www.dalagram.R;

import static kz.bgpro.www.dalagram.utils.MyConstants.PREFS_NAME;
import static kz.bgpro.www.dalagram.utils.MyConstants.confirm;

public class ActivityConfirmCode extends Activity {


    @BindView(R.id.code)
    EditText code;

    String phone;

    @BindView(R.id.time)
    TextView time;

    @BindView(R.id.phone)
    TextView phone_number;

    @BindView(R.id.progress)
    ProgressBar progress;

    SharedPreferences settings;

    @BindString(R.string.repeat_send_code)
    String repeat_send_code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmcode);
        ButterKnife.bind(this);
        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);


        phone = getIntent().getStringExtra("phone");
        phone_number.append(" "+phone);


        new CountDownTimer(40000, 1000) {

            public void onTick(long millisUntilFinished) {
                time.setText("00:" + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                time.setText(repeat_send_code);
            }

        }.start();



        code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("AAAAA",s.length()+"**");
                if(s.length()==6){
                    progress.setVisibility(View.VISIBLE);
                    code.setEnabled(false);
                    AndroidNetworking.post(confirm)
                            .addBodyParameter("phone", phone)
                            .addBodyParameter("sms_code", s+"")
                            .build().getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("ssss",response+" ");

                            try {
                                if(response.getBoolean("status")){

                                    JSONObject data = response.getJSONObject("data");
                                    SharedPreferences.Editor editor = settings.edit();
                                    editor.putInt("user_id",data.getInt("user_id"));
                                    editor.putString("token",data.getString("token"));
                                    Log.d("111token",data.getString("token"));
                                    editor.putString("avatar",data.has("avatar")?data.getString("avatar"):"null");
                                    editor.putString("user_name",data.has("user_name")?data.getString("user_name"):"");
                                    editor.putString("phone",phone);
                                    editor.commit();

                                    if(data.getBoolean("is_new_user")){
                                        startActivity(new Intent(ActivityConfirmCode.this,RegisterActivity.class));
                                    }else {
                                        startActivity(new Intent(ActivityConfirmCode.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                    }
                                    finish();
                                }else {
                                    progress.setVisibility(View.GONE);
                                    code.setEnabled(true);
                                    Toasty.error(ActivityConfirmCode.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            progress.setVisibility(View.GONE);
                            code.setEnabled(true);
                            Toasty.error(ActivityConfirmCode.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                        }
                    });

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

}
