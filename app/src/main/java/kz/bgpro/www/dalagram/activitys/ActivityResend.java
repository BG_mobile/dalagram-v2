package kz.bgpro.www.dalagram.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.ChatApplication;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.adapters.SelectedContactsAdapter;
import kz.bgpro.www.dalagram.models.FeedItem;
import kz.bgpro.www.dalagram.utils.NurJS;

import static android.widget.Toast.LENGTH_SHORT;
import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.contact;
import static kz.bgpro.www.dalagram.utils.MyConstants.resend;

public class ActivityResend extends Activity {

    private Socket mSocket;

    @BindView(R.id.contacts_list)
    ListView contacts_list;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.ed_search)
    EditText search;


    String jsonObject;

    ArrayList<FeedItem> feedItem;

    SelectedContactsAdapter selected_adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ChatApplication app = (ChatApplication)getApplication();
        mSocket = app.getSocket();

        setContentView(R.layout.activity_send_user);
        ButterKnife.bind(this);
        name.setText("Select Contact");

        jsonObject = getIntent().getStringExtra("js");



        GetJSONFromServer();

        contacts_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JSONObject js = null;
                try {
                    js = new JSONObject(jsonObject);
                    js.put("recipient_user_id",feedItem.get(position).getUser_id());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (js!=null)
                    Log.d("js",js+" ");
                    AndroidNetworking.post(resend+TOKEN).addJSONObjectBody(js).
                        build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("AAAAa",response+"*");
                        try {
                            if (response.getBoolean("status")) {
                                FeedItem item = feedItem.get(position);



                                String Name=" ";
                                if(item.getUser_id()!=0) {
                                    if(!TextUtils.isEmpty(item.getContact_user_name())) {
                                        Name = item.getContact_user_name();
                                    }else if(!TextUtils.isEmpty(item.getName())) {
                                        Name = item.getName();
                                    }else {
                                        Name = item.getPhone();
                                      }
                                }else {
                                    Name = item.getContact_user_name();
                                }

                                Log.d("AAAAAAMMMMM5",Name);
                                String avatar;
                                boolean is_ava ;
                                if (TextUtils.isEmpty(item.getAvatar())){
                                    if(Name.length()>1)
                                        avatar = Name.charAt(0)+""+Name.charAt(1);
                                    else
                                        avatar = Name.charAt(0)+"";
                                    is_ava = false;
                                }else {
                                    is_ava = true;
                                    avatar = item.getAvatar();
                                }



                                startActivity(new Intent(ActivityResend.this, ActivityChatMessage.class)
                                        .putExtra("who","chat")
                                        .putExtra("user_id",item.getUser_id())
                                        .putExtra("is_mute",item.getIs_mute())
                                        .putExtra("name",Name)
                                        .putExtra("chat_text",item.getLast_visit())
                                        .putExtra("avatar",avatar)
                                        .putExtra("is_ava",is_ava)
                                        .putExtra("dialog_id",item.getDialog_id())
                                );

                               } else {
                                Toasty.error(ActivityResend.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toasty.error(ActivityResend.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

    }


    @OnClick(R.id.back)
    void back(){
        finish();
    }

    private void GetJSONFromServer() {
        feedItem = new ArrayList<FeedItem>();
        AndroidNetworking.get(contact+TOKEN+"&page=1&per_page=10").build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("ASAS",response+"*");
                try {
                    if(response.getBoolean("status")){
                        JSONArray jsonArray = response.getJSONArray("data");
                        int total_count = jsonArray.length();
                        for (int i=0;i<total_count;i++){
                            JSONObject object = (JSONObject) jsonArray.get(i);
                            FeedItem item = new FeedItem();
                            item.setUser_id(object.getInt("user_id"));
                            item.setName(object.getString("user_name"));
                            item.setName(object.getString("nickname"));
                            item.setAvatar(NurJS.NurString(object,"avatar"));
                            item.setUser_status(object.getString("user_status"));
                            item.setLast_visit(object.getString("last_visit"));
                            item.setIs_selected(false);
                            feedItem.add(item);
                        }

                        selected_adapter= new SelectedContactsAdapter(feedItem, ActivityResend.this);
                        contacts_list.setAdapter(selected_adapter);



                    }else {
                        Toasty.error(ActivityResend.this,response.getString("error"),LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityResend.this,"Проверить подключение интернета",LENGTH_SHORT).show();

            }
        });
    }
}