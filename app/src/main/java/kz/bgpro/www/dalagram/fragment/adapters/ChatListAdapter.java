package kz.bgpro.www.dalagram.fragment.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.github.pavlospt.roundedletterview.RoundedLetterView;
import com.vanniktech.emoji.EmojiTextView;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import kz.bgpro.www.dalagram.MainActivity;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.ActivityChatMessage;
import kz.bgpro.www.dalagram.activitys.ActivityZoomImage;
import kz.bgpro.www.dalagram.models.FeedItem;

/**
 * Created by nurbaqyt on 22.05.2018.
 */

public class ChatListAdapter extends BaseAdapter {

    ArrayList<FeedItem> feed_item;
    FeedItem item;
    Activity activity;

    public ChatListAdapter(ArrayList<FeedItem> feedItem, Activity activity) {
        this.activity = activity;
        this.feed_item = feedItem;
    }


    @Override
    public int getCount() {
        return feed_item.size();
    }

    @Override
    public Object getItem(int location) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {

        @BindView(R.id.avatar_tv)
        RoundedLetterView avatar_tv;

        @BindView(R.id.avatar)
        CircleImageView avatar;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.chat_text)
        EmojiTextView chat_text;

        @BindView(R.id.chat_text_image)
        ImageView chat_text_image;

        @BindView(R.id.channel_logo)
        ImageView channel_logo;

        @BindView(R.id.badge_notification)
        TextView badge_notification;


        @BindView(R.id.pin_chat)
        ImageView pin_chat;

        @BindView(R.id.status)
        ImageView status;

        @BindView(R.id.chat_bg)
        RelativeLayout chat_bg;


        @BindView(R.id.chat_date)
        TextView chat_date;




        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_chat_list, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        item = feed_item.get(position);

        String Name;
        if(item.getUser_id()!=0) {
            if(!TextUtils.isEmpty(item.getContact_user_name()))
                Name = item.getContact_user_name();
            else if(!TextUtils.isEmpty(item.getChat_name()))
                Name = item.getChat_name();
            else
                Name = item.getPhone();
        }else {
            Name = item.getChat_name();
        }

        if (item.getChannel_id()==0){
            viewHolder.channel_logo.setVisibility(View.GONE);
        }else {
            viewHolder.channel_logo.setVisibility(View.VISIBLE);
        }

        if (item.getNew_message_count()!=0){
            viewHolder.badge_notification.setText(item.getNew_message_count()+"");
            viewHolder.badge_notification.setVisibility(View.VISIBLE);

        }else {
            viewHolder.badge_notification.setVisibility(View.GONE);
        }



        if (item.getIs_bookmark()==1){
            viewHolder.pin_chat.setVisibility(View.VISIBLE);
//            viewHolder.chat_bg.setBackgroundColor(activity.getResources().getColor(R.color.grey_100));
        }else {
            viewHolder.pin_chat.setVisibility(View.INVISIBLE);
//            viewHolder.chat_bg.setBackgroundColor(activity.getResources().getColor(R.color.white));
        }

        if (item.isIs_own_last_message()){
            viewHolder.status.setVisibility(View.VISIBLE);
            if (item.getIs_read()==0){
                viewHolder.status.setImageResource(R.drawable.check);
            }else {
                viewHolder.status.setImageResource(R.drawable.check2);
            }
        }else {
            viewHolder.status.setVisibility(View.GONE);
        }

        viewHolder.name.setText(Name);
        if(item.isIs_typing()){
            viewHolder.chat_text.setTextColor(activity.getResources().getColor(R.color.typing));
            viewHolder.chat_text_image.setVisibility(View.GONE);
            viewHolder.chat_text.setText(Html.fromHtml("печатаеть..."));
            viewHolder.chat_text.setMaxEms(220);

        }else {
            viewHolder.chat_text.setTextColor(activity.getResources().getColor(R.color.black));
            if(item.getChat_kind().equals("action")){
                viewHolder.chat_text.setText(item.getAction_name());

                viewHolder.chat_text_image.setVisibility(View.GONE);
            }else {
                if (item.getIs_has_file()==1){
                    viewHolder.chat_text_image.setColorFilter(ContextCompat.getColor(activity, R.color.pinkish_grey), android.graphics.PorterDuff.Mode.SRC_IN);
                    viewHolder.chat_text_image.setVisibility(View.VISIBLE);
                    switch (item.getSfile_format()){
                        case "image":
                            viewHolder.chat_text_image.setImageResource(R.drawable.ic_insert_photo);
                            viewHolder.chat_text.setText("Фото");
                            break;
                        case "audio":
                            viewHolder.chat_text_image.setImageResource(R.drawable.ic_mic);
                            viewHolder.chat_text.setText("Аудио");
                            break;
                        case "file":
                            viewHolder.chat_text_image.setImageResource(R.drawable.ic_file);
                            viewHolder.chat_text.setText("Файл");
                            break;
                        case "video":
                            viewHolder.chat_text_image.setImageResource(R.drawable.ic_video);
                            viewHolder.chat_text.setText("Видео");
                            break;
                    }

                }else if (item.getIs_contact()==1) {
                    viewHolder.chat_text_image.setColorFilter(ContextCompat.getColor(activity, R.color.pinkish_grey), android.graphics.PorterDuff.Mode.SRC_IN);
                    viewHolder.chat_text_image.setVisibility(View.VISIBLE);
                    viewHolder.chat_text_image.setImageResource(R.drawable.ic_person);
                    viewHolder.chat_text.setText("Контакты");
                }else if (item.getIs_sticker()==1) {
                    viewHolder.chat_text_image.setVisibility(View.VISIBLE);
                    viewHolder.chat_text_image.clearColorFilter();
                    Glide.with(activity).load(item.getSticker_image()).into(viewHolder.chat_text_image);
                    viewHolder.chat_text.setText(item.getChat_text());
                }else {
                    viewHolder.chat_text_image.setVisibility(View.GONE);
                    viewHolder.chat_text.setText(Html.fromHtml(item.getChat_text()));

                }
            }
        }

        viewHolder.chat_date.setText(item.getChat_date());


        if (item.getIs_mute()==0){
            viewHolder.name.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        }else {
            viewHolder.name.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_mute,0);
        }





        if (item.getAvatar().contains("default-user") || TextUtils.isEmpty(item.getAvatar())){
            viewHolder.avatar.setVisibility(View.GONE);
            viewHolder.avatar_tv.setVisibility(View.VISIBLE);
            if(Name.length()>1)
                viewHolder.avatar_tv.setTitleText(Name.charAt(0)+""+Name.charAt(1));
            else
                viewHolder.avatar_tv.setTitleText(Name.charAt(0)+"");
        }else {
            viewHolder.avatar.setVisibility(View.VISIBLE);
            viewHolder.avatar_tv.setVisibility(View.GONE);
            Glide.with(activity)
                    .load(item.getAvatar())
                    .into(viewHolder.avatar);

        }

        viewHolder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                if (!item.getAvatar().contains("default-user") || !TextUtils.isEmpty(item.getAvatar())) {
                    Intent intent = new Intent(activity,ActivityZoomImage.class);
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("url", new ArrayList<>(Arrays.asList(item.getAvatar())));
                    bundle.putInt("position", 0);
                    intent.putExtras(bundle);
                    activity.startActivity(intent);
                }

            }
        });



        return convertView;
    }
}
