package kz.bgpro.www.dalagram.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.fragment.adapters.BlogerListAdapter;
import kz.bgpro.www.dalagram.models.FeedItem;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.users;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;


public class FragmentSinger extends Fragment  implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.list)
    ListView listView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    ArrayList<FeedItem> feedItems;

    BlogerListAdapter adapter;

    String name [] = {"Индира Расылхан","Асхат Таргын","Нурлан Еспанов","Талгат Жорабаев","Айгуль Иманбаева","Жубаныш Жексенулы","Рустем Нуржигит"};
    String last [] = {"онлайн","онлайн", "был(а) в 03:50", "был(а) в 05:54", "был(а) в 05:59", "был(а) в 07:22","был(а) в 07:36","был(а) в 08:00","был(а) в 08:00","был(а) в 08:03"};
    String city [] = {"Қазақ елі","Шымкент", "Алматы", "Астана", "Қазақстан", "Алматы", "Астана","Алматы","Алматы", "Алматы"};
    String avater [] ={
            "https://toibiznes.kz/imagecache/medium/79/pics/5671bc523bad9.jpg",
            "https://toibiznes.kz/imagecache/medium/79/pics/5b06501f7353b.jpg",
            "https://toibiznes.kz/imagecache/medium/79/pics/58528ec8611cb.jpg",
            "https://toibiznes.kz/imagecache/medium/79/pics/5671bb0711ad1.jpg",
            "https://toibiznes.kz/imagecache/medium/79/pics/5671ab3171a4d.jpg",
            "https://toibiznes.kz/imagecache/medium/79/pics/5671bece19ec1.jpg",
            "https://toibiznes.kz/imagecache/medium/79/pics/5672faeb25465.jpg"
    };

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifi, container, false);
        ButterKnife.bind(this, view);
        swipe.setOnRefreshListener(this);
//
        feedItems = new ArrayList<FeedItem>();
        adapter = new BlogerListAdapter(feedItems,getActivity(),2);
        listView.setAdapter(adapter);
//
        GetBlogerFromServer();


        return view;
    }

    private void GetBlogerFromServer() {

        AndroidNetworking.get(users+TOKEN+"&speciality_id=1").build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")) {
                        feedItems.clear();
                        JSONArray data = response.getJSONArray("data");
                        for (int i = 0; i<data.length();i++){
                            JSONObject jsonObject = (JSONObject) data.get(i);
                            FeedItem item = new FeedItem();
                            item.setId(NurInt(jsonObject,"user_id"));
                            item.setName(NurString(jsonObject,"nickname"));
                            item.setAvatar(NurString(jsonObject,"avatar"));
                            item.setLast_visit(NurString(jsonObject,"last_visit"));
                            item.setPhone(NurString(jsonObject,"city"));
                            feedItems.add(item);

                        }


                        adapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);

                    } else {
                        Toasty.error(getActivity(), response.getString("error"), Toast.LENGTH_SHORT).show();
                        swipe.setRefreshing(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {

            }
        });

    }


//    private void GetNotifiFromServer() {
//        swipe.setRefreshing(true);
//
//        Log.d("notification", notification+TOKEN);
//        AndroidNetworking.get(notification+TOKEN).build().getAsJSONObject(new JSONObjectRequestListener() {
//            @Override
//            public void onResponse(JSONObject response) {
//                Log.d("response_notifi",response+"^^^^^");
//                try {
//                    if (response.getBoolean("status")) {
//                        feedItems.clear();
//                        JSONArray data = response.getJSONArray("data");
//                        for (int i = 0; i<data.length();i++){
//                            JSONObject jsonObject = (JSONObject) data.get(i);
//                            FeedItem item = new FeedItem();
//
//                            item.setUser_id(NurInt(jsonObject, "user_id"));
//                            item.setAction_name(NurString(jsonObject, "action"));
//                            item.setName(NurString(jsonObject, "nickname"));
//                            item.setAvatar(NurString(jsonObject, "avatar"));
//                            item.setLast_visit(NurString(jsonObject,"last_visit"));
//
//                            feedItems.add(item);
//                        }
//
//
//                        adapter.notifyDataSetChanged();
//                        swipe.setRefreshing(false);
//
//                    } else {
//                        Toasty.error(getActivity(), response.getString("error"), Toast.LENGTH_SHORT).show();
//                        swipe.setRefreshing(true);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onError(ANError anError) {
//                Toasty.error(getActivity(), "Проверить подключение интернета", Toast.LENGTH_SHORT).show();
//                swipe.setRefreshing(true);
//            }
//        });
//    }


    @Override
    public void onRefresh() {
        swipe.setRefreshing(false);
    }
}
