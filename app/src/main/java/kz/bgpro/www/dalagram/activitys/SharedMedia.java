package kz.bgpro.www.dalagram.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.fragment.FragmentMedia;

import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

public class SharedMedia extends FragmentActivity {

    private LinearLayout mTabsLinearLayout;

    @BindView(R.id.pager)
    ViewPager viewPager;

    @BindView(R.id.tablayout)
    TabLayout tabLayout;

    @BindArray(R.array.tabTitles)
    String[] tabTitles;


    ArrayList<String> img_file_url = new ArrayList<String>();
    ArrayList<String> img_file_name= new ArrayList<String>();
    ArrayList<String> img_file_format= new ArrayList<String>();

    ArrayList<String> doc_file_url= new ArrayList<String>();
    ArrayList<String> doc_file_name= new ArrayList<String>();
    ArrayList<String> doc_file_format= new ArrayList<String>();


    ArrayList<String> audio_file_url= new ArrayList<String>();
    ArrayList<String> audio_file_name= new ArrayList<String>();
    ArrayList<String> audio_file_format= new ArrayList<String>();
    ArrayList<Integer> audio_file_time= new ArrayList<Integer>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sharedmedia);
        ButterKnife.bind(this);


        tabLayout.addTab(tabLayout.newTab().setText(tabTitles[0]));
        tabLayout.addTab(tabLayout.newTab().setText(tabTitles[1]));
        tabLayout.addTab(tabLayout.newTab().setText(tabTitles[2]));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        viewPager.setAdapter(new MediaPager(getSupportFragmentManager(), tabTitles));
        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        Intent intent = getIntent();
        if (intent.hasExtra("media_response")){
            String response = intent.getStringExtra("media_response");
            Log.d("AAAAARRRR",response);
            try {
                JSONObject media_response = new JSONObject(response);
                JSONArray data = media_response.getJSONArray("data");
                int l = data.length();

                for (int i=0;i<l;i++){

                    JSONObject file = (JSONObject) data.get(i);
                    if (NurString(file,"file_format").equals("image") || NurString(file,"file_format").equals("video")) {
                        img_file_url.add(NurString(file, "file_url"));
                        img_file_name.add(NurString(file, "file_name"));
                        img_file_format.add(NurString(file, "file_format"));
                    }else if (NurString(file,"file_format").equals("file")){
                        doc_file_url.add(NurString(file, "file_url"));
                        doc_file_name.add(NurString(file, "file_name"));
                        doc_file_format.add(NurString(file, "file_format"));
                    }else if (NurString(file,"file_format").equals("audio")){
                        audio_file_url.add(NurString(file, "file_url"));
                        audio_file_name.add(NurString(file, "file_name"));
                        audio_file_format.add(NurString(file, "file_format"));
                        audio_file_time.add(NurInt(file, "file_time"));
                    }
                }
                Log.d("AA",img_file_format.size()+"*"+doc_file_format.size()+"**"+audio_file_format.size());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    @OnClick(R.id.back)
    void back(){
        finish();
    }


    class MediaPager extends FragmentPagerAdapter {
        private String[] tabTitles;

        public MediaPager(FragmentManager fm, String[] tabTitles) {
            super(fm);
            this.tabTitles = tabTitles;

        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = new FragmentMedia();
            Bundle bundle = new Bundle();
            bundle.putInt("position",position);
            bundle.putStringArrayList("file_format",img_file_format);

            switch (position){
                case 0 :
                    bundle.putStringArrayList("file_url",img_file_url);
                    bundle.putStringArrayList("file_name",img_file_name);
                    bundle.putStringArrayList("file_format",img_file_format);
                    break;
                case 1 :
                    bundle.putStringArrayList("file_url",doc_file_url);
                    bundle.putStringArrayList("file_name",doc_file_name);
                    bundle.putStringArrayList("file_format",doc_file_format);
                    break;
                case 2 :
                    bundle.putStringArrayList("file_url",audio_file_url);
                    bundle.putStringArrayList("file_name",audio_file_name);
                    bundle.putStringArrayList("file_format",audio_file_format);
                    bundle.putIntegerArrayList("file_time",audio_file_time);
                    break;
            }
            fragment.setArguments(bundle);
            return fragment;
        }


        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }
    }
}