package kz.bgpro.www.dalagram.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.fragment.adapters.BlogerListAdapter;
import kz.bgpro.www.dalagram.models.FeedItem;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.users;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

public class FragmentBloger extends Fragment  implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.list)
    ListView listView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    ArrayList<FeedItem> feedItems;

    BlogerListAdapter adapter;

    String name [] = {"Нуртас Адамбай","Газиз Тортбаев", "Аружан Саин", "Алишер Еликбаев", "Асель Баяндарова", "Ержан Рашев", "Мухамеджан Тазабек","Мурат Абенов","Мухтар Тайжан","Джохар Утебеков"};
    String last [] = {"онлайн","онлайн", "был(а) в 03:50", "был(а) в 05:54", "был(а) в 05:59", "был(а) в 07:22","был(а) в 07:36","был(а) в 08:00","был(а) в 08:00","был(а) в 08:03"};
    String city [] = {"Қазақ елі","Шымкент", "Алматы", "Астана", "Қазақстан", "Алматы", "Астана","Алматы","Алматы", "Алматы"};
    String avater [] ={
            "http://hommes.kz/media/blog/entries/photos/2018/01/29/ae/87/1d/3d/ae871d3d2a332318155b6fe9db056b68.jpg",
            "http://hommes.kz/media/blog/entries/photos/2018/01/29/eb/fa/08/6a/ebfa086a8f179a19d0b80484ae28ce48.jpg",
            "http://hommes.kz/media/blog/entries/photos/2018/01/29/86/7c/6b/3c/867c6b3c5a20c42601c464737b7437d9.jpg",
            "http://hommes.kz/media/blog/entries/photos/2018/01/29/bb/a7/ef/3f/bba7ef3fced66bccecaf5216396686b0.jpg",
            "http://hommes.kz/media/blog/entries/photos/2018/01/29/ff/40/86/7a/ff40867a2076328f7a1531c953fface1.jpg",
            "http://hommes.kz/media/blog/entries/photos/2018/01/29/bd/08/79/90/bd087990300d549d47f17b7c0993b06d.jpg",
            "http://hommes.kz/media/blog/entries/photos/2018/01/30/f8/51/70/ee/f85170ee996cd54c06f481e1f1b1c677.jpg",
            "http://hommes.kz/media/blog/entries/photos/2018/01/30/b9/55/2b/dd/b9552bdde2343350953a0739eef19a87.jpg",
            "http://hommes.kz/media/blog/entries/photos/2018/01/29/7b/7f/d3/b5/7b7fd3b59895964631cd15e8e4517d76.jpg",
            "http://hommes.kz/media/blog/entries/photos/2018/01/29/2b/39/ed/d5/2b39edd53338772c05373d6f22516f77.jpg"
    };

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifi, container, false);
        ButterKnife.bind(this, view);
        swipe.setOnRefreshListener(this);
//
        feedItems = new ArrayList<FeedItem>();
        adapter = new BlogerListAdapter(feedItems,getActivity(),1);
        listView.setAdapter(adapter);
//
        GetBlogerFromServer();


        return view;
    }

    private void GetBlogerFromServer() {
        AndroidNetworking.get(users+TOKEN+"&speciality_id=2").build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")) {
                        feedItems.clear();
                        JSONArray data = response.getJSONArray("data");
                        for (int i = 0; i<data.length();i++){
                            JSONObject jsonObject = (JSONObject) data.get(i);
                            FeedItem item = new FeedItem();
                            item.setId(NurInt(jsonObject,"user_id"));
                            item.setName(NurString(jsonObject,"nickname"));
                            item.setAvatar(NurString(jsonObject,"avatar"));
                            item.setLast_visit(NurString(jsonObject,"last_visit"));
                            item.setPhone(NurString(jsonObject,"city"));
                            feedItems.add(item);

                        }


                        adapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);

                    } else {
                        Toasty.error(getActivity(), response.getString("error"), Toast.LENGTH_SHORT).show();
                        swipe.setRefreshing(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {

            }
        });
    }


//    private void GetNotifiFromServer() {
//        swipe.setRefreshing(true);
//
//        Log.d("notification", notification+TOKEN);
//        AndroidNetworking.get(notification+TOKEN).build().getAsJSONObject(new JSONObjectRequestListener() {
//            @Override
//            public void onResponse(JSONObject response) {
//                Log.d("response_notifi",response+"^^^^^");
//                try {
//                    if (response.getBoolean("status")) {
//                        feedItems.clear();
//                        JSONArray data = response.getJSONArray("data");
//                        for (int i = 0; i<data.length();i++){
//                            JSONObject jsonObject = (JSONObject) data.get(i);
//                            FeedItem item = new FeedItem();
//
//                            item.setUser_id(NurInt(jsonObject, "user_id"));
//                            item.setAction_name(NurString(jsonObject, "action"));
//                            item.setName(NurString(jsonObject, "nickname"));
//                            item.setAvatar(NurString(jsonObject, "avatar"));
//                            item.setLast_visit(NurString(jsonObject,"last_visit"));
//
//                            feedItems.add(item);
//                        }
//
//
//                        adapter.notifyDataSetChanged();
//                        swipe.setRefreshing(false);
//
//                    } else {
//                        Toasty.error(getActivity(), response.getString("error"), Toast.LENGTH_SHORT).show();
//                        swipe.setRefreshing(true);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onError(ANError anError) {
//                Toasty.error(getActivity(), "Проверить подключение интернета", Toast.LENGTH_SHORT).show();
//                swipe.setRefreshing(true);
//            }
//        });
//    }


    @Override
    public void onRefresh() {
        swipe.setRefreshing(false);
    }
}
