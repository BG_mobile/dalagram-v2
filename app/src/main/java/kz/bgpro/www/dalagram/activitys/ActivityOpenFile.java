package kz.bgpro.www.dalagram.activitys;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.bgpro.www.dalagram.R;

/**
 * Created by nurbaqyt on 02.08.2018.
 */

public class ActivityOpenFile extends Activity{



    @BindView(R.id.webview)
    WebView webview;

    String GoogleDocs="http://docs.google.com/gview?embedded=true&url=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_openfile);
        ButterKnife.bind(this);



        String myPdfUrl = getIntent().getStringExtra("file_url");


        String doc="<iframe src='http://docs.google.com/gview?embedded=true&url="+myPdfUrl
                +"' width='100%' height='100%' style='border: none;'></iframe>";


        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);

        webview.loadData( doc , "text/html",  "UTF-8");



    }



}
