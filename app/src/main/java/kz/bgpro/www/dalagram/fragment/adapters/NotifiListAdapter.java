package kz.bgpro.www.dalagram.fragment.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import kz.bgpro.www.dalagram.MainActivity;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.profile.ActivityMyProfile;
import kz.bgpro.www.dalagram.models.FeedItem;

/**
 * Created by nurbaqyt on 22.05.2018.
 */

public class NotifiListAdapter extends BaseAdapter {

    ArrayList<FeedItem> feed_item;
    FeedItem item;
    Activity activity;

    public NotifiListAdapter(ArrayList<FeedItem> feedItem, Activity activity) {
        this.activity = activity;
        this.feed_item = feedItem;
    }


    @Override
    public int getCount() {
        return feed_item.size();
    }

    @Override
    public Object getItem(int location) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {

        @BindView(R.id.avatar)
        CircleImageView avatar;

        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.last_visible)
        TextView last_visible;

        @BindView(R.id.status_img)
        ImageView status_img;




        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_notifi_list, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        item = feed_item.get(position);
        String cap = item.getName().substring(0, 1).toUpperCase() + item.getName().substring(1);
        String string_text = cap+"   <font  color=\"black\">"+item.getAction_name()+"</font>";
        viewHolder.text.setText(Html.fromHtml(string_text));
        viewHolder.last_visible.setText(item.getLast_visit());

        if (item.getAvatar().isEmpty()){
            Glide.with(activity).load(R.drawable.ic_person).into(viewHolder.avatar);
        }else {
            Glide.with(activity).load(item.getAvatar()).into(viewHolder.avatar);
        }



        viewHolder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);

                activity.startActivity(new Intent(activity, ActivityMyProfile.class).putExtra("user_id",item.getUser_id()).putExtra("is_my",false));

            }
        });

        return convertView;
    }
}
