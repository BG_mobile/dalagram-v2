package kz.bgpro.www.dalagram.activitys.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.pavlospt.roundedletterview.RoundedLetterView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.profile.ActivityChatProfile;
import kz.bgpro.www.dalagram.activitys.groups.ActivityGroupProfile;
import kz.bgpro.www.dalagram.activitys.AdminMenuDialog;
import kz.bgpro.www.dalagram.models.FeedItem;

/**
 * Created by nurbaqyt on 22.05.2018.
 */

public class UsersAdapter extends BaseAdapter {

    ArrayList<FeedItem> feed_item;
    FeedItem item;
    ActivityGroupProfile activity;
    int IS_admin,group_id;

    public UsersAdapter(ArrayList<FeedItem> feedItem, ActivityGroupProfile activity, int is_admin, int group_id) {
        this.activity = activity;
        this.feed_item = feedItem;
        this.IS_admin = is_admin;
        this.group_id = group_id;
    }


    @Override
    public int getCount() {
        return feed_item.size();
    }

    @Override
    public Object getItem(int location) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {

        @BindView(R.id.avatar_tv)
        RoundedLetterView avatar_tv;

        @BindView(R.id.avatar_layout)
        FrameLayout avatar_layout;

        @BindView(R.id.avatar)
        CircleImageView avatar;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.status)
        TextView status;

        @BindView(R.id.is_admin)
        TextView is_admin;

        @BindView(R.id.btn_menu)
        ImageButton btn_menu;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_group_user_list, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        item = feed_item.get(position);
        viewHolder.name.setText(item.getName());
        viewHolder.status.setText(item.getLast_visit());

        if (item.getIs_admin()==1){
            viewHolder.is_admin.setVisibility(View.VISIBLE);
        }else {
            viewHolder.is_admin.setVisibility(View.GONE);
        }

        if (IS_admin==1){
            viewHolder.btn_menu.setVisibility(View.VISIBLE);
        }else {
            viewHolder.btn_menu.setVisibility(View.INVISIBLE);
        }

        if(TextUtils.isEmpty(item.getAvatar())){
            viewHolder.avatar.setVisibility(View.GONE);
            viewHolder.avatar_tv.setVisibility(View.VISIBLE);
            viewHolder.avatar_tv.setTitleText(item.getName().charAt(0)+""+item.getName().charAt(1));
        }else {
            viewHolder.avatar.setVisibility(View.VISIBLE);
            viewHolder.avatar_tv.setVisibility(View.GONE);
            Glide.with(activity).load(item.getAvatar()).into(viewHolder.avatar);
        }

        viewHolder.btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                AdminMenuDialog alert = new AdminMenuDialog();
                int[] location = new int[2];
                viewHolder.btn_menu.getLocationInWindow(location);
                Point point = new Point(location[0], location[1]);
                Log.d("ASASAS",point.x+"_"+point.y);

                alert.showDialog(activity,1,activity,null,item,point.x,point.y,group_id);

            }
        });

        viewHolder.avatar_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                activity.startActivity(new Intent(activity,ActivityChatProfile.class)
                        .putExtra("user_id",item.getUser_id()).putExtra("Is_mute",item.getIs_mute()));
            }
        });


        return convertView;
    }
}
