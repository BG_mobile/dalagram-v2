package kz.bgpro.www.dalagram.activitys.groups;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.ActivityAddUser;
import kz.bgpro.www.dalagram.activitys.ActivityZoomImage;
import kz.bgpro.www.dalagram.activitys.SharedMedia;
import kz.bgpro.www.dalagram.activitys.adapters.UsersAdapter;
import kz.bgpro.www.dalagram.models.FeedItem;
import kz.bgpro.www.dalagram.utils.NonScrollListView;
import kz.bgpro.www.dalagram.utils.NurJS;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.activitys.ActivityChatMessage.Is_mute;
import static kz.bgpro.www.dalagram.utils.MyConstants.group;
import static kz.bgpro.www.dalagram.utils.MyConstants.media;
import static kz.bgpro.www.dalagram.utils.MyConstants.mute;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

public class ActivityGroupProfile extends Activity implements ObservableScrollViewCallbacks  {




    @BindView(R.id.scroll)
    ObservableScrollView scroll;

    @BindView(R.id.toolBar)
    RelativeLayout toolBar;

    @BindView(R.id.avatar)
    ImageView avatar;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.status)
    TextView status;

    @BindView(R.id.media_count)
    TextView media_count;

    @BindView(R.id.users_count)
    TextView users_count;

    @BindView(R.id.email)
    TextView email;

//    @BindView(R.id.last_visible)
//    TextView last_visible;

    @BindView(R.id.notification)
    Switch notification;

    @BindView(R.id.menu)
    ImageButton menu;

    @BindView(R.id.add_user)
    LinearLayout add_user;

    @BindView(R.id.participant_list)
    NonScrollListView participant_list;

    ArrayList<FeedItem> feedItem_users = new ArrayList<FeedItem>();

    int group_id,is_admin;
    String AVATAR="";

    JSONObject media_response=null;


    UsersAdapter usersAdapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groupprofile);
        ButterKnife.bind(this);
        scroll.setScrollViewCallbacks(this);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);


        group_id = getIntent().getIntExtra("group_id",0);


        if (Is_mute==0){
            notification.setChecked(true);
        }else {
            notification.setChecked(false);
        }


        notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d("icheck",isChecked+" ");

                Is_mute = isChecked ? 0 : 1;

                AndroidNetworking.post(mute+TOKEN)
                        .addBodyParameter("is_mute",Is_mute+"")
                        .addBodyParameter("group_id",group_id+"")
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("mutemute",response+" ");
                        Log.d("Is_mute",Is_mute+" ");

                    }

                    @Override
                    public void onError(ANError anError) {
                        Toasty.error(ActivityGroupProfile.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });


    }
    @OnClick(R.id.menu)
    void menu(){
        if (is_admin==1) {
            startActivity(new Intent(ActivityGroupProfile.this, GProfileChange.class)
                    .putExtra("group_id", group_id)
                    .putExtra("name", name.getText().toString()+"")
                    .putExtra("avatar", AVATAR+"")
                    .putExtra("email", email.getText().toString()+"")
                    .putExtra("status", status.getText().toString()+"")
            );
        }


    }


    public void GetProfileFromServer(){

        Log.d("ggg",group+"/"+group_id+"?token="+TOKEN);
        AndroidNetworking.get(group+"/"+group_id+"?token="+TOKEN)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("PPP",response+" *");
                try {
                    if(response.getBoolean("status")){
                        Log.d("profile",response+"++");
                        JSONObject data = response.getJSONObject("data");
                        name.setText(NurString(data,"group_name"));
                        is_admin = NurInt(data,"is_admin");
                        if (is_admin==1){
                            menu.setVisibility(View.VISIBLE);
                            add_user.setVisibility(View.VISIBLE);
                        }else {
                            menu.setVisibility(View.GONE);
                            add_user.setVisibility(View.GONE);
                        }

                        usersAdapter   = new UsersAdapter(feedItem_users, ActivityGroupProfile.this,is_admin,group_id);
                        participant_list.setAdapter(usersAdapter);
                        GetGroupUsers();


//                        email.setText(NurString(data,"email"));
//                        status.setText(NurString(data,"user_status"));
//                        last_visible.setText(NurString(data,"last_visit"));
                        if(!TextUtils.isEmpty(NurString(data,"group_avatar"))){
                            AVATAR = data.getString("group_avatar");
                            Glide.with(ActivityGroupProfile.this).load(AVATAR).into(avatar);

                        }




                    }else {

                        Toasty.error(ActivityGroupProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityGroupProfile.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });


    }


    @OnClick(R.id.add_user)
    void add_user(){
      startActivity(new Intent(ActivityGroupProfile.this,ActivityAddUser.class).putExtra("group_id",group_id));
    }

    @OnClick(R.id.back)
    void back(){
        finish();
    }

    @OnClick(R.id.shared_media)
    void shared_media(){
       if(media_response!=null){
            Intent intent = new Intent(ActivityGroupProfile.this,SharedMedia.class);
            intent.putExtra("media_response",media_response+"");
            startActivity(intent);
        }

    }

    @OnClick(R.id.delete_exit)
    void delete_exit(){

        String url = group+"/user/"+group_id+"?token="+TOKEN;
        AndroidNetworking.delete(url).build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")){
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("AAAAA33",response+" ");
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityGroupProfile.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                Log.d("AAAAA ERr",anError+" ");
            }
        });
    }


    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        int baseColor = getResources().getColor(R.color.colorPrimary);
        float alpha = Math.min(1, (float) scrollY / 300);
        toolBar.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @OnClick(R.id.avatar)
    void  avatarclick (){
        if(!TextUtils.isEmpty(AVATAR)){
            Intent intent = new Intent(ActivityGroupProfile.this, ActivityZoomImage.class);
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("url", new ArrayList<>(Arrays.asList(AVATAR)));
            bundle.putInt("position", 0);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    public void GetGroupUsers() {
        Log.d("url",group+"/user/"+group_id+"?token="+TOKEN);

        AndroidNetworking.get(group+"/user/"+group_id+"?token="+TOKEN)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("RRRRRR",response+" ");
                feedItem_users.clear();
                try {
                    if(response.getBoolean("status")){
                        Log.d("profile",response+"++");
                        JSONArray data = response.getJSONArray("data");
                        String GroupCount;
                        if(data.length()==1){
                            GroupCount = data.length()+" участник";
                        }else if(data.length()==2 || data.length()==3 || data.length()==4){
                            GroupCount = data.length()+" участника";
                        }else {
                            GroupCount = data.length()+" участников";
                        }
                        users_count.setText(GroupCount);

                        for (int i=0;i<data.length();i++){
                            JSONObject object = (JSONObject) data.get(i);
                            FeedItem item = new FeedItem();
                            item.setUser_id(object.getInt("user_id"));

                            Log.d("sssss1__"+i,NurString(object,"contact_user_name")+"**");

                            if (!NurString(object,"nickname").equals("")){
                                item.setName(NurString(object,"nickname"));
                            }else if (!NurString(object,"contact_user_name").equals("")){
                                item.setName(NurString(object,"contact_user_name"));
                            }else  if (!NurString(object,"user_name").equals("")){
                                item.setName(NurString(object,"user_name"));
                            }else {
                                item.setName(NurString(object,"phone"));
                            }

                            item.setPhone(NurJS.NurString(object,"phone"));
                            item.setAvatar(NurJS.NurString(object,"avatar"));
                            item.setUser_status(NurJS.NurString(object,"user_status"));
                            item.setLast_visit(NurJS.NurString(object,"last_visit"));
                            item.setIs_admin(NurInt(object,"is_admin"));
                            feedItem_users.add(item);
                        }
                        usersAdapter.notifyDataSetChanged();



                    }else {

                        Toasty.error(ActivityGroupProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityGroupProfile.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void GetMediaFromServer() {
        Log.d("MEdia",media+TOKEN+"&group_id="+group_id);
        AndroidNetworking.get(media+TOKEN+"&group_id="+group_id+"&page=1&per_page=10000").build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if(response.getBoolean("status")){
                        media_response = response;
                        Log.d("media",response+"++");
                        JSONArray data = response.getJSONArray("data");
                        int l = data.length();
                        media_count.setText(l+" ");

                    }else {

                        Toasty.error(ActivityGroupProfile.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivityGroupProfile.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        GetMediaFromServer();
        GetProfileFromServer();
    }
}
