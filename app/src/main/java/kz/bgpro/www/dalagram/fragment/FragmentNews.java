package kz.bgpro.www.dalagram.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import kz.bgpro.www.dalagram.MainActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.News.ActivityPublishNews;
import kz.bgpro.www.dalagram.fragment.adapters.NewsListAdapter;
import kz.bgpro.www.dalagram.models.NewsItem;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.PREFS_NAME;
import static kz.bgpro.www.dalagram.utils.MyConstants.publication;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

public class FragmentNews extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.list)
    ListView listView;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    ArrayList<NewsItem>  feedItems = new ArrayList<NewsItem>();

    NewsListAdapter adapter;


    TextView mini_name;
    ImageView mini_avatar;
    ViewGroup header;
    SharedPreferences settings;

    public static int click_position=0;

    View footer;
    boolean loadingMore = true;
    public int page=1;

    public static   MediaPlayer newsmediaPlayer;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        ButterKnife.bind(this, view);

        swipe.setOnRefreshListener(this);
        settings = getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        click_position=0;
        page=1;
        loadingMore = true;
        header = (ViewGroup)inflater.inflate(R.layout.header_publish_news, listView, false);
        listView.addHeaderView(header);
        initHeaderView();

        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),ActivityPublishNews.class));
            }
        });

        mini_name.setText(settings.getString("user_name", " "));

        if (!TextUtils.isEmpty(settings.getString("avatar", ""))) {
            Glide.with(getActivity()).load(settings.getString("avatar", "")).into(mini_avatar);
        }



        adapter = new NewsListAdapter(feedItems,getActivity(),false);
        listView.setAdapter(adapter);

        footer = View.inflate(getActivity().getApplicationContext(), R.layout.footer_progress, null);
        listView.addFooterView(footer);
        footer.setVisibility(View.GONE);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                if( totalItemCount!=0 && firstVisibleItem+visibleItemCount == totalItemCount )
                {
                    if(!loadingMore)
                    {
                        Log.d("oooo","loadingMore");
                        GetNewsFromServer(page);
                        loadingMore=true;
                    }
                }
            }
        });


        return view;
    }

    private void initHeaderView() {
        mini_avatar = (ImageView) header.findViewById(R.id.mini_avatar);
        mini_name = (TextView) header.findViewById(R.id.mini_name);
    }

    public void GetNewsFromServer(int Page) {


        if (Page==1){
            feedItems.clear();
        }else {
            MainActivity.is_update_news = false;
        }

        Log.d("NEWS", publication+TOKEN+"&per_page=10&page="+Page);
        AndroidNetworking.get(publication+TOKEN+"&per_page=10&page="+Page).build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response_news",response+"^^^^^");
                try {
                    if (response.getBoolean("status")) {

                        JSONArray data = response.getJSONArray("data");
                        int length = data.length();
                        if (length == 10) {
                            loadingMore = false;
                            footer.setVisibility(View.VISIBLE);
                        } else {
                            loadingMore = true;
                            footer.setVisibility(View.GONE);
                        }

                        for (int i = 0; i<length;i++){
                            JSONObject jsonObject = (JSONObject) data.get(i);
                            NewsItem item = new NewsItem();

                            item.setPublication_id(NurInt(jsonObject, "publication_id"));
                            item.setPublication_desc(NurString(jsonObject, "publication_desc"));
                            item.setPublication_date(NurString(jsonObject, "publication_date"));
                            item.setView_count(NurInt(jsonObject, "view_count"));
                            item.setComment_count(NurInt(jsonObject, "comment_count"));
                            if (NurInt(jsonObject, "comment_count")!=0){
                                JSONObject comment = jsonObject.getJSONObject("comment");
                                item.setComment_id(NurInt(comment,"comment_id"));
                                item.setComment_text(NurString(comment,"comment_text"));
                                item.setComment_date(NurString(comment,"comment_date"));
                                JSONObject comment_author = comment.getJSONObject("author");
                                item.setComment_avatar(NurString(comment_author,"avatar"));
                                item.setComment_user_name(NurString(comment_author,"nickname"));
                                item.setIs_i_liked_comment(NurInt(comment,"is_liked"));
                                item.setComment_like_count(NurInt(comment,"like_count"));
                                item.setComment_is_has_file(NurInt(comment,"is_has_file"));
                                item.setComment_file_format("");
                                item.setComment_file_name("");
                                item.setComment_file_url("");
                                item.setComment_file_time("0");
                                if (NurInt(comment,"is_has_file")==1){
                                    JSONArray file_list = comment.getJSONArray("file_list");
                                    if (file_list.length()>0){
                                        JSONObject file = (JSONObject) file_list.get(0);
                                        item.setComment_file_format(NurString(file,"file_format"));
                                        item.setComment_file_name(NurString(file,"file_name"));
                                        item.setComment_file_url(NurString(file,"file_url"));
                                        item.setComment_file_time(NurString(file,"file_time"));
                                    }
                                }
                            }else {
                                item.setComment_id(0);
                                item.setComment_text(" ");
                                item.setComment_date(" ");
                                item.setComment_avatar(" ");
                                item.setComment_user_name(" ");
                                item.setIs_i_liked_comment(0);
                                item.setComment_like_count(0);
                                item.setComment_is_has_file(0);
                            }

                            item.setShare_count(NurInt(jsonObject, "share_count"));
                            item.setLike_count(NurInt(jsonObject, "like_count"));
                            item.setIs_has_file(NurInt(jsonObject, "is_has_file"));
                            item.setIs_i_liked(NurInt(jsonObject, "is_i_liked"));


                            JSONObject author = jsonObject.getJSONObject("author");
                            item.setUser_id(NurInt(author, "user_id"));
                            item.setUser_name(NurString(author, "nickname"));
                            item.setPhone(NurString(author, "phone"));
                            item.setAvatar(NurString(author, "avatar"));

                            JSONArray file_list = jsonObject.getJSONArray("file_list");
                            int l = file_list.length();
                            String []file_url = new String[l];
                            String []file_format = new String[l];
                            String []file_name = new String[l];
                            int file_time=0;
                            int image_count=0;
                            for (int f=0;f<l;f++){
                                JSONObject file = (JSONObject) file_list.get(f);
                                file_url[f] =file.getString("file_url");
                                file_format[f] =file.getString("file_format");
                                file_name[f] =file.getString("file_name");
                                file_time=file.getInt("file_time");
                                if (file.getString("file_format").equals("image") || file.getString("file_format").equals("video")){
                                    image_count++;
                                }
                            }
                            item.setImage_count(image_count);
                            item.setFile_url(file_url);
                            item.setFile_format(file_format);
                            item.setFile_name(file_name);
                            item.setFile_time(file_time);
                            int is_has_link = NurInt(jsonObject, "is_has_link");
                            String link_description=" ", link_title=" ",link_image=" ";
                            if (is_has_link==1){
                                JSONObject meta_tags = jsonObject.getJSONObject("meta_tags");
                                link_description = meta_tags.getString("description");
                                link_title = meta_tags.getString("title");
                                link_image = meta_tags.getString("image");
                            }
                            item.setIs_has_link(is_has_link);
                            item.setLink_description(link_description);
                            item.setLink_title(link_title);
                            item.setLink_image(link_image);

                            feedItems.add(item);
                        }


                        adapter.notifyDataSetChanged();
                        listView.setSelection(click_position);
                        swipe.setRefreshing(false);

                        if (Page!=1){
                            int currentPosition = listView.getFirstVisiblePosition()==0?0:listView.getFirstVisiblePosition()+1;
                            listView.setSelectionFromTop(currentPosition,0);
                        }

                        page++;
                    } else {
                        Toasty.error(getActivity(), response.getString("error"), Toast.LENGTH_SHORT).show();
                        swipe.setRefreshing(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(getActivity(), "Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                swipe.setRefreshing(true);
            }
        });
    }


    @Override
    public void onRefresh() {
        swipe.setRefreshing(true);
        feedItems.clear();
        page=1;
        click_position=0;



    }



    public void onStart() {

        if (click_position==0){
            page=1;
            loadingMore = true;
            Log.d("oooo","onStart()");
            GetNewsFromServer(1);
        }else {
            click_position=0;
        }

        adapter.notifyDataSetChanged();
        super.onStart();
    }


    @Override
    public void onStop() {
        if (newsmediaPlayer != null) {
//            handler_online.removeCallbacks(runnable);
            newsmediaPlayer.stop();
            newsmediaPlayer.release();
            newsmediaPlayer = null;

        }
        super.onStop();
    }

    public void callAllMethod() {
        feedItems.clear();
        page=1;
        click_position=0;
        Log.d("oooo","callAllMethod");
        GetNewsFromServer(1);


    }
}
