package kz.bgpro.www.dalagram.fragment.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;

import kz.bgpro.www.dalagram.activitys.profile.ActivityMyProfile;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.models.FeedItem;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.follower_accept;
import static kz.bgpro.www.dalagram.utils.MyConstants.follower_reject;

/**
 * Created by nurbaqyt on 22.05.2018.
 */

public class BlogerListAdapter extends BaseAdapter {

    ArrayList<FeedItem> feed_item;
    FeedItem item;
    Activity activity;
    int type;

    public BlogerListAdapter(ArrayList<FeedItem> feedItem, Activity activity, int i) {
        this.activity = activity;
        this.feed_item = feedItem;
        this.type = i;
    }


    @Override
    public int getCount() {
        return feed_item.size();
    }

    @Override
    public Object getItem(int location) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {

        @BindView(R.id.avatar)
        CircleImageView avatar;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.last_visible)
        TextView last_visible;


        @BindView(R.id.who)
        TextView who;


        @BindView(R.id.city)
        TextView city;





        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_bloger, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        item = feed_item.get(position);

        if (type==1){
            viewHolder.who.setText("Bloger");
        }else {
            viewHolder.who.setText("Singer");
        }
        viewHolder.name.setText(item.getName());
        viewHolder.last_visible.setText(item.getLast_visit());
        viewHolder.city.setText(item.getPhone());

        if (item.getAvatar().isEmpty()){
            Glide.with(activity).load(R.drawable.ic_person).into(viewHolder.avatar);
        }else {
            Glide.with(activity).load(item.getAvatar()).into(viewHolder.avatar);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                activity.startActivity(new Intent(activity, ActivityMyProfile.class).putExtra("user_id",item.getId()));
            }
        });


        return convertView;
    }
}
