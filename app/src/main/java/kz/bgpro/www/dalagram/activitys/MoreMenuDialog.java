package kz.bgpro.www.dalagram.activitys;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.activitys.ActivityChatMessage.i_block_partner;
import static kz.bgpro.www.dalagram.utils.MyConstants.block;
import static kz.bgpro.www.dalagram.utils.MyConstants.chat;
import static kz.bgpro.www.dalagram.utils.MyConstants.chat_detail;
import static kz.bgpro.www.dalagram.utils.MyConstants.mute;

/**
 * Created by nurbaqyt on 02.08.2018.
 */

public class MoreMenuDialog {


     ActivityChatMessage activityChatMessage;

    public void attachView( ActivityChatMessage activityChatMessage) {
        this.activityChatMessage = activityChatMessage;

    }

    public void showDialog(String WHO,final Activity activity, final int user_id, final int is_mute, final ImageView is_mute_img, int block_partner,
                           int CHANNEL_ID, int GROUP_ID
    ){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.more_menu_dialog);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        int pixels = (int) (260 * activity.getResources().getDisplayMetrics().density);
        wlp.width = pixels;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        final TextView notification = (TextView) dialog.findViewById(R.id.notification);
        final TextView block_user    = (TextView) dialog.findViewById(R.id.block_user);
        final TextView clear_chat    = (TextView) dialog.findViewById(R.id.clear_chat);

        if (WHO.equals("chat")){
            notification.setVisibility(View.VISIBLE);
            block_user .setVisibility(View.VISIBLE);
            clear_chat .setVisibility(View.VISIBLE);
        }else {
            notification.setVisibility(View.GONE);
            block_user .setVisibility(View.GONE);
            clear_chat .setVisibility(View.VISIBLE);
        }

        if (is_mute==0){
            notification.setText("Отключить уведомление");
        }else {
            notification.setText("Включить уведомление");
        }


        if (block_partner==0){
            block_user.setText("Заблокировать");
        }else {
            block_user.setText("Разблокировать");
        }


        dialog.show();




        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidNetworking.post(mute+TOKEN)
                        .addBodyParameter("is_mute",is_mute+"")
                        .addBodyParameter("partner_id",user_id+"")
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("mutemute",response+" ");
                        try {
                            if(response.getBoolean("status")){
                                if (is_mute==0){
                                    is_mute_img.setVisibility(View.VISIBLE);
                                    activityChatMessage.Is_mute = 1;
                                    notification.setText("Включить уведомление");
                                }else {
                                    is_mute_img.setVisibility(View.GONE);
                                    activityChatMessage.Is_mute = 0;
                                    notification.setText("Отключить уведомление");
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toasty.error(activity,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                    }
                });
                dialog.dismiss();

            }
        });
        block_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                AndroidNetworking.post(block+TOKEN).addBodyParameter("partner_id",user_id+"")
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getBoolean("status")){
                                if (block_partner==0){
                                    i_block_partner = 1;
                                    notification.setText("Разблокировать");
                                }else {
                                    i_block_partner = 0;
                                    notification.setText("Заблокировать");
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toasty.error(activity,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                    }
                });

            }
        });
        clear_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = null;
                switch (WHO) {
                    case "chat":
                        url = chat + TOKEN + "&is_delete_all=1&partner_id=" + user_id;
                        break;
                    case "group":
                        url = chat + TOKEN + "&is_delete_all=1&group_id=" + GROUP_ID;
                        break;
                    case "channel":
                        url = chat + TOKEN + "&is_delete_all=1&channel_id=" + CHANNEL_ID;
                        break;
                }
                dialog.dismiss();
                AndroidNetworking.delete(url)
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("AAAA",response+"  ");
                       String URL = chat_detail+TOKEN+"&recipient_user_id="+user_id+"&per_page=50";
                        activityChatMessage.GetDataFromJSON(URL);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toasty.error(activity,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                    }
                });

            }
        });


    }
}