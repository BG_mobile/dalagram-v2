package kz.bgpro.www.dalagram.activitys;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.fragment.FragmentPagerImageZoom;

public class ActivityZoomImage extends FragmentActivity {

    @BindView(R.id.total)
    TextView total;
    @BindView(R.id.current)
    TextView current;

    @BindView(R.id.viewPager)
    ViewPager viewPager;




    int current_position;
    private List<String> images;

    MyFragmentPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_image);
        ButterKnife.bind(this);

        images = getIntent().getStringArrayListExtra("url");
        current_position = getIntent().getIntExtra("position",0);
        total.setText(images.size()+"");
        current.setText(current_position+1+"");

        adapter = new MyFragmentPagerAdapter(images, getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(current_position);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                current.setText(position+1+"");
            }

            @Override
            public void onPageSelected(int position) {
                current.setText(position+1+"");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });






    }

    @OnClick(R.id.back)
    void back(){
        finish();
    }

    @OnClick(R.id.download)
    void download(){
        DownloadTogalerey(images.get(current_position));
    }

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        List<String> images;

        public MyFragmentPagerAdapter(List<String> images, FragmentManager fm) {
            super(fm);
            this.images = images;
        }

        @Override
        public Fragment getItem(int position) {
            return FragmentPagerImageZoom.newInstance(images.get(position));
        }

        @Override
        public int getCount() {
            return images.size();
        }

    }



    public void DownloadTogalerey(String url){
//        ProgressDialog progressDialog = new ProgressDialog(ActivityZoomImage.this);
//        progressDialog.setMessage("загрузить...");
//        progressDialog.setCancelable(false);
//        progressDialog.setMax(100);

        String fileName = url.substring(url.lastIndexOf('/') + 1);
        String DIR_NAME = "/Dalagram/Dalagram image";
        File direct =
                new File(Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                        .getAbsolutePath() + "/" + DIR_NAME + "/");


        if (!direct.exists()) {
            direct.mkdir();
            Log.d("aaa", "dir created for first time");
        }

        DownloadManager dm = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        Uri downloadUri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle(fileName)
                .setMimeType("image/jpeg")
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES,
                        File.separator + DIR_NAME + File.separator + fileName);

        dm.enqueue(request);
    }



}