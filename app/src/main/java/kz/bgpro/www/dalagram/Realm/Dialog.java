package kz.bgpro.www.dalagram.Realm;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nurbaqyt on 24.07.2018.
 */


public class Dialog extends RealmObject {

    @PrimaryKey
    private String dialog_id;


    private int chat_id,is_read,is_mute,i_block_partner,partner_block_me,is_has_file,is_contact,new_message_count,group_id,channel_id,user_id,is_admin,is_bookmark;
    private  String chat_kind,action_name,chat_name,avatar,phone,contact_user_name,chat_text,chat_date,last_visit,file_format;

    private long time;
    private boolean is_own_last_message;

    private int is_sticker;
    String sticker_image;

    public Dialog (){

    }

    public Dialog(int chat_id, int is_bookmark, int is_read, int is_mute, int i_block_partner, int partner_block_me, int is_has_file, int is_contact, int new_message_count, int group_id, int channel_id, int user_id,
                  int is_admin, String chat_kind, String action_name, String chat_name, String avatar, String phone, String contact_user_name, String chat_text, String chat_date,
                  String last_visit, String dialog_id,long time,String file_format,boolean is_own_last_message,int is_sticker,String sticker_image) {





        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        Dialog dialog = new Dialog();
        dialog.setChat_id(chat_id);
        dialog.setIs_bookmark(is_bookmark);
        dialog.setIs_read(is_read);
        dialog.setIs_mute(is_mute);
        dialog.setI_block_partner(i_block_partner);
        dialog.setPartner_block_me(partner_block_me);
        dialog.setIs_has_file(is_has_file);
        dialog.setIs_contact(is_contact);
        dialog.setNew_message_count(new_message_count);
        dialog.setGroup_id(group_id);
        dialog.setChannel_id(channel_id);
        dialog.setUser_id(user_id);
        dialog.setIs_admin(is_admin);
        dialog.setChat_kind(chat_kind);
        dialog.setAction_name(action_name);
        dialog.setChat_name(chat_name);
        dialog.setAvatar(avatar);
        dialog.setPhone(phone);
        dialog.setContact_user_name(contact_user_name);
        dialog.setChat_text(chat_text);
        dialog.setChat_date(chat_date);
        dialog.setLast_visit(last_visit);
        dialog.setDialog_id(dialog_id);
        dialog.setTime(time);
        dialog.setFile_format(file_format);
        dialog.setIs_own_last_message(is_own_last_message);
        dialog.setIs_sticker(is_sticker);
        dialog.setSticker_image(sticker_image);


        realm.copyToRealmOrUpdate(dialog);
        realm.commitTransaction();
    }


    public int getChat_id() {
        return chat_id;
    }

    public void setChat_id(int chat_id) {
        this.chat_id = chat_id;
    }

    public int getIs_bookmark() {
        return is_bookmark;
    }

    public void setIs_bookmark(int is_bookmark) {
        this.is_bookmark = is_bookmark;
    }

    public int getIs_read() {
        return is_read;
    }

    public void setIs_read(int is_read) {
        this.is_read = is_read;
    }

    public int getIs_mute() {
        return is_mute;
    }

    public void setIs_mute(int is_mute) {
        this.is_mute = is_mute;
    }

    public int getI_block_partner() {
        return i_block_partner;
    }

    public void setI_block_partner(int i_block_partner) {
        this.i_block_partner = i_block_partner;
    }

    public int getPartner_block_me() {
        return partner_block_me;
    }

    public void setPartner_block_me(int partner_block_me) {
        this.partner_block_me = partner_block_me;
    }

    public int getIs_has_file() {
        return is_has_file;
    }

    public void setIs_has_file(int is_has_file) {
        this.is_has_file = is_has_file;
    }

    public int getNew_message_count() {
        return new_message_count;
    }

    public void setNew_message_count(int new_message_count) {
        this.new_message_count = new_message_count;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public int getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(int channel_id) {
        this.channel_id = channel_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getChat_kind() {
        return chat_kind;
    }

    public void setChat_kind(String chat_kind) {
        this.chat_kind = chat_kind;
    }

    public String getAction_name() {
        return action_name;
    }

    public void setAction_name(String action_name) {
        this.action_name = action_name;
    }

    public String getChat_name() {
        return chat_name;
    }

    public void setChat_name(String chat_name) {
        this.chat_name = chat_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContact_user_name() {
        return contact_user_name;
    }

    public void setContact_user_name(String contact_user_name) {
        this.contact_user_name = contact_user_name;
    }

    public String getChat_text() {
        return chat_text;
    }

    public void setChat_text(String chat_text) {
        this.chat_text = chat_text;
    }

    public String getChat_date() {
        return chat_date;
    }

    public void setChat_date(String chat_date) {
        this.chat_date = chat_date;
    }

    public int getIs_admin() {
        return is_admin;
    }

    public void setIs_admin(int is_admin) {
        this.is_admin = is_admin;
    }

    public String getLast_visit() {
        return last_visit;
    }

    public void setLast_visit(String last_visit) {
        this.last_visit = last_visit;
    }

    public String getDialog_id() {
        return dialog_id;
    }

    public void setDialog_id(String dialog_id) {
        this.dialog_id = dialog_id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getFile_format() {
        return file_format;
    }

    public void setFile_format(String file_format) {
        this.file_format = file_format;
    }

    public boolean isIs_own_last_message() {
        return is_own_last_message;
    }

    public void setIs_own_last_message(boolean is_own_last_message) {
        this.is_own_last_message = is_own_last_message;
    }

    public int getIs_contact() {
        return is_contact;
    }

    public void setIs_contact(int is_contact) {
        this.is_contact = is_contact;
    }

    public int getIs_sticker() {
        return is_sticker;
    }

    public void setIs_sticker(int is_sticker) {
        this.is_sticker = is_sticker;
    }

    public String getSticker_image() {
        return sticker_image;
    }

    public void setSticker_image(String sticker_image) {
        this.sticker_image = sticker_image;
    }
}
