package kz.bgpro.www.dalagram.activitys.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.github.pavlospt.roundedletterview.RoundedLetterView;
import de.hdodenhof.circleimageview.CircleImageView;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.AdminMenuDialog;
import kz.bgpro.www.dalagram.activitys.groups.ActivityGroupProfile;
import kz.bgpro.www.dalagram.activitys.profile.ActivityChatProfile;
import kz.bgpro.www.dalagram.models.FeedItem;

import java.util.ArrayList;

/**
 * Created by nurbaqyt on 22.05.2018.
 */

public class StickersAdapter extends BaseAdapter {

    ArrayList<FeedItem> feed_item;
    FeedItem item;
    Activity activity;

    public StickersAdapter(Activity activity, ArrayList<FeedItem> feedItem) {
        this.activity = activity;
        this.feed_item = feedItem;

    }


    @Override
    public int getCount() {
        return feed_item.size();
    }

    @Override
    public Object getItem(int location) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {

        @BindView(R.id.image)
        ImageView image;



        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_sticker, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        item = feed_item.get(position);
       Glide.with(activity).load(item.getSticker_image()).into(viewHolder.image);


        return convertView;
    }
}
