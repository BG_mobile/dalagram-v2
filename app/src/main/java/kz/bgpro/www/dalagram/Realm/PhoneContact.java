package kz.bgpro.www.dalagram.Realm;


import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by nurbaqyt on 24.07.2018.
 */

public class PhoneContact extends RealmObject {

    private String contact_name="";
    protected String phone="";



    public PhoneContact(){


}

    public PhoneContact(String name, String phone) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<PhoneContact> existingContact = realm.where(PhoneContact.class)
                .equalTo("phone", phone)
                .findAll();

        if (!existingContact.isEmpty()) {
            PhoneContact contact = existingContact.first();
            contact.contact_name = name;

        }else {
            PhoneContact contact1 = realm.createObject(PhoneContact.class);
            contact1.contact_name = name;
            contact1.phone = phone;
        }

        realm.commitTransaction();
    }


    public String getContact_name() {
        return contact_name;
    }

    public String getPhone() {
        return phone;
    }
}
