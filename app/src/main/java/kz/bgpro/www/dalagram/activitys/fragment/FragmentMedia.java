package kz.bgpro.www.dalagram.activitys.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.Realm.DialogDetail;
import kz.bgpro.www.dalagram.activitys.ActivityVideoPlayer;
import kz.bgpro.www.dalagram.activitys.ActivityZoomImage;

/**
 * Created by nurbaqyt on 25.08.2018.
 */

@SuppressLint("ValidFragment")
public class FragmentMedia extends Fragment {

    @BindView(R.id.grid)
    GridView grid;
    static File myDir;


    ArrayList<String> file_url;
    ArrayList<String> file_name;
    ArrayList<String> file_format;
    ArrayList<Integer> file_time;
    int position;

    MediaPlayer mediaPlayer;
    AudioAdapter audioAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_media, container, false);
        ButterKnife.bind(this, view);

        myDir  = new File( Environment.getExternalStorageDirectory().toString() + "/.dalagram");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }

        Bundle bundle = getArguments();
        if (bundle!=null){
            position = bundle.getInt("position");
            file_url = bundle.getStringArrayList("file_url");
            file_name = bundle.getStringArrayList("file_name");
            file_format = bundle.getStringArrayList("file_format");

            if (position==0){
                grid.setNumColumns(2);
                grid.setAdapter(new ImageAdapter(file_url,file_name,file_format));
            }else if (position==1){
                grid.setNumColumns(1);
                grid.setAdapter(new FileAdapter(file_url,file_name,file_format));
            }else if (position==2){
                file_time = bundle.getIntegerArrayList("file_time");
                grid.setNumColumns(1);
                audioAdapter = new AudioAdapter(file_url,file_name,file_format,file_time);
                grid.setAdapter(audioAdapter);
            }
        }




        return view;
    }

    private class ImageAdapter extends BaseAdapter {
        ArrayList<String> img_file_url;
        ArrayList<String> img_file_format;
        ArrayList<String> img_file_name;
        public ImageAdapter(ArrayList<String> img_file_url,ArrayList<String> img_file_name,ArrayList<String> img_file_format) {
            this.img_file_url = img_file_url;
            this.img_file_format = img_file_format;
            this.img_file_name = img_file_name;
        }

        @Override
        public int getCount() {
            return img_file_url.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater lInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = convertView;
            if (view == null) {
                view = lInflater.inflate(R.layout.media_image, parent, false);
            }

            ImageView imageView = (ImageView) view.findViewById(R.id.image);
            ImageView video = (ImageView) view.findViewById(R.id.video);

            if (img_file_format.get(position).equals("image")){
                Picasso.get().load(img_file_url.get(position)).into(imageView);
                video.setVisibility(View.GONE);
            }else {
                video.setVisibility(View.VISIBLE);
                Glide.with(getActivity())
                        .asBitmap()
                        .load(img_file_url.get(position))
                        .into(imageView);
            }


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (img_file_format.get(position).equals("image")){
                        Intent intent = new Intent(getActivity(), ActivityZoomImage.class);
                        Bundle bundle = new Bundle();
                        bundle.putStringArrayList("url", new ArrayList<>(img_file_url));
                        bundle.putInt("position", position);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }else {
                        File imgfile = new File(myDir +"/.thumb/"+img_file_name.get(position)+".jpg");

                        File file_path = new File(myDir + "/video");
                        Intent intent = new Intent(getActivity(),ActivityVideoPlayer.class);

                        if (imgfile.exists()){
                            intent.putExtra("file_url",file_path.getPath()+"/"+img_file_name.get(position));
                        }else {
                            intent.putExtra("file_url",img_file_url.get(position));
                        }
                        startActivity(intent);
                        }


                }
            });
            return view;
        }
    }

    private class FileAdapter extends BaseAdapter {
        ArrayList<String> doc_file_url;
        ArrayList<String> doc_file_format;
        ArrayList<String> doc_file_name;
        public FileAdapter(ArrayList<String> img_file_url,ArrayList<String> img_file_name,ArrayList<String> img_file_format) {
            this.doc_file_url = img_file_url;
            this.doc_file_format = img_file_format;
            this.doc_file_name = img_file_name;
        }

        @Override
        public int getCount() {
            return doc_file_url.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater lInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = convertView;
            if (view == null) {
                view = lInflater.inflate(R.layout.media_file, parent, false);
            }

            ImageView imageView = (ImageView) view.findViewById(R.id.file_image);
            TextView name = (TextView) view.findViewById(R.id.file_name);
            TextView size = (TextView) view.findViewById(R.id.size);
            TextView format = (TextView) view.findViewById(R.id.file_format);
            View view_layout = (View) view.findViewById(R.id.view_layout);


            size.setVisibility(View.GONE);
            view_layout.setVisibility(View.VISIBLE);
            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            imageView.setColorFilter(color);
            name.setText(doc_file_name.get(position));
            final String extensionRemoved = doc_file_name.get(position).split("\\.")[1];
            format.setText(extensionRemoved);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    downloadFile(doc_file_url.get(position),doc_file_name.get(position),"file");
                }
            });
            return view;
        }
    }

    private class AudioAdapter extends BaseAdapter {
        int is_play = 10000;
        ArrayList<String> audio_file_url;
        ArrayList<String> audio_file_format;
        ArrayList<String> audio_file_name;
        ArrayList<Integer> audio_file_time;
        public AudioAdapter(ArrayList<String> img_file_url, ArrayList<String> img_file_name, ArrayList<String> img_file_format, ArrayList<Integer> file_time) {
            this.audio_file_url = img_file_url;
            this.audio_file_format = img_file_format;
            this.audio_file_name = img_file_name;
            this.audio_file_time = file_time;
        }

        @Override
        public int getCount() {
            return audio_file_url.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater lInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = convertView;
            if (view == null) {
                view = lInflater.inflate(R.layout.media_audio, parent, false);
            }

            ImageButton play = (ImageButton) view.findViewById(R.id.play);
            ProgressBar audioProgressbar = (ProgressBar) view.findViewById(R.id.audioProgressbar);
            TextView name = (TextView) view.findViewById(R.id.name);
            TextView file_time = (TextView) view.findViewById(R.id.file_time);


            if (is_play == position) {
                play.setImageResource(R.drawable.ic_pause);
                play.setColorFilter(ContextCompat.getColor(getActivity(), R.color.blue), android.graphics.PorterDuff.Mode.SRC_IN);
            } else {
                play.setImageResource(R.drawable.ic_play);
                play.setColorFilter(ContextCompat.getColor(getActivity(), R.color.blue), android.graphics.PorterDuff.Mode.SRC_IN);
            }

            audioProgressbar.setVisibility(View.GONE);

            int second = audio_file_time.get(position) % 60;
            int minute = audio_file_time.get(position) / 60;
            String timeString = String.format("%02d:%02d", minute, second);
            file_time.setText(timeString);
            name.setText(audio_file_name.get(position));


            play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    File audio_file;
                    if (audio_file_url.get(position).contains("AudioRecordingvoice") || audio_file_url.get(position).contains(".ogg")) {
                        audio_file = new File(myDir + "/.voiceaudio/" + audio_file_name.get(position));
                    } else {
                        audio_file = new File(myDir + "/Dalagram audio/" + audio_file_name.get(position));
                    }


                    if (is_play != position) {

                        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                            mediaPlayer.stop();
                        }

                        if (audio_file.exists()) {
                            play.setImageResource(R.drawable.ic_pause);
                            play.setColorFilter(ContextCompat.getColor(getActivity(), R.color.blue), android.graphics.PorterDuff.Mode.SRC_IN);
                            Uri uri = Uri.fromFile(audio_file);
                            mediaPlayer = MediaPlayer.create(getActivity(), uri);
                            mediaPlayer.start();

                            is_play = position;

                            notifyDataSetChanged();
                        } else {
                            is_play =10000;
                            audioProgressbar.setVisibility(View.VISIBLE);
                            downloadFile(audio_file_url.get(position),audio_file_name.get(position), "audio");
                        }

                    } else {
                        if (mediaPlayer.isPlaying()) {
                            mediaPlayer.pause();

                            play.setImageResource(R.drawable.ic_play);
                            play.setColorFilter(ContextCompat.getColor(getActivity(), R.color.blue), android.graphics.PorterDuff.Mode.SRC_IN);
                        } else {
                            mediaPlayer.start();
                            play.setImageResource(R.drawable.ic_pause);
                            play.setColorFilter(ContextCompat.getColor(getActivity(), R.color.blue), android.graphics.PorterDuff.Mode.SRC_IN);
                        }

                    }
                }
            });
            return view;
        }
    }

    private void downloadFile (String File_url ,String File_name ,String format){

        Activity activity = getActivity();
        File savefile;
        Log.d("File_name",File_url);
        if (format.equals("audio") ){
            if  (File_url.contains("AudioRecordingvoice") || File_url.contains(".ogg")){
                savefile = new File(myDir +"/.voice"+format);
                Log.d("File_name1",savefile+" ");
            }else {
                savefile = new File(myDir +"/Dalagram "+format);
                Log.d("File_name2",savefile+" ");
            }

        }else {
            savefile = new File(myDir +"/Dalagram "+format);
        }

        File file_url = new File(savefile+"/"+File_name);

        if (!file_url.exists()) {
            AndroidNetworking.download(File_url,savefile.getPath(),File_name)
                    .setTag("downloadTest")
                    .setPriority(Priority.HIGH)
                    .build()
                    .startDownload(new DownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            if (format.equals("file")){

                                Uri pdfUri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", file_url);
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(pdfUri, "application/*");
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                Intent chooser = Intent.createChooser(intent, "");

                                activity.startActivity(chooser);

                            }else {
                                if (!audioAdapter.isEmpty()){
                                    audioAdapter.notifyDataSetChanged();
                                }
                            }

                        }
                        @Override
                        public void onError(ANError anError) {
                            Toasty.error(activity, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                        }
                    });
        }else {
            if (format.equals("file")){
//                Uri path = Uri.fromFile(file_url);

                // создаём новое намерение
                Uri pdfUri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", file_url);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(pdfUri, "application/*");
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Intent chooser = Intent.createChooser(intent, "");

                activity.startActivity(chooser);



            }else {
                if (!audioAdapter.isEmpty()){
                    audioAdapter.notifyDataSetChanged();
                }
            }
        }


    }

    @Override
    public void onStop() {
        if (mediaPlayer != null) {
//            handler_online.removeCallbacks(runnable);
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;

        }
        super.onStop();
    }


}
