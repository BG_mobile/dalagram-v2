package kz.bgpro.www.dalagram.activitys.channel;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.fxn.pix.Pix;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.groups.GProfileChange;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.channel;
import static kz.bgpro.www.dalagram.utils.MyConstants.channel_avatar;
import static kz.bgpro.www.dalagram.utils.MyConstants.group;
import static kz.bgpro.www.dalagram.utils.MyConstants.group_avatar;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;


public class ChannelProfileChange extends AppCompatActivity  {



    @BindView(R.id.avatar)
    ImageView avatar;

   @BindView(R.id.channel_name)
   EditText channel_name;

    @BindView(R.id.channel_login)
    EditText channel_login;


    @BindView(R.id.done)
    ImageButton done;

    @BindView(R.id.progress)
    ProgressBar progress;

    int channel_id;
    String Name,Channel_login,Email,AVATAR;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channelprofile_change);
        ButterKnife.bind(this);

        progress.setVisibility(View.GONE);
        done.setVisibility(View.VISIBLE);

        if (getIntent().hasExtra("channel_id")){
            channel_id = getIntent().getIntExtra("channel_id",0);
            Name = getIntent().getStringExtra("name");
            Channel_login = getIntent().getStringExtra("channel_login");
            Email = getIntent().getStringExtra("email");
            AVATAR = getIntent().getStringExtra("avatar");

            if(!TextUtils.isEmpty(AVATAR)){
                Glide.with(this).load(AVATAR).into(avatar);
            }
            channel_name.setText(Name);
            channel_login.setText(Channel_login);

        }


    }
    @OnClick(R.id.avatar_btn)
    void avatar_btn(){
        Pix.start(ChannelProfileChange.this,                    //Activity or Fragment Instance
                704,                //Request code for activity results
                1);
    }

 @OnClick(R.id.back)
    void back(){
        finish();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 704) {
                ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                File f = new File(returnValue.get(0));
                Bitmap rotatedBMP = new BitmapDrawable(getResources(), f.getAbsolutePath()).getBitmap();
                avatar.setImageBitmap(rotatedBMP);
                UploadImage(f);
            }
        }
    }


    @OnClick(R.id.done)
    void done(){
        final String mName = channel_name.getText().toString();
        final String mLogin = channel_login.getText().toString();
        if (TextUtils.isEmpty(Name) && TextUtils.isEmpty(mLogin)){
            Toasty.info(this, "Заполните все поля", Toast.LENGTH_SHORT).show();
        }else if (!Name.equals(mName)){
            done.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);

            Log.d("UUUUU",channel+"/"+channel_id+"?token="+TOKEN);
                AndroidNetworking.post(channel+"/"+channel_id+"?token="+TOKEN)
                        .addBodyParameter("channel_name",mName)
                        .addBodyParameter("channel_login",mLogin)
                        .addBodyParameter("is_public","1")
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("RRRRRR",response+" ");
                            if(response.getBoolean("status")){


                                    done.setVisibility(View.VISIBLE);
                                    progress.setVisibility(View.GONE);
                                    finish();
                            }else {
                                Toasty.error(ChannelProfileChange.this, response.getString("error"), Toast.LENGTH_SHORT).show();

                                done.setVisibility(View.VISIBLE);
                                progress.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toasty.error(ChannelProfileChange.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                    }
                });


        }else {
            finish();
        }
    }

    public void UploadImage(File f) {

            AndroidNetworking.upload(channel_avatar+TOKEN)
                    .addMultipartFile("image", f)
                    .addMultipartParameter("channel_id",channel_id+"")
                    .build().getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("response",response+"  ");


                }

                @Override
                public void onError(ANError anError) {
                    Log.d("anError",anError+"  "+"Проверить подключение интернета");

                    Toasty.error(ChannelProfileChange.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                }
            });
    }



}
