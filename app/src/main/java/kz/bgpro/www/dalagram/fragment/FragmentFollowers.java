package kz.bgpro.www.dalagram.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import kz.bgpro.www.dalagram.activitys.profile.ActivityMyProfile;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.fragment.adapters.FollowerAdapter;
import kz.bgpro.www.dalagram.models.FeedItem;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.follower;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

public class FragmentFollowers extends Fragment  implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.list)
    ListView listView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    ArrayList<FeedItem> feedItems;

    FollowerAdapter adapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifi, container, false);
        ButterKnife.bind(this, view);
        swipe.setOnRefreshListener(this);
//
        feedItems = new ArrayList<FeedItem>();
        adapter = new FollowerAdapter(feedItems,getActivity());
        listView.setAdapter(adapter);
//
        GetNotifiFromServer();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                startActivity(new Intent(getActivity(), ActivityMyProfile.class).putExtra("user_id",feedItems.get(position).getUser_id()));
            }
        });
        return view;
    }

    private void GetNotifiFromServer() {
        swipe.setRefreshing(true);

        Log.d("notification", follower+TOKEN);
        AndroidNetworking.get(follower+TOKEN).build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response_notifi",response+"^^^^^");
                try {
                    if (response.getBoolean("status")) {
                        feedItems.clear();
                        JSONArray data = response.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jsonObject = (JSONObject) data.get(i);
                            FeedItem item = new FeedItem();

                            item.setUser_id(NurInt(jsonObject, "user_id"));
                            item.setName(NurString(jsonObject, "nickname"));
                            item.setAvatar(NurString(jsonObject, "avatar"));
                            item.setLast_visit(NurString(jsonObject, "last_visit"));

                            feedItems.add(item);
                        }
                        adapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);


                    } else {
                        Toasty.error(getActivity(), response.getString("error"), Toast.LENGTH_SHORT).show();
                        swipe.setRefreshing(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(getActivity(), "Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                swipe.setRefreshing(true);
            }
        });
    }


    @Override
    public void onRefresh() {
        swipe.setRefreshing(false);
    }
}
