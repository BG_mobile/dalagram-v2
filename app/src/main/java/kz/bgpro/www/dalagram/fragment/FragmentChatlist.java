package kz.bgpro.www.dalagram.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.graphics.Palette;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.github.clans.fab.FloatingActionMenu;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import kz.bgpro.www.dalagram.MainActivity;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.Realm.Dialog;
import kz.bgpro.www.dalagram.activitys.ActivityChatMessage;
import kz.bgpro.www.dalagram.activitys.ActivitySelectedUser;
import kz.bgpro.www.dalagram.fragment.adapters.ChatListAdapter;
import kz.bgpro.www.dalagram.models.FeedItem;

import static io.realm.internal.network.NetworkStateReceiver.isOnline;
import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.chat;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

@SuppressLint("ValidFragment")
public class FragmentChatlist extends Fragment  implements SwipeRefreshLayout.OnRefreshListener{


    public static ArrayList<FeedItem> feedItem;
    @BindView(R.id.list)
    ListView list;

    static Realm realm;


     static SwipeRefreshLayout swipe;

    @BindView(R.id.fab_menu)
    FloatingActionMenu fab_menu;

    EditText search;
    ImageButton close;

    @SuppressLint("StaticFieldLeak")
    public static ChatListAdapter chatListAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_list, container, false);
        ButterKnife.bind(this,view);




        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        feedItem = new ArrayList<FeedItem>();
        swipe.setOnRefreshListener(this);
        realm = Realm.getDefaultInstance();


        ViewGroup header = (ViewGroup)inflater.inflate(R.layout.layout_header_search, list, false);
        list.addHeaderView(header);

        search = (EditText) header.findViewById(R.id.ed_search);
        close = (ImageButton) header.findViewById(R.id.close);


        close.setVisibility(View.GONE);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search.setText(null);
                swipe.setRefreshing(true);
                GetDataFromJSON();
            }
        });

        chatListAdapter = new ChatListAdapter(feedItem, getActivity());
        list.setAdapter(chatListAdapter);

        SetDataFirstToList(1);





        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("aaaaaa start",start+"before"+before+"count"+count);
                if (s.length()>0){
                    close.setVisibility(View.VISIBLE);
                    GetSearchFromJSON(search.getText().toString());
                }
                if (s.length()==0){
                    close.setVisibility(View.GONE);
                    swipe.setRefreshing(true);
                    GetDataFromJSON();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("aaaaaa afterTextChanged","&"+s);
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               FeedItem item = feedItem.get(position-1);
                String Name;
                if(item.getUser_id()!=0) {
                    if(!TextUtils.isEmpty(item.getContact_user_name()))
                        Name = item.getContact_user_name();
                    else if(!TextUtils.isEmpty(item.getChat_name()))
                        Name = item.getChat_name();
                    else
                        Name = item.getPhone();
                }else
                    Name = item.getChat_name();

                Log.d("WHAT","IS2");
                String avatar;
                boolean is_ava ;
                if (TextUtils.isEmpty(item.getAvatar())){
                    if(Name.length()>1)
                        avatar = Name.charAt(0)+""+Name.charAt(1);
                    else
                        avatar = Name.charAt(0)+"";
                    is_ava = false;
                }else {
                    is_ava = true;
                    avatar = item.getAvatar();
                }
                Log.d("WHAT","IS3");
                item.setNew_message_count(0);
                chatListAdapter.notifyDataSetChanged();
                if (item.getDialog_id().contains("U")){
                    startActivity(new Intent(getActivity(), ActivityChatMessage.class)
                            .putExtra("who","chat")
                            .putExtra("user_id",item.getUser_id())
                            .putExtra("is_mute",item.getIs_mute())
                            .putExtra("name",Name)
                            .putExtra("chat_text",item.getLast_visit())
                            .putExtra("avatar",avatar)
                            .putExtra("is_ava",is_ava)
                            .putExtra("dialog_id",item.getDialog_id())
                            .putExtra("i_block_partner",item.getI_block_partner())
                            .putExtra("partner_block_me",item.getPartner_block_me())
                    );

                }else if (item.getDialog_id().contains("G")){
                    startActivity(new Intent(getActivity(), ActivityChatMessage.class)
                            .putExtra("who","group")
                            .putExtra("group_id",item.getGroup_id())
                            .putExtra("name",Name)
                            .putExtra("is_mute",item.getIs_mute())
                            .putExtra("chat_text",item.getLast_visit())
                            .putExtra("avatar",avatar)
                            .putExtra("is_ava",is_ava)
                            .putExtra("is_admin",item.getIs_admin())
                            .putExtra("dialog_id",item.getDialog_id())
                            .putExtra("i_block_partner",item.getI_block_partner())
                            .putExtra("partner_block_me",item.getPartner_block_me())
                    );
                }else if(item.getDialog_id().contains("C")){
                    startActivity(new Intent(getActivity(), ActivityChatMessage.class)
                            .putExtra("who","channel")
                            .putExtra("channel_id",item.getChannel_id())
                            .putExtra("is_mute",item.getIs_mute())
                            .putExtra("name",Name)
                            .putExtra("chat_text",item.getLast_visit())
                            .putExtra("avatar",avatar)
                            .putExtra("is_ava",is_ava)
                            .putExtra("is_admin",item.getIs_admin())
                            .putExtra("dialog_id",item.getDialog_id())
                            .putExtra("i_block_partner",item.getI_block_partner())
                            .putExtra("partner_block_me",item.getPartner_block_me())
                    );
                }


            }
        });

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                FeedItem item = feedItem.get(position-1);
                if (item.getDialog_id().contains("U")){
                    ((MainActivity)getActivity()).ShowMenu("partner_id",item.getChat_id(),item.getUser_id(),item.getDialog_id());
                }else if (item.getDialog_id().contains("G")){
                    ((MainActivity)getActivity()).ShowMenu("group_id",item.getChat_id(),item.getGroup_id(),item.getDialog_id());
                }else if(item.getDialog_id().contains("C")){
                    ((MainActivity)getActivity()).ShowMenu("channel_id",item.getChat_id(),item.getChannel_id(),item.getDialog_id());
                }
                return true;
            }
        });




        return  view;

    }




    @OnClick(R.id.fab1)
    void fab1(){
        fab_menu.close(true);
        startActivity(new Intent(getActivity(),ActivitySelectedUser.class).putExtra("who",3));

    }
    @OnClick(R.id.fab2)
    void fab2(){
        fab_menu.close(true);
        startActivity(new Intent(getActivity(),ActivitySelectedUser.class).putExtra("who",1));
    }
    @OnClick(R.id.fab3)
    void fab3(){
        fab_menu.close(true);
        startActivity(new Intent(getActivity(),ActivitySelectedUser.class).putExtra("who",2));
    }



    @Override
    public void onRefresh() {

        swipe.setRefreshing(true);
        GetDataFromJSON();
    }


    public void GetSearchFromJSON(String search) {

        swipe.setRefreshing(true);
        Log.d("chat_list",chat + TOKEN + "&per_page=100&page=1&q="+search);
        AndroidNetworking.get(chat + TOKEN + "&per_page=100&page=1&q="+search)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")) {
                        feedItem.clear();
                        JSONArray data = response.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {

                            JSONObject jsonObject = (JSONObject) data.get(i);
                            String file_format="";
                            if(jsonObject.getInt("is_has_file")==1){
                                file_format = ((JSONObject)jsonObject.getJSONArray("file_list").get(0)).getString("file_format");
                            }

                            FeedItem item = new FeedItem();
                            item.setChat_id(NurInt(jsonObject, "chat_id"));
                            item.setIs_bookmark(NurInt(jsonObject, "is_bookmark"));
                            item.setChat_kind(NurString(jsonObject, "chat_kind"));
                            item.setIs_read(NurInt(jsonObject, "is_read"));
                            item.setIs_admin(NurInt(jsonObject, "is_admin"));
                            item.setI_block_partner(NurInt(jsonObject, "i_block_partner"));
                            item.setPartner_block_me(NurInt(jsonObject, "partner_block_me"));
                            item.setIs_has_file(NurInt(jsonObject, "is_has_file"));
                            item.setIs_contact(NurInt(jsonObject, "is_contact"));
                            item.setNew_message_count(NurInt(jsonObject, "new_message_count"));
                            item.setAction_name(NurString(jsonObject, "action_name"));
                            item.setGroup_id(NurInt(jsonObject, "group_id"));
                            item.setChannel_id(NurInt(jsonObject, "channel_id"));
                            item.setIs_mute(NurInt(jsonObject, "is_mute"));
                            item.setLast_visit(NurString(jsonObject, "last_visit"));
                            item.setUser_id(NurInt(jsonObject, "user_id"));
                            item.setDialog_id(NurString(jsonObject, "dialog_id"));
                            item.setChat_name(NurString(jsonObject, "chat_name"));
                            item.setAvatar( NurString(jsonObject, "avatar"));
                            item.setPhone(NurString(jsonObject, "phone"));
                            item.setContact_user_name(NurString(jsonObject, "nickname"));
                            item.setChat_text(NurString(jsonObject, "chat_text"));
                            item.setChat_date( NurString(jsonObject, "chat_date"));
                            item.setIs_typing(false);
                            item.setSfile_format(file_format);
                            item.setIs_own_last_message(jsonObject.getBoolean("is_own_last_message"));
                            feedItem.add(item);

                        }
                        chatListAdapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);

                    } else {
                        Toasty.error(getActivity(), response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(getActivity(), "Проверить подключение интернета", Toast.LENGTH_SHORT).show();


            }
        });

    }

    public void GetDataFromJSON() {

        //swipe.setRefreshing(true);
        Log.d("chat_list",chat + TOKEN + "&per_page=100&page=1");
        AndroidNetworking.get(chat + TOKEN + "&per_page=100&page=1")
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if (response.getBoolean("status")) {

                        JSONArray data = response.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {

                            JSONObject jsonObject = (JSONObject) data.get(i);
                            String file_format="";
                            if(jsonObject.getInt("is_has_file")==1){
                                file_format = ((JSONObject)jsonObject.getJSONArray("file_list").get(0)).getString("file_format");
                            }

                            int is_sticker = NurInt(jsonObject, "is_sticker");
                            String sticker_image="";
                            if (is_sticker==1){
                                JSONObject sticker = jsonObject.getJSONObject("sticker");
                                sticker_image =  NurString(sticker, "sticker_image");
                            }
                            Dialog dialog = new Dialog(NurInt(jsonObject, "chat_id"),NurInt(jsonObject, "is_bookmark"), NurInt(jsonObject, "is_read"), NurInt(jsonObject, "is_mute"), NurInt(jsonObject, "i_block_partner"),
                                    NurInt(jsonObject, "partner_block_me"), NurInt(jsonObject, "is_has_file"), NurInt(jsonObject, "is_contact"), NurInt(jsonObject, "new_message_count"), NurInt(jsonObject, "group_id"), NurInt(jsonObject, "channel_id"),
                                    NurInt(jsonObject, "user_id"), NurInt(jsonObject, "is_admin"), NurString(jsonObject, "chat_kind"), NurString(jsonObject, "action_name"), NurString(jsonObject, "chat_name"),
                                    NurString(jsonObject, "avatar"), NurString(jsonObject, "phone"), NurString(jsonObject, "nickname"), NurString(jsonObject, "chat_text"), NurString(jsonObject, "chat_date"),
                                    NurString(jsonObject, "last_visit"), NurString(jsonObject, "dialog_id"), System.currentTimeMillis(),file_format,jsonObject.getBoolean("is_own_last_message"),is_sticker,sticker_image);

                        }
                        SetDataToList(1);
                    } else {
//                        Toasty.error(getActivity(), response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(getActivity(), "Проверить подключение интернета", Toast.LENGTH_SHORT).show();


            }
        });

    }

    public static void SetDataToList(int is_bookmark){

        if (is_bookmark==1){
            feedItem.clear();
        }

        realm.beginTransaction();
        RealmResults<Dialog> results = realm.where(Dialog.class).equalTo("is_bookmark", is_bookmark).sort("time").findAll();
        realm.commitTransaction();

        Log.d("AAAAA","REALM");
        for (int i = 0; i<results.size();i++){
            Dialog dialog = results.get(i);
            FeedItem item = new FeedItem();
            item.setChat_id(dialog.getChat_id());
            item.setIs_bookmark(dialog.getIs_bookmark());
            item.setChat_kind(dialog.getChat_kind());
            item.setIs_read(dialog.getIs_read());
            item.setIs_admin(dialog.getIs_admin());
            item.setI_block_partner(dialog.getI_block_partner());
            item.setPartner_block_me(dialog.getPartner_block_me());
            item.setIs_has_file(dialog.getIs_has_file());
            item.setIs_contact(dialog.getIs_contact());
            item.setNew_message_count(dialog.getNew_message_count());
            item.setAction_name(dialog.getAction_name());
            item.setGroup_id(dialog.getGroup_id());
            item.setChannel_id(dialog.getChannel_id());
            item.setIs_mute(dialog.getIs_mute());
            item.setLast_visit(dialog.getLast_visit());
            item.setUser_id(dialog.getUser_id());
            item.setDialog_id(dialog.getDialog_id());
            item.setChat_name(dialog.getChat_name());
            item.setAvatar(dialog.getAvatar());
            item.setPhone(dialog.getPhone());
            item.setContact_user_name(dialog.getContact_user_name());
            item.setChat_text(dialog.getChat_text());
            item.setChat_date(dialog.getChat_date());
            item.setIs_typing(false);
            item.setSfile_format(dialog.getFile_format());
            item.setIs_own_last_message(dialog.isIs_own_last_message());
            item.setIs_sticker(dialog.getIs_sticker());
            item.setSticker_image(dialog.getSticker_image());
            feedItem.add(item);

        }

        if (is_bookmark==1){
            SetDataToList(0);
        }else {
            chatListAdapter.notifyDataSetChanged();
            swipe.setRefreshing(false);
        }





    }

    public void SetDataFirstToList(int is_bookmark){

        if (is_bookmark==1){
            feedItem.clear();
        }

        realm.beginTransaction();
        RealmResults<Dialog> results = realm.where(Dialog.class).equalTo("is_bookmark", is_bookmark).sort("time").findAll();
        realm.commitTransaction();

        Log.d("AAAAA","REALM");
        for (int i = 0; i<results.size();i++){
            Dialog dialog = results.get(i);
            FeedItem item = new FeedItem();
            item.setChat_id(dialog.getChat_id());
            item.setIs_bookmark(dialog.getIs_bookmark());
            item.setChat_kind(dialog.getChat_kind());
            item.setIs_read(dialog.getIs_read());
            item.setIs_admin(dialog.getIs_admin());
            item.setI_block_partner(dialog.getI_block_partner());
            item.setPartner_block_me(dialog.getPartner_block_me());
            item.setIs_has_file(dialog.getIs_has_file());
            item.setIs_contact(dialog.getIs_contact());
            item.setNew_message_count(dialog.getNew_message_count());
            item.setAction_name(dialog.getAction_name());
            item.setGroup_id(dialog.getGroup_id());
            item.setChannel_id(dialog.getChannel_id());
            item.setIs_mute(dialog.getIs_mute());
            item.setLast_visit(dialog.getLast_visit());
            item.setUser_id(dialog.getUser_id());
            item.setDialog_id(dialog.getDialog_id());
            item.setChat_name(dialog.getChat_name());
            item.setAvatar(dialog.getAvatar());
            item.setPhone(dialog.getPhone());
            item.setContact_user_name(dialog.getContact_user_name());
            item.setChat_text(dialog.getChat_text());
            item.setChat_date(dialog.getChat_date());
            item.setIs_typing(false);
            item.setSfile_format(dialog.getFile_format());
            item.setIs_own_last_message(dialog.isIs_own_last_message());
            item.setIs_sticker(dialog.getIs_sticker());
            item.setSticker_image(dialog.getSticker_image());
            feedItem.add(item);

        }

        if (is_bookmark==1){
            SetDataFirstToList(0);
        }else {
            chatListAdapter.notifyDataSetChanged();
            swipe.setRefreshing(false);
            if(isOnline(getActivity())){
                GetDataFromJSON();
                Log.d("AAAAAAAaaaaa","YES");
            }else {
               Toasty.error(getActivity(), "Проверить подключение интернета", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onStart() {
        if(isOnline(getActivity())){
            GetDataFromJSON();
            Log.d("AAAAAAAaaaaa","YES");
        }else {
            Toasty.error(getActivity(), "Проверить подключение интернета", Toast.LENGTH_SHORT).show();
        }
        super.onStart();
    }



}