package kz.bgpro.www.dalagram.activitys.channel;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.github.pavlospt.roundedletterview.RoundedLetterView;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.ActivityChatMessage;
import kz.bgpro.www.dalagram.activitys.adapters.SelectedContactsAdapter;
import kz.bgpro.www.dalagram.models.FeedItem;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.channel_avatar;
import static kz.bgpro.www.dalagram.utils.MyConstants.new_channel;

/**
 * Created by nurbaqyt on 10.08.2018.
 */

public class ActivityNewChannel extends AppCompatActivity {

    @BindView(R.id.add_photo)
    ImageButton add_photo;

    @BindView(R.id.avatar_tv)
    RoundedLetterView avatar_tv;

    @BindView(R.id.avatar)
    CircleImageView avatar;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.desc)
    EditText desc;

    Bitmap rotatedBMP=null;


    @BindView(R.id.users_list)
    ListView users_list;

    @BindView(R.id.users_count)
    TextView users_count;

    @BindView(R.id.done)
    ImageButton done;

    @BindView(R.id.progress)
    ProgressBar progress;

    ArrayList<FeedItem> feedItem ;
    int length;
    JSONArray channel_users;
    SelectedContactsAdapter contactsDalaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_channel);
        ButterKnife.bind(this);

        feedItem = (ArrayList<FeedItem>)getIntent().getSerializableExtra("feed");
        length = feedItem.size();
        channel_users = new JSONArray();
        for (int i=0;i<length;i++){
            try {
                JSONObject jsonObject = new JSONObject();
                FeedItem item = feedItem.get(i);
                item.setIs_selected(false);
                jsonObject.put("user_id",item.getUser_id());
                channel_users.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        contactsDalaAdapter = new SelectedContactsAdapter(feedItem, ActivityNewChannel.this);
        users_list.setAdapter(contactsDalaAdapter);

        if(length==1){
            users_count.setText(length+" подписчик");
        }else if(length==2 || length==3 || length==4){
            users_count.setText(length+" подписчика");
        }else {
            users_count.setText(length+" подписчиков");
        }



        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()>0){
                    avatar_tv.setTitleText(s.charAt(0)+"");
                    add_photo.setImageResource(R.drawable.ic_empty);
                }else {
                    avatar_tv.setTitleText("");
                    add_photo.setImageResource(R.drawable.ic_add_a_photo);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



    }

    @OnClick(R.id.done)
    void done(){
        final String Name = name.getText().toString();
        String Login = desc.getText().toString();
        if (TextUtils.isEmpty(Name) || TextUtils.isEmpty(Login)){
            Toasty.info(this, "Введите все поля", Toast.LENGTH_SHORT).show();
        }else {
            done.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
            try {

                JSONObject js = new JSONObject();
                js.put("channel_name", Name);
                js.put("channel_login", Login);
                js.put("is_public", "1");
                js.put("channel_users",channel_users);
                AndroidNetworking.post(new_channel+TOKEN)
                        .addJSONObjectBody(js)
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("RRRRRR",response+" ");
                            if(response.getBoolean("status")){
                                int id = response.getInt("channel_id");
                                if(rotatedBMP!=null){
                                    UploadImage(rotatedBMP,id,Name);
                                }else {
                                    done.setVisibility(View.VISIBLE);
                                    progress.setVisibility(View.GONE);

                                    startActivity(new Intent(ActivityNewChannel.this, ActivityChatMessage.class)
                                            .putExtra("who","channel")
                                            .putExtra("channel_id",id)
                                            .putExtra("name",Name)
                                            .putExtra("chat_text","")
                                            .putExtra("avatar",Name.charAt(0)+" ")
                                            .putExtra("is_ava",false)
                                            .putExtra("is_admin",1)
                                            .putExtra("dialog_id",id+"C")

                                    );

                                }
                            }else {
                                Toasty.error(ActivityNewChannel.this, response.getString("error"), Toast.LENGTH_SHORT).show();

                                done.setVisibility(View.VISIBLE);
                                progress.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toasty.error(ActivityNewChannel.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }


    @OnClick(R.id.back)
    void back(){
        finish();
    }


    @OnClick(R.id.add_photo)
    void add_photo(){

        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Glide.with(this).load(resultUri).into(avatar);
                InputStream image_stream = null;
                try {
                    image_stream = getContentResolver().openInputStream(resultUri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                 rotatedBMP = BitmapFactory.decodeStream(image_stream);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }


    public void UploadImage(Bitmap _bitmap, final int channel_id, final String name) {
        long time = System.currentTimeMillis();
        String file_name = time+"channel.jpg";
        File f = new File(getCacheDir(),  file_name );
        try {
            f.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            _bitmap.compress(Bitmap.CompressFormat.JPEG, 90 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();


            AndroidNetworking.upload(channel_avatar+TOKEN)
                    .addMultipartFile("image", f)
                    .addMultipartParameter("channel_id",channel_id+"")
                    .build().getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {


                    startActivity(new Intent(ActivityNewChannel.this, ActivityChatMessage.class)
                            .putExtra("who","channel")
                            .putExtra("channel_id",channel_id)
                            .putExtra("name",name)
                            .putExtra("chat_text","")
                            .putExtra("avatar",name.charAt(0)+" ")
                            .putExtra("is_ava",false)
                            .putExtra("is_admin",1)
                            .putExtra("dialog_id",channel_id+"C")
                    );



                }

                @Override
                public void onError(ANError anError) {

                    startActivity(new Intent(ActivityNewChannel.this, ActivityChatMessage.class)
                            .putExtra("who","channel")
                            .putExtra("channel_id",channel_id)
                            .putExtra("name",name)
                            .putExtra("chat_text","")
                            .putExtra("avatar",name.charAt(0)+" ")
                            .putExtra("is_ava",false)
                            .putExtra("is_admin",1)
                            .putExtra("dialog_id",channel_id+"C")
                    );


                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}