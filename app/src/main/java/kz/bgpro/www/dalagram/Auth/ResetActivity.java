package kz.bgpro.www.dalagram.Auth;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.MainActivity;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.utils.NurJS;

import static kz.bgpro.www.dalagram.utils.MyConstants.reset;


public class ResetActivity extends Activity {

    @BindView(R.id.first_layout)
    LinearLayout first_layout;

    @BindView(R.id.second_layout)
    LinearLayout second_layout;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.progress)
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset);
        ButterKnife.bind(this);


        second_layout.setVisibility(View.GONE);
        first_layout.setVisibility(View.VISIBLE);


    }


    @OnClick(R.id.back)
    void back(){
        finish();
    }

    @OnClick(R.id.back2)
    void back2(){
        finish();
    }
    @OnClick(R.id.reset)
    void reset(){
        progress.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(email.getText().toString())){
            AndroidNetworking.post(reset)
                    .addBodyParameter("email",email.getText().toString())
                    .build().getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        if (response.getBoolean("status")) {

                            first_layout.setVisibility(View.GONE);
                            second_layout.setVisibility(View.VISIBLE);
                        } else {

                            Toasty.error(ResetActivity.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                        }
                        progress.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    Log.d("response",response+"**");

                }

                @Override
                public void onError(ANError anError) {
                    first_layout.setVisibility(View.GONE);
                    second_layout.setVisibility(View.VISIBLE);
                }
            });
        }else {
            progress.setVisibility(View.GONE);
            Toasty.error(this, "заполните поля", Toast.LENGTH_SHORT).show();
        }

    }


}
