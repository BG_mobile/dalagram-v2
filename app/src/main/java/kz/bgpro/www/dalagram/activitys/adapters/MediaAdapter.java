package kz.bgpro.www.dalagram.activitys.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.models.FeedItem;


/**
 * Created by nurbaqyt on 22.05.2018.
 */

public class MediaAdapter extends BaseAdapter {

    ArrayList<FeedItem> feed_item;
    FeedItem item;
    Activity activity;
    int count;

    public MediaAdapter(ArrayList<FeedItem> feedItem, Activity activity, int count) {
        this.activity = activity;
        this.feed_item = feedItem;
        this.count = count;
    }


    @Override
    public int getCount() {
        return count;
    }

    @Override
    public Object getItem(int location) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {

        @BindView(R.id.image)
        ImageView image;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_meida_list, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        item = feed_item.get(position);


        switch (item.getSfile_format()) {
            case "image":
                Glide.with(activity).load(item.getSfile_url()).into(viewHolder.image);
                break;
            case "video":
                try {
                    Bitmap bm = retriveVideoFrameFromVideo(item.getSfile_url());
                    viewHolder.image.setImageBitmap(bm);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                break;
            case "audio":
                viewHolder.image.setImageResource(R.drawable.ic_audio);
                break;
            default:
                viewHolder.image.setImageResource(R.drawable.ic_file_black);
                break;
        }



        return convertView;
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);

            bitmap = mediaMetadataRetriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }


}
