package kz.bgpro.www.dalagram.activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;
import it.sephiroth.android.library.widget.HListView;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.Realm.Contact;
import kz.bgpro.www.dalagram.Realm.PhoneContact;
import kz.bgpro.www.dalagram.activitys.adapters.HContactsAdapter;
import kz.bgpro.www.dalagram.activitys.adapters.SelectedContactsAdapter;
import kz.bgpro.www.dalagram.fragment.adapters.ContactsAdapter;
import kz.bgpro.www.dalagram.fragment.adapters.ContactsDalaAdapter;
import kz.bgpro.www.dalagram.models.FeedItem;
import kz.bgpro.www.dalagram.utils.NurJS;

import static android.widget.Toast.LENGTH_SHORT;
import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.PREFS_NAME;
import static kz.bgpro.www.dalagram.utils.MyConstants.contact;

/**
 * Created by nurbaqyt on 14.08.2018.
 */

public class ActivitySendUser extends Activity {

    @BindView(R.id.contacts_list)
    ListView contacts_list;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.ed_search)
    TextView search;

    Realm realm;
    ArrayList <FeedItem>  feedItem = new ArrayList<FeedItem>();
    SharedPreferences settings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_user);
        ButterKnife.bind(this);
        name.setText("Select Contact");
        realm = Realm.getDefaultInstance();
         settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);


        if(!settings.getBoolean("has_contact",false)){
            Log.d("aaaaaa","getContactList");
            getAllContacts();
        }else {
            getContactFromRealm("");
            Log.d("aaaaaa","getContactFromRealm");
        }



        contacts_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FeedItem item = feedItem.get(position);

                Intent intent = new Intent();
                intent.putExtra("name",item.getName());
                intent.putExtra("phone",item.getPhone());
                setResult(RESULT_OK, intent);
                finish();
            }
        });


        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0){
                    getContactFromRealm(search.getText().toString());
                }if (s.length()==0){
                    getContactFromRealm("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    public void getAllContacts() {
        long startnow;
        long endnow;
        startnow = android.os.SystemClock.uptimeMillis();

        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String selection = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Cursor cursor = getContentResolver().query(uri, new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER,   ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone._ID, ContactsContract.Contacts._ID}, selection, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        if(cursor.getCount()>0) {
            cursor.moveToFirst();
            JSONArray jsonArray = new JSONArray();
            try {
                while (cursor.isAfterLast() == false) {

                    String contactNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    contactNumber = contactNumber.replaceAll(" ","");
                    PhoneContact phoneContact = new PhoneContact(contactName, contactNumber);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("phone", contactNumber);
                    jsonObject.put("contact_user_name", contactName);
                    jsonArray.put(jsonObject);

                    cursor.moveToNext();
                }


                cursor.close();
                cursor = null;

                endnow = android.os.SystemClock.uptimeMillis();

                settings.edit().putBoolean("has_contact", true).commit();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            getContactFromRealm("");
        }

    }

    private void getContactFromRealm(String search) {
        realm.beginTransaction();
        feedItem = new ArrayList<FeedItem>();
        RealmResults<PhoneContact> phoneContacts;
        if (search.isEmpty()){
            phoneContacts = realm.where(PhoneContact.class).findAll();
        }else {
            phoneContacts = realm.where(PhoneContact.class)
                    .contains("contact_name", search, Case.INSENSITIVE)
                    .or()
                    .contains("phone", search, Case.INSENSITIVE)
                    .findAll();
        }
        for (int i=0;i<phoneContacts.size();i++){
            PhoneContact phoneContact = phoneContacts.get(i);
            FeedItem item = new FeedItem();
            item.setName(phoneContact.getContact_name());
            item.setPhone(phoneContact.getPhone());
            feedItem.add(item);
        }
        ContactsAdapter contactsAdapter = new ContactsAdapter(feedItem,ActivitySendUser.this);
        contacts_list.setAdapter(contactsAdapter);



        realm.commitTransaction();


    }


    @OnClick(R.id.back)
    void back(){
        finish();
    }

}