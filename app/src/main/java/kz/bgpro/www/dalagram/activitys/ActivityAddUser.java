package kz.bgpro.www.dalagram.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import it.sephiroth.android.library.widget.HListView;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.adapters.HContactsAdapter;
import kz.bgpro.www.dalagram.activitys.channel.ActivityNewChannel;
import kz.bgpro.www.dalagram.activitys.fragment.FragmentFriendUserList;
import kz.bgpro.www.dalagram.activitys.fragment.FragmentUserList;
import kz.bgpro.www.dalagram.activitys.groups.ActivityNewGroup;
import kz.bgpro.www.dalagram.models.FeedItem;

import static kz.bgpro.www.dalagram.activitys.fragment.FragmentUserList.selected_adapter;

/**
 * Created by nurbaqyt on 14.08.2018.
 */

public class ActivityAddUser extends AppCompatActivity {



    @BindView(R.id.tablayout)
    TabLayout tabLayout;

    @BindView(R.id.pager)
    ViewPager viewPager;

    @BindView(R.id.hlistView)
    HListView hlistView;



    public static TextView count;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.next)
    ImageButton next;

    @BindView(R.id.ed_search)
    EditText search;

    @BindString(R.string.contacts)
    String contacts;

    @BindString(R.string.friends)
    String friends;

    public static int group_id,channel_id;

    public static int selected_count=0,total_count=0;
    public static  ArrayList <FeedItem> feedItem_contacts;
    public static  ArrayList<FeedItem> h_feedItem;


    public static  HContactsAdapter hadapter;

   public static int who;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_user);
        ButterKnife.bind(this);

        count = (TextView)findViewById(R.id.count);


        group_id = getIntent().getIntExtra("group_id",0);
        channel_id = getIntent().getIntExtra("channel_id",0);

        if (group_id==0) who = 3;

        if (channel_id==0) who = 2;

        CPagerAdapter adapter = new CPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);


        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        name.setText("Добавить пользователя");
        hlistView.setVisibility(View.GONE);
        count.setVisibility(View.GONE);
        next.setVisibility(View.GONE);

        h_feedItem  = new ArrayList<FeedItem>();
        hadapter = new HContactsAdapter(h_feedItem,ActivityAddUser.this);
        hlistView.setAdapter(hadapter);
        hlistView.setOnItemClickListener(new it.sephiroth.android.library.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> parent, View view, int position, long id) {

                FeedItem item = h_feedItem.get(position);
                int p = feedItem_contacts.indexOf(item);
                feedItem_contacts.get(p).setIs_selected(false);
                h_feedItem.remove(item);
                hadapter.notifyDataSetChanged();
                selected_adapter.notifyDataSetChanged();
                selected_count--;
                count.setText("выбран "+selected_count);
            }
        });


        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String tag = "android:switcher:" + R.id.pager + ":" + viewPager.getCurrentItem();
                if (viewPager.getCurrentItem()==0){
                    FragmentUserList f = (FragmentUserList) getSupportFragmentManager().findFragmentByTag(tag);
                    if (s.length()>0){
                        f.GetJSONFromServer(search.getText().toString());
                    }else if (s.length()==0){
                        f.GetJSONFromServer("");
                    }
                }else {
                    FragmentFriendUserList f = (FragmentFriendUserList) getSupportFragmentManager().findFragmentByTag(tag);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



    }

    @OnClick(R.id.back)
    void back(){
        finish();
    }


    class CPagerAdapter extends FragmentPagerAdapter {



        public CPagerAdapter(FragmentManager fm)
        {
            super(fm);

        }


        @Override
        public Fragment getItem(int position) {
            switch(position)
            {

                case 0:
                    return new FragmentUserList(null,ActivityAddUser.this,who);
                case 1:
                    return new FragmentFriendUserList(null,ActivityAddUser.this,who);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            if (position == 0) {
                title = contacts;
            } else if (position == 1) {
                title = friends;
            }
            return title;
        }

    }

}