package kz.bgpro.www.dalagram.activitys;

import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.bumptech.glide.Glide;
import com.vanniktech.emoji.EmojiTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.Realm.DialogDetail;
import kz.bgpro.www.dalagram.models.FeedItem;

import static kz.bgpro.www.dalagram.utils.MessageType.IMAGE_MESSAGE;

/**
 * Created by nurbaqyt on 02.08.2018.
 */

public class MessageInfoDialog {


    private ActivityChatMessage activityChatMessage;
     private int position;
     Dialog dialog;
 public void attachView(int position, ActivityChatMessage usersActivity) {
     this.position = position;
     this.activityChatMessage = usersActivity;
    }


    public void showDialog(final Activity activity, final FeedItem item, final RelativeLayout answer_layout, final TextView answer_name, final String name, final EmojiTextView answer_chat_text, ImageView answer_image){
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.message_info_dialog);
        dialog.show();


        final TextView download_txt,answer_txt,copy_txt,forward_txt,delete_txt;
        answer_txt = (TextView) dialog.findViewById(R.id.answer_txt);
        copy_txt = (TextView) dialog.findViewById(R.id.copy_txt);
        forward_txt = (TextView) dialog.findViewById(R.id.forward_txt);
        delete_txt = (TextView) dialog.findViewById(R.id.delete_txt);
        download_txt = (TextView) dialog.findViewById(R.id.download_txt);
        if (item.getMessage_type()==IMAGE_MESSAGE){
            download_txt.setVisibility(View.VISIBLE);
        }else {
            download_txt.setVisibility(View.GONE);
        }
        download_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                downloadImage(item.getFile_url()[0]);

            }
        });

        answer_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityChatMessage.answer_chat_id = item.getChat_id();

                answer_layout.setVisibility(View.VISIBLE);
                if (item.getIs_has_file()==1){
                    switch (item.getFile_format()[0]){
                        case "image":
                            answer_chat_text.setText("фото");
                            answer_image.setVisibility(View.VISIBLE);
                            Glide.with(activity).load(item.getFile_url()[0]).into(answer_image);
                            break;
                        case "audio":
                            answer_image.setVisibility(View.GONE);
                            answer_chat_text.setText("аудио - "+item.getFile_name()[0]);
                            break;
                        case "video":
                            answer_image.setVisibility(View.GONE);
                            answer_chat_text.setText("видео - "+item.getFile_name()[0]);
                            break;
                        case "file":
                            answer_image.setVisibility(View.GONE);
                            answer_chat_text.setText("файл - "+item.getFile_name()[0]);
                            break;
                    }

                }else {
                    answer_image.setVisibility(View.GONE);
                    answer_chat_text.setText(item.getChat_text());
                }

                answer_name.setText(name);
                dialog.dismiss();

            }
        });
        copy_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager)
                        activity.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("message text", item.getChat_text());
                clipboard.setPrimaryClip(clip);
                dialog.dismiss();
            }
        });
        forward_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject js = new JSONObject();
                    JSONArray chat_ids = new JSONArray();
                    chat_ids.put(item.getChat_id());
                    js.put("chat_ids",chat_ids);
                    Intent intent = new Intent(activity,ActivityResend.class);
                    intent.putExtra("js",js+"");
                    activity.startActivity(intent);
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        delete_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activityChatMessage.DeleteChat(item.getChat_id(),position);

            }
        });
    }

    private void downloadImage (String url ){


        String fileName = url.substring(url.lastIndexOf('/') + 1);
        String DIR_NAME = "/Dalagram/Dalagram image";
        File direct =
                new File(Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                        .getAbsolutePath() + "/" + DIR_NAME + "/");


        if (!direct.exists()) {
            direct.mkdir();
            Log.d("aaa", "dir created for first time");
        }

        DownloadManager dm = (DownloadManager) activityChatMessage.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri downloadUri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle(fileName)
                .setMimeType("image/jpeg")
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES,
                        File.separator + DIR_NAME + File.separator + fileName);

        dm.enqueue(request);
    }
}