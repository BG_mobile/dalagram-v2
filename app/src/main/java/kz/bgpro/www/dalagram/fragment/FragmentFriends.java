package kz.bgpro.www.dalagram.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import kz.bgpro.www.dalagram.activitys.profile.ActivityMyProfile;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.fragment.adapters.FriendsListAdapter;
import kz.bgpro.www.dalagram.models.FeedItem;
import kz.bgpro.www.dalagram.utils.NonScrollListView;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.follower_friend;
import static kz.bgpro.www.dalagram.utils.MyConstants.follower_in;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

public class FragmentFriends extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.first_layout)
    LinearLayout first_layout;

    @BindView(R.id.list1)
    NonScrollListView list1;

    @BindView(R.id.list2)
    NonScrollListView list2;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    ArrayList<FeedItem> feedItems_in;
    ArrayList<FeedItem> feedItems_friend;

    FriendsListAdapter adapter;
    FriendsListAdapter adapter2;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends, container, false);
        ButterKnife.bind(this, view);
        swipe.setOnRefreshListener(this);

        feedItems_in = new ArrayList<FeedItem>();
        adapter = new FriendsListAdapter(feedItems_in,getActivity(),1);
        list1.setAdapter(adapter);

        feedItems_friend = new ArrayList<FeedItem>();
        adapter2 = new FriendsListAdapter(feedItems_friend,getActivity(),2);
        list2.setAdapter(adapter2);

        GetDataFromJSON_follower_in();

        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                startActivity(new Intent(getActivity(), ActivityMyProfile.class).putExtra("user_id",feedItems_in.get(position).getUser_id()));
            }
        });
        list2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                startActivity(new Intent(getActivity(), ActivityMyProfile.class).putExtra("user_id",feedItems_friend.get(position).getUser_id()));
            }
        });

        return view;
    }

    public void GetDataFromJSON_follower_in() {
        swipe.setRefreshing(true);
        Log.d("chat_list",follower_in + TOKEN + "&per_page=100&page=1");
        AndroidNetworking.get(follower_in + TOKEN + "&per_page=100&page=1")
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")) {

                        JSONArray data = response.getJSONArray("data");
                        if (data.length()>0) {
                            first_layout.setVisibility(View.VISIBLE);
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonObject = (JSONObject) data.get(i);
                                FeedItem item = new FeedItem();

                                item.setUser_id(NurInt(jsonObject, "user_id"));
                                item.setName(NurString(jsonObject, "nickname"));
                                item.setAvatar(NurString(jsonObject, "avatar"));
                                item.setLast_visit(NurString(jsonObject, "last_visit"));

                                feedItems_in.add(item);
                            }
                            adapter.notifyDataSetChanged();
                            swipe.setRefreshing(false);
                        }else {
                            first_layout.setVisibility(View.GONE);
                            swipe.setRefreshing(false);
                        }

                        GetDataFromJSON_follower_friend("");
                    } else {
                        Toasty.error(getActivity(), response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }



            @Override
            public void onError(ANError anError) {
                Toasty.info(getActivity(), "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

            }
        });


    }
    public void GetDataFromJSON_follower_friend(String search) {
        feedItems_friend.clear();
        swipe.setRefreshing(true);
        Log.d("chat_list",follower_friend + TOKEN + "&per_page=100&page=1&search="+search);
        AndroidNetworking.get(follower_friend + TOKEN + "&per_page=100&page=1&search="+search)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")) {

                        JSONArray data = response.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jsonObject = (JSONObject) data.get(i);
                            FeedItem item = new FeedItem();

                            item.setUser_id(NurInt(jsonObject, "user_id"));
                            item.setName(NurString(jsonObject, "nickname"));
                            item.setAvatar(NurString(jsonObject, "avatar"));
                            item.setLast_visit(NurString(jsonObject, "last_visit"));

                            feedItems_friend.add(item);
                        }
                        adapter2.notifyDataSetChanged();
                        swipe.setRefreshing(false);

                    } else {
                        Toasty.error(getActivity(), response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }



            @Override
            public void onError(ANError anError) {
                Toasty.error(getActivity(), "Проверить подключение интернета", Toast.LENGTH_SHORT).show();


            }
        });

    }

    @Override
    public void onRefresh() {
        GetDataFromJSON_follower_in();
    }
}
