package kz.bgpro.www.dalagram.activitys;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.bgpro.www.dalagram.R;


/**
 * Created by nurbaqyt on 02.08.2018.
 */

public class ActivityVideoPlayer extends Activity{



    @BindView(R.id.myVideo)
    VideoView myVideo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        ButterKnife.bind(this);

        String videourl = getIntent().getStringExtra("file_url");


        MediaController vidControl = new MediaController(this);
        vidControl.setAnchorView(myVideo);
        myVideo.setMediaController(vidControl);

        Uri vidUri = Uri.parse(videourl);
        myVideo.setVideoURI(vidUri);
        myVideo.start();

    }

    @OnClick(R.id.back)
    void back(){
        finish();
    }

}
