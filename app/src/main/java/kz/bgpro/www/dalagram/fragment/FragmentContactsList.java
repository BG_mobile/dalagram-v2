package kz.bgpro.www.dalagram.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.Realm.Contact;
import kz.bgpro.www.dalagram.Realm.PhoneContact;
import kz.bgpro.www.dalagram.activitys.ActivityChatMessage;
import kz.bgpro.www.dalagram.fragment.adapters.ContactsAdapter;
import kz.bgpro.www.dalagram.fragment.adapters.ContactsDalaAdapter;
import kz.bgpro.www.dalagram.models.FeedItem;
import kz.bgpro.www.dalagram.utils.NurJS;

import static android.widget.Toast.LENGTH_SHORT;
import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.PREFS_NAME;
import static kz.bgpro.www.dalagram.utils.MyConstants.contact;


public class FragmentContactsList extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.list_view_contact)
    ListView list_view_contact;

    @BindView(R.id.list_view_dala)
    ListView list_view_dala;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    ContactsAdapter contactsAdapter;
    ContactsDalaAdapter contactsDalaAdapter;

    ArrayList<FeedItem> feedItem_contact;
    ArrayList<FeedItem> feedItem_dala;
    String TAG = "fragment_contacts";
    SharedPreferences settings;
    JSONObject object;
    Realm realm;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacts_list, container, false);
        ButterKnife.bind(this,view);
        realm = Realm.getDefaultInstance();

        Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.READ_CONTACTS)
                .withListener(new PermissionListener() {
                    @Override public void onPermissionGranted(PermissionGrantedResponse response) {/* ... */}
                    @Override public void onPermissionDenied(PermissionDeniedResponse response) {/* ... */}
                    @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {/* ... */}
                }).check();

        swipe.setOnRefreshListener(this);


        feedItem_contact = new ArrayList<FeedItem>();
        feedItem_dala = new ArrayList<FeedItem>();

        contactsDalaAdapter = new ContactsDalaAdapter(feedItem_dala, getActivity());
        list_view_dala.setAdapter(contactsDalaAdapter);

        contactsAdapter = new ContactsAdapter(feedItem_contact, getActivity());
        list_view_contact.setAdapter(contactsAdapter);
        settings = getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        swipe.setRefreshing(true);

//        realm.beginTransaction();
//
//        RealmResults<Contact> results = realm.where(Contact.class).findAll();
//        RealmResults<PhoneContact> results2 = realm.where(PhoneContact.class).findAll();
//
//        realm.commitTransaction();
//
//        int lengh1 = results.size();
//        int lengh2 = results2.size();
//        Log.d("sssssфффффф",lengh1+"**"+lengh2);
//        if (lengh1>0 && lengh2>0){
            getContactFromRealm();
//            Log.d("111","1111");
//        }
//        else if (lengh1>0) {
//            getAllContacts();
//            Log.d("2222","2222");
//
//        }else if (lengh2>0 ){
//            GetJSONFromServer();
//            Log.d("3333","3333");
//
//        }else{
//            getAllContacts();
//            Log.d("4444","44444");
//        }




        list_view_dala.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               FeedItem item = feedItem_dala.get(position);


                Log.d("WHAT","IS1");
                String Name=item.getName();
                Log.d("WHAT","IS2"+Name);
                String avatar;
                boolean is_ava ;
                if (TextUtils.isEmpty(item.getAvatar())){
                    if(Name.length()>1)
                        avatar = Name.charAt(0)+""+Name.charAt(1);
                    else
                        avatar = Name.charAt(0)+"";
                    is_ava = false;
                }else {
                    is_ava = true;
                    avatar = item.getAvatar();
                }
                Log.d("WHAT","IS3");

                startActivity(new Intent(getActivity(), ActivityChatMessage.class)
                        .putExtra("who","chat")
                        .putExtra("user_id",item.getUser_id())
                        .putExtra("is_mute",item.getIs_mute())
                        .putExtra("name",Name)
                        .putExtra("chat_text",item.getLast_visit())
                        .putExtra("avatar",avatar)
                        .putExtra("is_ava",is_ava)
                        .putExtra("dialog_id",item.getUser_id()+"U")
                );


            }
        });
        return  view;
    }



    public void getAllContacts() {
        long startnow;
        long endnow;
        startnow = android.os.SystemClock.uptimeMillis();

        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String selection = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Cursor cursor = getContext().getContentResolver().query(uri, new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER,   ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone._ID, ContactsContract.Contacts._ID}, selection, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        if(cursor.getCount()>0) {
            cursor.moveToFirst();
            JSONArray jsonArray = new JSONArray();
            try {
                while (cursor.isAfterLast() == false) {

                    String contactNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    contactNumber = contactNumber.replaceAll(" ","");
                    PhoneContact phoneContact = new PhoneContact(contactName, contactNumber);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("phone", contactNumber);
                    jsonObject.put("contact_user_name", contactName);
                    jsonArray.put(jsonObject);

                    cursor.moveToNext();
                }


                cursor.close();
                cursor = null;

                endnow = android.os.SystemClock.uptimeMillis();

                object = new JSONObject();
                object.put("contact_users", jsonArray);
                Log.d("jsonArray",jsonArray.length()+" *");
                settings.edit().putBoolean("has_contact", true).commit();
                AddContactsToServer(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            GetJSONFromServer("");
        }

    }

    private void AddContactsToServer(JSONObject jsonObject) {
        AndroidNetworking.post(contact+TOKEN)
                .addJSONObjectBody(jsonObject)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response",response+"8");
               GetJSONFromServer();
            }
            @Override
            public void onError(ANError anError) {
                Toasty.error(getActivity(),"Проверить подключение интернета",LENGTH_SHORT).show();
                GetJSONFromServer();
            }
        });



    }

    public void GetJSONFromServer() {

        Log.d("GetJSONFromServer",contact+TOKEN+"&page=1&per_page=10");


        AndroidNetworking.get(contact+TOKEN+"&page=1&per_page=10").build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("ASAS",response+"*");
                try {
                    if(response.getBoolean("status")){
                        JSONArray jsonArray = response.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject object = (JSONObject) jsonArray.get(i);
                            Contact contact = new Contact(object.getInt("user_id"),object.getString("nickname"),object.getString("contact_user_name"),object.getString("avatar"),object.getString("user_status"));
                        }
                        getContactFromRealm();
                    }else {
                        Toasty.error(getActivity(),response.getString("error"),LENGTH_SHORT).show();
                        getContactFromRealm();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(getActivity(),"Проверить подключение интернета",LENGTH_SHORT).show();
                swipe.setRefreshing(false);

            }
        });
    }


    public void GetJSONFromServer(String search) {

        Log.d("GetJSONFromServer",contact+TOKEN+"&page=1&per_page=10&search="+search);


        AndroidNetworking.get(contact+TOKEN+"&page=1&per_page=10&search="+search).build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                feedItem_dala.clear();
                feedItem_contact.clear();
                Log.d("ASAS",response+"*");
                try {
                    if(response.getBoolean("status")){
                        JSONArray jsonArray = response.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject object = (JSONObject) jsonArray.get(i);
                            FeedItem item = new FeedItem();
                            item.setUser_id(object.getInt("user_id"));
                            item.setName(object.getString("contact_user_name"));

                            item.setAvatar(object.getString("avatar"));
                            item.setUser_status(object.getString("user_status"));
                            item.setLast_visit(object.getString("last_visit"));
                            feedItem_dala.add(item);}

                        RealmResults<PhoneContact> phoneContacts = realm.where(PhoneContact.class).contains("contact_name",search ,Case.INSENSITIVE).findAll();
                        Log.d("ASAS11111",phoneContacts+"*");
                        for (int i=0;i<phoneContacts.size();i++){
                            PhoneContact phoneContact = phoneContacts.get(i);
                            FeedItem item = new FeedItem();
                            item.setName(phoneContact.getContact_name());
                            item.setPhone(phoneContact.getPhone());
                            feedItem_contact.add(item);
                        }



                    }else {
                        Toasty.error(getActivity(),response.getString("error"),LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                contactsDalaAdapter.notifyDataSetChanged();
                contactsAdapter.notifyDataSetChanged();
                swipe.setRefreshing(false);

            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(getActivity(),"Проверить подключение интернета",LENGTH_SHORT).show();
                swipe.setRefreshing(false);

            }
        });
    }


    private void getContactFromRealm() {
            realm.beginTransaction();

        feedItem_contact.clear();



        RealmResults<PhoneContact> phoneContacts = realm.where(PhoneContact.class).findAll();
        Log.d("phoneContacts",phoneContacts.size()+"**");
        for (int i=0;i<phoneContacts.size();i++){
            PhoneContact phoneContact = phoneContacts.get(i);
            FeedItem item = new FeedItem();
            item.setName(phoneContact.getContact_name());
            item.setPhone(phoneContact.getPhone());
            feedItem_contact.add(item);
        }

        feedItem_dala.clear();
        RealmResults<Contact> results = realm.where(Contact.class).findAll();
        Log.d("Contacts",results.size()+"**");

        for (int i = 0; i < results.size(); i++) {
               Contact contact1 = results.get(i);
               FeedItem item = new FeedItem();
               item.setUser_id(contact1.getUser_id());
               item.setName(contact1.getName());

               item.setAvatar(contact1.getAvatar());
               item.setUser_status(contact1.getUser_status());
               item.setLast_visit(contact1.getLast_visit());
               feedItem_dala.add(item);
           }



        contactsDalaAdapter.notifyDataSetChanged();
        contactsAdapter.notifyDataSetChanged();




        realm.commitTransaction();

         swipe.setRefreshing(false);

    }





    @OnClick(R.id.refresh)
    void Refresh(){
        feedItem_dala.clear();
        feedItem_contact.clear();
        swipe.setRefreshing(true);
        getAllContacts();
    }

    @Override
    public void onRefresh() {
        if (object==null){

            feedItem_dala.clear();
            feedItem_contact.clear();
            AddContactsToServer(object);
        }
    }
}
