package kz.bgpro.www.dalagram.News;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.fxn.pix.Pix;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.squareup.picasso.Picasso;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;

import com.vanniktech.emoji.EmojiTextView;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.NormalFile;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import kz.bgpro.www.dalagram.activitys.ActivityVideoPlayer;
import kz.bgpro.www.dalagram.activitys.profile.ActivityMyProfile;
import kz.bgpro.www.dalagram.fragment.FragmentNews;
import kz.bgpro.www.dalagram.utils.MyBounceInterpolator;
import kz.bgpro.www.dalagram.utils.MyPicassoEngine;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import kz.bgpro.www.dalagram.News.adapter.CommentListAdapter;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.ActivityZoomImage;
import kz.bgpro.www.dalagram.models.NewsItem;

import static kz.bgpro.www.dalagram.MainActivity.TOKEN;
import static kz.bgpro.www.dalagram.utils.MyConstants.*;
import static kz.bgpro.www.dalagram.utils.NurJS.NurInt;
import static kz.bgpro.www.dalagram.utils.NurJS.NurString;

public class ActivitySingleNews extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    CircleImageView avatar;
    TextView name;
    TextView date;
    EmojiTextView desc;
    TextView more_btn;
    RelativeLayout audio_layout;
    TextView audio_name;
    TextView audio_time;
    ImageButton audio_play;
    LinearLayout like_layout;
    TextView like_count;
    ImageView like_img;
    MaterialRippleLayout comment_layout;
    MaterialRippleLayout share_layout;
    TextView comment_count;
    TextView share_count;
    TextView view_count;
    LinearLayout link_layout;
    TextView link_title;
    TextView link_desc;
    ImageView link_image;
    ImageButton more_menu;

    @BindView(R.id.comments)
   ListView commentsList;

    ArrayList<NewsItem> commentItems;

    CommentListAdapter adapter;

    @BindView(R.id.rootView)
    ViewGroup rootView;

    @BindView(R.id.emojiEditText)
    EmojiEditText emojiEditText;

    @BindView(R.id.attach_btn)
    ImageButton attach_btn ;
    @BindView(R.id.emotion_btn) ImageButton emotion_btn;

    EmojiPopup emojiPopup;
    InputMethodManager imm ;
    String TAG = "Single";
    public static MediaPlayer mediaPlayernews;
    int publication_id,answer_comment_id=0;
    String answer_comment_name="";
    int answer_position=1000000;


    FrameLayout videolayout;
    ImageView videoview;
    ImageView playvideo;
    ProgressBar videoProgressbar;
    LinearLayout images;
    View file_layout;
    ImageView send_file_image;
    TextView send_file_name;
    TextView send_file_format;
    ProgressBar file_upload_progress;

    ArrayList<String> img_file_url = new ArrayList<String>();

    @BindView(R.id.answer_layout)
    RelativeLayout answer_layout;

    @BindView(R.id.answer_name)
    TextView answer_name;

    int RequestCodeForGallerey = 700;
    int REQUEST_TAKE_GALLERY_VIDEO = 713;
    int REQUEST_TAKE_FILE = 518;
    int REQUEST_AUDIO_FILE = 320;
    private ArrayList<MediaFile> mediaFiles = new ArrayList<>();

    int file_time=0;
    String file_format;
    Dialog dialog;
    ProgressDialog progressDialog;
    Animation myAnim;

    View header;
    boolean toComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_news);
        ButterKnife.bind(this);
        myAnim = AnimationUtils.loadAnimation(this, R.anim.scale);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.1, 60);
        myAnim.setInterpolator(interpolator);

        header =  (View)getLayoutInflater().inflate(R.layout.item_news_list,null);
        commentsList.addHeaderView(header);
        commentsList.setSelectionAfterHeaderView();
        initHeaderView();

        swipe.setOnRefreshListener(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setTitle("Отправить...");
        progressDialog.setIndeterminate(false);
        progressDialog.setMax(100);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                progressDialog.dismiss();
                AndroidNetworking.cancel("upload");
            }
        });

        mediaPlayernews = new MediaPlayer();

        emojiPopup = EmojiPopup.Builder.fromRootView(rootView).build(emojiEditText);
        emojiPopup = EmojiPopup.Builder.fromRootView(rootView)
                .setOnEmojiBackspaceClickListener(ignore -> Log.d(TAG, "Clicked on Backspace"))
                .setOnEmojiClickListener((ignore, ignore2) -> Log.d(TAG, "Clicked on emoji"))
                .setOnEmojiPopupShownListener(() -> emotion_btn.setImageResource(R.drawable.ic_keyboard))
                .setOnSoftKeyboardOpenListener(ignore -> Log.d(TAG, "Opened soft keyboard"))
                .setOnEmojiPopupDismissListener(() -> emotion_btn.setImageResource(R.drawable.ic_emoticon))
                .setOnSoftKeyboardCloseListener(() -> Log.d(TAG, "Closed soft keyboard"))
                .build(emojiEditText);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        publication_id = getIntent().getIntExtra("publication_id", 0);
        boolean is_my = getIntent().getBooleanExtra("is_my", false);
         toComment = getIntent().getBooleanExtra("tocomment", false);

        if (is_my){
            more_menu.setVisibility(View.VISIBLE);
            dialog = new Dialog(this);
            dialog.setCancelable(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.myprofile_more_menu_dialog);

            Window window = dialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
            int pixels = (int) (200 * getResources().getDisplayMetrics().density);
            wlp.width = pixels;
            wlp.x = 100;
            wlp.y = 250;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(wlp);
        }else {
            more_menu.setVisibility(View.GONE);
        }



        GetNewFromServer();





    }

    private void initHeaderView() {

        avatar = (CircleImageView) header.findViewById(R.id.avatar);
        name = (TextView) header.findViewById(R.id.name);
        date = (TextView) header.findViewById(R.id.date);
        desc = (EmojiTextView) header.findViewById(R.id.desc);
        more_btn = (TextView) header.findViewById(R.id.more_btn);
        audio_layout = (RelativeLayout) header.findViewById(R.id.audio_layout);
        audio_name = (TextView) header.findViewById(R.id.audio_name);
        audio_time = (TextView) header.findViewById(R.id.audio_time);
        audio_play = (ImageButton) header.findViewById(R.id.audio_play);
        like_layout = (LinearLayout) header.findViewById(R.id.like_layout);
        like_count = (TextView) header.findViewById(R.id.like_count);
        like_img = (ImageView) header.findViewById(R.id.like_img);
        comment_layout = (MaterialRippleLayout) header.findViewById(R.id.comment_layout);
        share_layout = (MaterialRippleLayout) header.findViewById(R.id.share_layout);
        comment_count = (TextView) header.findViewById(R.id.comment_count);
        share_count = (TextView) header.findViewById(R.id.share_count);
        view_count = (TextView) header.findViewById(R.id.view_count);
        link_layout = (LinearLayout) header.findViewById(R.id.link_layout);
        link_title = (TextView) header.findViewById(R.id.link_title);
        link_desc = (TextView) header.findViewById(R.id.link_desc);
        link_image = (ImageView) header.findViewById(R.id.link_image);
        more_menu = (ImageButton) header.findViewById(R.id.more_menu);
        videolayout = (FrameLayout) header.findViewById(R.id.videolayout);
        videoview = (ImageView) header.findViewById(R.id.videoview);
        playvideo = (ImageView) header.findViewById(R.id.playvideo);
        videoProgressbar = (ProgressBar) header.findViewById(R.id.videoProgressbar);
        images = (LinearLayout) header.findViewById(R.id.images);
        file_layout = (View) header.findViewById(R.id.file_layout);
        send_file_image = (ImageView) header.findViewById(R.id.file_image);
        send_file_name = (TextView) header.findViewById(R.id.file_name);
        send_file_format = (TextView) header.findViewById(R.id.file_format);
        file_upload_progress = (ProgressBar) header.findViewById(R.id.file_upload_progress);

        more_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });
        share_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONObject js = new JSONObject();
                try {
                    js.put("publication_desc","  aaa");
                    js.put("share_publication_id",publication_id);


                    AndroidNetworking.post(publication+TOKEN)
                            .addJSONObjectBody(js)
                            .build().getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("AAAAAAMMMMM",response+" ");
//                            try {
//                                if (response.getBoolean("status")) {
//                                    finish();
//                                } else {
//                                    Toasty.error(ActivitySingleNews.this, response.getString("error"), Toast.LENGTH_SHORT).show();
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            Toasty.error(ActivitySingleNews.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }




    private void GetNewFromServer() {
        AndroidNetworking.get(publication_byId+"/"+publication_id+"?token="+TOKEN)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response_single",response+" ");
                try {
                    if (response.getBoolean("status")) {
                        NewsItem item = new NewsItem();
                        JSONArray data = response.getJSONArray("data");
                        for (int i = 0; i<data.length();i++){
                            JSONObject jsonObject = (JSONObject) data.get(i);
                             item = new NewsItem();

                            item.setPublication_id(NurInt(jsonObject, "publication_id"));
                            item.setPublication_desc(NurString(jsonObject, "publication_desc"));
                            item.setPublication_date(NurString(jsonObject, "publication_date"));
                            item.setView_count(NurInt(jsonObject, "view_count"));
                            item.setComment_count(NurInt(jsonObject, "comment_count"));
                            item.setShare_count(NurInt(jsonObject, "share_count"));
                            item.setLike_count(NurInt(jsonObject, "like_count"));
                            item.setIs_has_file(NurInt(jsonObject, "is_has_file"));
                            item.setIs_i_liked(NurInt(jsonObject, "is_i_liked"));


                            JSONObject author = jsonObject.getJSONObject("author");
                            item.setUser_id(NurInt(author, "user_id"));
                            item.setUser_name(NurString(author, "nickname"));
                            item.setPhone(NurString(author, "phone"));
                            item.setAvatar(NurString(author, "avatar"));

                            JSONArray file_list = jsonObject.getJSONArray("file_list");
                            int l = file_list.length();
                            String []file_url = new String[l];
                            String []file_format = new String[l];
                            String []file_name = new String[l];
                            int file_time=0;
                            int image_count=0;
                            for (int f=0;f<l;f++){
                                JSONObject file = (JSONObject) file_list.get(f);
                                file_url[f] =file.getString("file_url");
                                file_format[f] =file.getString("file_format");
                                file_name[f] =file.getString("file_name");
                                file_time=file.getInt("file_time");
                                if (file.getString("file_format").equals("image") || file.getString("file_format").equals("video")){
                                    image_count++;
                                }
                            }
                            item.setImage_count(image_count);
                            item.setFile_url(file_url);
                            item.setFile_format(file_format);
                            item.setFile_name(file_name);
                            item.setFile_time(file_time);
                            int is_has_link = NurInt(jsonObject, "is_has_link");
                            String link_description=" ", link_title=" ",link_image=" ";
                            if (is_has_link==1){
                                JSONObject meta_tags = jsonObject.getJSONObject("meta_tags");
                                link_description = meta_tags.getString("description");
                                link_title = meta_tags.getString("title");
                                link_image = meta_tags.getString("image");
                            }
                            item.setIs_has_link(is_has_link);
                            item.setLink_description(link_description);
                            item.setLink_title(link_title);
                            item.setLink_image(link_image);


                        }

                        InitData(item);
                        swipe.setRefreshing(false);

                    } else {
                        Toasty.error(ActivitySingleNews.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                        swipe.setRefreshing(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {

            }
        });
    }

    private void InitData(final NewsItem item) {

        name.setText(item.getUser_name());
        if(TextUtils.isEmpty(item.getPublication_desc())){
            desc.setVisibility(View.GONE);
        }else {
            String desc_text = item.getPublication_desc().replaceAll("\n","<br>");
            desc.setText(Html.fromHtml(desc_text));
        }

        more_btn.setVisibility(View.GONE);
        date.setText(item.getPublication_date());

        if (item.getLike_count()==0){
            like_count.setText(" ");
        }else {
            like_count.setText(item.getLike_count()+"");
        }

        if (item.getComment_count() == 0) {
            comment_count.setText(" ");
        }else {
            comment_count.setText(item.getComment_count()+"");
        }

        if (item.getShare_count() == 0) {
            share_count.setText(" ");
        }else {
            share_count.setText(item.getShare_count()+"");
        }

        if (item.getView_count() == 0) {
            view_count.setText(" ");
        }else {
            view_count.setText(item.getView_count()+"");
        }


        if (item.getAvatar().isEmpty()) {
            Glide.with(this).load(R.drawable.ic_person).into(avatar);
        } else {
            Glide.with(this).load(item.getAvatar()).into(avatar);
        }


        if (item.getIs_has_link()==1){
            link_layout.setVisibility(View.VISIBLE);
            link_title.setText(Html.fromHtml(item.getLink_title()));
            link_desc.setText(Html.fromHtml(item.getLink_description()));
            if (item.getLink_title().isEmpty() && item.getLink_description().isEmpty() && item.getLink_image().isEmpty()){
                link_layout.setVisibility(View.GONE);
            }else {
                link_layout.setVisibility(View.VISIBLE);
            }
            if (!item.getLink_image().isEmpty() && !item.getLink_image().equals(" ")) {
                link_image.setVisibility(View.VISIBLE);
                Picasso.get().load(item.getLink_image())
                        .placeholder(R.drawable.no_image)
                        .into(link_image);
            }else {
                link_image.setVisibility(View.GONE);
            }

        }else {
            link_layout.setVisibility(View.GONE);

        }


        if (item.getIs_i_liked()==1){
            like_img.setImageResource(R.drawable.ic_like);
            like_count.setTextColor(getResources().getColor(R.color.like_color));
        }else {
            like_img.setImageResource(R.drawable.ic_dislike);
            like_count.setTextColor(getResources().getColor(R.color.chattext));

        }

        if (item.getIs_has_file()==1){
            videolayout.setVisibility(View.GONE);
            audio_layout.setVisibility(View.GONE);

            for (int i=0;i<item.getFile_format().length;i++) {
                switch (item.getFile_format()[i]) {
                    case "image":
                        img_file_url.add(item.getFile_url()[i]);
                        float marginDp = 8 * getResources().getDisplayMetrics().density;

                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        lp.setMargins(0, (int)marginDp, 0, 0);
                        ImageView imageView = new ImageView(this); // initialize ImageView
                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                        imageView.setAdjustViewBounds(true);
                        Glide.with(ActivitySingleNews.this).load(item.getFile_url()[i]).into(imageView);
                        imageView.setLayoutParams(lp);
                        images.addView(imageView);
                        int finalI = i;
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent intent = new Intent(ActivitySingleNews.this, ActivityZoomImage.class);
                                Bundle bundle = new Bundle();
                                bundle.putStringArrayList("url", new ArrayList<>(img_file_url));
                                bundle.putInt("position", finalI);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        });
                        break;
                    case "audio":
                        audio_layout.setVisibility(View.VISIBLE);
                        audio_name.setText(item.getFile_name()[i]);
                        int second = item.getFile_time() % 60;
                        int minute = item.getFile_time() / 60;
                        String timeString = String.format("%02d:%02d", minute, second);
                        audio_time.setText(timeString);
                        break;
                    case "video":
                        videolayout.setVisibility(View.VISIBLE);

                        Glide.with(ActivitySingleNews.this)
                                .asBitmap()
                                .load(item.getFile_url()[i])
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap,
                                                                Transition<? super Bitmap> transition) {
                                        int w = bitmap.getWidth();
                                        int h = bitmap.getHeight();
                                        if (h>w){
                                            videoview.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                        }else {
                                            videoview.setScaleType(videoview.getScaleType());
                                        }
                                        videoview.setImageBitmap(bitmap);
                                    }
                                });
                        videoProgressbar.setVisibility(View.GONE);
                        break;

                    case "file":
                        file_layout.setVisibility(View.VISIBLE);


                        send_file_name.setText(item.getFile_name()[i]);
                        final String extensionRemoved = item.getFile_name()[i].split("\\.")[1];
                        send_file_format.setText(extensionRemoved);

                        switch (extensionRemoved){
                            case "pdf":
                                send_file_image.setColorFilter(getResources().getColor(R.color.color_pdf));
                                break;
                            case "xls":
                            case "xlsx":
                                send_file_image.setColorFilter(getResources().getColor(R.color.color_xls));
                                break;
                            case "doc":
                            case "docx":
                                send_file_image.setColorFilter(getResources().getColor(R.color.color_doc));
                                break;
                            case "ppt":
                            case "pptx":
                                send_file_image.setColorFilter(getResources().getColor(R.color.color_ppt));
                                break;
                            default:
                                send_file_image.setColorFilter(getResources().getColor(R.color.color_txt));
                                break;
                        }

                        break;
                }
            }
        }else {
            videolayout.setVisibility(View.GONE);
            audio_layout.setVisibility(View.GONE);
        }



        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivitySingleNews.this, ActivityMyProfile.class).putExtra("user_id", item.getUser_id()));
            }
        });

        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivitySingleNews.this, ActivityMyProfile.class).putExtra("user_id", item.getUser_id()));
            }
        });

        like_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidNetworking.post(like+TOKEN).addBodyParameter("publication_id",item.getPublication_id()+"")
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("status")){
                                item.setLike_count(response.getInt("like_count"));
                                if (item.getLike_count()==0){
                                    like_count.setText(" ");
                                }else {
                                    like_count.setText(item.getLike_count()+"");
                                }

                                if (item.getIs_i_liked()==1){
                                    item.setIs_i_liked(0);
                                    like_img.setImageResource(R.drawable.ic_dislike);
                                    like_count.setTextColor(getResources().getColor(R.color.chattext));
                                    like_img.startAnimation(myAnim);
                                }else {
                                    item.setIs_i_liked(1);
                                    like_img.setImageResource(R.drawable.ic_like);
                                    like_count.setTextColor(getResources().getColor(R.color.like_color));
                                    like_img.startAnimation(myAnim);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("LIKE", response+"  ");
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("LIKEerr", anError+"  ");
                    }
                });
            }
        });

        file_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<item.getFile_format().length;i++) {
                    if (item.getFile_format()[i].equals("file")) {
                        file_upload_progress.setVisibility(View.VISIBLE);
                        downloadFile(item.getFile_url()[i],item.getFile_name()[i],"file");
                    }
                }

            }
        });



        emojiEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.d("CharSequence ",s+"   start   "+start+"   before  "+before+"  count   "+count);
                if (s.length() == 0) {
                    attach_btn.setVisibility(View.VISIBLE);

                } else {
                    attach_btn.setVisibility(View.GONE);

                }
                if (answer_comment_id!=0){
                    Log.d("aaaaaa",s.length()+"&&&&&"+answer_comment_name.length());
                    if (s.length()<answer_comment_name.length()){
                        emojiEditText.setText(answer_comment_name);
                        emojiEditText.setSelection(emojiEditText.getText().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        audio_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<item.getFile_format().length;i++) {
                    if (item.getFile_format()[i].equals("audio")) {
                        if (mediaPlayernews != null) {
                            if (mediaPlayernews.isPlaying()) {
                                audio_play.setImageResource(R.drawable.ic_play);
                                mediaPlayernews.pause();
                            } else {
                                audio_play.setImageResource(R.drawable.ic_pause);
                                mediaPlayernews.start();
                            }
                        } else {

                            try {
                                audio_play.setImageResource(R.drawable.ic_pause);
                                mediaPlayernews = new MediaPlayer();
                                mediaPlayernews.setDataSource(item.getFile_url()[0]);
                                mediaPlayernews.prepare();
                                mediaPlayernews.start();

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });

        playvideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<item.getFile_format().length;i++) {
                    if (item.getFile_format()[i].equals("video")) {
                        Intent intent = new Intent(ActivitySingleNews.this, ActivityVideoPlayer.class);
                        intent.putExtra("file_url", item.getFile_url()[i]);
                        startActivity(intent);
                    }
                }

            }
        });

        GetCommentList(true);


    }


    @OnClick(R.id.emotion_btn)
    void emotion_btnClick(){

        Log.d("SSSSS",emojiPopup.isShowing()+"****");
        if (emojiPopup.isShowing()){
            emojiPopup.dismiss();
        }else {
            emojiPopup.toggle();
        }
    }

    @OnClick(R.id.attach_btn)
    void attach_btnClick(){

        AttachMenuDialog alert = new AttachMenuDialog();
        alert.attachView( ActivitySingleNews.this);
        alert.showDialog();

    }

    public void SendFile(int position){
        switch (position){
            case 1:
                Pix.start(ActivitySingleNews.this,                    //Activity or Fragment Instance
                        RequestCodeForGallerey,                //Request code for activity results
                        1);
                break;
            case 2:
                Matisse.from(ActivitySingleNews.this)
                        .choose(MimeType.ofAll())
                        .countable(true)
                        .maxSelectable(1)
                        .autoHideToolbarOnSingleTap(true)
                        .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                        .thumbnailScale(0.85f)
                        .imageEngine(new MyPicassoEngine())
                        .forResult(REQUEST_TAKE_GALLERY_VIDEO);
                break;
            case 3:
                mediaFiles.clear();

                Intent intent = new Intent(ActivitySingleNews.this, FilePickerActivity.class);
                intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                        .setCheckPermission(true)
                        .setSelectedMediaFiles(mediaFiles)
                        .setShowImages(false)
                        .setShowVideos(false)
                        .setShowAudios(true)
                        .setMaxSelection(1)
                        .build());
                startActivityForResult(intent, REQUEST_AUDIO_FILE);
                break;
            case 4:
                Intent intent4 = new Intent(this, NormalFilePickActivity.class);
                intent4.putExtra(Constant.MAX_NUMBER, 1);
                intent4.putExtra(NormalFilePickActivity.SUFFIX, new String[] {"xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf","zip","rar", "txt"});
                startActivityForResult(intent4, REQUEST_TAKE_FILE);
                break;
        }

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == RequestCodeForGallerey) {
                ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                File f = new File(returnValue.get(0));
                file_format = "фото";
                SendFileToServer(f);
            }else if (requestCode == REQUEST_TAKE_GALLERY_VIDEO) {

                List<String> paths = Matisse.obtainPathResult(data);
                if (!paths.isEmpty()) {
                    if (paths.get(0) != null) {
                        File f = new File(paths.get(0));
                        MediaMetadataRetriever retriever = new MediaMetadataRetriever();

                        retriever.setDataSource(ActivitySingleNews.this, Uri.fromFile(f));
                        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                        long timeInMillisec = Long.parseLong(time );



                        file_format = "видео";
                        file_time = (int) TimeUnit.MILLISECONDS.toSeconds(timeInMillisec);

                        SendFileToServer(f);
                    }
                }
            }else   if (requestCode == REQUEST_AUDIO_FILE) {
                Log.d("audio_ ","request");

                mediaFiles.clear();
                mediaFiles.addAll(data.<MediaFile>getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                if (!mediaFiles.isEmpty()) {

                    String selectedVideoPath = mediaFiles.get(0).getPath();
                    if (selectedVideoPath != null) {
                        Log.d("AAAAff", selectedVideoPath);
                        File f =  new File(selectedVideoPath);
                        int sec = (int) TimeUnit.MILLISECONDS.toSeconds(mediaFiles.get(0).getDuration());
                        file_time  = sec;
                        file_format = "аудио";
                        Log.d("AAAAA",sec+"___"+mediaFiles.get(0).getDuration());
                        SendFileToServer(f);
                    }

                }
            }else  if (requestCode == REQUEST_TAKE_FILE) {
                ArrayList<NormalFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);

                if (!list.isEmpty()) {
                     String selectedVideoPath = list.get(0).getPath();
                    if (selectedVideoPath != null) {
                        Log.d("AAAAff", selectedVideoPath);
                        File f = new File(selectedVideoPath);
                        file_time  = 0;
                        file_format = "файл";
                        SendFileToServer(f);
                    }
                }
            }
        }
    }



    public void SendFileToServer(File f){

        progressDialog.show();
        Log.d("AAAAAAAA","**"+f);
        AndroidNetworking.upload(file+TOKEN)
                .setTag("upload")
                .addMultipartFile("file", f)
                .setPriority(Priority.HIGH)
                .build()
                    .setUploadProgressListener(new UploadProgressListener() {
                        @Override
                        public void onProgress(long bytesUploaded, long totalBytes) {
                            // do anything with progress
                            int progress = (int) (bytesUploaded*100 / totalBytes);
                            Log.d("PPPPPPPP",progress+"**");
                            progressDialog.setProgress(progress);
                        }
                    })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("file response _ ", response + "***");
                        try {
                            if (response.getBoolean("status")) {
                                JSONArray file_list = new JSONArray();
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("file_name",response.getString("file_name"));
                                jsonObject.put("file_url",response.getString("file_url"));
                                jsonObject.put("file_format",response.getString("file_format"));
                                jsonObject.put("file_time",file_time);
                                file_list.put(jsonObject);
                                progressDialog.dismiss();
                                SendComment(file_list);


                            } else {
                                progressDialog.dismiss();
                                Toasty.error(ActivitySingleNews.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
//                        Toasty.error(ActivityChatMessage.this,"Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        Log.d("file anError _ ", "Проверить подключение интернета" + "***");
                        Log.d("file anError _ ", anError.getErrorCode() + "***");
                    }
                });

    }





    @OnClick(R.id.record_button)
    void record_button(){
        SendComment(null);
    }



    private void SendComment(JSONArray file_list) {

        Log.d("answer_comment_id",answer_comment_id+" ");


        String comment_text =   emojiEditText.getText().toString();
        if(answer_comment_name.equals(comment_text)){
            comment_text="";
        }

        JSONObject js = new JSONObject();
        try {

            js.put("publication_id",publication_id);
            js.put("answer_comment_id",answer_comment_id);

            if (file_list!=null){
                js.put("file_list",file_list);
                js.put("comment_text",file_format);
            }else {
                js.put("comment_text",comment_text);
            }
            Log.d("jjjjjj",js+"**");
        AndroidNetworking.post(comment+TOKEN)
                .addJSONObjectBody(js).build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("rrrrrcccc",response+"*");
                try {
                    if (response.getBoolean("status")){

                        answer_comment_id=0;
                        answer_comment_name ="";
                        answer_layout.setVisibility(View.GONE);
                        emojiEditText.setText(null);
                        GetCommentList(false);

                    }else {
                        Toasty.error(ActivitySingleNews.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Log.d("an",anError.getResponse()+"");
            }
        });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void GetCommentList(boolean first) {
        swipe.setRefreshing(true);

        Log.d("comment", comment + TOKEN+"&publication_id="+publication_id);
        AndroidNetworking.get(comment+TOKEN+"&publication_id="+publication_id).build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response_comment",response+"^^^^^");
                commentItems = new ArrayList<NewsItem>();
                try {
                    if (response.getBoolean("status")) {

                        JSONArray data = response.getJSONArray("data");
                        for (int i = 0; i<data.length();i++){
                            JSONObject jsonObject = (JSONObject) data.get(i);
                            NewsItem item = new NewsItem();
                            item.setComment_id(NurInt(jsonObject,"comment_id"));
                            item.setIs_i_liked(NurInt(jsonObject,"is_liked"));
                            item.setParent_id(NurInt(jsonObject,"parent_id"));
                            if (NurInt(jsonObject,"parent_id")!=0) {
                                item.setParent_author_id(NurInt(jsonObject, "parent_author_id"));
                            }
                            item.setComment_text(NurString(jsonObject,"comment_text"));
                            item.setComment_date(NurString(jsonObject,"comment_date"));
                            item.setLike_count(NurInt(jsonObject,"like_count"));
                            JSONObject author = jsonObject.getJSONObject("author");
                            item.setUser_id(NurInt(author,"user_id"));
                            item.setAvatar(NurString(author,"avatar"));
                            item.setUser_name(NurString(author,"nickname"));
                            item.setIs_has_file(NurInt(jsonObject,"is_has_file"));
                            JSONArray file_list = jsonObject.getJSONArray("file_list");
                            int l = file_list.length();
                            String []file_url = new String[l];
                            String []file_format = new String[l];
                            String []file_name = new String[l];
                            int file_time=0;
                            for (int f=0;f<l;f++){
                                JSONObject file = (JSONObject) file_list.get(f);
                                file_url[f] =file.getString("file_url");
                                file_format[f] =file.getString("file_format");
                                file_name[f] =file.getString("file_name");
                                file_time=file.getInt("file_time");
                            }
                            item.setFile_url(file_url);
                            item.setFile_format(file_format);
                            item.setFile_name(file_name);
                            item.setFile_time(file_time);
                            commentItems.add(item);
                        }



                        adapter = new CommentListAdapter(commentItems, ActivitySingleNews.this);
                        commentsList.setAdapter(adapter);

                        swipe.setRefreshing(false);
                        if (toComment){

                            commentsList.setSelection(commentItems.size()-1);
                        }
                    } else {
                        Toasty.error(ActivitySingleNews.this, response.getString("error"), Toast.LENGTH_SHORT).show();
                        swipe.setRefreshing(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toasty.error(ActivitySingleNews.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();
                swipe.setRefreshing(true);
            }
        });


    }



    @OnClick(R.id.back)
    void back() {
        finish();
    }

    @Override
    public void onRefresh() {
        adapter.notifyDataSetChanged();
        swipe.setRefreshing(false);
    }

    @Override
    protected void onStart() {
        if (mediaPlayernews != null) {

            mediaPlayernews.stop();
            mediaPlayernews.release();
            mediaPlayernews = null;
        }
        super.onStart();
    }


    @Override
    protected void onPause() {
        if (mediaPlayernews != null) {

            mediaPlayernews.stop();
            mediaPlayernews.release();
            mediaPlayernews = null;
        }
        super.onPause();
    }

    private void downloadFile (String File_url ,String File_name ,String format){
        File myDir  = new File( Environment.getExternalStorageDirectory().toString() + "/.dalagram");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        File savefile = new File(myDir +"/Dalagram "+format);


        File file_url = new File(savefile+"/"+File_name);

        if (!file_url.exists()) {
            AndroidNetworking.download(File_url,savefile.getPath(),File_name)
                    .setTag("downloadTest")
                    .setPriority(Priority.HIGH)
                    .build()
                    .startDownload(new DownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            file_upload_progress.setVisibility(View.GONE);
                            Uri pdfUri = FileProvider.getUriForFile(ActivitySingleNews.this, getApplicationContext().getPackageName() + ".provider", file_url);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(pdfUri, "application/*");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            Intent chooser = Intent.createChooser(intent, "");

                            startActivity(chooser);
                        }
                        @Override
                        public void onError(ANError anError) {
                            Toasty.error(ActivitySingleNews.this, "Проверить подключение интернета", Toast.LENGTH_SHORT).show();

                        }
                    });
        }else {
            file_upload_progress.setVisibility(View.GONE);
            Uri pdfUri = FileProvider.getUriForFile(ActivitySingleNews.this, getApplicationContext().getPackageName() + ".provider", file_url);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(pdfUri, "application/*");
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Intent chooser = Intent.createChooser(intent, "");

            startActivity(chooser);

        }


    }

    @OnClick(R.id.answer_delete)
    void answer_delete(){
        answer_layout.setVisibility(View.GONE);
        answer_comment_id = 0;
        answer_comment_name ="";
        emojiEditText.setText(null);
    }

    public void AnswerComment(int position) {
        answer_position = position;
        NewsItem item = commentItems.get(position);
        answer_layout.setVisibility(View.VISIBLE);
        answer_comment_id = item.getComment_id();
        answer_comment_name = item.getUser_name()+", ";
        Log.d("answer_comment_id",answer_comment_id+" ");
        answer_name.setText(item.getUser_name());
        emojiEditText.setText(answer_comment_name);
        emojiEditText.setSelection(emojiEditText.getText().length());
    }
}