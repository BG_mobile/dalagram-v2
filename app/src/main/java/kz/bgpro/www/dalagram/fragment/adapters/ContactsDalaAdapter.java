package kz.bgpro.www.dalagram.fragment.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.pavlospt.roundedletterview.RoundedLetterView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import kz.bgpro.www.dalagram.R;
import kz.bgpro.www.dalagram.activitys.ActivityChatMessage;
import kz.bgpro.www.dalagram.models.FeedItem;

/**
 * Created by nurbaqyt on 22.05.2018.
 */

public class ContactsDalaAdapter extends BaseAdapter {

    ArrayList<FeedItem> feed_item;
    FeedItem item;
    Activity activity;

    public ContactsDalaAdapter(ArrayList<FeedItem> feedItem, Activity activity) {
        this.activity = activity;
        this.feed_item = feedItem;
    }


    @Override
    public int getCount() {
        return feed_item.size();
    }

    @Override
    public Object getItem(int location) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {

        @BindView(R.id.avatar_tv)
        RoundedLetterView avatar_tv;

        @BindView(R.id.avatar)
        CircleImageView avatar;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.status)
        TextView status;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_contacts_list, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        item = feed_item.get(position);


        viewHolder.status.setText(item.getLast_visit());
        viewHolder.name.setText(item.getName());

        if(TextUtils.isEmpty(item.getAvatar())){
            viewHolder.avatar.setVisibility(View.GONE);
            viewHolder.avatar_tv.setVisibility(View.VISIBLE);


            if (item.getName().isEmpty()) {
                viewHolder.avatar_tv.setTitleText(" ");
                viewHolder.name.setText(item.getPhone());
            }else if (item.getName().length()>2){
                viewHolder.avatar_tv.setTitleText(item.getName().charAt(2)+"");
                viewHolder.name.setText(item.getName());
            }else {
                viewHolder.avatar_tv.setTitleText(item.getName()+"");
                viewHolder.name.setText(item.getName());
            }

        }else {
            viewHolder.avatar.setVisibility(View.VISIBLE);
            viewHolder.avatar_tv.setVisibility(View.GONE);

            Glide.with(activity).load(item.getAvatar()).into(viewHolder.avatar);


        }

//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                item = feed_item.get(position);
//
//
//                Log.d("WHAT","IS1");
//                String Name=item.getName();
//                Log.d("WHAT","IS2");
//                String avatar;
//                boolean is_ava ;
//                if (TextUtils.isEmpty(item.getAvatar())){
//                    if(Name.length()>1)
//                        avatar = Name.charAt(0)+""+Name.charAt(1);
//                    else
//                        avatar = Name.charAt(0)+"";
//                    is_ava = false;
//                }else {
//                    is_ava = true;
//                    avatar = item.getAvatar();
//                }
//                Log.d("WHAT","IS3");
//
//                activity.startActivity(new Intent(activity, ActivityChatMessage.class)
//                        .putExtra("who","chat")
//                        .putExtra("user_id",item.getUser_id())
//                        .putExtra("is_mute",item.getIs_mute())
//                        .putExtra("name",Name)
//                        .putExtra("chat_text",item.getLast_visit())
//                        .putExtra("avatar",avatar)
//                        .putExtra("is_ava",is_ava)
//                        .putExtra("dialog_id",item.getUser_id()+"U")
//                );
//
//
//
//            }
//        });
        return convertView;
    }
}
